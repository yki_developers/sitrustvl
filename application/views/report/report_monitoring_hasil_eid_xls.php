<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=report_monitoring_hasil_eid.xls");
?>

<table class="table table-bordered">
<thead>
<tr class="text-center">
<th><?php echo $this->lang->line('number');?></th>
<th><?php echo $this->lang->line('province');?></th>
<th><?php echo $this->lang->line('district');?></th>
<th><?php echo $this->lang->line('order_number');?></th>
<th><?php echo $this->lang->line('specimen_id');?></th>
<th><?php echo $this->lang->line('patient_name');?></th>
<th><?php echo $this->lang->line('patient_regnas');?></th>
<th>Jenis Kelamin</th>
<th>Tanggal Lahir</th>
<th>Tanggal ART</th>
<th>Tanggal Test</th>
<th><?php echo $this->lang->line('order_type');?></th>
<th><?php echo $this->lang->line('order_date');?></th>
<th><?php echo $this->lang->line('pickup');?></th>
<th><?php echo $this->lang->line('delivered');?></th>
<th>Tanggal Diterima</th>
<th><?php echo $this->lang->line('specimen_date_release');?></th>
<th>Faskes Pengirim</th>
<th>Faskes Tujuan</th>
<th>Kurir</th>
<th>Kondisi Spesimen</th>
<th><?php echo $this->lang->line('order_status');?></th>
</tr>

</thead>

<tbody>
<?php 
$i=1;
foreach($report as $list){
    $prov = $this->M_reverse->provName($list->hf_province);
    $dist = $this->M_reverse->distName($list->hf_district);
    $fas = $this->M_reverse->hfName($list->order_hf_sender);
    $rec = $this->M_reverse->hfName($list->order_hf_recipient);
    if($list->order_shipper_id!=''){
$ship = $this->M_reverse->shipperName($list->order_shipper_id);
    }
    ?>
<tr>
<td><?php echo $i; ?></td>
<td><?php echo $prov->province_name;?></td>
<td><?php echo $dist->district_name;?></td>
<td><?php echo $list->order_number;?></td>
<td><?php echo $list->specimen_id;?></td>
<td><?php echo $list->patient_name;?></td>
<td><?php echo $list->patient_regnas;?></td>
<td><?php echo specimen_sex($list->patient_sex);?></td>
<td><?php echo $list->patient_bday;?></td>
<td><?php echo $list->patient_art_date;?></td>
<td><?php echo $list->eid_result_exam_date;?></td>
<td><?php echo $list->order_type;?></td>
<td><?php echo $list->order_date;?></td>
<td><?php echo $list->order_date_pickup;?></td>
<td><?php echo $list->order_date_delivered;?></td>
<td><?php echo $list->order_date_received;?></td>
<td><?php echo $list->eid_result_date_release;?></td>
<td><?php echo $fas->hf_name;?></td>
<td><?php echo $rec->hf_name;?></td>
<td><?php 

if($list->order_shipper_id!=''){
echo $ship->shipper_name;
}


?></td>
<td><?php 
if($list->specimen_condition!=null){
echo specimen_condition($list->specimen_condition);
}

?></td>
<td><?php echo $this->lang->line(order_status($list->order_status));?></td>
    </tr>
    <?php $i++;} ?>
</tbody>

</table>

