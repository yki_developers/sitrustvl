<style>.datepicker{z-index:1151 !important;}



</style>
<nav aria-label="breadcrumb" style="margin-top: 50px;">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('vl_monitoring');?></li>
  </ol>
</nav>









<div class="card">
<div class="card-header bg-merah" id="headercard">

<?php

$max = ceil($total/100);
  if($this->uri->segment(4)>5 && ($this->uri->segment(4)+5)>6){

  
   if($this->uri->segment(4)==$max || ($this->uri->segment(4)+2)>$max){ 
     $maxnum = 1+$this->uri->segment(4);
  }else{ 
    $maxnum = 3+$this->uri->segment(4);
  }
  
  $start = $maxnum-5;
  $prev = $start-1; 

}else{
      $maxnum = 6;
      $start = 1;
      $prev = 1;

  }
  ?>

<ul class="pagination pagination-sm justify-content-center">
  <?php if($this->uri->segment(4)>1){?>
  <li class="page-item prev"><a class="page-link fas fa-arrow-circle-left" href="<?php echo $prev;?>">&nbsp;</a></li>
  <?php }else{?>
    <li class="page-item prev disabled"><a class="page-link fas fa-arrow-circle-left" href="<?php echo $prev;?>">&nbsp;</a></li>
    <?php }?>

<?php
//echo $max;
  for($i=$start;$i<=($maxnum-1);$i++){

    if($i==$this->uri->segment(4)){
        ?>
        <li class="page-item active" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo $i;?>"><?php echo $i;?></a></li>
        <?php

    }else{

      if($i>$max){
       ?>
        <li class="page-item disabled" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo $i;?>"><?php echo $i;?></a></li>
<?php
      }else{
      ?>
<li class="page-item" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo $i;?>"><?php echo $i;?></a></li>

      <?php
      }
    }

  }
  
  if($maxnum<$max){
  ?>


  <li class="page-item next"><a class="page-link fas fa-arrow-circle-right" href="<?php echo $maxnum; ?>">&nbsp;</a></li>
  <?php }else{?>
    <li class="page-item next disabled"><a class="page-link fas fa-arrow-circle-right" href="<?php echo $maxnum; ?>">&nbsp;</a></li>
  
    <?php }?>
</ul>

</div>
<div class="card-body">
<div class="input-group  col-md-4">
    <input type="text" id="search" name="search" class="form-control" placeholder="Pencarian">
    <div class="input-group-append"><span class="btn  btn-primary" id="src"><i class="fas fa-search"></i></span>
    
    </div>
    <div class="col-md-2"><a href="<?php echo base_url()."report/shipment_report/shipmentxls";?>" class="btn btn-success"><i class="far fa-file-excel"></i> Export</a> </div>
</div>
</div>
<div class="card-body">
<div class="tableFixHead">
<table class="table">
<thead>

<tr class="bg-merah text-center">
    <th scope="col" class="col-1"><span class="align-top"><?php echo $this->lang->line('number');?></span></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_number');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_courier');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_origin');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('lab_ref');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('total_specimen');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_date');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_time');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('pickup_date');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('pickup_time');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('pickup_info');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('pickup_duration');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('delivered_date');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('delivered_time');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('delivered_info');?></th> 
</tr>
</thead>
<tbody id="dataBody">
    <?php
    $i=1;
    foreach($response as $list){ 
        $m = $this->M_reverse->hfName($list->order_hf_recipient);
        $s = $this->M_reverse->hfName($list->order_hf_sender);
      $k = $this->M_reverse->shipperName($list->order_shipper_id);
        $prov = $this->M_reverse->provName($list->hf_province);
        $dist = $this->M_reverse->distName($list->hf_district);
        ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->order_number;?></td>
    <td scope="col"><?php echo $k->shipper_name;?></td>
    <td scope="col"><?php echo $s->hf_name;?></td>
    <td scope="col"><?php echo $m->hf_name;?></td>

    <td scope="col"><?php echo $list->total_specimen;?></td>
    
    <td scope="col"><?php echo $list->order_date;?></td>
    <td scope="col"><?php echo $list->order_time;?></td>
    <td scope="col"><?php echo $list->pickup_date;?></td>
    <td scope="col"><?php echo $list->pickup_time;?></td>
    <td scope="col"><?php echo $list->order_pickup_info;?></td>
   
    <td scope="col"><?php echo $list->pickup_duration;?></td>
    <td scope="col"><?php echo $list->delivered_date;?></td>
    <td scope="col"><?php echo $list->delivered_time;?></td>
    <td scope="col"><?php echo $list->order_shipper_info;?></td>
</tr>




    <?php $i++;
} ?>
</tbody>
</table>
</div>

</div>
<div class="card-footer text-center bg-merah">

</div>
</div>














