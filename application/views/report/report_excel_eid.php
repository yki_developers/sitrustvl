<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=report_pemeriksaan_eid.xls");
?>

<table class="table table-bordered">
<thead>
<tr class="text-center">
    <th><?php echo $this->lang->line('number');?></th>
    <th>Tanggal Order</th>
    <th><?php echo $this->lang->line('province');?></th>
    <th><?php echo $this->lang->line('district');?></th>
    <th><?php echo $this->lang->line('report_sender_facility');?></th>
    <th>Faskes Tujuan</th>
    <th><?php echo $this->lang->line('mother_nid');?></th>
    <th><?php echo $this->lang->line('child_nid');?></th>
    <th><?php echo $this->lang->line('child_name');?></th>
    <th><?php echo $this->lang->line('specimen_id');?></th>
    <th><?php echo $this->lang->line('patient_sex');?></th>
    <th><?php echo $this->lang->line('patient_bday');?></th>
    <th><?php echo $this->lang->line('range_age');?></th>
    <th><?php echo $this->lang->line('specimen_date_collected');?></th>
    <th><?php echo $this->lang->line('specimen_exam_date_eid');?></th>
    <th><?php echo $this->lang->line('specimen_date_release');?></th>
    <th><?php echo $this->lang->line('result');?></th>
    <th><?php echo $this->lang->line('report_date');?></th>
</tr>

</thead>

<tbody>
<?php 
$i=1;
foreach($report as $list){
    $prov = $this->M_reverse->provName($list->hf_province);
    $dist = $this->M_reverse->distName($list->hf_district);
    $fas = $this->M_reverse->hfName($list->order_hf_sender);
    $dfas = $this->M_reverse->hfName($list->order_hf_recipient);
    ?>
<tr>
<td><?php echo $i; ?></td>
<td><?php echo $list->order_date;?></td>
<td><?php if($this->session->userdata("user_group")=='6'){ 
  echo $prov->province_name;
}else{
  echo $list->hf_province;
}?></td>
<td><?php if($this->session->userdata("user_group")=='6'){ echo $dist->district_name; }else{  echo $list->hf_district; }?></td>
<td><?php  if($this->session->userdata("user_group")=='6'){ echo $fas->hf_name; }else{ echo $list->order_hf_sender; }?></td>
<td><?php echo $dfas->hf_name;?></td>
<td><?php echo $list->patient_eid_mother_nid;?></td>
<td><?php echo $list->patient_nid;?></td>
<td><?php echo $list->patient_name;?></td>
<td><?php echo $list->specimen_id;?></td>
<td><?php echo specimen_sex($list->patient_sex);?></td>
<td><?php echo $list->date_bday;?></td>
<td><?php echo age_range($list->umur_hari);?></td>
<td><?php echo $list->specimen_date_collected;?></td>
<td><?php echo $list->date_exam;?></td>
<td><?php echo $list->date_release;?></td>
<td><?php echo result_eid($list->eid_result_eidresult);?></td>

<td><?php echo $list->vl_tgl;?></td>
    </tr>
    <?php $i++;} ?>
</tbody>

</table>