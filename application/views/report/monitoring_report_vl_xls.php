<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=monitoring_report.xls");
?>


<table class="table">
<thead>

<tr class="bg-merah text-center">
    <th scope="col" class="col-1"><span class="align-top"><?php echo $this->lang->line('number');?></span></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('province');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('district');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_number');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_type');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_date');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_pickup_date');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_arrival_date');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_received_date');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_origin');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_destination');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_courier');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('total_specimen');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('total_specimen_condition_good');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('total_specimen_condition_broken');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('condition_broken');?></th>
    <th scope="col"><?php echo $this->lang->line('order_status');?></th>


   
</tr>
</thead>
<tbody id="dataBody">
    <?php
    $stp = ($this->uri->segment(4)-1)*100;
    $i=1;
    foreach($orderlist as $list){ 
        $m = $this->M_reverse->hfName($list->order_hf_recipient);
        $s = $this->M_reverse->hfName($list->order_hf_sender);
      //  $k = $this->M_reverse->shipperName($list->order_shipper_id);
        $prov = $this->M_reverse->provName($list->hf_province);
        $dist = $this->M_reverse->distName($list->hf_district);
        ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $prov->province_name;?></td>
    <td scope="col"><?php echo $dist->district_name;?></td>
    <td scope="col"><?php echo $list->order_number;?></td>
    <td scope="col"><?php echo $list->order_type;?></td>
    <td scope="col"><?php echo $list->order_date;?></td>
    <td scope="col"><?php echo $list->order_date_pickup;?></td>
    <td scope="col"><?php echo $list->order_date_delivered;?></td>
    <td scope="col"><?php echo $list->order_date_received;?></td>
    <td scope="col"><?php echo $s->hf_name;?></td>
    <td scope="col"><?php echo $m->hf_name;?></td>
    <td scope="col"><?php echo $list->shipper_name;?></td>
    <td scope="col"><?php echo $list->total_specimen;?></td>
    <td scope="col"><?php echo $list->total_good_specimen;?></td>
    <td scope="col"><?php echo $list->total_broken_specimen;?></td>
    <td scope="col"><?php 
    $cond = $this->M_reverse->getBrokenCondition($list->order_id);
    foreach($cond as $des){
      echo $des->total."&nbsp;spesimen : &nbsp;".$des->specimen_condition_descript."<br>";
    }
    
    ?></td>
    
    <td scope="col"><?php echo specimen_vl_status($list->order_status);
    
   
    ?></td> 
    
</tr>




    <?php $i++;
} ?>
</tbody>
</table>