
<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=Monitoring_kurir.xls");
?>
<table class="table">
<thead>

<tr class="bg-merah text-center">
    <th scope="col" class="col-1"><span class="align-top"><?php echo $this->lang->line('number');?></span></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_number');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_courier');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_origin');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('lab_ref');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('total_specimen');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_date');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_time');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('pickup_date');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('pickup_time');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('pickup_info');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('pickup_duration');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('delivered_date');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('delivered_time');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('delivered_info');?></th> 
</tr>
</thead>
<tbody id="dataBody">
    <?php
    $i=1;
    foreach($response as $list){ 
        $m = $this->M_reverse->hfName($list->order_hf_recipient);
        $s = $this->M_reverse->hfName($list->order_hf_sender);
      $k = $this->M_reverse->shipperName($list->order_shipper_id);
        $prov = $this->M_reverse->provName($list->hf_province);
        $dist = $this->M_reverse->distName($list->hf_district);
        ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->order_number;?></td>
    <td scope="col"><?php echo $k->shipper_name;?></td>
    <td scope="col"><?php echo $s->hf_name;?></td>
    <td scope="col"><?php echo $m->hf_name;?></td>

    <td scope="col"><?php echo $list->total_specimen;?></td>
    
    <td scope="col"><?php echo $list->order_date;?></td>
    <td scope="col"><?php echo $list->order_time;?></td>
    <td scope="col"><?php echo $list->pickup_date;?></td>
    <td scope="col"><?php echo $list->pickup_time;?></td>
    <td scope="col"><?php echo $list->order_pickup_info;?></td>
   
    <td scope="col"><?php echo $list->pickup_duration;?></td>
    <td scope="col"><?php echo $list->delivered_date;?></td>
    <td scope="col"><?php echo $list->delivered_time;?></td>
    <td scope="col"><?php echo $list->order_shipper_info;?></td>
</tr>




    <?php $i++;
} ?>
</tbody>
</table>