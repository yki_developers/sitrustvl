<?php
//header("Content-type: application/vnd-ms-excel");
//header("Content-Disposition: attachment; filename=nama_filenya.xls");
?>
<style>
.scroll {
    max-height: 500px;
    overflow-y: scroll;
}

</style>
<nav aria-label="breadcrumb" style="margin-top: 50px;">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('report');?></li>
  </ol>
</nav>

<div class="card mx-auto">
<div class="card-header bg-danger text-right" id="headercard">
<?php

$max = ceil($total/50);
  if($this->uri->segment(4)>5 && ($this->uri->segment(4)+5)>6){

  
   if($this->uri->segment(4)==$max || ($this->uri->segment(4)+2)>$max){ 
     $maxnum = 1+$this->uri->segment(4);
  }else{ 
    $maxnum = 3+$this->uri->segment(4);
  }
  
  $start = $maxnum-5;
  $prev = $start-1; 

}else{
      $maxnum = 6;
      $start = 1;
      $prev = 1;

  }
  ?>

<ul class="pagination pagination-sm justify-content-center">
  <li class="page-item prev"><a class="page-link fas fa-arrow-circle-left" href="<?php echo $prev;?>">&nbsp;</a></li>
  

<?php
//echo $max;
  for($i=$start;$i<=($maxnum-1);$i++){

    if($i==$this->uri->segment(4)){
        ?>
        <li class="page-item active" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo $i;?>"><?php echo $i;?></a></li>
        <?php

    }else{

      ?>
<li class="page-item" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo $i;?>"><?php echo $i;?></a></li>

      <?php
    }

  }
  ?>
  <li class="page-item next"><a class="page-link fas fa-arrow-circle-right" href="<?php echo $maxnum; ?>">&nbsp;</a></li>
</ul>



&nbsp;
<a href="<?php echo base_url()."report/exam_report/internalvlmonitoringxls";?>" class="btn btn-success"><i class="far fa-file-excel"></i> Export</a>
</div>
<div class="card-body">

<div class="tableFixHead">
<table class="table table-bordered">
<thead>
<tr class="text-center">
<th><?php echo $this->lang->line('number');?></th>
<th>
<?php 
if($this->session->userdata("user_group")=='6'){
echo $this->lang->line('patient_regnas');
}else{
    echo $this->lang->line('patient_id');
}

?></th>
<th><?php echo $this->lang->line('order_date');?></th>
<th><?php echo $this->lang->line('patient_name');?></th>
<th><?php echo $this->lang->line('patient_sex');?></th>
<th><?php echo $this->lang->line('patient_bday');?></th>
<th><?php echo $this->lang->line('specimen_art_date');?></th>
<th><?php echo $this->lang->line('specimen_exam_date');?></th>
<th><?php echo $this->lang->line('result');?></th>
<th><?php echo $this->lang->line('result_conclusion');?></th>
    
    <th><?php echo $this->lang->line('province');?></th>
    <th><?php echo $this->lang->line('district');?></th>
    <th><?php echo $this->lang->line('report_sender_facility');?></th>
    <th><?php echo $this->lang->line('specimen_id');?></th>
    <th><?php echo $this->lang->line('specimen_date_release');?></th>
   
    <th><?php echo $this->lang->line('report_month_to');?></th>
    <th><?php echo $this->lang->line('report_periode');?></th>
    <th><?php echo $this->lang->line('report_month');?></th>
    <th><?php echo $this->lang->line('report_year');?></th>
    <th><?php echo $this->lang->line('report_date');?></th>
</tr>

</thead>

<tbody>
<?php 
$i=1;
foreach($report as $list){
    $prov = $this->M_reverse->provName($list->hf_province);
    $dist = $this->M_reverse->distName($list->hf_district);
    $fas = $this->M_reverse->hfName($list->hf_code);
    ?>
<tr>
<th><?php echo $i; ?></th>
<td><?php 
if($this->session->userdata('user_group')=='6'){
echo $list->patient_regnas;
}else{
    echo $list->patient_id;
}

?></td>

<td><?php echo $list->date_order;?></td>

<td><?php 
if($this->session->userdata('user_group')=='6'){
echo $list->patient_name;
}else{
    echo $list->patient_name_encode;
}

?>


</td>
<td><?php echo specimen_sex($list->patient_sex);?></td>
<td><?php echo $list->date_bday;?></td>
<td><?php echo $list->patient_art_date;?></td>
<td><?php echo $list->date_exam;?></td>

<td><?php echo result_exam($list->vlresult);?></td>
<td><?php echo $list->vlresult_conclusion;?></td>

<td><?php echo $prov->province_name;?></td>
<td><?php echo $dist->district_name;?></td>
<td><?php echo $fas->hf_name;?></td>
<td><?php echo $list->specimen_id;?></td>
<td><?php echo $list->date_release;?></td>
<td><?php echo $list->vl_bln_ke;?></td>
<td><?php echo specimen_test_periode($list->vl_bln_ke);?></td>
<td><?php echo $list->vl_bln;?></td>
<td><?php echo $list->vl_th;?></td>
<td><?php echo $list->vl_tgl;?></td>
    </tr>
    <?php $i++;} ?>
</tbody>

</table>
</div>



</div>
<div class="card-footer">
&nbsp;
</div>


</div>
