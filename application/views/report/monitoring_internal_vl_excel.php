<?php
header("Content-type: application/vnd-ms-excel");
header("Content-Disposition: attachment; filename=monitoring_internal_report.xls");
?>


<table class="table table-bordered">
<thead>
<tr class="text-center">
<th><?php echo $this->lang->line('number');?></th>
<th>
<?php 
if($this->session->userdata("user_group")=='6'){
echo $this->lang->line('patient_regnas');
}else{
    echo $this->lang->line('patient_id');
}

?></th>
<th><?php echo $this->lang->line('order_date');?></th>
<th><?php echo $this->lang->line('patient_name');?></th>
<th><?php echo $this->lang->line('patient_sex');?></th>
<th><?php echo $this->lang->line('patient_bday');?></th>
<th><?php echo $this->lang->line('specimen_art_date');?></th>
<th><?php echo $this->lang->line('specimen_exam_date');?></th>
<th><?php echo $this->lang->line('result');?></th>
<th><?php echo $this->lang->line('result_conclusion');?></th>
    
    <th><?php echo $this->lang->line('province');?></th>
    <th><?php echo $this->lang->line('district');?></th>
    <th><?php echo $this->lang->line('report_sender_facility');?></th>
    <th><?php echo $this->lang->line('specimen_id');?></th>
    <th><?php echo $this->lang->line('specimen_date_release');?></th>
   
    <th><?php echo $this->lang->line('report_month_to');?></th>
    <th><?php echo $this->lang->line('report_periode');?></th>
    <th><?php echo $this->lang->line('report_month');?></th>
    <th><?php echo $this->lang->line('report_year');?></th>
    <th><?php echo $this->lang->line('report_date');?></th>
</tr>

</thead>

<tbody>
<?php 
$i=1;
foreach($report as $list){
    $prov = $this->M_reverse->provName($list->hf_province);
    $dist = $this->M_reverse->distName($list->hf_district);
    $fas = $this->M_reverse->hfName($list->hf_code);
    ?>
<tr>
<th><?php echo $i; ?></th>
<td><?php 
if($this->session->userdata('user_group')=='6'){
echo $list->patient_regnas;
}else{
    echo $list->patient_id;
}

?></td>

<td><?php echo $list->date_order;?></td>
<td><?php 
if($this->session->userdata('user_group')=='6'){
echo $list->patient_name;
}else{
    echo $list->patient_name_encode;
}

?>


</td>
<td><?php echo specimen_sex($list->patient_sex);?></td>
<td><?php echo $list->date_bday;?></td>
<td><?php echo $list->patient_art_date;?></td>
<td><?php echo $list->date_exam;?></td>

<td><?php echo result_exam($list->vlresult);?></td>
<td><?php echo $list->vlresult_conclusion;?></td>

<td><?php echo $prov->province_name;?></td>
<td><?php echo $dist->district_name;?></td>
<td><?php echo $fas->hf_name;?></td>
<td><?php echo $list->specimen_id;?></td>
<td><?php echo $list->date_release;?></td>
<td><?php echo $list->vl_bln_ke;?></td>
<td><?php echo specimen_test_periode($list->vl_bln_ke);?></td>
<td><?php echo $list->vl_bln;?></td>
<td><?php echo $list->vl_th;?></td>
<td><?php echo $list->vl_tgl;?></td>
    </tr>
    <?php $i++;} ?>
</tbody>

</table>