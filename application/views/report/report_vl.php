<?php
//header("Content-type: application/vnd-ms-excel");
//header("Content-Disposition: attachment; filename=nama_filenya.xls");
?>
<style>
.scroll {
    max-height: 500px;
    overflow-y: scroll;
}

</style>
<nav aria-label="breadcrumb" style="margin-top: 50px;">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('report');?></li>
  </ol>
</nav>

<div class="card mx-auto">
<div class="card-header bg-merah text-right" id="headercard">&nbsp;

<div class="input-group  col-md-12">
    <input type="text" id="search" name="search" class="form-control" placeholder="Pencarian">
    <div class="input-group-append"><span class="btn  btn-light" id="src"><i class="fas fa-search"></i></span>
    
    </div>
    <div class="col-md-8">
<?php
if($this->session->userdata('user_group')=='6'){
  $fs = $this->M_reverse->hfName($this->session->userdata('user_unit'));
  if($fs->hf_referral=='1'){ ?> 
  <a href="<?php echo base_url()."report/exam_report/labvlexcel";?>" class="btn btn-success"><i class="far fa-file-excel"></i> Export</a>
&nbsp;
  <?php }else{ 
    ?>
    <a href="<?php echo base_url()."report/exam_report/vlexcel";?>" class="btn btn-success"><i class="far fa-file-excel"></i> Export</a>
&nbsp;
    <?php
   }
  
}else{
  ?>
  <a href="<?php echo base_url()."report/exam_report/vlexcel";?>" class="btn btn-success"><i class="far fa-file-excel"></i> Export</a>
&nbsp;
  <?php
}
?>
<a href="<?php echo base_url()."report/exam_report/excelark";?>" class="btn btn-success"><i class="far fa-file-excel"></i> Export ARK</a> &nbsp;
<a href="<?php echo base_url()."report/exam_report/statusresultxls";?>" class="btn btn-success"><i class="far fa-file-excel"></i> Export Status Hasil</a>
</div>
</div>
</div>

<div class="card-body">
<?php

$max = ceil($total/100);
  if($this->uri->segment(4)>5 && ($this->uri->segment(4)+5)>6){

  
   if($this->uri->segment(4)==$max || ($this->uri->segment(4)+2)>$max){ 
     $maxnum = 1+$this->uri->segment(4);
  }else{ 
    $maxnum = 3+$this->uri->segment(4);
  }
  
  $start = $maxnum-5;
  $prev = $start-1; 

}else{
      $maxnum = 6;
      $start = 1;
      $prev = 1;

  }
  ?>

<ul class="pagination pagination-sm justify-content-center">
  <?php if($this->uri->segment(4)>1){?>
  <li class="page-item prev"><a class="page-link fas fa-arrow-circle-left" href="<?php echo $prev;?>">&nbsp;</a></li>
  <?php }else{?>
    <li class="page-item prev disabled"><a class="page-link fas fa-arrow-circle-left" href="<?php echo $prev;?>">&nbsp;</a></li>
    <?php }?>

<?php
//echo $max;
  for($i=$start;$i<=($maxnum-1);$i++){

    if($i==$this->uri->segment(4)){
        ?>
        <li class="page-item active" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo $i;?>"><?php echo $i;?></a></li>
        <?php

    }else{

      if($i>$max){
       ?>
        <li class="page-item disabled" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo $i;?>"><?php echo $i;?></a></li>
<?php
      }else{
      ?>
<li class="page-item" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo $i;?>"><?php echo $i;?></a></li>

      <?php
      }
    }

  }
  
  if($maxnum<$max){
  ?>


  <li class="page-item next"><a class="page-link fas fa-arrow-circle-right" href="<?php echo $maxnum; ?>">&nbsp;</a></li>
  <?php }else{?>
    <li class="page-item next disabled"><a class="page-link fas fa-arrow-circle-right" href="<?php echo $maxnum; ?>">&nbsp;</a></li>
  
    <?php }?>
</ul>

</div>
<div class="card-body">
<div class="tableFixHead">
<table class="table table-bordered">
<thead>
<tr class="text-center">
<th><?php echo $this->lang->line('number');?></th>
<th><?php echo $this->lang->line('order_number');?></th>
<th><?php echo $this->lang->line('order_type');?></th>
<?php if($this->session->userdata("user_group")=="2" || $this->session->userdata("user_group")=="8"){?>
  <th><?php echo $this->lang->line('order_date');?></th>
  <?php } ?>
  <th><?php echo $this->lang->line('patient_id');?></th>
<th>
<?php 

echo $this->lang->line('patient_regnas');

?>
</th>

<th><?php echo $this->lang->line('patient_name');?></th>
<th><?php echo $this->lang->line('patient_sex');?></th>
<th><?php echo $this->lang->line('patient_bday');?></th>
<th><?php echo $this->lang->line('specimen_art_date');?></th>
<th><?php echo $this->lang->line('specimen_exam_date');?></th>
<th><?php echo $this->lang->line('result');?></th>
<th><?php echo $this->lang->line('result_conclusion');?></th>
    
    <th><?php echo $this->lang->line('province');?></th>
    <th><?php echo $this->lang->line('district');?></th>
    <th><?php echo $this->lang->line('report_sender_facility');?></th>
    <th>Faskes Tujuan</th>
    <th><?php echo $this->lang->line('specimen_id');?></th>
    <th><?php echo $this->lang->line('specimen_date_release');?></th>
   
    <th><?php echo $this->lang->line('report_month_to');?></th>
    <th><?php echo $this->lang->line('report_periode');?></th>
    <th><?php echo $this->lang->line('report_month');?></th>
    <th><?php echo $this->lang->line('report_year');?></th>
    <th><?php echo $this->lang->line('report_date');?></th>


    <th>Tanggal Pickup</th>
    <th>Tanggal Sampai</th>
    <th>Tanggal Diterima</th>
</tr>

</thead>

<tbody id="dataBody">
<?php 
if($this->uri->segment(4)!=""){
    $stp = ($this->uri->segment(4)-1)*100;
    }else{
        $stp=0;
    }
    $n=array();
$i=1;
foreach($report as $list){
    $prov = $this->M_reverse->provName($list->hf_province);
    $dist = $this->M_reverse->distName($list->hf_district);
    $fas = $this->M_reverse->hfName($list->order_hf_sender);
    $dfas = $this->M_reverse->hfName($list->order_hf_recipient);
    ?>
<tr>
<td><?php echo $i+$stp; ?></td>
<td><?php echo $list->order_number;?></td>
<td><?php echo $list->order_type;?></td>
<?php
if($this->session->userdata('user_group')=='2' ||$this->session->userdata("user_group")=="8"){?>
<td><?php echo $list->date_order;?></td>
<?php } ?>
<td><?php echo $list->patient_id;?></td>
<td><?php echo $list->patient_regnas;
?></td>

<td><?php echo $list->patient_name;
?></td>
<td><?php echo specimen_sex($list->patient_sex);?></td>
<td><?php echo $list->date_bday;?></td>
<td><?php echo $list->date_art;?></td>
<td><?php echo $list->date_exam;?></td>

<td><?php echo result_exam($list->vlresult);?></td>
<td><?php echo $list->vlresult_conclusion;?></td>

<td><?php echo $prov->province_name;?></td>
<td><?php echo $dist->district_name;?></td>
<td><?php echo $fas->hf_name;?></td>
<td><?php echo $dfas->hf_name;?></td>
<td><?php echo $list->specimen_id;?></td>
<td><?php echo $list->date_release;?></td>
<td><?php echo $list->vl_bln_ke;?></td>
<td><?php echo specimen_test_periode($list->vl_bln_ke);?></td>
<td><?php echo $list->vl_bln;?></td>
<td><?php echo $list->vl_th;?></td>
<td><?php echo $list->vl_tgl;?></td>
<td><?php echo $list->date_pickup;?></td>
<td><?php echo $list->date_delivered;?></td>
<td><?php echo $list->date_received;?></td>
    </tr>
    <?php $n[]=$i; $i++;} ?>
</tbody>

</table>
</div>


</div>
<div class="card-footer">
  Halaman : <?php echo $this->uri->segment("4"); ?><br>
  Ditampilkan : <?php echo sizeof(@$n);?> baris dari Total Data : <?php echo $total;?> baris
&nbsp;
</div>


</div>

<script>
$('document').ready(function(){

  $('#src').click(function(){
    $('#dataBody').load('<?php echo base_url()."report/exam_report/searchreport";?>',{"search":$('#search').val()});
  })
})
</script>