<style>
.scroll {
    max-height: 500px;
    overflow-y: scroll;
}

</style>

<nav aria-label="breadcrumb"  style="margin-top: 50px;">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('eid_report');?></li>
  </ol>
</nav>

<div class="card mx-auto">
<div class="card-header  bg-merah text-right" id="headercard">&nbsp;
<a href="<?php echo base_url()."report/exam_report/eidexcel";?>" class="btn btn-success"><i class="far fa-file-excel"></i> Export</a>&nbsp;
<a href="<?php echo base_url()."report/exam_report/eidstatusresultxls";?>" class="btn btn-success"><i class="far fa-file-excel"></i> Export Status Hasil</a>
</div>
<div class="card-body">
<?php

$max = ceil($total/100);
  if($this->uri->segment(4)>5 && ($this->uri->segment(4)+5)>6){

  
   if($this->uri->segment(4)==$max || ($this->uri->segment(4)+2)>$max){ 
     $maxnum = 1+$this->uri->segment(4);
  }else{ 
    $maxnum = 3+$this->uri->segment(4);
  }
  
  $start = $maxnum-5;
  $prev = $start-1; 

}else{
      $maxnum = 6;
      $start = 1;
      $prev = 1;

  }
  ?>

<ul class="pagination pagination-sm justify-content-center">
  <?php if($this->uri->segment(4)>1){?>
  <li class="page-item prev"><a class="page-link fas fa-arrow-circle-left" href="<?php echo $prev;?>">&nbsp;</a></li>
  <?php }else{?>
    <li class="page-item prev disabled"><a class="page-link fas fa-arrow-circle-left" href="<?php echo $prev;?>">&nbsp;</a></li>
    <?php }?>

<?php
//echo $max;
  for($i=$start;$i<=($maxnum-1);$i++){

    if($i==$this->uri->segment(4)){
        ?>
        <li class="page-item active" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo $i;?>"><?php echo $i;?></a></li>
        <?php

    }else{

      if($i>$max){
       ?>
        <li class="page-item disabled" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo $i;?>"><?php echo $i;?></a></li>
<?php
      }else{
      ?>
<li class="page-item" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo $i;?>"><?php echo $i;?></a></li>

      <?php
      }
    }

  }
  
  if($maxnum<$max){
  ?>


  <li class="page-item next"><a class="page-link fas fa-arrow-circle-right" href="<?php echo $maxnum; ?>">&nbsp;</a></li>
  <?php }else{?>
    <li class="page-item next disabled"><a class="page-link fas fa-arrow-circle-right" href="<?php echo $maxnum; ?>">&nbsp;</a></li>
  
    <?php }?>
</ul>

</div>

<div class="card-body">
<div class="tableFixHead">
<table class="table table-bordered">
<thead>
<tr class="text-center">
    <th><?php echo $this->lang->line('number');?></th>
    <th>Tanggal Order</th>
    <th><?php echo $this->lang->line('province');?></th>
    <th><?php echo $this->lang->line('district');?></th>
    <th><?php echo $this->lang->line('report_sender_facility');?></th>
    <th>Faskes Tujuan</th>
    <th><?php echo $this->lang->line('mother_nid');?></th>
    <th><?php echo $this->lang->line('child_nid');?></th>
    <th><?php echo $this->lang->line('child_name');?></th>
    <th><?php echo $this->lang->line('specimen_id');?></th>
    <th><?php echo $this->lang->line('patient_sex');?></th>
    <th><?php echo $this->lang->line('patient_bday');?></th>
    <th><?php echo $this->lang->line('range_age');?></th>
    <th><?php echo $this->lang->line('specimen_date_collected');?></th>
    <th><?php echo $this->lang->line('specimen_exam_date_eid');?></th>
    <th><?php echo $this->lang->line('specimen_date_release');?></th>
    <th><?php echo $this->lang->line('result');?></th>
    <th><?php echo $this->lang->line('report_date');?></th>
</tr>

</thead>

<tbody>
<?php
$n=array();
$i=1;
foreach($report as $list){
    $prov = $this->M_reverse->provName($list->hf_province);
    $dist = $this->M_reverse->distName($list->hf_district);
    $fas = $this->M_reverse->hfName($list->order_hf_sender);
    $dfas = $this->M_reverse->hfName($list->order_hf_recipient);
    ?>
<tr>
<td><?php echo $i; ?></td>
<td><?php echo $list->order_date;?></td>
<td><?php if($this->session->userdata("user_group")=='6'){ 
  echo $prov->province_name;
}else{
  echo $list->hf_province;
}?></td>
<td><?php if($this->session->userdata("user_group")=='6'){ echo $dist->district_name; }else{  echo $list->hf_district; }?></td>
<td><?php  if($this->session->userdata("user_group")=='6'){ echo $fas->hf_name; }else{ echo $list->order_hf_sender; }?></td>
<td><?php echo $dfas->hf_name;?></td>
<td><?php echo $list->patient_eid_mother_nid;?></td>
<td><?php echo $list->patient_nid;?></td>
<td><?php if($this->session->userdata("user_group")=='6'){ echo $list->patient_name; }else{ echo substr(sha1($list->patient_name),0,8);}?></td>
<td><?php echo $list->specimen_id;?></td>
<td><?php echo specimen_sex($list->patient_sex);?></td>
<td><?php echo $list->date_bday;?></td>
<td><?php echo age_range($list->umur_hari);?></td>
<td><?php echo $list->specimen_date_collected;?></td>
<td><?php echo $list->date_exam;?></td>
<td><?php echo $list->date_release;?></td>
<td><?php echo result_eid($list->eid_result_eidresult);?></td>

<td><?php echo $list->vl_tgl;?></td>
    </tr>
    <?php $n[]=$i; $i++;} ?>
</tbody>

</table>
</div>


</div>
<div class="card-footer">
  Halaman : <?php echo $this->uri->segment('4');?><br>
  Ditampilkan : <?php echo sizeof(@$n);?> baris dari Total : <?php echo $total;?>
&nbsp;
</div>


</div>
