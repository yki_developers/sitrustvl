<style>.datepicker{z-index:1151 !important;}



</style>
<nav aria-label="breadcrumb" style="margin-top: 50px;">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('vl_monitoring');?></li>
  </ol>
</nav>









<div class="card">
<div class="card-header bg-merah" id="headercard">

<?php

$max = ceil($total/100);
  if($this->uri->segment(4)>5 && ($this->uri->segment(4)+5)>6){

  
   if($this->uri->segment(4)==$max || ($this->uri->segment(4)+2)>$max){ 
     $maxnum = 1+$this->uri->segment(4);
  }else{ 
    $maxnum = 3+$this->uri->segment(4);
  }
  
  $start = $maxnum-5;
  $prev = $start-1; 

}else{
      $maxnum = 6;
      $start = 1;
      $prev = 1;

  }
  ?>

<ul class="pagination pagination-sm justify-content-center">
  <?php if($this->uri->segment(4)>1){?>
  <li class="page-item prev"><a class="page-link fas fa-arrow-circle-left" href="<?php echo $prev;?>">&nbsp;</a></li>
  <?php }else{?>
    <li class="page-item prev disabled"><a class="page-link fas fa-arrow-circle-left" href="<?php echo $prev;?>">&nbsp;</a></li>
    <?php }?>

<?php
//echo $max;
  for($i=$start;$i<=($maxnum-1);$i++){

    if($i==$this->uri->segment(4)){
        ?>
        <li class="page-item active" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo $i;?>"><?php echo $i;?></a></li>
        <?php

    }else{

      if($i>$max){
       ?>
        <li class="page-item disabled" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo $i;?>"><?php echo $i;?></a></li>
<?php
      }else{
      ?>
<li class="page-item" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo $i;?>"><?php echo $i;?></a></li>

      <?php
      }
    }

  }
  
  if($maxnum<$max){
  ?>


  <li class="page-item next"><a class="page-link fas fa-arrow-circle-right" href="<?php echo $maxnum; ?>">&nbsp;</a></li>
  <?php }else{?>
    <li class="page-item next disabled"><a class="page-link fas fa-arrow-circle-right" href="<?php echo $maxnum; ?>">&nbsp;</a></li>
  
    <?php }?>
</ul>

</div>
<div class="card-body">
<div class="input-group  col-md-4">
    <input type="text" id="search" name="search" class="form-control" placeholder="Pencarian">
    <div class="input-group-append"><span class="btn  btn-primary" id="src"><i class="fas fa-search"></i></span>
    
    </div>
    <div class="col-md-2"><a href="<?php echo base_url()."report/exam_report/eidmonitoringxls/".$this->uri->segment('4');?>" class="btn btn-success"><i class="far fa-file-excel"></i> Export</a> </div>
</div>
</div>
<div class="card-body">
<div class="tableFixHead">
<table class="table">
<thead>

<tr class="bg-merah text-center">
    <th scope="col" class="col-1"><span class="align-top"><?php echo $this->lang->line('number');?></span></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('province');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('district');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_number');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_type');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_date');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_pickup_date');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_arrival_date');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_received_date');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_origin');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_destination');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('order_courier');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('total_specimen');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('total_specimen_condition_good');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('total_specimen_condition_broken');?></th>
    <th scope="col" class="col-1"><?php echo $this->lang->line('condition_broken');?></th>

    <th scope="col"><?php echo $this->lang->line('order_status');?></th>
    <?php
    if($this->session->userdata("user_hf_group")=='2'){
      ?>
      <th scope="col"><?php echo $this->lang->line('action');?></th>
      <?php
    }
    ?>

   
</tr>
</thead>
<tbody id="dataBody">
    <?php
    $i=1;
    foreach($orderlist as $list){ 
        $m = $this->M_reverse->hfName($list->order_hf_recipient);
        $s = $this->M_reverse->hfName($list->order_hf_sender);
      //  $k = $this->M_reverse->shipperName($list->order_shipper_id);
        $prov = $this->M_reverse->provName($list->hf_province);
        $dist = $this->M_reverse->distName($list->hf_district);
        ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $prov->province_name;?></td>
    <td scope="col"><?php echo $dist->district_name;?></td>
    <td scope="col"><?php echo $list->order_number;?></td>
    <td scope="col"><?php echo $list->order_type;?></td>
    <td scope="col"><?php echo $list->order_date;?></td>
    <td scope="col"><?php echo $list->order_date_pickup;?></td>
    <td scope="col"><?php echo $list->order_date_delivered;?></td>
    <td scope="col"><?php echo $list->order_date_received;?></td>
    <td scope="col"><?php echo $s->hf_name;?></td>
    <td scope="col"><?php echo $m->hf_name;?></td>
    <td scope="col"><?php echo $list->shipper_name;?></td>
    <td scope="col"><?php echo $list->total_specimen;?></td>
    <td scope="col"><?php echo $list->total_good_specimen;?></td>
    <td scope="col"><?php echo $list->total_broken_specimen;?></td>

    <td scope="col"><?php 
    $cond = $this->M_reverse->getBrokenCondition($list->order_id);
    foreach($cond as $des){
      echo $des->total."&nbsp;spesimen : &nbsp;".$des->specimen_condition_descript."<br>";
    }
    
    ?></td>


    <td scope="col"><?php
     echo specimen_vl_status($list->order_status);
   
    ?></td> 
    <?php if($this->session->userdata("user_hf_group")=='2'){?>
    <td scope="col">

    <a href="<?php echo base_url()."transactional/result/detail/".$list->order_id;?>" id="btnedit" class="btn btn-success btn-sm"  onclick=""><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('detail');?></a>
    </td>
    <?php } ?>
</tr>




    <?php $i++;
} ?>
</tbody>
</table>
</div>

</div>
<div class="card-footer text-center bg-merah">
  Ditampilkan halaman : <?php echo $this->uri->segment('4');?>&nbsp; dari Total: <?php echo $total;?> 

</div>
</div>















<script>
function dbdate(tgl){
var tg = tgl.split("-");
var dbFormat = tg[2]+"-"+tg[1]+"-"+tg[0];
return dbFormat;
}

function finish(id){
  if(confirm('Apakah anda yakin sudah selesai?')){
    document.location = "<?php echo base_url()."transactional/order/finish/";?>"+id;
  }
}

function edit(id){
 
 $.ajax({
    url:"<?php echo base_url()."transactional/order/detail";?>",
    type:"POST",
    dataType : "json",
    data:{
      "order_id":id
    },
    success:function(jsondata){
      $('#order_date_update').val(dbdate(jsondata.response.order_date));
      $('#order_hf_recipient_update').val(jsondata.response.order_hf_recipient);
      $('#order_shipper_id_update').val(jsondata.response.order_shipper_id);
      $('#order_id').val(jsondata.response.order_id);
    }
  });
  $('#modalForm').modal('show');
}



function reorder(id){
 
 $.ajax({
    url:"<?php echo base_url()."transactional/order/detail";?>",
    type:"POST",
    dataType : "json",
    data:{
      "order_id":id
    },
    success:function(jsondata){
      $('#order_reorder').val('1');
      $('#order_date_update').val(dbdate(jsondata.response.order_date));
      $('#order_hf_recipient_update').val(jsondata.response.order_hf_recipient);
      $('#order_shipper_id_update').val(jsondata.response.order_shipper_id);
      $('#order_id').val(jsondata.response.order_id);
    }
  });
  $('#modalForm').modal('show');
}



$('document').ready(function(){
  $('#search').keyup(function(){
    $('#dataBody').load("<?php echo base_url()."report/exam_report/search";?>",{"search":$(this).val()});
  })
  //alert('bas')
  //$('#order_date_update').datepicker();
  $('.tanggal').datepicker({
  format:"dd-mm-yyyy",
  startView:"year",
  minView:"year"
}).on('changeDate',function(ev){
  $(this).blur();
  $(this).datepicker('hide');
});

  $('#btnSubmit').click(function(){
        $.ajax({
            url:"<?php echo base_url()."transactional/order/add";?>",
            type:"POST",
            dataType:"json",
            data:{
                "order_hf_sender":"<?php echo $this->session->userdata('user_unit');?>",
                "order_date":dbdate($('#order_date').val()),
                "order_hf_recipient":$("#order_hf_recipient").val(),
                "order_shipper_id":$("#order_shipper_id").val()
            },
            success:function(jdata){
                if(jdata.status=='success'){
                    alert(jdata.message);
                    document.location = "<?php echo base_url()."transactional/specimen/list/";?>"+jdata.order_id;
                }

            }
        })
    }); // end of btnsubmit


$('#btnEdit').click(function(){
  if($('#order_reorder').val()=="0"){
  $.ajax({
      url:"<?php echo base_url()."transactional/order/update/";?>"+$('#order_id').val(),
      type:"POST",
      dataType:"json",
      data:{
          "order_date":$('#order_date_update').val(),
          "order_hf_recipient":$("#order_hf_recipient_update").val(),
          "order_shipper_id":$("#order_shipper_id_update").val(),
          "order_status":"1",
          "order_approved":"0"
      },
      success:function(jdata){
          if(jdata.status=='success'){
              alert(jdata.message);
              document.location = "<?php echo base_url()."transactional/specimen/list/";?>"+$('#order_id').val();
          }

      }
  });


}else{

  $.ajax({
            url:"<?php echo base_url()."transactional/order/add";?>",
            type:"POST",
            dataType:"json",
            data:{
              "order_hf_sender":"<?php echo $this->session->userdata('user_unit');?>",
                "order_date":$('#order_date_update').val(),
                "order_hf_recipient":$("#order_hf_recipient_update").val(),
                "order_shipper_id":$("#order_shipper_id_update").val(),
                "order_status":"1",
                "order_approved":"0"
            },
            success:function(jdata){
                if(jdata.status=='success'){

                  $.ajax({
                    url:"<?php echo base_url()."transactional/specimen/reorderspecimen/";?>"+$('#order_id').val(),
                    type:"POST",
                    dataType:"json",
                    data:{
                      "specimen_order_id":jdata.order_id
                    },
                    success:function(jdata){
                      //alert(jdata.message);

                      $.ajax({
                        url:"<?php echo base_url()."transactional/order/update/";?>"+$('#order_id').val(),
                        type: "POST",
                        dataType:"json",
                        data:{
                          "order_archived":"1"
                        },
                        success:function(jsdata){
                          if(jsdata.status=='success'){
                            alert(jdata.message);
                            document.location ="<?php echo base_url()."transactional/order/list";?>";

                          }
                        }
                      });


                    }
                  });  
                  
                  
                  
                }

            }
          });
        }//end of if order_id
    });



}); //end of document



    
</script>