
<nav aria-label="breadcrumb" style="margin-top: 50px;">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item"><a href="<?php echo base_url()."report/exam_report/vlmonitoring/1";?>"><i class="fas fa-desktop"></i>&nbsp;<?php echo $this->lang->line('vl_monitoring');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('result');?></li>
  </ol>
</nav>







<div class="card">
<div class="card-header bg-merah" id="headercard">&nbsp;
</div>
<div class="card-body">

<table class="table table-stripe">
<thead>
<tr>
    <th scope="col" colspan="2">
    <div class="input-group input-group-sm">
    <input type="text" id="search" name="search" class="form-control" placeholder="Pencarian">
    <div class="input-group-append"><span class="btn  btn-primary" id="src"><i class="fas fa-search"></i></span></div>
</div>
</th>
    <th scope="col" class="text-right">
    &nbsp;
  <!--  <a href="#cardform" class="btn btn-light btn-xs" id="add" data-toggle="modal" data-target="#modalForm"><i class="fas fa-plus-circle"></i>&nbsp;<?php echo $this->lang->line('add');?></a> -->
</th>

</tr>

<tr class="bg-merah">
    <th scope="col"><?php echo $this->lang->line('number');?></th>
    <th scope="col"><?php echo $this->lang->line('patient_nid');?></th>
    <th scope="col"><?php echo $this->lang->line('patient_name');?></th>
    <th scope="col"><?php echo $this->lang->line('patient_sex');?></th>
    <th scope="col"><?php echo $this->lang->line('specimen_number');?></th>
    <th scope="col"><?php echo $this->lang->line('specimen_test_type');?></th>
    <th scope="col"><?php echo $this->lang->line('specimen_type');?></th>


    <th scope="col" class="text-center">Kondisi Specimen</th>
    <th scope="col" class="text-center">&nbsp;</th>
</tr>
</thead>
<tbody id="dataBody">
    <?php
    $i=1;
    foreach($specimen as $list){ 
       // $m = $this->M_reverse->hfName($list->order_hf_recipient);
        //$k = $this->M_reverse->shipperName($list->order_shipper_id);
        ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->patient_nid?></td>
    <td scope="col"><?php echo $list->patient_name;?></td>
    <td scope="col"><?php echo specimen_sex($list->patient_sex);?></td>
    <td scope="col"><?php echo $list->specimen_id;?></td>
    <td scope="col"><?php echo specimen_vl_test($list->specimen_exam_category);?></td>
    <td scope="col"><?php echo specimen_vl_type($list->specimen_type);?></td>
    <td scope="col"><?php echo specimen_condition($list->specimen_condition);?>
    </td>
    <td scope="col"><?php if($list->specimen_result_flag=='1'){ 
        $this->load->model("report/M_report");
        if($list->specimen_exam_category=='2'){
        $m = $this->M_report->getResultVl($list->specimen_num_id);

        if($m->vl_result_vlresult_optional=='3'){
            echo result_exam($m->vl_result_vlresult);
            }else{
                echo result_exam($m->vl_result_vlresult_optional);
            }

        }else{
            $m = $this->M_report->getResultVl($list->specimen_num_id); 
        }
       
        ?>     
        <?php }?></td>
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>


</div>
<div class="card-footer text-center">
&nbsp;
</div>
</div>






  

<script>

function upload(id){
  $('#modalFoto').modal('show');
   $('#specimen_num_id').val(id);
}

function finish(id){
  if(confirm('Apakah anda yakin sudah selesai?')){
    document.location = "<?php echo base_url()."transactional/result/finish/";?>"+id;
  }
}
    $('document').ready(function(){


      $('#btnUpload').click(function(){
  $(".preloader").fadeIn();
  $('#modalFoto').hide();
  var dataForm = new FormData($('#formFoto')[0]);
  $.ajax({
  url:"<?php echo base_url()."transactional/result/vlupdate";?>",
  type:'POST',
  dataType:'json',
  cache: false,
  contentType: false,
  processData: false,
  data: dataForm,
  success:function(jdata){
      $(".preloader").fadeOut();
      if (jdata.status=='200') {
        
          alert(jdata.message);
          document.location="<?php echo base_url()."transactional/result/detail/".$this->uri->segment('4');?>";
          
      }else{
        $(".preloader").fadeOut();
          alert(jdata.error);

      }
  }
  
       });
});



        $('#patient_province').change(function(){
    $(".preloader").fadeIn();
    $.ajax({
        url : '<?php echo base_url()."master/district/districtbyprovince";?>',
        type:'POST',
        dataType:'json',
        data:{
            'province_code':$('#patient_province').val()
        },
        success:function(jdata){

            var str ='<option value=""><?php echo $this->lang->line('district');?></option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
            })
            $('#patient_district').html(str);
            $(".preloader").fadeOut();
        }
    })
})





        
    $('#btnSubmit').click(function(){
        $.ajax({
            url:"<?php echo base_url()."transactional/specimen/add";?>",
            type:"POST",
            dataType:"json",
            data:{
                "patient_name":$('#patient_name').val(),
                "patient_nid":$("#patient_nid").val(),
                "patient_bday":$('#patient_bday').val(),
                "patient_sex":$('#patient_sex').val(),
                "patient_regnas":$('#patient_regnas').val(),
                "specimen_art_date":$('#specimen_art_date').val(),
                "specimen_order_id":<?php echo $this->uri->segment(4);?>,
                "specimen_id":$('#specimen_id').val(),
                "specimen_date_collected":$('#specimen_date_collected').val(),
                "specimen_type":$('#specimen_type').val(),
                "specimen_exam_category":$('#specimen_exam_categori').val()
            },
            success:function(jdata){
                if(jdata.status=='success'){
                    alert(jdata.message);
                    document.location = "<?php echo base_url()."transactional/specimen/list/";?>"+jdata.orderid;
                }

            }
        })
    });
    

    });
</script>