<style>.datepicker{z-index:1151 !important;
}
label{
  color: red;
  margin-left: 10px;
}
</style>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><i class="fas fa-home"></i><a href="<?php echo base_url()."home";?>">&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('result');?></li>
  </ol>
</nav>

<div class="card col-md-4 mx-auto">
<div class="card-header" id="headercard">&nbsp;<h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('result');?></h4>
</div>
<div class="card-body">
<form id="result_vl" method="POST">


<div class="form-row">
<div class="col-md-12">
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('patient_regnas');?></span>
    </div>
              
                <input type="hidden" value="<?php echo $specimen->patient_id;?>"  id="patient_id" name="patient_id">
                <input type="hidden" value="<?php echo $specimen->specimen_internal_num_id;?>"  id="specimen_internal_num_id" name="specimen_internal_num_id">
               
                <input type="text" value="<?php echo $specimen->patient_regnas;?>"  id="patient_regnas" name="patient_regnas" class="form-control"  required="required" autofocus="autofocus" readonly="readonly">

                  
               
                </div>
              </div>
</div>



<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('specimen_id');?></span>
    </div>
              
               
                <input type="text" value=""  id="vl_int_specimen_id" name="vl_int_specimen_id" class="form-control"  autofocus="autofocus">

                  
              
                </div>
              </div>
</div>



<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('specimen_date_collected');?></span>
    </div>
              
               
                <input type="text"  id="vl_int_date_collected" name="vl_int_date_collected" class="form-control tanggal"  required="required" autofocus="autofocus">

                  
            
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('specimen_exam_date');?></span>
    </div>
              
               
                <input type="text"  id="vl_int_exam_date" name="vl_int_exam_date" class="form-control tanggal"  required="required" autofocus="autofocus">

                  
              
            
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('specimen_date_release');?></span>
    </div>
               
                <input type="text"  id="vl_int_date_release" name="vl_int_date_release" class="form-control tanggal"  required="required" autofocus="autofocus">

                  
              
                </div>
              </div>
</div>



<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
              <?php echo $this->lang->line("result");?>
                </div>
              </div>
<div class="form-row">


<div class="col-md-12"  style="margin-top: 10px;">
                <div class="form-check" style="margin-left:20px">
                <input class="form-check-input" type="radio" name="specimen_vl_result_option" id="specimen_vl_result_option3" value="3">
  <label class="form-check-label" for="specimen_vl_result_option3">Input Angka Absolute</label>
              <br>
                <input class="form-check-input" type="radio" name="specimen_vl_result_option" id="specimen_vl_result_option1" value="1">
  <label class="form-check-label" for="specimen_vl_result_option1">DETECTED &gt; 10<sup>7</sup> copies/mL</label><br>

  <input class="form-check-input" type="radio" name="specimen_vl_result_option" id="specimen_vl_result_option2" value="2">
  <label class="form-check-label" for="specimen_vl_result_option2">DETECTED &lt; 40 copies/mL</label>
              <br>

           


          

              <input class="form-check-input" type="radio" name="specimen_vl_result_option" id="specimen_vl_result_option4" value="4">
  <label class="form-check-label" for="specimen_vl_result_option4">NOT DETECTED</label>
              <br>

              <input class="form-check-input" type="radio" name="specimen_vl_result_option" id="specimen_vl_result_option5" value="5">
  <label class="form-check-label" for="specimen_vl_result_option5">INVALID</label>
              <br>
              <input class="form-check-input" type="radio" name="specimen_vl_result_option" id="specimen_vl_result_option5" value="6">
  <label class="form-check-label" for="specimen_vl_result_option6">ERROR</label>
              <br>

              <input class="form-check-input" type="radio" name="specimen_vl_result_option" id="specimen_vl_result_option7" value="7">
  <label class="form-check-label" for="specimen_vl_result_option7">NO RESULT</label>
              <br>
               
                
            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('result_absolut');?></span>
    </div>
<input type="text"  id="specimen_vl_result" name="specimen_vl_result" class="form-control"   autofocus="autofocus"  style="display:none">         

                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
<div class="form-label-group">
                
<button type="submit" class="btn btn-success btn-block" id="btnSubmit"><?php echo $this->lang->line("submit");?></button>
               
               
                </div>
              </div>
</div>


</form>


</div>
<div class="card-footer">
</div>
</div>

<script>
    $('document').ready(function(){

      $('.tanggal').datepicker({
  format:"dd-mm-yyyy",
  startView:"year",
  minView:"year"
}).on('changeDate',function(ev){
  $(this).blur();
  $(this).datepicker('hide');
});

$('.tanggal').attr('readonly','readonly');

$('input[name="specimen_vl_result_option"]').change(function(){
  if($(this).val()=='3'){
    $('#specimen_vl_result').show().attr("required","required").focus();
    $('#lab_result').show();
  }else{
    $('#specimen_vl_result').hide().removeAttr("required");
    $('#lab_result').hide();
  }
})



$('#result_vl').validate({
  
  messages:{
    vl_int_date_collected:{
      required:"<?php echo $this->lang->line('required');?>"
    },
    vl_int_exam_date:{
      required:"<?php echo $this->lang->line('required');?>"
    },
    vl_int_date_release:{
      required:"<?php echo $this->lang->line('required');?>"
    },
    vl_int_vlresult_optional:{
      required:"<?php echo $this->lang->line('required');?>"
    }

  },
  submitHandler:function(form){
    
    $.ajax({
                url:"<?php echo base_url()."intern/internal/insertresult/";?>"+$('#specimen_internal_num_id').val(),
                type:"POST",
                dataType:"json",
                data:{
                   
                    "vl_int_specimen_num_id":$('#specimen_internal_num_id').val(),
                    "vl_int_specimen_id":$('#vl_int_specimen_id').val(),
                    "vl_int_date_collected":formatDate($('#vl_int_date_collected').val()),
                    "vl_int_exam_date":formatDate($('#vl_int_exam_date').val()),
                    "vl_int_date_release":formatDate($('#vl_int_date_release').val()),
                    "vl_int_vlresult_absolut":$('#specimen_vl_result').val(),
                    "vl_int_vlresult_optional":$('input[name="specimen_vl_result_option"]:checked').val()
                },
                success:function(jdata){
                    if(jdata.status=='success'){
                        alert(jdata.message);
                        document.location="<?php echo base_url()."intern/internal/result";?>";
                    }
                }
            })
  }


});



      



    });
    </script>