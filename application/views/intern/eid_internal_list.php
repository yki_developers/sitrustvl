<style>
.datepicker{z-index:1151 !important;}
label{
  color: red;
  margin-left: 10px;
}


</style>
<nav aria-label="breadcrumb" style="margin-top: 50px;">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-vial"></i>&nbsp;<?php echo $this->lang->line('specimen');?></li>
  </ol>
</nav>





<!---- MODAL ORDER -->

<div class="modal fade" id="modalForm">
    <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-merah">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('order');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form method="POST" id="specimen_eid">

        <div class="card card-body">
<span><i class="fas fa-edit"></i> Tanggal Permintaan Pemeriksaan</span>

        <div class="form-row">


<div class="col-md-12" style="margin-top: 10px;">
        
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text">Tanggal Permintaan</span>
    </div>
    <input type="text"  id="specimen_internal_date_request" name="specimen_internal_date_request" class="form-control tanggal" value="<?php echo date("d-m-Y");?>" placeholder="<?php echo $this->lang->line('order_date');?>" required="required" autofocus="autofocus" readonly="readonly">
</div>

        <!--
                <div class="form-label-group">
                <input type="hidden" value="0" id="patient_id">
              <input type="hidden" value="0" id="specimen_internal_num_id">
                <input type="text"  id="specimen_internal_date_request" name="specimen_internal_date_request" class="form-control tanggal" value="<?php echo date("d-m-Y");?>" placeholder="<?php echo $this->lang->line('order_date');?>" required="required" autofocus="autofocus" readonly="readonly">
                  <label for="specimen_internal_date_request">Tanggal</label>
            
                </div>


                --->
              </div>
</div>

        </div>


        <div class="card card-body" style="margin-top: 5px;>
<span><i class="fas fa-edit"></i> ISIAN DATA IDENTITAS IBU</span>
<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('mother_name');?></span>
    </div>
                <input type="text"  id="patient_eid_mother" name="patient_eid_mother" class="form-control eidpart bg-hijau" placeholder="<?php echo $this->lang->line('mother_name');?>" required="required">
                  <!-- <label for="patient_eid_mother" class=" eidpart"><?php echo $this->lang->line('mother_name');?></label> -->
            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('mother_nid');?></span>
    </div>
              
                <input type="text"  id="patient_eid_mother_nid" name="patient_eid_mother_nid" class="form-control eidpart bg-hijau" placeholder="<?php echo $this->lang->line('mother_nid');?>" required="required" maxlength="16">
                  
            
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('mother_regnas');?></span>
    </div>
                <input type="text"  id="patient_eid_mother_regnas" name="patient_eid_mother_regnas" class="form-control eidpart bg-hijau" placeholder="<?php echo $this->lang->line('mother_regnas');?>" >
                 
            
                </div>
              </div>
</div>


</div>


<div class="card card-body" style="margin-top: 5px;">
<span><i class="fas fa-edit"></i><?php echo $this->lang->line("child_form");?></span>


<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('specimen_test_type');?></span>
    </div>
                <select name="specimen_eid_number" id="specimen_eid_number" class="form-control custom-select shadow"  required="required" placeholder="<?php echo $this->lang->line("specimen_test_type");?>">
                      <option value=""><?php echo $this->lang->line("specimen_test_type");?></option>
                      <?php for($i=1;$i<=3;$i++){?>
                      <option value="<?php echo $i;?>"><?php echo specimen_eid_number($i); ?></option>
                      <?php }?>
               

</select>
            
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('child_nid');?></span>
    </div>
                <input type="hidden" value="1" id="specimen_exam_categori">
                <input type="hidden" value="0" id="patient_id">
              <input type="hidden" value="0" id="specimen_num_id">
                <input type="text"  id="patient_nid" name="patient_nid"  maxlength="16" class="form-control shadow noneid" maxlength="16" placeholder="<?php echo $this->lang->line('patient_nid');?>">
                 
                </div>
              </div>
</div>




<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('patient_med_record');?></span>
    </div>
              
                <input type="text"  id="patient_med_record" name="patient_med_record" class="form-control" placeholder="<?php echo $this->lang->line('patient_med_record');?>"  autofocus="autofocus">
                  
            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('patient_name');?></span>
    </div>
              
                <input type="text"  id="patient_name" name="patient_name" class="form-control" placeholder="<?php echo $this->lang->line('child_name');?>" required="required" autofocus="autofocus">
                
            
                </div>
              </div>
</div>



<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('patient_sex');?></span>
    </div>
              
                <select name="patient_sex" id="patient_sex" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("patient_sex");?>">
                      <option value=""><?php echo $this->lang->line("patient_sex");?></option>
                  <option value="1"><?php echo  $this->lang->line("sex_m");?></option>
                  <option value="2"><?php echo  $this->lang->line("sex_f");?></option>

</select>

            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('patient_bday');?></span>
    </div>
              
                <input type="text"  id="patient_bday" name="patient_bday" class="form-control tanggal" placeholder="<?php echo $this->lang->line('patient_bday');?>" required="required" autofocus="autofocus" readonly="readonly">
            
            
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('province');?></span>
    </div>
              
                <select name="patient_province" id="patient_province" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("province");?>">
                      <option value=""><?php echo $this->lang->line("province");?></option>
                      <?php foreach($province as $combolist){?>
                      <option value="<?php echo $combolist->province_code;?>"><?php echo $combolist->province_name; ?></option>
                      <?php }?>
               

</select>
            
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('district');?></span>
    </div>
                <select name="patient_district" id="patient_district" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("district");?>" disabled="disabled">
                      <option value=""><?php echo $this->lang->line("district");?></option>
               

</select>
            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('subdistrict');?></span>
    </div>
              
                <select name="patient_subdistrict" id="patient_subdistrict" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("subdistrict");?>" disabled="disabled">
                      <option value=""><?php echo $this->lang->line("subdistrict");?></option>
               

</select>
            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('address');?></span>
    </div>
              
                <input type="text"  id="patient_address" name="patient_address" class="form-control" placeholder="<?php echo $this->lang->line('specimen_address');?>" required="required" autofocus="autofocus">
                 
            
                </div>
              </div>
</div>

<!-- 
<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <input type="text"  id="specimen_id" name="specimen_id" class="form-control" placeholder="<?php echo $this->lang->line('specimen_id');?>" required="required" autofocus="autofocus">
                  <label for="specimen_id"><?php echo $this->lang->line('specimen_id');?></label>
            
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <input type="text"  id="specimen_date_collected" name="specimen_id" class="form-control tanggal" placeholder="<?php echo $this->lang->line('specimen_date_collected');?>" required="required" autofocus="autofocus">
                  <label for="specimen_date_collected"><?php echo $this->lang->line('specimen_date_collected');?></label>
            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <input type="text"  id="specimen_time_collected" name="specimen_id" class="form-control" placeholder="00:00:00" maxlength="8" required="required" autofocus="autofocus">
                  <label for="specimen_time_collected"><?php echo $this->lang->line('specimen_time_collected');?></label>
            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <select name="specimen_type" id="specimen_type" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("specimen_type");?>">
                      <option value=""><?php echo $this->lang->line("specimen_type");?></option>
                      <?php for($i=1;$i<=4;$i++){?>
                      <option value="<?php echo $i;?>"><?php echo specimen_vl_type($i); ?></option>
                      <?php }?>
               

</select>
            
                </div>
              </div>
</div>

                      -->

<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('doctor_name');?></span>
    </div>
              
                <input type="text"  id="doctor_name" name="doctor_name" class="form-control" placeholder="<?php echo $this->lang->line('doctor_name');?>" required="required" autofocus="autofocus">
                
            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <button type="submit" class="btn btn-success btn-block" id="btnSubmit"><?php echo $this->lang->line("submit");?></button>
               
               
                </div>
              </div>
</div>

</div>




        </form>
        </div>
      </div>
    </div>
</div>

<!-- END MODAL -->


<div class="card">
<div class="card-header" id="headercard">&nbsp;
</div>
<div class="card-body">

<table class="table table-stripe">
<thead>
<tr>
    <th scope="col" colspan="2">
    <div class="input-group input-group-sm">
    <input type="text" id="search" name="search" class="form-control" placeholder="Pencarian">
    <div class="input-group-append"><span class="btn  btn-success" id="src"><i class="fas fa-search"></i></span></div>
</div>
</th>
    <th scope="col" colspan="7" class="text-right">
    &nbsp;
    
    <a href="#cardform" class="btn btn-success btn-xs" id="add" data-toggle="modal" data-backdrop="static"  data-keyboard="false" data-target="#modalForm"><i class="fas fa-plus-circle"></i>&nbsp;<?php echo $this->lang->line('add');?></a>
 
</th>

</tr>

<tr>
    <th scope="col"><?php echo $this->lang->line('number');?></th>
    <th scope="col"><?php echo $this->lang->line('mother_nid');?></th>
    <th scope="col"><?php echo $this->lang->line('mother_regnas');?></th>
    <th scope="col"><?php echo $this->lang->line('mother_name');?></th>
    <th scope="col"><?php echo $this->lang->line('child_nid');?></th>
    <th scope="col"><?php echo $this->lang->line('patient_med_record');?></th>
    <th scope="col"><?php echo $this->lang->line('child_name');?></th>
    <th scope="col"><?php echo $this->lang->line('result');?></th>
    <th scope="col"><?php echo $this->lang->line('action');?></th>
</tr>
</thead>
<tbody id="dataBody">
    <?php
    $i=1;
    foreach($datalist as $list){ 
        $m = $this->M_reverse->hfName($list->specimen_internal_hf_code);
        //$k = $this->M_reverse->shipperName($list->order_shipper_id);
        ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->patient_eid_mother_nid;?></td>
    <td scope="col"><?php echo $list->patient_eid_mother_regnas;?></td>
    <td scope="col"><?php echo $list->patient_eid_mother;?></td>
    <td scope="col"><?php echo $list->patient_nid;?></td>
    <td scope="col"><?php echo $list->patient_med_record;?></td>
    <td scope="col"><?php echo $list->patient_name;?></td>
    <td scope="col">
    <?php echo result_eid($list->eid_int_eidresult);?>
    </td>
   
    <td scope="col" class="text-center">
   
&nbsp;
<?php if($list->specimen_result_flag=='0'){?>
    <a href="#" class="btn btn-danger btn-sm" onclick="conf('<?php echo $list->specimen_internal_num_id;?>')"><i class="fas fa-trash-alt"></i>&nbsp;<?php echo $this->lang->line('delete');?></a>
    &nbsp;
    <a href="#" id="btnedit" class="btn btn-success btn-sm"   onclick="edit('<?php echo $list->specimen_internal_num_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>
<?php } ?>
    </td>
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>


</div>
<div class="card-footer text-center">
&nbsp;
</div>
</div>

<script>


function conf(id,oid){
  if(confirm("<?php echo $this->lang->line('delete_confirmation');?>")){
    document.location = "<?php echo base_url()."intern/eidinternal/delete/";?>"+id;
  }
}
function dbdate(tgl){
var tg = tgl.split("-");
var dbFormat = tg[2]+"-"+tg[1]+"-"+tg[0];
return dbFormat;
}

function edit(id){
  $.ajax({
    url:"<?php echo base_url()."intern/internal/detail/";?>"+id,
    type:"POST",
    dataType:"json",
    data:{
      "specimen_num_id":id
    },
    success:function(jdata){
      if(jdata.status=="200"){
        $('#specimen_date_request').val(dbdate(jdata.response.specimen_internal_timestamp));
        $('#patient_eid_mother').val(jdata.response.patient_eid_mother);
        $('#patient_eid_mother_nid').val(jdata.response.patient_eid_mother_nid);
        $('#patient_eid_mother_regnas').val(jdata.response.patient_eid_mother_regnas);
       $('#specimen_num_id').val(id);
        $('#patient_nid').val(jdata.response.patient_nid);
        $('#patient_regnas').val(jdata.response.patient_regnas);
        $('#patient_med_record').val(jdata.response.patient_med_record);
        $('#patient_name').val(jdata.response.patient_name);
        $('#patient_address').val(jdata.response.patient_address);

        $('#patient_id').val(jdata.response.patient_id);
        $('#patient_sex').val(jdata.response.patient_sex);
       $('#patient_bday').val(dbdate(jdata.response.patient_bday));

       // $('#patient_art_date').val(dbdate(jdata.response.patient_art_date));

       $('#specimen_eid_number').val(jdata.response.specimen_internal_eid_number);
       $('#specimen_internal_exam_categori').val(jdata.response.specimen_internal_exam_category);
        $('#specimen_internal_date_request').val(dbdate(jdata.response.specimen_internal_timestamp));
        $('#doctor_name').val(jdata.response.specimen_internal_doctor_name);

        $('#patient_province').val(jdata.response.patient_province);
              $.ajax({
                url : '<?php echo base_url()."master/district/districtbyprovince";?>',
                type:'POST',
                dataType:'json',
                data:{
                  'province_code':jdata.response.patient_province
                  },
                  success:function(jsdata){
                    var str ='<option value=""></option>';
                    $.each(jsdata.response,function(i,item){
                      str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
                      })
                      $('#patient_district').html(str).removeAttr('disabled').val(jdata.response.patient_district);
                      //$(".preloader").fadeOut();
                      }
              });



              
              $.ajax({
                url : '<?php echo base_url()."master/subdistrict/bydistrict";?>',
                type:'POST',
                dataType:'json',
                data:{
                  'district_code':jdata.response.patient_district
                  },
                  success:function(jsondata){
                    var str ='<option value=""></option>';
                    $.each(jsondata.response,function(i,item){
                      str +='<option value="'+item.subdistrict_code+'">'+item.subdistrict_name+'</option>';
                      })
                      $('#patient_subdistrict').html(str).removeAttr('disabled').val(jdata.response.patient_subdistrict);
                      //$(".preloader").fadeOut();
                      }
              });


        $('#modalForm').modal(
        {
          backdrop:"static",
          keyboard:false,
          show:true
        }
        );

      }
    }
  })
}

function finish(id){
  if(confirm('Apakah anda yakin sudah selesai?')){
    document.location = "<?php echo base_url()."transactional/order/finish/";?>"+id;
  }
}
    $('document').ready(function(){


$('#specimen_eid').validate({
  rules:{
    patient_eid_mother:{
      required:true
    },
    patient_eid_mother_nid:{
      required:true,
      minlength:16
    },
    patient_nid:{
      required:false,
      minlength:"16"
    }
  },
  messages:{
    patient_eid_mother:{
      required:"<?php echo $this->lang->line('required');?>"
    },
    patient_eid_mother_nid:{
      required:"<?php echo $this->lang->line("required");?>",
      minlength:"Masukkan 16 digit NIK ibu"
    },
    specimen_eid_number:{
      required:"<?php echo $this->lang->line("required");?>"
    },
    patient_nid:{
     minlength:"Masukkan 16 digit NIK"
    },
    patient_name:{
      required:"<?php echo $this->lang->line("required");?>"
    },
    patient_sex:{
      required:"<?php echo $this->lang->line("required");?>"
    },
    patient_bday:{
      required:"<?php echo $this->lang->line("required");?>"
    },
    patient_province:{
      required:"<?php echo $this->lang->line("required");?>"
    },
    patient_district:{
      required:"<?php echo $this->lang->line("required");?>"
    },
    patient_subdistrict:{
      required:"<?php echo $this->lang->line("required");?>"
    },
    patient_address:{
      required:"<?php echo $this->lang->line("required");?>"
    },
    doctor_name:{
      required:"<?php echo $this->lang->line("required");?>"
    }
  },
  submitHandler(form){

    var conf = confirm("Apakah data yang anda masukkan sudah lengkap dan sesuai?");
    if(conf){


    if($('#specimen_num_id').val()=="0"){
       $.ajax({
           url:"<?php echo base_url()."transactional/specimen/add";?>",
           type:"POST",
           dataType:"json",
           data:{
               "patient_name":$('#patient_name').val(),
               "patient_nid":$("#patient_nid").val(),
               "patient_bday":dbdate($('#patient_bday').val()),
               "patient_sex":$('#patient_sex').val(),
               "patient_regnas":$('#patient_regnas').val(),
               "patient_med_record":$('#patient_med_record').val(),
               "patient_province":$('#patient_province').val(),
               "patient_district": $('#patient_district').val(),
               "patient_subdistrict":$('#patient_subdistrict').val(),
               "patient_address":$('#patient_address').val(),
               "patient_eid_mother":$('#patient_eid_mother').val(),
               "patient_eid_mother_nid":$('#patient_eid_mother_nid').val(),
               "patient_eid_mother_regnas":$('#patient_eid_mother_regnas').val()
           },
           success:function(jdata){
               if(jdata.status=='success'){
                  
                $.ajax({
                    url : "<?php echo base_url()."intern/eidinternal/insert";?>",
                    type: "POST",
                    dataType:"json",
                    data:{
                      "specimen_internal_patient_id":jdata.response,
                      "specimen_internal_date_request":"<?php echo date("Y-m-d");?>",
                      "specimen_internal_timestamp":dbdate($('#specimen_internal_date_request').val()),

                      "specimen_internal_hf_code":"<?php echo $this->session->userdata("user_unit");?>",
                       "specimen_internal_exam_category":"1",
                       "specimen_internal_doctor_name":$('#doctor_name').val(),
                       "specimen_internal_eid_number":$('#specimen_eid_number').val()
                    },
                    success:function(jdata){
                      if(jdata.status=='success'){
                        alert(jdata.message);
                        document.location = "<?php echo base_url()."intern/eidinternal";?>";
                      }else{
                        alert(jdata.message);
                      }
                    }
                  })



               }

           }
       })
     }else{


       $.ajax({
           url:"<?php echo base_url()."transactional/specimen/update/";?>"+$("#patient_id").val(),
           type:"POST",
           dataType:"json",
           data:{
               "patient_name":$('#patient_name').val(),
               "patient_nid":$("#patient_nid").val(),
               "patient_bday":dbdate($('#patient_bday').val()),
               "patient_sex":$('#patient_sex').val(),
               "patient_med_record":$('#patient_med_record').val(),
               "patient_province":$('#patient_province').val(),
               "patient_district":$('#patient_district').val(),
               "patient_subdistrict":$('#patient_subdistrict').val(),
               "patient_address":$('#patient_address').val(),
               "patient_eid_mother":$('#patient_eid_mother').val(),
               "patient_eid_mother_nid":$('#patient_eid_mother_nid').val(),
               "patient_eid_mother_regnas":$('#patient_eid_mother_regnas').val()
               

           },
           success:function(jdata){
               if(jdata.status=='success'){
                  
                $.ajax({
                    url : "<?php echo base_url()."intern/internal/update/";?>"+$('#specimen_num_id').val(),
                    type: "POST",
                    dataType:"json",
                    data:{
                      "specimen_internal_date_request":"<?php echo date("Y-m-d");?>",
                      "specimen_internal_timestamp":dbdate($('#specimen_internal_date_request').val()),
                      "specimen_internal_patient_id":$('#patient_id').val(),
                "specimen_internal_exam_category":"1",
                "specimen_internal_doctor_name":$('#doctor_name').val(),
                "specimen_internal_eid_number":$('#specimen_eid_number').val()
                    },
                    success:function(jdata){
                      if(jdata.status=='success'){
                        alert(jdata.message);
                        document.location = "<?php echo base_url()."intern/eidinternal";?>";
                      }else{
                        alert(jdata.message);
                      }
                    }
                  })


               }

           }
       })









     }
  }
  }
});


     
      $('#src').click(function(){
        $('#dataBody').load("<?php echo base_url()."intern/internal/search";?>",{"search":$('#search').val()});
      })
$('#search').keyup(function(){
$('#dataBody').load("<?php echo base_url()."intern/internal/search";?>",{"search":$('#search').val()});
});

      $('#patient_nid').blur(function(){

       

        if($(this).val()!=""){
        $.ajax({
          url:"<?php echo base_url()."transactional/specimen/patientid";?>",
          type:"POST",
          dataType:"json",
          data:{
            "patient_nid":$('#patient_nid').val()

          },
          success:function(jdata){
            if(jdata.status=='success'){
              $('#patient_id').val(jdata.response.patient_id);
              $('#patient_name').val(jdata.response.patient_name);
              $('#patient_sex').val(jdata.response.patient_sex);
              $('#patient_regnas').val(jdata.response.patient_regnas);
              $('#patient_bday').val(dbdate(jdata.response.patient_bday));
              $('#patient_art_date').val(dbdate(jdata.response.patient_art_date));
              $('#patient_med_record').val(jdata.response.patient_med_record);
              $('#specimen_id').focus();
            }
          }
        })
      }
      });


      $('#patient_regnas').blur(function(){
        if($(this).val()!=""){
        $.ajax({
          url:"<?php echo base_url()."transactional/specimen/patientid";?>",
          type:"POST",
          dataType:"json",
          data:{
            "patient_regnas":$('#patient_regnas').val()

          },
          success:function(jdata){
            if(jdata.status=='success'){
              $('#patient_id').val(jdata.response.patient_id);
              $('#patient_name').val(jdata.response.patient_name);
              $('#patient_sex').val(jdata.response.patient_sex);
              $('#patient_nid').val(jdata.response.patient_nid);
              $('#patient_bday').val(dbdate(jdata.response.patient_bday));
              $('#patient_art_date').val(dbdate(jdata.response.patient_art_date));
              $('#patient_med_record').val(jdata.response.patient_med_record);
              $('#specimen_id').focus();
            }
          }
        });
      }
      });


      $('#specimen_id').blur(function(){
        $.ajax({
          url:"<?php echo base_url()."transactional/specimen/checkspecimen";?>",
          type:"POST",
          dataType:"json",
          data:{
            "specimen_id":$('#specimen_id').val(),
            "specimen_order_id":"<?php echo $this->uri->segment(4);?>"
          },
          success:function(jdata){
            if(jdata.status==false){
              alert('ID Specimen Ini sudah terdaftar');
              $("#specimen_id").val('').focus();
            }
          }
        })
      });


      $('.tanggal').datepicker({
  format:"dd-mm-yyyy",
  startView:"year",
  minView:"year"
}).on('changeDate',function(ev){
  $(this).blur();
  $(this).datepicker('hide');
});

        $('#patient_province').change(function(){
    $(".preloader").fadeIn();
    $.ajax({
        url : '<?php echo base_url()."master/district/districtbyprovince";?>',
        type:'POST',
        dataType:'json',
        data:{
            'province_code':$('#patient_province').val()
        },
        success:function(jdata){

            var str ='<option value=""><?php echo $this->lang->line('district');?></option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
            })
            $('#patient_district').html(str).removeAttr("disabled");
            $(".preloader").fadeOut();
        }
    })
})



$('#patient_district').change(function(){
    $(".preloader").fadeIn();
    $.ajax({
        url : '<?php echo base_url()."master/subdistrict/bydistrict";?>',
        type:'POST',
        dataType:'json',
        data:{
            'district_code':$('#patient_district').val()
        },
        success:function(jdata){

            var str ='<option value=""><?php echo $this->lang->line('subdistrict');?></option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.subdistrict_code+'">'+item.subdistrict_name+'</option>';
            })
            $('#patient_subdistrict').html(str).removeAttr('disabled');
            $(".preloader").fadeOut();
        }
    })
})

        
$('.close,#tutup').click(function(){
        document.location = "<?php echo base_url()."intern/eidinternal";?>";
      })

    });
</script>