
<style>
.datepicker{z-index:1151 !important;}
label{
  color: red;
  margin-left: 10px;
}


</style>

<nav aria-label="breadcrumb" style="margin-top: 50px;">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-vial"></i>&nbsp;<?php echo $this->lang->line('specimen');?></li>
  </ol>
</nav>




<div class="modal fade shadow" id="modalForm">
    <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-hijau">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('specimen');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form method="POST" id="specimen"  class="needs-validation" validate>
        <input type="hidden" value="2" id="specimen_internal_exam_categori">
        <input type="hidden" value="0" id="patient_id">
        <input type="hidden" value="0" id="specimen_internal_num_id">


        <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text">Tanggal Order</span>
    </div>
    <input type="text"  id="specimen_internal_date_request" name="specimen_internal_date_request" class="form-control tanggal" value="<?php echo date("d-m-Y");?>" placeholder="dd-mm-yyyy" required="required" autofocus="autofocus"></div> 


    <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('patient_nid');?></span>
    </div>
    <input type="text"  id="patient_nid" name="patient_nid" class="form-control" maxlength="16"  required autofocus="autofocus">&nbsp;</div>
    
    
    <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('patient_regnas');?></span>
    </div>
    <input type="text"  id="patient_regnas" name="patient_regnas" class="form-control noneid"  required="required" autofocus="autofocus"></div>  


    <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('patient_med_record');?></span>
    </div>
    <input type="text"  id="patient_med_record" name="patient_med_record" class="form-control"   autofocus="autofocus"></div>  


    <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('patient_name');?></span>
    </div>
    <input type="text"  id="patient_name" name="patient_name" class="form-control"  required="required" autofocus="autofocus"></div> 


    <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line("patient_sex");?></span>
    </div>
    <select name="patient_sex" id="patient_sex" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("patient_sex");?>">
                      <option value=""></option>
                  <option value="1"><?php echo  $this->lang->line("sex_m");?></option>
                  <option value="2"><?php echo  $this->lang->line("sex_f");?></option>

</select> 
  </div> 



  <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('patient_bday');?></span>
    </div>
    <input type="text"  id="patient_bday" name="patient_bday" class="form-control tanggal" placeholder="dd-mm-yyyy" required="required" autofocus="autofocus"></div> 


    <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('specimen_art_date');?></span>
    </div>
    <input type="text"  id="patient_art_date" name="patient_art_date" class="form-control tanggal noneid" placeholder="dd-mm-yyyy" required="required" autofocus="autofocus"></div> 



    <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line("province");?></span>
    </div>
    <select name="patient_province" id="patient_province" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("province");?>">
                      <option value=""></option>
                      <?php foreach($province as $combolist){?>
                      <option value="<?php echo $combolist->province_code;?>"><?php echo $combolist->province_name; ?></option>
                      <?php }?>
               

</select>
  </div> 



  <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line("district");?></span>
    </div>
    <select name="patient_district" id="patient_district" class="form-control custom-select"  required="required">
                      <option value=""></option>
               

</select>
  </div> 


  <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line("subdistrict");?></span>
    </div>
    <select name="patient_subdistrict" id="patient_subdistrict" class="form-control custom-select"  required="required">
                      <option value=""></option>
               

</select>
  </div> 



  <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('address');?></span>
    </div>
    <input type="text"  id="patient_address" name="patient_address" class="form-control"  required="required" autofocus="autofocus">
  </div> 


  <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('doctor_name');?></span>
    </div>
    <input type="text"  id="doctor_name" name="doctor_name" class="form-control" placeholder="" required="required" autofocus="autofocus">
  </div> 





<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                <button type="submit" class="btn btn-success btn-block" id="btnSubmit"><?php echo $this->lang->line("submit");?></button>
          
                <!-- <a class="btn btn-success btn-block"   id="btnSubmit"><?php echo $this->lang->line('submit');?></a> -->
               
               
                </div>
              </div>
</div>

        </form>
        </div>

        <div class="modal-footer">

        
                <button type="button" class="btn btn-outline-danger" id="tutup">Tutup</button>
        </div>
      </div>
    </div>
</div>

<!-- END MODAL -->


<div class="card">
<div class="card-header" id="headercard">&nbsp;
</div>
<div class="card-body">

<table class="table table-stripe">
<thead>
<tr>
    <th scope="col" colspan="2">
    <div class="input-group input-group-sm">
    <input type="text" id="search" name="search" class="form-control" placeholder="Pencarian">
    <div class="input-group-append"><span class="btn  btn-success" id="src"><i class="fas fa-search"></i></span></div>
</div>
</th>
    <th scope="col" colspan="6" class="text-right">
    &nbsp;
    
    <a href="#cardform" class="btn btn-success btn-xs" id="add" data-toggle="modal" data-target="#modalForm"><i class="fas fa-plus-circle"></i>&nbsp;<?php echo $this->lang->line('add');?></a>
 
</th>

</tr>

<tr>
    <th scope="col"><?php echo $this->lang->line('number');?></th>
    <th scope="col"><?php echo $this->lang->line('patient_nid');?></th>
    <th scope="col"><?php echo $this->lang->line('patient_reg_nation');?></th>
    <th scope="col"><?php echo $this->lang->line('patient_med_record');?></th>
    <th scope="col"><?php echo $this->lang->line('patient_name');?></th>
    <th scope="col"><?php echo $this->lang->line('result');?></th>
    <th scope="col"><?php echo $this->lang->line('specimen_test_type');?></th>
    <th scope="col"><?php echo $this->lang->line('action');?></th>
</tr>
</thead>
<tbody id="dataBody">
    <?php
    $i=1;
    foreach($datalist as $list){ 
        $m = $this->M_reverse->hfName($list->specimen_internal_hf_code);
        //$k = $this->M_reverse->shipperName($list->order_shipper_id);
        ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->patient_nid;?></td>
    <td scope="col"><?php echo $list->patient_regnas;?></td>
    <td scope="col"><?php echo $list->patient_med_record;?></td>
    <td scope="col"><?php echo $list->patient_name;?></td>
    <td scope="col">
    <?php 
    if($list->specimen_result_flag!='0'){


    if($list->vl_int_vlresult_optional!='3'){
echo result_exam($list->vl_int_vlresult_optional);
    }else{
      echo $list->vl_int_vlresult_absolut." copies/mL";
    }
  }
    ?></td>
    <td scope="col"><?php echo specimen_vl_test($list->specimen_internal_exam_category);?></td>
    <td scope="col" class="text-center">
   
&nbsp;
<?php if($list->specimen_result_flag=='0'){?>
    <a href="#" class="btn btn-danger btn-sm" onclick="conf('<?php echo $list->specimen_internal_num_id;?>')"><i class="fas fa-trash-alt"></i>&nbsp;<?php echo $this->lang->line('delete');?></a>
    &nbsp;
    <a href="#" id="btnedit" class="btn btn-success btn-sm"   onclick="edit('<?php echo $list->specimen_internal_num_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>
<?php } ?>
    </td>
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>


</div>
<div class="card-footer text-center">
&nbsp;
</div>
</div>

<script>


function conf(id,oid){
  if(confirm("<?php echo $this->lang->line('delete_confirmation');?>")){
    document.location = "<?php echo base_url()."intern/internal/delete/";?>"+id;
  }
}
function dbdate(tgl){
var tg = tgl.split("-");
var dbFormat = tg[2]+"-"+tg[1]+"-"+tg[0];
return dbFormat;
}

function edit(id){
  $.ajax({
    url:"<?php echo base_url()."intern/internal/detail/";?>"+id,
    type:"POST",
    dataType:"json",
    data:{
      "specimen_num_id":id
    },
    success:function(jdata){
      if(jdata.status=="200"){
       $('#specimen_internal_num_id').val(id);
        $('#patient_nid').val(jdata.response.patient_nid);
        $('#patient_regnas').val(jdata.response.patient_regnas);
        $('#patient_med_record').val(jdata.response.patient_med_record);
        $('#patient_name').val(jdata.response.patient_name);

        $('#patient_id').val(jdata.response.patient_id);
        $('#patient_sex').val(jdata.response.patient_sex);
        $('#patient_bday').val(dbdate(jdata.response.patient_bday));
        $('#patient_art_date').val(dbdate(jdata.response.patient_art_date));

        $('#specimen_internal_exam_categori').val(jdata.response.specimen_internal_exam_category);
        $('#specimen_internal_date_request').val(jdata.response.specimen_internal_timestamp);
        $('#doctor_name').val(jdata.response.specimen_internal_doctor_name);
        $('#patient_province').val(jdata.response.patient_province);
        $.ajax({
                url : '<?php echo base_url()."master/district/districtbyprovince";?>',
                type:'POST',
                dataType:'json',
                data:{
                  'province_code':jdata.response.patient_province
                  },
                  success:function(jsdata){
                    var str ='<option value=""></option>';
                    $.each(jsdata.response,function(i,item){
                     
                     if(item.district_code==jdata.response.patient_district){
                      str +='<option value="'+item.district_code+'"  selected="selected">'+item.district_name+'</option>';

                     }else{
                      str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
                     }  
                    })
                      $('#patient_district').html(str).val(jdata.response.patient_district);
                      //$(".preloader").fadeOut();
                      }
              });


              $.ajax({
                url : '<?php echo base_url()."master/subdistrict/bydistrict";?>',
                type:'POST',
                dataType:'json',
                data:{
                  'district_code':jdata.response.patient_district
                  },
                  success:function(jsdata){
                    var str ='<option value=""></option>';
                    $.each(jsdata.response,function(i,item){
                      str +='<option value="'+item.subdistrict_code+'">'+item.subdistrict_name+'</option>';
                      })
                      $('#patient_subdistrict').html(str).val(jdata.response.patient_subdistrict);
                      //$(".preloader").fadeOut();
                      }
              });
        $('#patient_address').val(jdata.response.patient_address);

        $('#modalForm').modal('show');

      }
    }
  })
}

function finish(id){
  if(confirm('Apakah anda yakin sudah selesai?')){
    document.location = "<?php echo base_url()."transactional/order/finish/";?>"+id;
  }
}
    $('document').ready(function(){
      $('.tanggal').attr("readonly","readonly");




      $('#patient_province').change(function(){
    $(".preloader").fadeIn();
    $.ajax({
        url : '<?php echo base_url()."master/district/districtbyprovince";?>',
        type:'POST',
        dataType:'json',
        data:{
            'province_code':$('#patient_province').val()
        },
        success:function(jdata){

            var str ='<option value=""><?php echo $this->lang->line('district');?></option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
            })
            $('#patient_district').html(str).removeAttr('disabled');
            $(".preloader").fadeOut();
        }
    })
})




$('#patient_district').change(function(){
    $(".preloader").fadeIn();
    $.ajax({
        url : '<?php echo base_url()."master/subdistrict/bydistrict";?>',
        type:'POST',
        dataType:'json',
        data:{
            'district_code':$('#patient_district').val()
        },
        success:function(jdata){

            var str ='<option value=""><?php echo $this->lang->line('subdistrict');?></option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.subdistrict_code+'">'+item.subdistrict_name+'</option>';
            })
            $('#patient_subdistrict').html(str).removeAttr('disabled');
            $(".preloader").fadeOut();
        }
    })
})

     
      $('#src').click(function(){
        $('#dataBody').load("<?php echo base_url()."intern/internal/search";?>",{"search":$('#search').val()});
      })
$('#search').keyup(function(){
$('#dataBody').load("<?php echo base_url()."intern/internal/search";?>",{"search":$('#search').val()});
});

      $('#patient_nid').blur(function(){

       

        if($(this).val()!=""){
        $.ajax({
          url:"<?php echo base_url()."transactional/specimen/patientid";?>",
          type:"POST",
          dataType:"json",
          data:{
            "patient_nid":$('#patient_nid').val()

          },
          success:function(jdata){
            if(jdata.status=='success'){
              var conf = confirm("NIK ini sudah terdaftar atas nama "+jdata.response.patient_name+", Apakah anda akan menggunakan data ini?");
              if(conf){
                $('#patient_nid').attr('readonly','readonly');
              $('#patient_id').val(jdata.response.patient_id);
              $('#patient_name').val(jdata.response.patient_name).attr('readonly','readonly');
              $('#patient_sex').val(jdata.response.patient_sex).attr('readonly','readonly');;
              $('#patient_regnas').val(jdata.response.patient_regnas).attr('readonly','readonly');;
              $('#patient_bday').val(dbdate(jdata.response.patient_bday));
              $('#patient_art_date').val(dbdate(jdata.response.patient_art_date));
              $('#patient_med_record').val(jdata.response.patient_med_record).focus();

              $('#patient_province').val(jdata.response.patient_province);
        $.ajax({
                url : '<?php echo base_url()."master/district/districtbyprovince";?>',
                type:'POST',
                dataType:'json',
                data:{
                  'province_code':jdata.response.patient_province
                  },
                  success:function(jsdata){
                    var str ='<option value=""></option>';
                    $.each(jsdata.response,function(i,item){
                     
                     if(item.district_code==jdata.response.patient_district){
                      str +='<option value="'+item.district_code+'"  selected="selected">'+item.district_name+'</option>';

                     }else{
                      str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
                     }  
                    })
                      $('#patient_district').html(str).val(jdata.response.patient_district);
                      //$(".preloader").fadeOut();
                      }
              });


              $.ajax({
                url : '<?php echo base_url()."master/subdistrict/bydistrict";?>',
                type:'POST',
                dataType:'json',
                data:{
                  'district_code':jdata.response.patient_district
                  },
                  success:function(jsdata){
                    var str ='<option value=""></option>';
                    $.each(jsdata.response,function(i,item){
                      str +='<option value="'+item.subdistrict_code+'">'+item.subdistrict_name+'</option>';
                      })
                      $('#patient_subdistrict').html(str).val(jdata.response.patient_subdistrict);
                      //$(".preloader").fadeOut();
                      }
              });
        $('#patient_address').val(jdata.response.patient_address);


              $('#specimen_id').focus();
              }else{
                $('#patient_nid').val("").focus();
              }
            }
          }
        })
      }
      });



      $('#patient_regnas').blur(function(){
        if ( $(this).is('[readonly]') ) { 

         $('#patient_med_record').focus();


        }else{

        if($(this).val()!=""){
        $.ajax({
          url:"<?php echo base_url()."transactional/specimen/patientid";?>",
          type:"POST",
          dataType:"json",
          data:{
            "patient_regnas":$('#patient_regnas').val()

          },
          success:function(jdata){
            if(jdata.status=='success'){
              var conf = confirm("No. Regnas ini sudah terdaftar atas nama "+jdata.response.patient_name+", Apakah anda akan menggunakan data ini?");
              if(conf){
              $('#patient_id').val(jdata.response.patient_id).attr('readonly','readonly');;
              $('#patient_name').val(jdata.response.patient_name).attr('readonly','readonly');;
              $('#patient_sex').val(jdata.response.patient_sex).attr('readonly','readonly');;
              $('#patient_nid').val(jdata.response.patient_nid).attr('readonly','readonly');;
              $('#patient_bday').val(dbdate(jdata.response.patient_bday));
              $('#patient_art_date').val(dbdate(jdata.response.patient_art_date));
              $('#patient_med_record').val(jdata.response.patient_med_record).focus();
              $('#patient_regnas').attr('readonly','readonly');
              $('#patient_province').val(jdata.response.patient_province);
        $.ajax({
                url : '<?php echo base_url()."master/district/districtbyprovince";?>',
                type:'POST',
                dataType:'json',
                data:{
                  'province_code':jdata.response.patient_province
                  },
                  success:function(jsdata){
                    var str ='<option value=""></option>';
                    $.each(jsdata.response,function(i,item){
                     
                     if(item.district_code==jdata.response.patient_district){
                      str +='<option value="'+item.district_code+'"  selected="selected">'+item.district_name+'</option>';

                     }else{
                      str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
                     }  
                    })
                      $('#patient_district').html(str).val(jdata.response.patient_district);
                      //$(".preloader").fadeOut();
                      }
              });


              $.ajax({
                url : '<?php echo base_url()."master/subdistrict/bydistrict";?>',
                type:'POST',
                dataType:'json',
                data:{
                  'district_code':jdata.response.patient_district
                  },
                  success:function(jsdata){
                    var str ='<option value=""></option>';
                    $.each(jsdata.response,function(i,item){
                      str +='<option value="'+item.subdistrict_code+'">'+item.subdistrict_name+'</option>';
                      })
                      $('#patient_subdistrict').html(str).val(jdata.response.patient_subdistrict);
                      //$(".preloader").fadeOut();
                      }
              });
        $('#patient_address').val(jdata.response.patient_address);

              }else{
                $('#patient_regnas').val("").focus();
              }
            }
          }
        });
      }
    }
      });


  

      $('.tanggal').datepicker({
  format:"dd-mm-yyyy",
  startView:"year",
  minView:"year"
}).on('changeDate',function(ev){
  $(this).blur();
  $(this).datepicker('hide');
});

        $('#patient_province').change(function(){
    $(".preloader").fadeIn();
    $.ajax({
        url : '<?php echo base_url()."master/district/districtbyprovince";?>',
        type:'POST',
        dataType:'json',
        data:{
            'province_code':$('#patient_province').val()
        },
        success:function(jdata){

            var str ='<option value=""><?php echo $this->lang->line('district');?></option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
            })
            $('#patient_district').html(str);
            $(".preloader").fadeOut();
        }
    })
})





        
    









    
$('#specimen').validate({

rules:{
  patient_nid:{
    required: true,
    minlength:16,
  },
  patient_regnas:{
    required: true
  }
},
messages:{
  patient_nid:{
    required:"<?php echo $this->lang->line('required');?>",
    minlength:"Masukkan 16 digit NIK"
  },
  patient_regnas:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  patient_med_record:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  patient_name:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  patient_bday:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  patient_sex:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  patient_art_date:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  patient_province:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  patient_district:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  patient_subdistrict:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  patient_address:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  doctor_name:{
    required:"<?php echo $this->lang->line('required');?>"
  }

},
submitHandler: function(form) {

  if($('#specimen_internal_num_id').val()=="0"){
        $.ajax({
            url:"<?php echo base_url()."transactional/specimen/add";?>",
            type:"POST",
            dataType:"json",
            data:{
                "patient_name":$('#patient_name').val(),
                "patient_nid":$("#patient_nid").val(),
                "patient_bday":dbdate($('#patient_bday').val()),
                "patient_sex":$('#patient_sex').val(),
                "patient_regnas":$('#patient_regnas').val(),
                "patient_med_record":$('#patient_med_record').val(),
                "patient_art_date":dbdate($('#patient_art_date').val()),
                "patient_province":$('#patient_province').val(),
                "patient_district":$('#patient_district').val(),
                "patient_subdistrict":$('#patient_subdistrict').val(),
                "patient_address":$('#patient_address').val()
            },
            success:function(jdata){
                if(jdata.status=='success'){
                   
                  $.ajax({
                    url : "<?php echo base_url()."intern/internal/insert";?>",
                    type: "POST",
                    dataType:"json",
                    data:{
                      "specimen_internal_patient_id":jdata.response,
                      "specimen_internal_date_request":"<?php echo date("Y-m-d");?>",
                      "specimen_internal_timestamp":dbdate($('#specimen_internal_date_request').val()),
                      "specimen_internal_hf_code":"<?php echo $this->session->userdata("user_unit");?>",
                "specimen_internal_exam_category":$('#specimen_internal_exam_categori').val(),
                "specimen_internal_doctor_name":$('#doctor_name').val()
                    },
                    success:function(jdata){
                      if(jdata.status=='success'){
                        alert(jdata.message);
                        document.location = "<?php echo base_url()."intern/internal";?>";
                      }else{
                        alert(jdata.message);
                      }
                    }
                  })


                }

            }
        })
      }else{
        $.ajax({
            url:"<?php echo base_url()."transactional/specimen/update/";?>"+$("#patient_id").val(),
            type:"POST",
            dataType:"json",
            data:{
                "patient_name":$('#patient_name').val(),
                "patient_nid":$("#patient_nid").val(),
                "patient_bday":dbdate($('#patient_bday').val()),
                "patient_sex":$('#patient_sex').val(),
                "patient_regnas":$('#patient_regnas').val(),
                "patient_med_record":$('#patient_med_record').val(),
                "patient_art_date":dbdate($('#patient_art_date').val()),
                "patient_province":$('#patient_province').val(),
                "patient_district":$('#patient_district').val(),
                "patient_subdistrict":$('#patient_subdistrict').val(),
                "patient_address":$('#patient_address').val()

            },
            success:function(jdata){
                if(jdata.status=='success'){
                   
                  $.ajax({
                    url : "<?php echo base_url()."intern/internal/update/";?>"+$('#specimen_internal_num_id').val(),
                    type: "POST",
                    dataType:"json",
                    data:{
                      "specimen_internal_timestamp":dbdate($('#specimen_internal_date_request').val()),
                      "specimen_internal_patient_id":$('#patient_id').val(),
                "specimen_internal_exam_category":"2",
                "specimen_internal_doctor_name":$('#doctor_name').val()
                    },
                    success:function(jdata){
                      if(jdata.status=='success'){
                        alert(jdata.message);
                        document.location = "<?php echo base_url()."intern/internal";?>";
                      }else{
                        alert(jdata.message);
                      }
                    }
                  })


                }

            }
        })









      }
    }
});

    });
</script>