<style>.datepicker{z-index:1151 !important;}</style>
<nav aria-label="breadcrumb" style="margin-top: 50px;">
  <ol class="breadcrumb">
 <li class="breadcrumb-item"> <a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
 
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-clipboard-check"></i>&nbsp;<?php echo $this->lang->line('result');?></li>
  </ol>
</nav>





<!---- MODAL ORDER -->

<div class="modal fade" id="modalForm">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-merah" >
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('order');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form method="POST" id="specimen">
       

<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
                <input type="hidden" value="0" id="patient_id">
              <input type="hidden" value="0" id="specimen_internal_num_id">
                <input type="text"  id="patient_nid" name="patient_nid" class="form-control" placeholder="<?php echo $this->lang->line('patient_nid');?>" required="required" autofocus="autofocus">
                  <label for="patient_nid"><?php echo $this->lang->line('patient_nid');?></label>
            
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <input type="text"  id="patient_regnas" name="patient_regnas" class="form-control" placeholder="<?php echo $this->lang->line('patient_regnas');?>" required="required" autofocus="autofocus">
                  <label for="patient_regnas"><?php echo $this->lang->line('patient_reg_nation');?></label>
            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <input type="text"  id="patient_med_record" name="patient_med_record" class="form-control" placeholder="<?php echo $this->lang->line('patient_regnas');?>" required="required" autofocus="autofocus">
                  <label for="patient_med_record"><?php echo $this->lang->line('patient_med_record');?></label>
            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
                <div class="form-label-group">
              
                <input type="text"  id="patient_name" name="patient_name" class="form-control" placeholder="<?php echo $this->lang->line('patient_name');?>" required="required" autofocus="autofocus">
                  <label for="patient_name"><?php echo $this->lang->line('patient_name');?></label>
            
                </div>
              </div>
</div>



<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <select name="patient_sex" id="patient_sex" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("patient_sex");?>">
                      <option value=""><?php echo $this->lang->line("patient_sex");?></option>
                  <option value="1"><?php echo  $this->lang->line("sex_m");?></option>
                  <option value="2"><?php echo  $this->lang->line("sex_f");?></option>

</select>

            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <input type="text"  id="patient_bday" name="patient_bday" class="form-control tanggal" placeholder="<?php echo $this->lang->line('patient_bday');?>" required="required" autofocus="autofocus">
                  <label for="patient_bday"><?php echo $this->lang->line('patient_bday');?></label>
            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <input type="text"  id="patient_art_date" name="patient_art_date" class="form-control tanggal" placeholder="<?php echo $this->lang->line('specimen_art_date');?>" required="required" autofocus="autofocus">
                  <label for="patient_art_date"><?php echo $this->lang->line('specimen_art_date');?></label>
            
                </div>
              </div>
</div>





<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <select name="specimen_internal_exam_categori" id="specimen_internal_exam_categori" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("specimen_test_type");?>">
                      <option value=""><?php echo $this->lang->line("specimen_test_type");?></option>
                      <?php for($i=1;$i<=2;$i++){?>
                      <option value="<?php echo $i;?>"><?php echo specimen_vl_test($i); ?></option>
                      <?php }?>
               

</select>
            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <input type="text"  id="doctor_name" name="doctor_name" value="<?php echo $this->session->userdata('user_fname');?>" class="form-control" placeholder="<?php echo $this->lang->line('doctor_name');?>" required="required" autofocus="autofocus">
                  <label for="doctor_name"><?php echo $this->lang->line('doctor_name');?></label>
            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <a class="btn btn-primary btn-block"   id="btnSubmit"><?php echo $this->lang->line('submit');?></a>
               
               
                </div>
              </div>
</div>

        </form>
        </div>
      </div>
    </div>
</div>

<!-- END MODAL -->


<div class="card">
<div class="card-header" id="headercard">&nbsp;
</div>
<div class="card-body">

<table class="table table-stripe">
<thead>
<tr>
    <th scope="col" colspan="2">
    <div class="input-group input-group-sm">
    <input type="text" id="search" name="search" class="form-control" placeholder="Pencarian">
    <div class="input-group-append"><span class="btn  btn-success" id="src"><i class="fas fa-search"></i></span></div>
</div>
</th>
    <th scope="col" colspan="6" class="text-right">
    &nbsp;
    
   
</th>

</tr>

<tr>
    <th scope="col"><?php echo $this->lang->line('number');?></th>
    <th scope="col"><?php echo $this->lang->line('patient_nid');?></th>
    <th scope="col"><?php echo $this->lang->line('patient_reg_nation');?></th>
    <th scope="col"><?php echo $this->lang->line('patient_med_record');?></th>
    <th scope="col"><?php echo $this->lang->line('patient_name');?></th>
    <th scope="col"><?php echo $this->lang->line('result');?></th>
    <th scope="col"><?php echo $this->lang->line('specimen_test_type');?></th>
    <th scope="col"><?php echo $this->lang->line('action');?></th>
</tr>
</thead>
<tbody id="dataBody">
    <?php
    $i=1;
    foreach($datalist as $list){ 
        $m = $this->M_reverse->hfName($list->specimen_internal_hf_code);
        //$k = $this->M_reverse->shipperName($list->order_shipper_id);
        ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->patient_nid;?></td>
    <td scope="col"><?php echo $list->patient_regnas;?></td>
    <td scope="col"><?php echo $list->patient_med_record;?></td>
    <td scope="col"><?php echo $list->patient_name;?></td>
    <td scope="col"> <?php 
    if($list->specimen_result_flag!='0'){


    if($list->vl_int_vlresult_optional!='3'){
echo result_exam($list->vl_int_vlresult_optional);
    }else{
      echo $list->vl_int_vlresult_absolut." copies/mL";
    }
  }
    ?></td>
    <td scope="col"><?php echo specimen_vl_test($list->specimen_internal_exam_category);?></td>
    <td scope="col" class="text-left">
    <a href="<?php echo base_url()."edit/identitas/id/".$list->specimen_internal_patient_id;?>" class="btn btn-danger"><i class="fas fa-edit"></i>&nbsp;Edit Identitas</a>
   
&nbsp;
<?php if($list->specimen_internal_exam_category=='2'){
      if($list->specimen_result_flag!='1'){

if($list->specimen_internal_received=='1'){?> 

<a href="<?php echo base_url()."intern/internal/received/".$list->specimen_internal_num_id;?>" class="btn btn-success"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('confirmation');?></a>
<?php }else{?>

    <a href="<?php echo base_url()."intern/internal/vlresult/".$list->specimen_internal_num_id;?>" class="btn btn-primary"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('result');?></a>

<?php 
}

}else{
  ?>




   <a href="<?php echo base_url()."intern/internal/editvlresult/".$list->specimen_internal_num_id;?>" class="btn btn-warning" onclick="result('<?php echo $list->specimen_internal_num_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('result_edit');?></a>

  <?php
}


}else{
  if($list->specimen_result_flag!='1'){
  

if($list->specimen_internal_received=='1'){?> 

<a href="<?php echo base_url()."intern/internal/received_eid/".$list->specimen_internal_num_id;?>" class="btn btn-success"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('confirmation');?></a>
<?php }else{?>

  <a href="<?php echo base_url()."intern/internal/eidresult/".$list->specimen_internal_num_id;?>"  class="btn btn-primary" onclick="result('<?php echo $list->specimen_internal_num_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('result');?></a>
    

<?php 
}
  }
}

   
    ?>
    </td>
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>


</div>
<div class="card-footer text-center">
&nbsp;
</div>
</div>

<script>


function conf(id,oid){
  if(confirm("<?php echo $this->lang->line('delete_confirmation');?>")){
    document.location = "<?php echo base_url()."intern/internal/delete/";?>"+id+"/"+oid;
  }
}
function dbdate(tgl){
var tg = tgl.split("-");
var dbFormat = tg[2]+"-"+tg[1]+"-"+tg[0];
return dbFormat;
}

function edit(id){
  $.ajax({
    url:"<?php echo base_url()."transactional/specimen/detail";?>",
    type:"POST",
    dataType:"json",
    data:{
      "specimen_num_id":id
    },
    success:function(jdata){
      if(jdata.status=="success"){
       $('#specimen_num_id').val(id);
        $('#patient_nid').val(jdata.response.patient_nid);
        $('#patient_regnas').val(jdata.response.patient_regnas);
        $('#patient_med_record').val(jdata.response.patient_med_record);
        $('#patient_name').val(jdata.response.patient_name);

        $('#patient_id').val(jdata.response.patient_id);
        $('#patient_sex').val(jdata.response.patient_sex);
        $('#patient_bday').val(dbdate(jdata.response.patient_bday));
        $('#patient_art_date').val(dbdate(jdata.response.patient_art_date));
        $('#specimen_id').val(jdata.response.specimen_id);
        $('#specimen_date_collected').val(dbdate(jdata.response.specimen_date_collected));
        $('#specimen_type').val(jdata.response.specimen_type);
        $('#specimen_exam_categori').val(jdata.response.specimen_exam_category);
        $('#doctor_name').val(jdata.response.specimen_doctor_name);
        $('#modalForm').modal('show');

      }
    }
  })
}

function finish(id){
  if(confirm('Apakah anda yakin sudah selesai?')){
    document.location = "<?php echo base_url()."transactional/order/finish/";?>"+id;
  }
}
    $('document').ready(function(){
      $('#src').click(function(){
        $('#dataBody').load("<?php echo base_url()."intern/internal/searchresult";?>",{"search":$('#search').val()});
      })
      $('#search').keyup(function(){
$('#dataBody').load("<?php echo base_url()."intern/internal/searchresult";?>",{"search":$('#search').val()});
});

      $('#patient_nid').blur(function(){
        $.ajax({
          url:"<?php echo base_url()."transactional/specimen/patientid";?>",
          type:"POST",
          dataType:"json",
          data:{
            "patient_nid":$('#patient_nid').val()

          },
          success:function(jdata){
            if(jdata.status=='success'){
              $('#patient_name').val(jdata.response.patient_name);
              $('#patient_sex').val(jdata.response.patient_sex);
              $('#patient_regnas').val(jdata.response.patient_regnas);
              $('#patient_bday').val(dbdate(jdata.response.patient_bday));
              $('#patient_art_date').val(dbdate(jdata.response.patient_art_date));
              $('#patient_med_record').val(jdata.response.patient_med_record);
              $('#specimen_id').focus();
            }
          }
        })
      });


      $('#patient_regnas').blur(function(){
        $.ajax({
          url:"<?php echo base_url()."transactional/specimen/patientid";?>",
          type:"POST",
          dataType:"json",
          data:{
            "patient_regnas":$('#patient_regnas').val()

          },
          success:function(jdata){
            if(jdata.status=='success'){
              $('#patient_name').val(jdata.response.patient_name);
              $('#patient_sex').val(jdata.response.patient_sex);
              $('#patient_nid').val(jdata.response.patient_nid);
              $('#patient_bday').val(dbdate(jdata.response.patient_bday));
              $('#patient_art_date').val(dbdate(jdata.response.patient_art_date));
              $('#patient_med_record').val(jdata.response.patient_med_record);
              $('#specimen_id').focus();
            }
          }
        })
      });


      $('#specimen_id').blur(function(){
        $.ajax({
          url:"<?php echo base_url()."transactional/specimen/checkspecimen";?>",
          type:"POST",
          dataType:"json",
          data:{
            "specimen_id":$('#specimen_id').val(),
            "specimen_order_id":"<?php echo $this->uri->segment(4);?>"
          },
          success:function(jdata){
            if(jdata.status==false){
              alert('ID Specimen Ini sudah terdaftar');
              $("#specimen_id").val('').focus();
            }
          }
        })
      });


      $('.tanggal').datepicker({
  format:"dd-mm-yyyy",
  startView:"year",
  minView:"year"
}).on('changeDate',function(ev){
  $(this).blur();
  $(this).datepicker('hide');
});

        $('#patient_province').change(function(){
    $(".preloader").fadeIn();
    $.ajax({
        url : '<?php echo base_url()."master/district/districtbyprovince";?>',
        type:'POST',
        dataType:'json',
        data:{
            'province_code':$('#patient_province').val()
        },
        success:function(jdata){

            var str ='<option value=""><?php echo $this->lang->line('district');?></option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
            })
            $('#patient_district').html(str);
            $(".preloader").fadeOut();
        }
    })
})





        
    $('#btnSubmit').click(function(){
      if($('#specimen_internal_num_id').val()=="0"){
        $.ajax({
            url:"<?php echo base_url()."transactional/specimen/add";?>",
            type:"POST",
            dataType:"json",
            data:{
                "patient_name":$('#patient_name').val(),
                "patient_nid":$("#patient_nid").val(),
                "patient_bday":dbdate($('#patient_bday').val()),
                "patient_sex":$('#patient_sex').val(),
                "patient_regnas":$('#patient_regnas').val(),
                "patient_med_record":$('#patient_med_record').val(),
                "patient_art_date":dbdate($('#patient_art_date').val())

            },
            success:function(jdata){
                if(jdata.status=='success'){
                   
                  $.ajax({
                    url : "<?php echo base_url()."intern/internal/insert";?>",
                    type: "POST",
                    dataType:"json",
                    data:{
                      "specimen_internal_patient_id":jdata.response,
                      "specimen_internal_hf_code":"<?php echo $this->session->userdata("user_unit");?>",
                "specimen_internal_exam_category":$('#specimen_internal_exam_categori').val(),
                "specimen_internal_doctor_name":$('#doctor_name').val()
                    },
                    success:function(jdata){
                      if(jdata.status=='success'){
                        alert(jdata.message);
                        document.location = "<?php echo base_url()."intern/internal";?>";
                      }else{
                        alert(jdata.message);
                      }
                    }
                  })


                }

            }
        })
      }else{


        $.ajax({
            url:"<?php echo base_url()."transactional/specimen/update/";?>"+$("#patient_id").val(),
            type:"POST",
            dataType:"json",
            data:{
                "patient_name":$('#patient_name').val(),
                "patient_nid":$("#patient_nid").val(),
                "patient_bday":dbdate($('#patient_bday').val()),
                "patient_sex":$('#patient_sex').val(),
                "patient_regnas":$('#patient_regnas').val(),
                "patient_med_record":$('#patient_med_record').val(),
                "patient_art_date":dbdate($('#patient_art_date').val())

            },
            success:function(jdata){
                if(jdata.status=='success'){
                   
                  $.ajax({
                    url : "<?php echo base_url()."intern/internal/update/";?>"+$('#specimen_internal_num_id').val(),
                    type: "POST",
                    dataType:"json",
                    data:{
                      "specimen_patient_id":$('#patient_id').val(),
                "specimen_internal_exam_category":$('#specimen_internal_exam_categori').val(),
                "specimen_internal_doctor_name":$('#doctor_name').val()
                    },
                    success:function(jdata){
                      if(jdata.status=='success'){
                        alert(jdata.message);
                        document.location = "<?php echo base_url()."intern/internal";?>";
                      }else{
                        alert(jdata.message);
                      }
                    }
                  })


                }

            }
        })









      }
    });

    });
</script>