<style>.datepicker{z-index:1151 !important;}
label{
  color: red;
  margin-left: 10px;
}
</style>
<nav aria-label="breadcrumb"  style="margin-top: 50px;">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item"><a href="<?php echo base_url()."intern/internal/result_eid";?>"><i class="fas fa-paper-plane"></i>&nbsp;<?php echo $this->lang->line('order');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('result');?></li>
  </ol>
</nav>

<div class="card col-md-4 mx-auto">
<div class="card-header" id="headercard">&nbsp;<h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('result');?></h4>
</div>
<div class="card-body">
<form id="result_vl" method="POST">


<div class="form-row">
<div class="col-md-12">
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('patient_regnas');?></span>
    </div>
                <input type="hidden" value="<?php echo $specimen->patient_id;?>"  id="patient_id" name="patient_id">
                <input type="hidden" value="<?php echo $specimen->specimen_internal_num_id;?>"  id="specimen_internal_num_id" name="specimen_internal_num_id">
               
                <input type="text" value="<?php echo $specimen->patient_regnas;?>"  id="patient_regnas" name="patient_regnas" class="form-control"   autofocus="autofocus" readonly="readonly">

                  
               
            
                </div>
              </div>
</div>



<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('specimen_id');?></span>
    </div>
               
                <input type="text" value=""  id="eid_int_specimen_id" name="eid_int_specimen_id" class="form-control"  autofocus="autofocus">

                  
              
            
                </div>
              </div>
</div>



<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('specimen_date_collected');?></span>
    </div>
               
                <input type="text"  id="eid_int_date_collected" name="eid_int_date_collected" class="form-control tanggal"  required="required" autofocus="autofocus">

                  
                
            
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('specimen_exam_date');?></span>
    </div>
              
               
                <input type="text"  id="eid_int_exam_date" name="eid_int_exam_date" class="form-control tanggal"  required="required" autofocus="autofocus">

                  
                
            
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('specimen_date_release');?></span>
    </div>
              
               
                <input type="text"  id="eid_int_date_release" name="eid_int_date_release" class="form-control tanggal"  required="required" autofocus="autofocus">

                  
              
            
                </div>
              </div>
</div>




<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('result');?></span>
    </div>
               
               <!-- <input type="text"  id="specimen_eid_result" name="specimen_eid_result" class="form-control"  required="required" autofocus="autofocus">

                  
                <label for="specimen_eid_result"><?php echo $this->lang->line('result');?></label>
            -->

            <select name="eid_int_eidresult" id="eid_int_eidresult" class="form-control" required="required">
            <option value=""><?php echo $this->lang->line('result');?></option>
            <?php
            for($i=1;$i<=5;$i++)
            {?>
            <option value="<?php echo $i;?>"><?php echo result_eid($i);?></option>
            <?php } ?>
            </select>
                </div>
              </div>
</div>

<!--
<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('result_absolut');?></span>
    </div>
                
<input type="text"  id="specimen_eid_result" name="specimen_eid_result" class="form-control"   autofocus="autofocus" disabled="disabled">         
               
                </div>
              </div>
</div>-->


<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
<div class="form-label-group">
                
<button type="submit" class="btn btn-success btn-block" id="btnSubmit"><?php echo $this->lang->line("submit");?></button>
               
               
                </div>
              </div>
</div>


</form>


</div>
<div class="card-footer">
</div>
</div>

<script>
    $('document').ready(function(){

      $('.tanggal').datepicker({
  format:"dd-mm-yyyy",
  startView:"year",
  minView:"year"
}).on('changeDate',function(ev){
  $(this).blur();
  $(this).datepicker('hide');
});
$('.tanggal').attr('readonly','readonly');

$('#result_vl').validate({
  
  messages:{
    eid_int_date_collected:{
      required:"<?php echo $this->lang->line('required');?>"
    },
    eid_int_exam_date:{
      required:"<?php echo $this->lang->line('required');?>"
    },
    eid_int_date_release:{
      required:"<?php echo $this->lang->line('required');?>"
    },
    eid_int_eidresult:{
      required:"<?php echo $this->lang->line('required');?>"
    }

  },
  submitHandler:function(form){
    
    $.ajax({
                url:"<?php echo base_url()."intern/internal/insertresult_eid/";?>"+$('#specimen_internal_num_id').val(),
                type:"POST",
                dataType:"json",
                data:{
                   
                    "eid_int_specimen_num_id":$('#specimen_internal_num_id').val(),
                    "eid_int_specimen_id":$('#eid_int_specimen_id').val(),
                    "eid_int_date_collected":formatDate($('#eid_int_date_collected').val()),
                    "eid_int_exam_date":formatDate($('#eid_int_exam_date').val()),
                    "eid_int_date_release":formatDate($('#eid_int_date_release').val()),
                    "eid_int_eidresult":$('#eid_int_eidresult').val(),
                   
                },
                success:function(jdata){
                    if(jdata.status=='success'){
                        alert(jdata.message);
                        document.location="<?php echo base_url()."intern/internal/result_eid";?>";
                    }
                }
            })
  }


});

      



    });
    </script>