<style>.datepicker{z-index:1151 !important;}
label{
  color: red;
  margin-left: 10px;
}
</style>
<nav aria-label="breadcrumb" style="margin-top:50px">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;Identitas Pasien</li>
  </ol>
</nav>

<div class="card col-md-4 mx-auto">
<div class="card-header" id="headercard">&nbsp;<h6 class="modal-title"><i class="fas fa-edit"></i>&nbsp;Edit Identitas Pasien</h6>
</div>
<div class="card-body">


<form method="POST" id="idpatient"  class="needs-validation" validate>
        <input type="hidden" value="<?php echo $identitas->patient_id;?>" name="patient_id" id="patient_id">
      

    <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('patient_nid');?></span>
    </div>
    <input type="text"  id="patient_nid" name="patient_nid" class="form-control" maxlength="16" value="<?php echo $identitas->patient_nid;?>"  required autofocus="autofocus">&nbsp;</div>
    
    
    <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('patient_regnas');?></span>
    </div>
    <input type="text"  id="patient_regnas" name="patient_regnas" class="form-control noneid" value="<?php echo $identitas->patient_regnas;?>" required="required" autofocus="autofocus"></div>  


    <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('patient_med_record');?></span>
    </div>
    <input type="text"  id="patient_med_record" name="patient_med_record" class="form-control"  value="<?php echo $identitas->patient_med_record;?>" required="required" autofocus="autofocus"></div>  


    <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('patient_name');?></span>
    </div>
    <input type="text"  id="patient_name" name="patient_name" class="form-control"  required="required" autofocus="autofocus" value="<?php echo $identitas->patient_name;?>"></div> 


    <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line("patient_sex");?></span>
    </div>
    <select name="patient_sex" id="patient_sex" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("patient_sex");?>">

                  <option value="1" <?php if($identitas->patient_sex=='1'){ echo "selected"; } ?>><?php echo  $this->lang->line("sex_m");?></option>
                  <option value="2"<?php if($identitas->patient_sex=='2'){ echo "selected"; } ?>><?php echo  $this->lang->line("sex_f");?></option>

</select> 
  </div> 



  <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('patient_bday');?></span>
    </div>
    <input type="text"  id="patient_bday" name="patient_bday" class="form-control tanggal" placeholder="dd-mm-yyyy" required="required" autofocus="autofocus" value="<?php echo db_date($identitas->patient_bday);?>"></div> 


    



    <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line("province");?></span>
    </div>
    <select name="patient_province" id="patient_province" class="form-control custom-select"   placeholder="<?php echo $this->lang->line("province");?>">
                      <option value=""></option>
                      <?php foreach($province as $combolist){?>
                      <option value="<?php echo $combolist->province_code;?>" <?php if($combolist->province_code==$identitas->patient_province){ echo "selected"; }?>><?php echo $combolist->province_name; ?></option>
                      <?php }?>
               

</select>
  </div> 



  <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line("district");?></span>
    </div>
    <select name="patient_district" id="patient_district" class="form-control custom-select">
                      <option value=""></option>
               

</select>
  </div> 


  <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line("subdistrict");?></span>
    </div>
    <select name="patient_subdistrict" id="patient_subdistrict" class="form-control custom-select">
                      <option value=""></option>
               

</select>
  </div> 



  <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('address');?></span>
    </div>
    <input type="text"  id="patient_address" name="patient_address" value="<?php echo $identitas->patient_address;?>" class="form-control"   autofocus="autofocus">
  </div> 

  <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('specimen_art_date');?></span>
    </div>
    <input type="text"  id="patient_art_date" name="patient_art_date" class="form-control tanggal noneid" value="<?php echo db_date($identitas->patient_art_date);?>" placeholder="dd-mm-yyyy" required="required" autofocus="autofocus"></div> 

<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                <button type="submit" class="btn btn-success btn-block" id="btnSubmitEdit"><?php echo $this->lang->line("submit");?></button>
          
                <!-- <a class="btn btn-success btn-block"   id="btnSubmit"><?php echo $this->lang->line('submit');?></a> -->
               
               
                </div>
              </div>
</div>

        </form>



        </div>
<div class="card-footer">
</div>
</div>

<script>
function dbdate(tgl){
var tg = tgl.split("-");
var dbFormat = tg[2]+"-"+tg[1]+"-"+tg[0];
return dbFormat;
}


$('document').ready(function(){

    $('.tanggal').datepicker({
  format:"dd-mm-yyyy",
  startView:"year",
  minView:"year"
}).on('changeDate',function(ev){
  $(this).blur();
  $(this).datepicker('hide');
});
$('.tanggal').attr("readonly","readonly");

    $.ajax({
                url : '<?php echo base_url()."master/district/districtbyprovince";?>',
                type:'POST',
                dataType:'json',
                data:{
                  'province_code':"<?php echo $identitas->patient_province;?>"
                  },
                  success:function(jsdata){
                    var str ='<option value=""></option>';
                    $.each(jsdata.response,function(i,item){
                     
                  
                      str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
                      
                    })
                      $('#patient_district').html(str).val('<?php echo $identitas->patient_district;?>');
                      //$(".preloader").fadeOut();
                      }

              });


    
              $.ajax({
                url : '<?php echo base_url()."master/subdistrict/bydistrict";?>',
                type:'POST',
                dataType:'json',
                data:{
                  'district_code':"<?php echo $identitas->patient_district;?>"
                  },
                  success:function(jsdata){
                    var str ='<option value=""></option>';
                    $.each(jsdata.response,function(i,item){
                      str +='<option value="'+item.subdistrict_code+'">'+item.subdistrict_name+'</option>';
                      })
                      $('#patient_subdistrict').html(str).val("<?php echo $identitas->patient_subdistrict;?>");
                      //$(".preloader").fadeOut();
                      }
              });





              $('#patient_province').change(function(){
    $(".preloader").fadeIn();
    $.ajax({
        url : '<?php echo base_url()."master/district/districtbyprovince";?>',
        type:'POST',
        dataType:'json',
        data:{
            'province_code':$('#patient_province').val()
        },
        success:function(jdata){

            var str ='<option value=""><?php echo $this->lang->line('district');?></option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
            })
            $('#patient_district').html(str).removeAttr('disabled');
            $(".preloader").fadeOut();
        }
    })
})




$('#patient_district').change(function(){
    $(".preloader").fadeIn();
    $.ajax({
        url : '<?php echo base_url()."master/subdistrict/bydistrict";?>',
        type:'POST',
        dataType:'json',
        data:{
            'district_code':$('#patient_district').val()
        },
        success:function(jdata){

            var str ='<option value=""><?php echo $this->lang->line('subdistrict');?></option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.subdistrict_code+'">'+item.subdistrict_name+'</option>';
            })
            $('#patient_subdistrict').html(str).removeAttr('disabled');
            $(".preloader").fadeOut();
        }
    })
})




$('#idpatient').validate({
  rules:{
    patient_nid:{
      required: true,
      minlength:16,
      remote:{
          url:"<?php echo base_url()."edit/identitas/checknik";?>",
          type:"POST",
          data: {
              patient_id:function(){
                  return $('#patient_id').val();
              }
          }
      }
    },
    patient_regnas:{
        required: true,
        remote:{
            url:"<?php echo base_url()."edit/identitas/checkregnas";?>",
            type:"POST",
            data:{
                patient_id:function(){
                  return $('#patient_id').val();
              }
            }
        }
    }
  },
  messages:{
    patient_nid:{
      required:"<?php echo $this->lang->line('required');?>",
      minlength:"Masukkan 16 digit NIK",
      remote:"NIK sudah digunakan"
    },
    patient_regnas:{
      required:"<?php echo $this->lang->line('required');?>",
      remote:"No. Regnas Sudah digunakan"
    },
    patient_med_record:{
      required:"<?php echo $this->lang->line('required');?>"
    },
    patient_name:{
      required:"<?php echo $this->lang->line('required');?>"
    },
    patient_bday:{
      required:"<?php echo $this->lang->line('required');?>"
    },
    patient_sex:{
      required:"<?php echo $this->lang->line('required');?>"
    },
    patient_art_date:{
      required:"<?php echo $this->lang->line('required');?>"
    },
    patient_province:{
      required:"<?php echo $this->lang->line('required');?>"
    },
    patient_district:{
      required:"<?php echo $this->lang->line('required');?>"
    },
    patient_subdistrict:{
      required:"<?php echo $this->lang->line('required');?>"
    },
    patient_address:{
      required:"<?php echo $this->lang->line('required');?>"
    }
  },
  submitHandler:function(form){

    $.ajax({
            url:"<?php echo base_url()."edit/identitas/update/";?>"+$("#patient_id").val(),
            type:"POST",
            dataType:"json",
            data:{
                "patient_name":$('#patient_name').val(),
                "patient_nid":$("#patient_nid").val(),
                "patient_bday":dbdate($('#patient_bday').val()),
                "patient_sex":$('#patient_sex').val(),
                "patient_regnas":$('#patient_regnas').val(),
                "patient_med_record":$('#patient_med_record').val(),
                "patient_art_date":dbdate($('#patient_art_date').val()),
                "patient_province":$('#patient_province').val(),
                "patient_district":$('#patient_district').val(),
                "patient_subdistrict":$('#patient_subdistrict').val(),
                "patient_address":$('#patient_address').val()
                

            },
            success:function(jdata){
                if(jdata.status=='success'){
                  alert(jdata.message);
                  document.location = "<?php echo base_url()."edit/identitas/id/".$this->uri->segment('4');?>";
                }else{
                  alert(jdata.message);
                }

            }
        })

  }


});


})
</script>