<script>
  $(".preloader").fadeIn();
  </script>
<nav aria-label="breadcrumb"   style="margin-top:50px">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('health_facility');?></li>
  </ol>
</nav>



<!---- Form Add New Fasyankes -->
<div class="modal fade" id="modalForm">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-merah">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('data_form');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form method="POST" action="<?php echo base_url()."master/hf/add";?>" id="newkec">
        <div class="form-row">
<div class="col-md-12">
                <div class="form-label-group">
              
                  <select name="province_code" id="province_code" class="form-control custom-select"  required="required">
                      <option value=""><?php echo $this->lang->line('province');?></option>
                  <?php

foreach($province as $lpropinsi){
?>
<option value="<?php echo $lpropinsi->province_code;?>"><?php echo $lpropinsi->province_name;?></option>
<?php
}
?>

</select>

            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                  <select name="district_code" id="district_code" class="form-control custom-select"  required="required">
                      <option value=""><?php echo $this->lang->line('district');?></option>
                    </select>

            
                </div>
              </div>
</div>





<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
                <div class="form-label-group">
              
                  <select name="hf_type" id="hf_type" class="form-control custom-select"  required="required">
                      <option value=""><?php echo $this->lang->line('healthfacility_type');?></option>
                    <?php foreach($hftype as $type){?>
                    
                    <option value="<?php echo $type->hf_type;?>"><?php echo $this->lang->line($type->hf_type);?></option>
                        <?php } ?>
                    </select>

            
                </div>
              </div>
</div>







<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                  <input type="text" id="healthfacility_code" name="healthfacility_code" class="form-control" placeholder="Kode fasyankes" required="required" autofocus="autofocus">
                  <label for="healthfacility_code"><?php echo $this->lang->line('healthfacility_code');?></label>
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                  <input type="text" id="healthfacility_name" name="healthfacility_name" class="form-control" placeholder="Nama fasyankes" required="required" autofocus="autofocus">
                  <label for="healthfacility_name"><?php echo $this->lang->line('healthfacility_name');?></label>
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-group form-check">
                  <input type="checkbox" id="hf_referral" value="1" name="hf_referall">
                  <label for="hf_referral"><?php echo $this->lang->line('healthfacility_referral');?></label>
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-group form-check">
                  <input type="checkbox" id="hf_treatments" value="1" name="hf_treatment">
                  <label for="hf_treatment">Faskes PDP Pengampu</label>
                </div>
              </div>
</div>




<div class="form-row">
<div class="col-md-12" style="padding-top:10px color:#ffffff">
                <div class="form-label-group">
                
                <a class="btn btn-success btn-block"   id="btnSubmit"><?php echo $this->lang->line('submit');?></a>
               
               
                </div>
              </div>
</div>


</form>

        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer bg-merah">
          &nbsp;
        </div>
        
      </div>
    </div>
  </div>

<!---- End Form -->











<div class="card">
<div class="card-header bg-merah" id="headercard">
<?php
$max = ceil($total/50);
  if($this->uri->segment(4)>5 && ($this->uri->segment(4)+5)>6){

  
   if($this->uri->segment(4)==$max || ($this->uri->segment(4)+2)>$max){ 
     $maxnum = 1+$this->uri->segment(4);
  }else{ 
    $maxnum = 3+$this->uri->segment(4);
  }
  
  $start = $maxnum-5;
  $prev = $start-1; 
  

}else{
    
      $maxnum = 6;
      $start = 1;
      $prev = 1;

  }
 // $ix = (($this->uri->segment('3')*50)+1)-50;
  ?>
<ul class="pagination pagination-sm justify-content-center">
  <li class="page-item prev"><a class="page-link fas fa-arrow-circle-left" href="<?php echo base_url()."master/hf/list/".$prev;?>">&nbsp;</a></li>
  

<?php
//echo $max;
  for($i=$start;$i<=($maxnum-1);$i++){

    if($i==$this->uri->segment(4)){
        ?>
        <li class="page-item active" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo base_url()."master/hf/list/".$i;?>"><?php echo $i;?></a></li>
        <?php

    }else{

      ?>
<li class="page-item" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo base_url()."master/hf/list/".$i;?>"><?php echo $i;?></a></li>

      <?php
    }

  }
  ?>
  <li class="page-item next"><a class="page-link fas fa-arrow-circle-right" href="<?php echo base_url()."master/hf/list/".$maxnum; ?>">&nbsp;</a></li>
</ul>


</div>
<div class="card-body col-md-4">
<div class="input-group">
    <input type="text" id="search" name="search" class="form-control" placeholder="<?php echo $this->lang->line('search');?>">
    <div class="input-group-append"><span class="btn  btn-success" id="src"><i class="fas fa-search"></i></span></div>
    &nbsp;
    <a href="#cardform" class="btn btn-success btn-xs" id="add" data-toggle="modal" data-target="#modalForm"><i class="fas fa-plus-circle"></i>&nbsp;<?php echo $this->lang->line('add');?></a>
</div>
</div>
<div class="card-body">
<div class="tableFixHead">
<table class="table table-stripe">
<thead>


<tr>
    <th scope="col"><?php echo $this->lang->line('number');?></th>
    <th scope="col"><?php echo $this->lang->line('healthfacility_code');?></th>
    <th scope="col"><?php echo $this->lang->line('province_name');?></th>
    <th scope="col"><?php echo $this->lang->line('district_name');?></th>
    
    <th scope="col"><?php echo $this->lang->line('healthfacility_name');?></th>
    <th scope="col"><?php echo $this->lang->line('action');?></th>
</tr>
</thead>
<tbody id="dataBody">
    <?php

if($this->uri->segment(4)!=''){    
$xi = ($this->uri->segment(4)-1)*50;
}else{
    $xi = 0;
}
    $i=1;
    foreach($response as $list){ ?>
    <tr>
    <th scope="row"><?php echo $i+$xi;?></th>
    <td scope="col"><?php echo $list->hf_code?></td>
    <td scope="col"><?php echo $list->province_name;?></td>
    <td scope="col"><?php echo $list->district_name;?></td>
  
    <td scope="col"><?php echo $list->hf_name;?></td>
    <td scope="col">
    <a href="#" class="btn btn-danger btn-sm" onclick="conf('<?php echo $list->hf_code?>')"><i class="fas fa-trash-alt"></i>&nbsp;<?php echo $this->lang->line('delete');?></a>
    &nbsp;
    <a href="#" id="btnedit" class="btn btn-success btn-sm"   onclick="edit('<?php echo $list->hf_code?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>
    &nbsp;
    <a href="#" id="btnnetwork" class="btn btn-success btn-sm"   onclick="addnetwork('<?php echo $list->hf_code?>')"><i class="fas fa-plus-square"></i>&nbsp;<?php echo $this->lang->line('add_network');?></a>

    &nbsp;
    <a href="#" id="netkurir" class="btn btn-success btn-sm"   onclick="addkurir('<?php echo $list->hf_code?>')"><i class="fas fa-plus-square"></i>&nbsp;Kurir</a>



    </td>
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>
</div>

</div>
<div class="card-footer text-center bg-merah">

<!-- <ul class="pagination pagination-sm">
  <li class="page-item"><a class="page-link" href="#">Previous</a></li>
  <li class="page-item"><a class="page-link" href="#">1</a></li>
  <li class="page-item"><a class="page-link" href="#">2</a></li>
  <li class="page-item"><a class="page-link" href="#">3</a></li>
  <li class="page-item"><a class="page-link" href="#">Next</a></li>
</ul> -->


</div>
</div>






<!---- Form Edit  Fasyankes -->
<div class="modal fade" id="modalEdit">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-merah">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('data_form');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form method="POST" action="<?php echo base_url()."master/hf/add";?>" id="newkec">
        <div class="form-row">
<div class="col-md-12">
                <div class="form-label-group">
              
                  <select name="province_code_update" id="province_code_update" class="form-control custom-select"  required="required">
                      <option value=""><?php echo $this->lang->line('province');?></option>
                  <?php

foreach($province as $lpropinsi){
?>
<option value="<?php echo $lpropinsi->province_code;?>"><?php echo $lpropinsi->province_name;?></option>
<?php
}
?>

</select>

            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                  <select name="district_code_update" id="district_code_update" class="form-control custom-select"  required="required">
                      <option value=""><?php echo $this->lang->line('district');?></option>
                    </select>

            
                </div>
              </div>
</div>





<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
                <div class="form-label-group">
              
                  <select name="hf_type_update" id="hf_type_update" class="form-control custom-select"  required="required">
                      <option value=""><?php echo $this->lang->line('healthfacility_type');?></option>
                    <?php foreach($hftype as $type){?>
                    
                    <option value="<?php echo $type->hf_typeid;?>"><?php echo $this->lang->line($type->hf_type);?></option>
                        <?php } ?>
                    </select>

            
                </div>
              </div>
</div>







<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                <input type="hidden" id="hf_code" value="">
                  <input type="text" id="hf_code_update" name="hf_code_update" class="form-control" placeholder="Kode fasyankes" required="required" autofocus="autofocus">
                  <label for="hf_code_update"><?php echo $this->lang->line('healthfacility_code');?></label>
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                  <input type="text" id="hf_name_update" name="hf_name_update" class="form-control" placeholder="Nama fasyankes" required="required" autofocus="autofocus">
                  <label for="hf_name_update"><?php echo $this->lang->line('healthfacility_name');?></label>
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-group form-check">
                  <input type="checkbox" id="hf_referral_update" value="1" name="hf_referall_update">
                  <label for="hf_referral_update"><?php echo $this->lang->line('healthfacility_referral');?></label>
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-group form-check">
                  <input type="checkbox" id="hf_treatment_update" value="1" name="hf_treatment_update">
                  <label for="hf_treatment_update">Faskes PDP Pengampu</label>
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12" style="padding-top:10px;color:#ffffff">
                <div class="form-label-group">
                
                <a class="btn btn-success btn-block"   id="btnUpdate"><?php echo $this->lang->line('update');?></a>
               
               
                </div>
              </div>
</div>


</form>

        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer bg-merah">
          &nbsp;
        </div>
        
      </div>
    </div>
  </div>

<!---- End Form -->





<script>



function addnetwork(id){
  document.location ="<?php echo base_url()."network/laboratorium/new/";?>"+id;
}

function addkurir(id){
  document.location = "<?php echo base_url()."network/kurir/list/";?>"+id;
}


function edit(idfasyankes){
    $(".preloader").fadeIn();
  
  $.ajax({
    url:"<?php echo base_url()."master/hf/detail/";?>"+idfasyankes,
    type:"POST",
    dataType : "json",
    data:{
      "hf_code":idfasyankes
    },
    success:function(jsondata){
      $('#hf_code').val(jsondata.response.hf_code);
      $('#hf_code_update').val(jsondata.response.hf_code);
      $('#province_code_update').val(jsondata.response.hf_province);
      $('#district_code_update').val(jsondata.response.hf_district);
      $('#hf_type_update').val(jsondata.response.hf_type);
      $('#hf_name_update').val(jsondata.response.hf_name);
      //$('#hf_referral_update').val(jsondata.response.hf_referral);
      if(jsondata.response.hf_referral=='1'){
        $('#hf_referral_update').attr("checked",true);
      }else{
        $('#hf_referral_update').attr("checked",false);
      }

      if(jsondata.response.hf_treatment=='1'){
        $('#hf_treatment_update').attr("checked",true);
      }else{
        $('#hf_treatment_update').attr("checked",false);
      }


      $.ajax({
        url : '<?php echo base_url()."master/district/districtbyprovince";?>',
        type:'POST',
        dataType:'json',
        data:{
            'province_code':jsondata.response.hf_province
        },
        success:function(jdata){

            var str ='<option value=""><?php echo $this->lang->line('district');?></option>';
            $.each(jdata.response,function(i,item){
                if(item.district_code==jsondata.response.hf_district){
                    str +='<option value="'+item.district_code+'" selected>'+item.district_name+'</option>';
                }else{
                str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
                }
            })
            $('#district_code_update').html(str);
            $('#modalEdit').modal('show');
        $(".preloader").fadeOut();
        }

       
    });

    

    

   

    }
  })
}





function conf(id){
  var n = confirm('<?php echo $this->lang->line("delete_confirmation");?>');
  
  if(n==true){
    $(".preloader").fadeIn();
document.location = "<?php echo base_url()."master/hf/delete/";?>"+id;
  }
}



$('document').ready(function(){

var page = "<?php echo $this->uri->segment(4);?>";
var maxn = "<?php echo $max;?>";
var maxnum = "<?php echo $maxnum;?>";


if(page=='' || page=='1'){
    $('.prev').addClass('disabled');
}

var m = parseInt(maxn)<parseInt(maxnum);
if(m==true){
    $('.next').addClass('disabled');
}





$('#btnSubmit').click(function(){
  $(".preloader").fadeIn();
  $.ajax({
    url:"<?php echo base_url()."master/hf/add";?>",
    type:"POST",
    dataType:"json",
    data:{
      "hf_province":$('#province_code').val(),
      "hf_district":$('#district_code').val(),
      "hf_code":$('#healthfacility_code').val(),
      "hf_name":$('#healthfacility_name').val(),
      "hf_type":$('#hf_type').val(),
      "hf_referral":$("#hf_referral").val(),
      "hf_treatment":$("#hf_treatment").val()
    },
    success:function(jdata){
     
      if(jdata.status=='success'){
        alert(jdata.message);
      $(".preloader").fadeOut();
      $('#modalForm').hide();
      document.location = "<?php echo base_url()."master/hf/list/1";?>";
      }else{
        $(".preloader").fadeOut();
        alert(jdata.message);
        document.location = "<?php echo base_url()."master/hf/list/1";?>";

      }
    },
    error:function(jdata){
      alert(jdata.error)
      $(".preloader").fadeOut();
    }

  })

})


$('#btnUpdate').click(function(){
  var referal;
  var treatment;
  if($('#hf_referral_update').is(':checked')){
    referal = '1';
  }else{
    referal='0';
  }
  if($('#hf_treatment_update').is('checked')){
    treatment ='1';
  }else{
    treatment="0";
  }

  $(".preloader").fadeIn();
  $.ajax({
    url:"<?php echo base_url()."master/hf/update/";?>"+$('#hf_code').val(),
    type:"POST",
    dataType:"json",
    data:{
      "hf_province":$('#province_code_update').val(),
      "hf_district":$('#district_code_update').val(),
      "hf_code":$('#hf_code_update').val(),
      "hf_name":$('#hf_name_update').val(),
      "hf_type":$('#hf_type_update').val(),
      "hf_referral":referal,
      "hf_treatment":treatment

    },
    success:function(jdata){
     
      if(jdata.status=='success'){
        alert(jdata.message);
      $(".preloader").fadeOut();
      $('#modalForm').hide();
      document.location = "<?php echo base_url()."master/hf/list/1";?>";
      }else{
        $(".preloader").fadeOut();
        alert(jdata.message);
        document.location = "<?php echo base_url()."master/hf/list/1";?>";

      }
    },
    error:function(jdata){
      alert(jdata.error)
      $(".preloader").fadeOut();
    }

  })

})




$('#province_code').change(function(){
    $(".preloader").fadeIn();
    $.ajax({
        url : '<?php echo base_url()."master/district/districtbyprovince";?>',
        type:'POST',
        dataType:'json',
        data:{
            'province_code':$('#province_code').val()
        },
        success:function(jdata){

            var str ='<option value=""><?php echo $this->lang->line('district');?></option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
            })
            $('#district_code').html(str);
            $(".preloader").fadeOut();
        }
    })
})


$('#idkabupaten').change(function(){
    $(".preloader").fadeIn();
    $.ajax({
        url : '<?php echo base_url()."fasyankes/keclist";?>',
        type:'POST',
        dataType:'json',
        data:{
            'idkabupaten':$('#idkabupaten').val()
        },
        success:function(jdata){

            var str ='<option value="">Kecamatan</option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.idkecamatan+'">'+item.nama_kecamatan+'</option>';
            })
            $('#idkecamatan').html(str);
            $(".preloader").fadeOut();
        }
    })
})



$('#idpropinsi_edit').change(function(){
    $(".preloader").fadeIn();
    $.ajax({
        url : '<?php echo base_url()."fasyankes/kablist";?>',
        type:'POST',
        dataType:'json',
        data:{
            'idpropinsi':$('#idpropinsi_edit').val()
        },
        success:function(jdata){

            var str ='<option value="">Kabupaten/Kota</option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.idkabupaten+'">'+item.nama_kabupaten+'</option>';
            })
            $('#idkabupaten_edit').html(str);
            $('#idkecamatan_edit').html('<option value="">Kecamatan</option>');
            $(".preloader").fadeOut();
        }
    })
})


$('#idkabupaten_edit').change(function(){
    $(".preloader").fadeIn();
    $.ajax({
        url : '<?php echo base_url()."fasyankes/keclist";?>',
        type:'POST',
        dataType:'json',
        data:{
            'idkabupaten':$('#idkabupaten_edit').val()
        },
        success:function(jdata){

            var str ='<option value="">Kecamatan</option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.idkecamatan+'">'+item.nama_kecamatan+'</option>';
            })
            $('#idkecamatan_edit').html(str);
            $(".preloader").fadeOut();
        }
    })
})


$('#src').click(function(){

  $('#dataBody').load("<?php echo base_url()."master/hf/search";?>",
  {"search":$('#search').val()}
  );
})



});












</script>