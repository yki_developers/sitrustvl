<script>
  $(".preloader").fadeIn();
  </script>
<nav aria-label="breadcrumb" style="margin-top:50px">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('shipper_company');?></li>
  </ol>
</nav>


<!---- Form Add New Fasyankes -->
<div class="modal fade" id="modalForm">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('data_form');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form method="POST" action="<?php echo base_url()."master/shipper/add";?>" id="shipper">
        <div class="form-row">
<div class="col-md-12">
                <div class="form-label-group">
                  <input type="hidden" name="idshipper" id="idshipper" value="0">

              
                <input type="text" id="shipper_code" name="shipper_code" class="form-control" placeholder="<?php echo $this->lang->line('shipper_code');?>" required="required" autofocus="autofocus">
                  <label for="shipper_code"><?php echo $this->lang->line('shipper_code');?></label>
            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <input type="text" id="shipper_name" name="shipper_name" class="form-control" placeholder="<?php echo $this->lang->line('shipper_name');?>" required="required" autofocus="autofocus">
                  <label for="shipper_name"><?php echo $this->lang->line('shipper_name');?></label>

            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <a class="btn btn-primary btn-block"   id="btnSubmit"><?php echo $this->lang->line('submit');?></a>
               
               
                </div>
              </div>
</div>

        </form>
        </div>
      </div>
    </div>
</div>


<div class="card">
<div class="card-header bg-merah" id="headercard">&nbsp;
</div>
<div class="card-body">

<table class="table table-stripe">
<thead>
<tr>
    <th scope="col" colspan="3">
    <div class="input-group input-group-sm">
    <input type="text" id="search" name="search" class="form-control" placeholder="<?php echo $this->lang->line('search');?>">
    <div class="input-group-append"><span class="btn  btn-primary" id="src"><i class="fas fa-search"></i></span></div>
</div>
</th>
    <th scope="col" class="text-right">
    &nbsp;
    <a href="#cardform" class="btn btn-success btn-xs" id="add" data-toggle="modal" data-target="#modalForm"  data-backdrop="static" data-keyboard='false'><i class="fas fa-plus-circle"></i>&nbsp;<?php echo $this->lang->line('add');?></a>
</th>

</tr>

<tr>
    <th scope="col"><?php echo $this->lang->line('number');?></th>
    <th scope="col"><?php echo $this->lang->line('shipper_code');?></th>
    <th scope="col"><?php echo $this->lang->line('shipper_name');?></th>
    <th scope="col" class="d-none d-sm-block"><?php echo $this->lang->line('action');?></th>
</tr>
</thead>
<tbody id="dataBody">
    <?php
    $i=1;
    foreach($shipper as $list){ ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->shipper_code?></td>
    <td scope="col"><?php echo $list->shipper_name;?></td>
    <td scope="col" class="text-left d-none d-sm-block">
    
    <?php if($list->shipper_status=='1'){?>
    <a href="#" class="btn btn-danger btn-sm" onclick="deactive('<?php echo $list->shipper_id; ?>')"><i class="fas fa-ban"></i>&nbsp;Non Aktifkan</a>
    &nbsp;
    
   <a href="#" id="btnedit" class="btn btn-success btn-sm"   onclick="edit('<?php echo $list->shipper_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>
    <?php }else{ ?>
      <a href="#" class="btn btn-danger btn-sm" onclick="conf('<?php echo $list->shipper_id; ?>')"><i class="fas fa-trash-alt"></i>&nbsp;<?php echo $this->lang->line('delete');?></a>
    &nbsp;
      <a href="#" class="btn btn-success btn-sm" onclick="active('<?php echo $list->shipper_id; ?>')"><i class="fas fa-check-square"></i>&nbsp;Aktifkan</a>
    <?php } ?>
    
    

    </td>
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>


</div>
<div class="card-footer bg-merah text-center">
</div>
</div>

<script>


function edit(id){
  $.ajax({
    url:"<?php echo base_url()."master/shipper/detail/";?>"+id,
    type:"POST",
    dataType:"json",
    success:function(jsdata){
      $('#idshipper').val(jsdata.response.shipper_id);
      $('#shipper_code').val(jsdata.response.shipper_code).attr("readonly","readonly");
      $('#shipper_name').val(jsdata.response.shipper_name);
      $('#modalForm').modal();
    }
  });
}
  function deactive(id){
    var conf = confirm("Apakah anda akan menonaktifkan kurir ini?");
    if(conf){
    $.ajax({
      url:"<?php echo base_url()."master/shipper/update/";?>"+id,
            type:"POST",
            dataType:"json",
            data:{
                "shipper_status":"2"
            },
            success:function(jdata){
                $(".preloader").fadeOut();
                if(jdata.status=="success"){
                    alert("Kurir berhasil di non aktifkan");
                    document.location = "<?php echo base_url()."master/shipper";?>";
                }else{
                    alert("non aktif gagal");
                    document.location ="<?php echo base_url()."master/shipper";?>";
                }
            }
    })

    }
  }


  function active(id){
    $.ajax({
      url:"<?php echo base_url()."master/shipper/update/";?>"+id,
            type:"POST",
            dataType:"json",
            data:{
                "shipper_status":"1"
            },
            success:function(jdata){
                $(".preloader").fadeOut();
                if(jdata.status=="success"){
                    alert("Kurir berhasil diaktifkan");
                    document.location = "<?php echo base_url()."master/shipper";?>";
                }else{
                    alert("aktifasi gagal");
                    document.location ="<?php echo base_url()."master/shipper";?>";
                }
            }
    })
  }

    function conf(id){
        if(confirm("<?php echo $this->lang->line("delete_confirmation");?>")){
            document.location = "<?php echo base_url()."master/shipper/delete/";?>"+id;
        }
    }
    $(document).ready(function(){
        $(".preloader").fadeOut();
$('#add').click(function(){
  $('#shipper_code').removeAttr("readonly");
})

    $('#btnSubmit').click(function(){
        $(".preloader").fadeIn();

        if($('#idshipper').val()=='0'){

        $.ajax({
            url:"<?php echo base_url()."master/shipper/add";?>",
            type:"POST",
            dataType:"json",
            data:{
                "shipper_code":$('#shipper_code').val(),
                "shipper_name":$("#shipper_name").val()
            },
            success:function(jdata){
                $(".preloader").fadeOut();
                if(jdata.status=="success"){
                    $('#modalForm').modal();
                    alert(jdata.message);
                    document.location = "<?php echo base_url()."master/shipper";?>";
                }else{
                    alert(jdata.message);
                    document.location ="<?php echo base_url()."master/shipper";?>";
                }
            }

        })



    

    }else{

      $.ajax({
            url:"<?php echo base_url()."master/shipper/update/";?>"+$('#idshipper').val(),
            type:"POST",
            dataType:"json",
            data:{
                "shipper_name":$("#shipper_name").val()
            },
            success:function(jdata){
                $(".preloader").fadeOut();
                if(jdata.status=="success"){
                    alert(jdata.message);
                    document.location = "<?php echo base_url()."master/shipper";?>";
                }else{
                    alert(jdata.message);
                    document.location ="<?php echo base_url()."master/shipper";?>";
                }
            }

        })
    }



    });
    })
    </script>