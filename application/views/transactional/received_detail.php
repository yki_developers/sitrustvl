
<style>
    label {
        color:red;
        margin-left:10px
    }
    </style>
<nav aria-label="breadcrumb" style="margin-top:50px;">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item"><a href="<?php echo base_url()."transactional/order/received";?>"><i class="fas fa-box-open"></i>&nbsp;<?php echo $this->lang->line('receive');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('specimen');?></li>
  </ol>
</nav>








<div class="card">
<div class="card-header bg-merah" id="headercard">&nbsp;
</div>
<div class="card-body">
<div class="input-group col-lg-4">
    <input type="text" id="search" name="search" class="form-control" placeholder="<?php echo $this->lang->line('search');?>">
    <div class="input-group-append"><span class="btn  btn-success" id="src"><i class="fas fa-search"></i></span></div>
    &nbsp;
   
</div>

</div>


<div class="card-body">

<table class="table table-stripe">
<thead>
    <th scope="col"><?php echo $this->lang->line('number');?></th>
    <th scope="col"><?php echo $this->lang->line('order_number');?></th>
    <th scope="col"><?php echo $this->lang->line('order_date');?></th>
    <th scope="col"><?php echo $this->lang->line('patient_name');?></th>
    <th scope="col"><?php echo $this->lang->line('specimen_number');?></th>
    <th scope="col"><?php echo $this->lang->line('specimen_test_type');?></th>
    <th scope="col"><?php echo $this->lang->line('specimen_type');?></th>


    <th scope="col" class="d-none d-sm-block"><?php echo $this->lang->line('action');?></th>
</tr>
</thead>
<tbody id="dataBody">
    <?php
    $i=1;
    foreach($specimen as $list){ 
        $m = $this->M_reverse->hfName($list->order_hf_recipient);
        $k = $this->M_reverse->shipperName($list->order_shipper_id);
        ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->order_number?></td>
    <td scope="col"><?php echo $list->order_date;?></td>
    <td scope="col"><?php echo $list->patient_name;?></td>
    <td scope="col"><?php echo $list->specimen_id;?></td>
    <td scope="col"><?php echo specimen_vl_test($list->specimen_exam_category);?></td>
    <td scope="col"><?php echo specimen_vl_type($list->specimen_type);?></td>
    <td scope="col" class="text-center d-none d-sm-block">
   
&nbsp;
<?php if($list->specimen_condition==null){?>
    <a href="#" class="btn btn-danger btn-sm" onclick="condition('<?php echo $list->specimen_num_id;?>','0')"><?php echo $this->lang->line('condition_bad');?></a>
    &nbsp;
    <a href="#" id="btnedit" class="btn btn-success btn-sm"   onclick="condition('<?php echo $list->specimen_num_id;?>','1')">&nbsp;<?php echo $this->lang->line('condition_good');?></a>
    &nbsp;
    <a href="#" id="btnedit" class="btn btn-success btn-sm"   onclick="condition('<?php echo $list->specimen_num_id;?>','2')">&nbsp;Spesimen Tidak ada</a>
<?php } ?>
    </td>
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>


</div>
<div class="card-footer text-center">
  <?php if($condition->noconfirm=='0'){ ?>
<a href="#" id="btnfinish" class="btn btn-success btn-sm" onclick="finish('<?php echo $this->uri->segment(4);?>')"><i class="far fa-check-square"></i>&nbsp;<?php echo $this->lang->line('confirmation_received');?></a>
  <?php } ?>
</div>
</div>




<div class="modal fade" id="modalFeedback">
    <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-hijau">
          <h4 class="modal-title"><i class="fas fa-edit"></i>&nbsp;Kondisi Spesimen</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">

        <form id="feedback_condition">
    <input type="hidden" id="specimen_condition">
    <input type="hidden" id="specimen_num_id">
    <input type="hidden" id="specimen_order_id">
    
    
<div class="input-group col-lg-12">
<div class="input-group-prepend">
      <span class="input-group-text"><i class="fas fa-file-signature"></i>&nbsp;Keterangan Kondisi</span>
    </div>
    <input type="text" id="specimen_condition_descript" name="specimen_condition_descript" class="form-control" required="required">
    <div class="input-group-append"><button type="submit" id="btnSubmit" class="btn btn-outline-success"><?php echo $this->lang->line("submit");?></button></span>
    &nbsp;
</div>
</div>

   
    
    </form>
        </div>

        <!--- modal footer -->
        <div class="modal-footer bg-hijau">

        </div>
      </div>
    </div>
</div>

<script>


function condition(id,val){
$('#specimen_condition').val(val);
if(val==1){
    $('#specimen_condition_descript').removeAttr("required");
}
$('#specimen_num_id').val(id);
$('#specimen_order_id').val('<?php echo $this->uri->segment('4');?>');
   $('#modalFeedback').modal(
       {
           backdrop:"static",
           keyboard:false,
           show:true
       }
       )
   // document.location = "<?php echo base_url()."transactional/received/conditional";?>"+id+"/"+oid+"/"+val;
}


function finish(id){
  if(confirm('Apakah anda yakin sudah selesai?')){
    document.location = "<?php echo base_url()."transactional/order/rec/";?>"+id;
  }
}
    $('document').ready(function(){

        $('#feedback_condition').validate({
            messages:{
                specimen_condition_descript:"<?php echo $this->lang->line("required");?>"
            },
            submitHandler:function(form){
               // alert('submit');
                //$(".preloader").fadeIn();
                $.ajax({
                    url:"<?php echo base_url()."transactional/received/conditional/";?>",
                    type:"POST",
                    dataType:"json",
                    data:{
                        "specimen_condition":$('#specimen_condition').val(),
                        "specimen_order_id":$("#specimen_order_id").val(),
                        "specimen_num_id":$('#specimen_num_id').val(),
                        "specimen_condition_descript":$('#specimen_condition_descript').val()
                    },
                    success:function(jdata){
                        if(jdata.status=="success"){
                            alert(jdata.message);
                            document.location = "<?php echo base_url()."transactional/received/list/";?>"+$("#specimen_order_id").val()

                        }else{
                            alert(jdata.message);
                        }
                    }
                });
            }
        });

      $(".close").click(function(){
          document.location = "<?php echo base_url()."transactional/received/list/".$this->uri->segment("4");?>"
      })
      $('#search').keyup(function(){
        $('#dataBody').load("<?php echo base_url()."transactional/received/search/".$this->uri->segment('4');?>",{"search":$('#search').val()});
      })



    });
</script>