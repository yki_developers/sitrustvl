<?php
    $i=1;
    foreach($specimen as $list){ 
        $m = $this->M_reverse->hfName($list->order_hf_recipient);
        $k = $this->M_reverse->shipperName($list->order_shipper_id);
        ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->order_number?></td>
    <td scope="col"><?php echo $list->order_date;?></td>
    <td scope="col"><?php echo $list->patient_name;?></td>
    <td scope="col"><?php echo $list->specimen_id;?></td>
    <td scope="col"><?php echo specimen_vl_test($list->specimen_exam_category);?></td>
    <td scope="col"><?php echo specimen_vl_type($list->specimen_type);?></td>
    <td scope="col" class="text-center d-none d-sm-block">
   
&nbsp;
<?php if($list->specimen_condition==null){?>
    <a href="#" class="btn btn-danger btn-sm" onclick="condition('<?php echo $list->specimen_num_id;?>','0')"><?php echo $this->lang->line('condition_bad');?></a>
    &nbsp;
    <a href="#" id="btnedit" class="btn btn-success btn-sm"   onclick="condition('<?php echo $list->specimen_num_id;?>','1')">&nbsp;<?php echo $this->lang->line('condition_good');?></a>
<?php } ?>
    </td>
    
</tr>

    <?php $i++;
} ?>