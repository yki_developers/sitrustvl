<?php
    $i=1;
    foreach($specimen as $list){ 
      
        $m = $this->M_reverse->hfName($list->order_hf_recipient);
        $k = $this->M_reverse->shipperName($list->order_shipper_id);
        ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->order_number?></td>
    <td scope="col"><?php echo $list->order_date;?></td>
    <td scope="col"><?php echo $list->patient_name;?></td>
    <td scope="col"><?php echo $list->specimen_id;?></td>
    <td scope="col"><?php echo specimen_vl_test($list->specimen_exam_category);?></td>
    <td scope="col"><?php echo specimen_vl_type($list->specimen_type);?></td>
    <td scope="col"><?php
    if($list->specimen_result_flag!='1' && $this->session->userdata("user_hf_group")=='5' && $list->specimen_exam_category=='2'){
        ?>


        <a href="<?php echo base_url()."transactional/result/vlresult/".$list->specimen_num_id;?>" class="btn btn-success" onclick="result('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('result');?></a>
        <?php 
      
      
      }elseif($list->specimen_result_flag!='1' && $this->session->userdata("user_hf_group")=='5' && $list->specimen_exam_category=='1'){?>
      <a href="<?php echo base_url()."transactional/result/eidresult/".$list->specimen_num_id;?>"  class="btn btn-primary" onclick="result('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('result');?></a>
      <?php 
    
    }elseif($list->specimen_result_flag=='1' &&  $this->session->userdata("user_hf_group")=='5' && $list->specimen_exam_category=='2' &&  $list->vl_result_dokumen==null){?>
        <a href="<?php echo base_url()."transactional/result/vlresultedit/".$list->specimen_num_id;?>" class="btn btn-success" onclick="result('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>
        <a href="#" class="btn btn-success" onclick="upload('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-images"></i>&nbsp;<?php echo $this->lang->line('result_document');?></a>

      <?php 
    
    }elseif($list->specimen_result_flag=='1' &&  $this->session->userdata("user_hf_group")=='5' && $list->specimen_exam_category=='1' &&  $list->eid_result_dokumen==null){?>
        <a href="<?php echo base_url()."transactional/result/eidresultedit/".$list->specimen_num_id;?>" class="btn btn-success" onclick="result('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>
        <a href="#" class="btn btn-success" onclick="eidupload('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-images"></i>&nbsp;<?php echo $this->lang->line('result_document');?></a>
      
      
        <?php }elseif($list->specimen_result_flag=='1' &&  $this->session->userdata("user_hf_group")=='5' && $list->specimen_exam_category=='1' &&  $list->eid_result_dokumen!=null){?>
          <a href="#" class="btn btn-success" onclick="eidresultview('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-eye"></i>&nbsp;<?php echo $this->lang->line('result_view');?></a>
          <a href="#" class="btn btn-success" onclick="eidupload('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('change_result_document');?></a>
      
      
      
        <?php }elseif($list->specimen_result_flag=='1' &&  $this->session->userdata("user_hf_group")=='2' &&  $list->vl_result_dokumen!=null && $list->specimen_exam_category=='2'){?>
        <a href="<?php echo base_url()."assets/resultdoc/".$list->vl_result_dokumen;?>" class="btn btn-success" target="_blank"><i class="fas fa-images"></i>&nbsp;<?php echo $this->lang->line('view_result_document');?></a>
      <?php }elseif($list->specimen_result_flag=='1' &&   $this->session->userdata("user_hf_group")=='2' &&  $list->vl_result_dokumen==null && $list->specimen_exam_category=='2'){?>
        <a href="#" class="btn btn-success" onclick="resultview('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-eye"></i>&nbsp;<?php echo $this->lang->line('result_view');?></a>
      <?php }elseif($list->specimen_result_flag=='1' &&  $this->session->userdata("user_hf_group")=='2' &&  $list->eid_result_dokumen!=null && $list->specimen_exam_category=='1'){?>
        <a href="<?php echo base_url()."assets/resultdoc/".$list->eid_result_dokumen;?>" class="btn btn-success" target="_blank"><i class="fas fa-images"></i>&nbsp;<?php echo $this->lang->line('view_result_document');?></a>
      <?php }elseif($list->specimen_result_flag=='1' &&   $this->session->userdata("user_hf_group")=='2' &&  $list->eid_result_dokumen==null && $list->specimen_exam_category=='1'){?>
        <a href="#" class="btn btn-success" onclick="resultview('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-eye"></i>&nbsp;<?php echo $this->lang->line('result_view');?></a>
      <?php }

    ?>

    &nbsp;
   
    </td>
    
</tr>

    <?php $i++;
} ?>