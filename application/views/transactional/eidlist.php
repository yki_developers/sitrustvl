<style>.datepicker{z-index:1151 !important;}

label{
  color: red;
  margin-left: 10px;
}
</style>
<nav aria-label="breadcrumb" style="margin-top:50px">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-plus-square"></i>&nbsp;<?php echo $this->lang->line('order');?></li>
  </ol>
</nav>





<!---- MODAL ORDER -->

<div class="modal fade" id="modalForm">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-merah">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('order');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form method="POST" id="orderForm">
        <input type="hidden" id="order_reorder" value="0">
        <input type="hidden" id="order_id" name="order_id">
        <input type="hidden" id="order_approved" name="order_approved">
        <input type="hidden" id="order_status" name="order_status">

        <div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text"><?php echo $this->lang->line('order_date');?></span>
    </div>
    <input type="text" value="<?php echo date("d-m-Y");?>" id="order_date" name="order_date" class="form-control tanggal" required="required" autofocus="autofocus">&nbsp;
  </div>



  <div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text"><?php echo $this->lang->line("order_destination");?></span>
    </div>
    <select name="order_hf_recipient" id="order_hf_recipient" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("order_destination");?>">
                      <option value="">&nbsp;</option>
                  <?php

foreach($lab as $lablist){
    //$qn = $this->M_reverse->hfName($lablist->labnetwork_destination)
?>
<option value="<?php echo $lablist->hf_code; ?>"><?php echo $lablist->hf_name;?></option>
<?php
}
?>

</select>

  </div>





  <div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text"><?php echo $this->lang->line("order_courier");?></span>
    </div>
    <select name="order_shipper_id" id="order_shipper_id" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("order_destination");?>">
                      <option value="">&nbsp;</option>
                  <?php

foreach($kurir as $list){
?>
<option value="<?php echo $list->shipper_code; ?>"><?php echo $list->shipper_name;?></option>
<?php
}
?>

</select>

  </div>











<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                <button type="submit" class="btn btn-outline-success" id="btnSubmit"><?php echo $this->lang->line('submit');?></button>
                  <button type="button" class="btn btn-outline-danger"  data-dismiss="modal" id="tutup"><?php echo $this->lang->line('cancel');?></button>
               
               
                </div>
              </div>
</div>

        </form>
        </div>
      </div>
    </div>
</div>

<!-- END MODAL -->



<!---- MODAL EDIT ORDER -->

<div class="modal fade" id="modalEdit">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-merah">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('order');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form method="POST" action="<?php echo base_url()."account/user/add";?>" id="shipper">
        <div class="form-row">
<div class="col-md-12">
                <div class="form-label-group">
               
                <input type="text" value="<?php echo date("d-m-Y");?>" id="order_date_update" name="order_date_update" class="form-control tanggal" placeholder="<?php echo $this->lang->line('order_date');?>" required="required" autofocus="autofocus">
                  <label for="order_date_update"><?php echo $this->lang->line('order_date');?></label>
            
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <select name="order_hf_recipient_update" id="order_hf_recipient_update" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("order_destination");?>">
                      <option value=""><?php echo $this->lang->line("order_destination");?></option>
                  <?php

foreach($lab as $lablist){
    $qn = $this->M_reverse->hfName($lablist->labnetwork_destination)
?>
<option value="<?php echo $lablist->labnetwork_destination; ?>"><?php echo $qn->hf_name;?></option>
<?php
}
?>

</select>

            
                </div>
              </div>
</div>



<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <select name="order_shipper_id_update" id="order_shipper_id_update" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("order_destination");?>">
                      <option value=""><?php echo $this->lang->line("order_courier");?></option>
                  <?php

foreach($kurir as $list){
?>
<option value="<?php echo $list->shipper_code; ?>"><?php echo $list->shipper_name;?></option>
<?php
}
?>

</select>

            
                </div>
              </div>
</div>






<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <a class="btn btn-success btn-block"   id="btnEdit"><?php echo $this->lang->line('submit');?></a>
               
               
                </div>
              </div>
</div>

        </form>
        </div>
      </div>
    </div>
</div>

<!-- END MODAL -->



<div class="card">
<div class="card-header bg-merah" id="headercard">&nbsp;
</div>
<div class="card-body">
<div class="input-group col-lg-4">
    <input type="text" id="search" name="search" class="form-control" placeholder="<?php echo $this->lang->line('search');?>">
    <div class="input-group-append"><span class="btn  btn-success" id="src"><i class="fas fa-search"></i></span></div>
    &nbsp;
    <a href="#cardform" class="btn btn-success btn-xs" id="add" data-toggle="modal" data-target="#modalForm"  data-backdrop="static" data-keyboard="false"><i class="fas fa-plus-circle"></i>&nbsp;<?php echo $this->lang->line('add');?></a>
</div>

</div>


<div class="card-body">

<table class="table table-stripe">
<thead>
<tr class="bg-merah">
    <th scope="col"><?php echo $this->lang->line('number');?></th>
    <th scope="col"><?php echo $this->lang->line('order_number');?></th>
    <th scope="col"><?php echo $this->lang->line('order_date');?></th>
    <th scope="col"><?php echo $this->lang->line('order_origin');?></th>
    <th scope="col"><?php echo $this->lang->line('order_destination');?></th>
    <th scope="col"><?php echo $this->lang->line('order_courier');?></th>
    <th scope="col"><?php echo $this->lang->line('order_status');?></th>

    <th scope="col" class="text-center"><?php echo $this->lang->line('action');?></th>
</tr>
</thead>
<tbody id="dataBody">
    <?php
    $i=1;
    foreach($orderlist as $list){ 
        $m = $this->M_reverse->hfName($list->order_hf_recipient);
        $k = $this->M_reverse->shipperName($list->order_shipper_id);
        ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->order_number?></td>
    <td scope="col"><?php echo $list->order_date;?></td>
    <td scope="col"><?php echo $list->faskes_pengirim;?></td>
    <td scope="col"><?php echo $m->hf_name;?></td>
    <td scope="col"><?php echo $k->shipper_name;?></td>
    <td scope="col"><?php
    if($list->order_status!='2'){
    echo specimen_vl_status($list->order_status);
    }else{
      echo specimen_vl_approval($list->order_approved);
      if($list->order_approved=='2'){
        echo "<br>".$this->lang->line("order_reason").": ".$list->order_reason;
      }

    }
    ?></td>
    <td scope="col" class="text-left">
    
&nbsp;
    
    &nbsp;
    <?php if($list->order_status==0){?>
      <a href="<?php echo base_url()."transactional/specimen/eid/".$list->order_id;?>" id="btnSpecimen" class="btn btn-success btn-sm"   onclick="specimen('<?php echo $list->order_id;?>')"><i class="fas fa-plus-square"></i>&nbsp;<?php echo $this->lang->line('add_specimen');?></a>
      <a href="#" id="btnedit" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modalForm"   data-backdrop="static" data-keyboard="false"  onclick="edit('<?php echo $list->order_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>
      <a href="#" class="btn btn-danger btn-sm" onclick="conf('<?php echo $list->order_id; ?>')"><i class="fas fa-trash-alt"></i>&nbsp;<?php echo $this->lang->line('delete');?></a>

<?php if($list->total_specimen>0){?>
    <a href="#" id="btnfinish" class="btn btn-success btn-sm"   onclick="finish('<?php echo $list->order_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('create_order');?></a>
<?php } ?>
    <?php }else{
      
      if($list->order_approved=="2"){
      
      if($list->order_archived=='0'){
      ?>
       <a href="#" id="btnedit" class="btn btn-success btn-sm"   onclick="reorder('<?php echo $list->order_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>
       <a href="#" class="btn btn-danger btn-sm" onclick="conf('<?php echo $list->order_id; ?>')"><i class="fas fa-trash-alt"></i>&nbsp;<?php echo $this->lang->line('delete');?></a>
       <a href="<?php echo base_url()."transactional/specimen/eid/".$list->order_id;?>" id="btndetail" class="btn btn-success btn-sm"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('detail');?></a>

             <?php
      }
      }else{
        if($list->order_status=='1'){?>
        
      <a href="#" id="btncancel" class="btn btn-danger btn-sm"  onclick="cancel('<?php echo $list->order_id;?>')"><i class="fas fa-ban"></i>&nbsp;<?php echo $this->lang->line('order_cancel');?></a>
      <a href="<?php echo base_url()."transactional/specimen/eid/".$list->order_id;?>" id="btndetail" class="btn btn-success btn-sm"  onclick=""><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('detail');?></a>


      <?php 
        }else{?>
          <a href="<?php echo base_url()."transactional/specimen/eid/".$list->order_id;?>" id="btnedit" class="btn btn-success btn-sm"  onclick=""><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('detail');?></a>
        <?php }  
    }
    } ?>
    </td>
    
</tr>



    <?php $i++;
} ?>
</tbody>
</table>


</div>
<div class="card-footer text-center bg-merah">

</div>
</div>















<script>
function dbdate(tgl){
var tg = tgl.split("-");
var dbFormat = tg[2]+"-"+tg[1]+"-"+tg[0];
return dbFormat;
}

function finish(id){
  if(confirm('Apakah anda yakin sudah selesai?')){
    document.location = "<?php echo base_url()."transactional/order/finish/";?>"+id;
  }
}
function cancel(id){
  $.ajax({
    url:"<?php echo base_url()."transactional/order/update/";?>"+id,
      type:"POST",
      dataType:"json",
      data:{
          "order_status":"0",
          "order_approved":"0"
      },
      success:function(jdata){
          if(jdata.status=='success'){
              alert(jdata.message);
             $('#dataBody').load('<?php echo base_url()."transactional/order/datalist";?>');
             // document.location = "<?php echo base_url()."transactional/specimen/eid/";?>"+id;
          }

      }
  });

}
function edit(id){
 
 $.ajax({
    url:"<?php echo base_url()."transactional/order/detail";?>",
    type:"POST",
    dataType : "json",
    data:{
      "order_id":id
    },
    success:function(jsondata){
      $('#order_date').val(dbdate(jsondata.response.order_date));
      $('#order_hf_recipient').val(jsondata.response.order_hf_recipient);
      $('#order_shipper_id').val(jsondata.response.order_shipper_id);
      $('#order_id').val(jsondata.response.order_id);
      $('#order_status').val(jsondata.response.order_status);
      $('#order_approved').val(jsondata.response.order_approved);
    }
  });
  
}



function reorder(id){
 
 $.ajax({
    url:"<?php echo base_url()."transactional/order/detail";?>",
    type:"POST",
    dataType : "json",
    data:{
      "order_id":id
    },
    success:function(jsondata){
      $('#order_reorder').val('1');
      $('#order_date').val(dbdate(jsondata.response.order_date));
      $('#order_hf_recipient').val(jsondata.response.order_hf_recipient);
      $('#order_shipper_id').val(jsondata.response.order_shipper_id);
      $('#order_status').val(jsondata.response.order_status);
      $('#order_approved').val(jsondata.response.order_approved);
      $('#order_id').val(jsondata.response.order_id);
    }
  });
  $('#modalForm').modal({
    backdrop: 'static',
    keyboard: false,
    show:true
  }

  );
}


function conf(id){
  if(confirm("<?php echo $this->lang->line('delete_confirmation');?>")){
    document.location = "<?php echo base_url()."transactional/order/delete/";?>"+id;
  }
}


$('document').ready(function(){

  $('#search').keyup(function(){
    $('#dataBody').load("<?php echo base_url()."transactional/order/search";?>",{"search":$('#search').val()});
  })
  //alert('bas')
  //$('#order_date_update').datepicker();
  $('.tanggal').attr("readonly","readonly");
  $('.tanggal').datepicker({
  format:"dd-mm-yyyy",
  startView:"year",
  minView:"year"
}).on('changeDate',function(ev){
  $(this).blur();
  $(this).datepicker('hide');
});


$('#orderForm').validate({
messages:{
  order_date:"<?php echo $this->lang->line('required');?>",
  order_hf_recipient:"<?php echo $this->lang->line('required');?>",
  order_shipper_id:"<?php echo $this->lang->line('required');?>"

},
submitHandler:function(form){

  if(!$('#order_id').val()){
  $.ajax({
            url:"<?php echo base_url()."transactional/order/add";?>",
            type:"POST",
            dataType:"json",
            data:{
                "order_hf_sender":"<?php echo $this->session->userdata('user_unit');?>",
                "order_date":dbdate($('#order_date').val()),
                "order_hf_recipient":$("#order_hf_recipient").val(),
                "order_shipper_id":$("#order_shipper_id").val(),
                "order_kategori":"1"
            },
            success:function(jdata){
                if(jdata.status=='success'){
                    alert(jdata.message);
                    document.location = "<?php echo base_url()."transactional/specimen/eid/";?>"+jdata.order_id;
                }

            }
        })
}else{


  if($('#order_reorder').val()=="0"){
  $.ajax({
      url:"<?php echo base_url()."transactional/order/update/";?>"+$('#order_id').val(),
      type:"POST",
      dataType:"json",
      data:{
          "order_date":dbdate($('#order_date').val()),
          "order_hf_recipient":$("#order_hf_recipient").val(),
          "order_shipper_id":$("#order_shipper_id").val(),
          "order_status":$('#order_status').val(),
          "order_approved":$('#order_approved').val(),
          "order_kategori":"1"
      },
      success:function(jdata){
          if(jdata.status=='success'){
              alert(jdata.message);
              document.location = "<?php echo base_url()."transactional/specimen/eid/";?>"+$('#order_id').val();
          }

      }
  });

}else{

//Start  Reorder

$.ajax({
            url:"<?php echo base_url()."transactional/order/add";?>",
            type:"POST",
            dataType:"json",
            data:{
              "order_hf_sender":"<?php echo $this->session->userdata('user_unit');?>",
                "order_date":dbdate($('#order_date_update').val()),
                "order_hf_recipient":$("#order_hf_recipient_update").val(),
                "order_shipper_id":$("#order_shipper_id_update").val(),
                "order_status":"1",
                "order_approved":"0",
                "order_kategori":"1"
            },
            success:function(jdata){
                if(jdata.status=='success'){

                  $.ajax({
                    url:"<?php echo base_url()."transactional/specimen/reorderspecimen/";?>"+$('#order_id').val(),
                    type:"POST",
                    dataType:"json",
                    data:{
                      "specimen_order_id":jdata.order_id
                    },
                    success:function(jdata){
                      //alert(jdata.message);

                      $.ajax({
                        url:"<?php echo base_url()."transactional/order/update/";?>"+$('#order_id').val(),
                        type: "POST",
                        dataType:"json",
                        data:{
                          "order_archived":"1"
                        },
                        success:function(jsdata){
                          if(jsdata.status=='success'){
                            alert(jdata.message);
                            document.location ="<?php echo base_url()."transactional/order/eid";?>";

                          }
                        }
                      });


                    }
                  });  
                  
                  
                  
                }

            }
          });

          //end reorder
}

}

}




});






    $(".close, #tutup").click(function(){
  document.location ="<?php echo base_url()."transactional/order/eid";?>";
})




}); //end of document



    
</script>