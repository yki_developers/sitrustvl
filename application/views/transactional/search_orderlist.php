<?php
    $i=1;
    foreach($orderlist as $list){ 
        $m = $this->M_reverse->hfName($list->order_hf_recipient);
        $k = $this->M_reverse->shipperName($list->order_shipper_id);
        ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->order_number?></td>
    <td scope="col"><?php echo $list->order_date;?></td>
    <td scope="col"><?php echo $list->faskes_pengirim;?></td>
    <td scope="col"><?php echo $m->hf_name;?></td>
    <td scope="col"><?php echo $k->shipper_name;?></td>
    <td scope="col"><?php
    if($list->order_status!='2'){
    echo specimen_vl_status($list->order_status);
    }else{
      echo specimen_vl_approval($list->order_approved);
      if($list->order_approved=='2'){
        echo "<br>".$this->lang->line("order_reason").": ".$list->order_reason;
      }

    }
    ?></td>
    <td scope="col" class="text-center">
    
&nbsp;
    
    &nbsp;
    <?php if($list->order_status==0){?>
      <a href="<?php echo base_url()."transactional/specimen/list/".$list->order_id;?>" id="btnedit" class="btn btn-success btn-sm"   onclick="specimen('<?php echo $list->order_id;?>')"><i class="fas fa-plus-square"></i>&nbsp;<?php echo $this->lang->line('add_specimen');?></a>
      <a href="#" id="btnedit" class="btn btn-success btn-sm" data-toggle="modal" data-target="#modalForm" data-backdrop="static" data-keyboard="false" onclick="edit('<?php echo $list->order_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>
      <a href="#" class="btn btn-danger btn-sm" onclick="conf('<?php echo $list->order_id; ?>')"><i class="fas fa-trash-alt"></i>&nbsp;<?php echo $this->lang->line('delete');?></a>

<?php if($list->total_specimen>0){?>
    <a href="#" id="btnfinish" class="btn btn-success btn-sm"   onclick="finish('<?php echo $list->order_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('create_order');?></a>
<?php } ?>
    <?php }else{
      
      if($list->order_approved=="2"){
      
      if($list->order_archived=='0'){
      ?>
       <a href="#" id="btnedit" class="btn btn-success btn-sm"   onclick="reorder('<?php echo $list->order_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>
       <a href="#" class="btn btn-danger btn-sm" onclick="conf('<?php echo $list->order_id; ?>')"><i class="fas fa-trash-alt"></i>&nbsp;<?php echo $this->lang->line('delete');?></a>
       <a href="<?php echo base_url()."transactional/specimen/list/".$list->order_id;?>" id="btnedit" class="btn btn-success btn-sm"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('detail');?></a>

             <?php
      }
      }else{
        if($list->order_status=='1' || $list->order_status=='2'){?>
        
      <a href="#" id="btncancel" class="btn btn-danger btn-sm"  onclick="cancel('<?php echo $list->order_id;?>')"><i class="fas fa-ban"></i>&nbsp;<?php echo $this->lang->line('order_cancel');?></a>
      <a href="<?php echo base_url()."transactional/specimen/list/".$list->order_id;?>" id="btnedit" class="btn btn-success btn-sm"  onclick=""><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('detail');?></a>


      <?php 
        }else{?>
          <a href="<?php echo base_url()."transactional/specimen/list/".$list->order_id;?>" id="btnedit" class="btn btn-success btn-sm"  onclick=""><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('detail');?></a>
        <?php }  
    }
    } ?>
    </td>
    
</tr>



    <?php $i++;
} ?>