<style>.datepicker{z-index:1151 !important;}

label{
  color: red;
  margin-left: 10px;
}
</style>
<nav aria-label="breadcrumb"  style="margin-top: 50px;">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('result');?></li>
  </ol>
</nav>

<div class="card  mx-auto" style="width:600px">
<div class="card-header bg-hijau" id="headercard">&nbsp;<h5><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('result');?></h5>
</div>
<div class="card-body">
<form id="result_eid" method="POST">



<div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text"><?php echo $this->lang->line('mother_regnas');?></span>
    </div>
    <input type="hidden" value="<?php echo $specimen->patient_id;?>"  id="patient_id" name="patient_id" class="form-control" >
                <input type="hidden" value="<?php echo $specimen->specimen_num_id;?>"  id="specimen_num_id" name="specimen_num_id" class="form-control" >
                <input type="text" value="<?php echo $specimen->patient_eid_mother_regnas;?>"  id="patient_eid_mother_regnas" name="patient_eid_mother_regnas" class="form-control" autofocus="autofocus" readonly="readonly">
  </div>

  <div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text"><?php echo $this->lang->line('mother_nid');?></span>
    </div>
                <input type="text" value="<?php echo $specimen->patient_eid_mother_nid;?>"  id="patient_eid_mother_nid" name="patient_eid_mother_nid" class="form-control" autofocus="autofocus" readonly="readonly">
  </div>




  <div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text"><?php echo $this->lang->line('specimen_id');?></span>
    </div>
                <input type="text" value="<?php echo $specimen->specimen_id;?>"  id="specimen_id" name="specimen_id" class="form-control"  required="required" autofocus="autofocus" readonly="readonly">

</div>


<div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text"><?php echo $this->lang->line('specimen_eid_test_date');?></span>
    </div>     
                <input type="text"  id="specimen_exam_date" name="specimen_exam_date" value="<?php echo localdate($specimen->eid_result_exam_date);?>" class="form-control tanggal"  required="required" autofocus="autofocus">

                  
</div>

<div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text"><?php echo $this->lang->line('specimen_date_release');?></span>
    </div> 
                <input type="text"  id="specimen_date_release"  value="<?php echo localdate($specimen->eid_result_date_release);?>"  name="specimen_date_release" class="form-control tanggal"  required="required" autofocus="autofocus">

</div>


<div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text"><?php echo $this->lang->line('result');?></span>
    </div> 

            <select name="specimen_eid_result" id="specimen_eid_result" class="form-control" required="required">
            <option value=""></option>
            <?php
            for($i=1;$i<=5;$i++)
            {?>
            <option value="<?php echo $i;?>"  <?php if($specimen->eid_result_eidresult==$i){?> selected <?php } ?>><?php echo result_eid($i);?></option>
            <?php } ?>
            </select>
                
</div>



<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
<div class="form-label-group">
                
<button type="submit" class="btn btn-success btn-block" id="btnSubmit"><?php echo $this->lang->line('submit');?></button>
               
               
                </div>
              </div>
</div>


</form>


</div>
<div class="card-footer">
</div>
</div>







<!-- Update Foto -->
<div class="modal fade" id="modalFoto">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-merah">
          <h4 class="modal-title"><i class="fas fa-images"></i>&nbsp;<?php echo $this->lang->line('result_document');?></h4>
          <button type="button" class="close" id="closeBtn">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
        <form id="formFoto">
        <input type="hidden" id="specimen_num_id" name="specimen_num_id" value="<?php echo $specimen->specimen_num_id;?>">
        <div class="form-row">
<div class="col-md-12 text-center" style="padding-top:10px">
                <img src="" id="preview" style="width:250px">
              </div>
</div>

        <div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <input type="file" id="foto" name="foto" class="custom-file-input" placeholder="<?php echo $this->lang->line('result_document');?>"  autofocus="autofocus">
                <label for="foto" class="custom-file-label"><?php echo $this->lang->line('result_document');?></label>
                </div>
              </div>
</div>



<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <a class="btn btn-primary btn-block"   id="btnUpload">Upload</a>
               
               
                </div>
              </div>
</div>

        </form>

        </div>
         <!-- Modal footer -->
         <div class="modal-footer bg-merah">&nbsp;
        </div>
        
      </div>
    </div>
  </div>


<script>

    $('document').ready(function(){
      $('.tanggal').datepicker({
  format:"dd-mm-yyyy",
  startView:"year",
  minView:"year"
}).on('changeDate',function(ev){
  $(this).blur();
  $(this).datepicker('hide');
});


$('#result_eid').validate({
  messages:{
    specimen_exam_date:"<?php echo $this->lang->line("required");?>",
    specimen_date_release:"<?php echo $this->lang->line("required");?>",
    specimen_eid_result:"<?php echo $this->lang->line("required");?>"
  },
  submitHandler:function(form){
    $.ajax({
                url:"<?php echo base_url()."transactional/result/updateresulteid/";?>"+$('#specimen_num_id').val(),
                type:"POST",
                dataType:"json",
                data:{
                    "eid_result_patient_id":$('#patient_id').val(),
                    "eid_result_specimen_id":$('#specimen_id').val(),
                    "eid_result_exam_date":formatDate($('#specimen_exam_date').val()),
                    "eid_result_date_release":formatDate($('#specimen_date_release').val()),
                    "eid_result_eidresult":$('#specimen_eid_result').val(),
                    "eid_result_specimen_num_id":$('#specimen_num_id').val()
                    
                },
                success:function(jdata){
                    if(jdata.status=='success'){
                       
                        document.location="<?php echo base_url()."transactional/result/detail/".$specimen->specimen_order_id;?>";
 
                    }
                }
            })
  }
})

      




$('#btnUpload').click(function(){
  $(".preloader").fadeIn();
  $('#modalFoto').hide();
  var dataForm = new FormData($('#formFoto')[0]);
  $.ajax({
  url:"<?php echo base_url()."transactional/result/eidupdate";?>",
  type:'POST',
  dataType:'json',
  cache: false,
  contentType: false,
  processData: false,
  data: dataForm,
  success:function(jdata){
      $(".preloader").fadeOut();
      if (jdata.status=='200') {
        
          alert(jdata.message);
          document.location="<?php echo base_url()."transactional/result/detail/".$specimen->specimen_order_id;?>";
          
      }else{
        $(".preloader").fadeOut();
          alert(jdata.error);

      }
  }
  
       });
});


    });
    </script>