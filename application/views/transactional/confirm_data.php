<?php
    $this->load->model('transactional/M_order');
    $i=1;
    foreach($orderlist as $list){ 
        $m = $this->M_reverse->hfName($list->order_hf_recipient);
        $k = $this->M_reverse->shipperName($list->order_shipper_id);
        ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->order_number?></td>
    <td scope="col"><?php echo $list->order_date;?></td>
    <td scope="col"><?php echo $list->faskes_pengirim;?></td>
    <td scope="col"><?php echo $m->hf_name;?></td>
    <td scope="col"><?php echo $k->shipper_name;?></td>
    <td scope="col"><?php echo $this->M_order->countSpecimen($list->order_id);?></td>


    <td scope="col"><?php echo specimen_vl_status($list->order_status);?></td>
    <td scope="col" class="text-center d-none d-sm-block">
    
    <a href="#" class="btn btn-danger btn-sm" id="add" data-toggle="modal" data-target="#modalForm_<?php echo $list->order_id;?>">&nbsp;<?php echo $this->lang->line('rejected');?></a>
    &nbsp;
    <a href="#" id="btnedit" class="btn btn-success btn-sm"   onclick="approved('<?php echo $list->order_id;?>')">&nbsp;<?php echo $this->lang->line('approved');?></a>&nbsp;
   

    </td>
    
</tr>


<!---- MODAL -->

<div class="modal fade" id="modalForm_<?php echo $list->order_id;?>">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('order_rejected');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form method="POST" action="<?php echo base_url()."account/user/add";?>" id="shipper">
        <div class="form-row">
<div class="col-md-12">
                <div class="form-label-group">
              <input type="hidden" id="order_id" name="order_id" value="<?php echo $list->order_id;?>">
                <input type="text"  id="order_reason" name="order_reason" class="form-control" placeholder="<?php echo $this->lang->line('order_reason');?>" required="required" autofocus="autofocus">
                  <label for="order_reason"><?php echo $this->lang->line('order_reason');?></label>
            
                </div>
              </div>
</div>







<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <a class="btn btn-primary btn-block"   id="btnSubmit" onclick="tolak('<?php echo $list->order_id;?>')"><?php echo $this->lang->line('submit');?></a>
               
               
                </div>
              </div>
</div>

        </form>
        </div>
      </div>
    </div>
</div>

<!-- END MODAL -->

    <?php $i++;
} ?>