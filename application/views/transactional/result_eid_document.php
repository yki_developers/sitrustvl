<div class="modal fade" id="modalForm">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-merah">
          <h4 class="modal-title"><i class="fas fa-user"></i>&nbsp;User Login</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
       

        <div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text">Username</span>
    </div>
    <input type="text" value="" id="username" name="username" class="form-control" required="required" autofocus="autofocus">&nbsp;
  </div>


  <div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text">Password</span>
    </div>
    <input type="password" id="user_passwd" name="user_passwd" class="form-control">
  </div>



<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                  <button type="submit" class="btn btn-outline-success" id="btnSubmit">Lanjutkan</button>
                  <button type="button" class="btn btn-outline-danger" id="tutup">Batalkan</button>
              <!--  <a class="btn btn-success btn-block"   id="btnSubmit"><?php echo $this->lang->line('submit');?></a> -->
               
               
                </div>
              </div>
</div>

        </form>
        </div>
      </div>
    </div>
</div>





<div class="card" style="margin-top: 50px;">
<div class="card-header bg-merah">Dokumen Hasil Pemeriksaan</div>
<div class="card-body" id="dataBody" style="height: 600px;">&nbsp;</div>
<div class="card-footer">...</div>
</div>
<script>
$('document').ready(function(){
    $('#modalForm').modal({
     backdrop: 'static',
    keyboard: false,
    show:true
  });

  $('#btnSubmit').click(function(){
      $.ajax({
          url:"<?php echo base_url()."transactional/result/validation/".$this->uri->segment('4');?>",
          dataType:"json",
          type:"post",
          data:{
              "user_name":$('#username').val(),
              "user_passwd":$('#user_passwd').val()
          },
          success:function(jdata){

           if(jdata.status=='success'){
               if(jdata.response.user_hf_group=='5' || jdata.response.user_hf_group=='2'){
               $('#dataBody').load("<?php echo base_url()."transactional/result/eiddocview/".$this->uri->segment('4');?>");
               $('#modalForm').modal('hide');
               }else{
                   alert('Anda Tidak diijinkan mengakses dokumen ini')
               }
           }else{
               alert('Login gagal');
           }

          }
      })
  })
})
</script>