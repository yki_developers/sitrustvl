
<nav aria-label="breadcrumb" style="margin-top: 50px;">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('delivered');?></li>
  </ol>
</nav>








<div class="card">
<div class="card-header bg-merah" id="headercard">&nbsp;
</div>
<div class="card-body col-md-4">
<div class="input-group">
    <input type="text" id="search" name="search" class="form-control" placeholder="<?php echo $this->lang->line("order_number");?>">
    <div class="input-group-append"><span class="btn  btn-success" id="src"><i class="fas fa-search"></i></span></div>
</div>
</div>
<div class="card-body">

<table class="table table-stripe">
<thead>
<tr class="bg-merah">
    <th scope="col"><?php echo $this->lang->line('number');?></th>
    <th scope="col"><?php echo $this->lang->line('order_number');?></th>
    <th scope="col"><?php echo $this->lang->line('order_date');?></th>
    <th scope="col"><?php echo $this->lang->line('order_origin');?></th>
    <th scope="col"><?php echo $this->lang->line('order_destination');?></th>
    <th scope="col"><?php echo $this->lang->line('order_courier');?></th>
    <th scope="col"><?php echo $this->lang->line('order_num_specimen');?></th>
    <th scope="col"><?php echo $this->lang->line('order_status');?></th>

    <th scope="col" ><?php echo $this->lang->line('action');?></th>
</tr>
</thead>
<tbody id="dataBody">
    <?php
    $this->load->model('transactional/M_order');
    $i=1;
    foreach($orderlist as $list){ 
        $m = $this->M_reverse->hfName($list->order_hf_recipient);
        $k = $this->M_reverse->shipperName($list->order_shipper_id);
        ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->order_number?></td>
    <td scope="col"><?php echo $list->order_date;?></td>
    <td scope="col"><?php echo $list->faskes_pengirim;?></td>
    <td scope="col"><?php echo $m->hf_name;?></td>
    <td scope="col"><?php echo $k->shipper_name;?></td>
    <td scope="col"><?php echo $list->total_specimen;?></td>


    <td scope="col"><?php echo specimen_vl_status($list->order_status);?></td>
    <td scope="col">
    
    &nbsp;
    <a href="#" id="btnedit" class="btn btn-success btn-sm"   onclick="delivered('<?php echo $list->order_id;?>')">&nbsp;<?php echo $this->lang->line('delivered');?></a>&nbsp;
   

    </td>
    
</tr>
    <?php $i++;
} ?>
</tbody>
</table>


</div>
<div class="card-footer text-center">

</div>
</div>




<!---- MODAL -->

<div class="modal fade" id="modalForm">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-merah">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('delivered');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form method="POST" id="shipper">
        <div class="form-row">
<div class="col-md-12">
                <div class="form-label-group">
              <input type="hidden" id="order_id" name="order_id">
                <input type="text"  id="order_shipper_info" name="order_pickup_info" class="form-control" placeholder="Keterangan" required>
                  <label for="order_shipper_info">Keterangan</label>
            
                </div>
              </div>
</div>







<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <a class="btn btn-success btn-block"   id="btnSubmit"><?php echo $this->lang->line('submit');?></a>
               
               
                </div>
              </div>
</div>

        </form>
        </div>
      </div>
    </div>
</div>

<!-- END MODAL -->



<script>


function delivered(id){
  $('#order_id').val(id);
$('#modalForm').modal('show');
//document.location = "<?php echo base_url()."transactional/order/delivered/";?>"+id;

}

$('document').ready(function(){
  
$(".preloader").fadeOut();
  $('#src').click(function(){
  $('#dataBody').load("<?php echo base_url()."transactional/pickup/search";?>",{"search":$('#search').val(),"order_status":"3"});
})



  $('#btnSubmit').click(function(){
    $(".preloader").fadeIn();
    $('#modalForm').modal('hide');
    $.ajax({
      url:"<?php echo base_url()."transactional/order/delivered/";?>"+$('#order_id').val(),
      type:"POST",
      dataType:"json",
      data:{
        "order_shipper_info":$('#order_shipper_info').val()
      },
      success:function(jdata){
        if(jdata.status=='success'){
         alert(jdata.message);
         $(".preloader").fadeOut();
          $('#dataBody').load('<?php echo base_url()."transactional/pickup/delivered";?> #dataBody');
          
          
        }
      }

    })

  });


})
   
</script>