
<nav aria-label="breadcrumb" style="margin-top: 50px;">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('pickup');?></li>
  </ol>
</nav>








<div class="card">
<div class="card-header bg-merah" id="headercard">&nbsp;
</div>
<div class="card-body">
<div class="input-group col-md-4">
    <input type="text" id="search" name="search" class="form-control" placeholder="<?php echo $this->lang->line('order_number');?>">
    <div class="input-group-append"><span class="btn  btn-primary" id="src"><i class="fas fa-search"></i></span></div>
</div>

</div>
<div class="card-body">

<table class="table table-stripe">

<tr class="bg-merah">
    <th scope="col"><?php echo $this->lang->line('number');?></th>
    <th scope="col"><?php echo $this->lang->line('order_number');?></th>
    <th scope="col"><?php echo $this->lang->line('order_date');?></th>
    <th scope="col"><?php echo $this->lang->line('order_origin');?></th>
    <th scope="col"><?php echo $this->lang->line('order_destination');?></th>
    <th scope="col"><?php echo $this->lang->line('order_courier');?></th>
    <th scope="col"><?php echo $this->lang->line('order_num_specimen');?></th>
    <th scope="col"><?php echo $this->lang->line('order_status');?></th>

    <th scope="col" class="d-none d-sm-block"><?php echo $this->lang->line('action');?></th>
</tr>
</thead>
<tbody id="dataBody">
    <?php
    $this->load->model('transactional/M_order');
    $i=1;
    foreach($orderlist as $list){ 
        $m = $this->M_reverse->hfName($list->order_hf_recipient);
        $k = $this->M_reverse->shipperName($list->order_shipper_id);
        ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->order_number?></td>
    <td scope="col"><?php echo $list->order_date;?></td>
    <td scope="col"><?php echo $list->faskes_pengirim;?></td>
    <td scope="col"><?php echo $m->hf_name;?></td>
    <td scope="col"><?php echo $k->shipper_name;?></td>
    <td scope="col">&nbsp;</td>


    <td scope="col"><?php echo specimen_vl_status($list->order_status);?></td>
    <td scope="col" class="text-center d-none d-sm-block">
    
    &nbsp;
    <a href="#" id="btnedit" class="btn btn-success btn-sm"   onclick="pickup('<?php echo $list->order_id;?>')">&nbsp;<?php echo $this->lang->line('pickup');?></a>&nbsp;
   

    </td>
    
</tr>




    <?php $i++;
} ?>
</tbody>
</table>


</div>
<div class="card-footer text-center">

</div>
</div>



<!---- MODAL -->

<div class="modal fade" id="modalForm">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-merah">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('pickup');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form method="POST" id="shipper">
        <div class="form-row">
<div class="col-md-12">
                <div class="form-label-group">
              <input type="hidden" id="order_id" name="order_id">
                <input type="text"  id="order_pickup_info" name="order_pickup_info" class="form-control" placeholder="Keterangan" required>
                  <label for="order_pickup_info">Keterangan</label>
            
                </div>
              </div>
</div>







<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <a class="btn btn-success btn-block"   id="btnSubmit"><?php echo $this->lang->line('submit');?></a>
               
               
                </div>
              </div>
</div>

        </form>
        </div>
      </div>
    </div>
</div>

<!-- END MODAL -->


<script>


function pickup(id){
  $('#order_id').val(id);
$('#modalForm').modal('show');
//document.location = "<?php echo base_url()."transactional/order/pickup/";?>"+id;
}


$('document').ready(function(){

$('#src').click(function(){
  $('#dataBody').load("<?php echo base_url()."transactional/pickup/search";?>",{"search":$('#search').val(),"order_status":"2"});
})


  $(".preloader").fadeOut();
  $('#btnSubmit').click(function(){
    $(".preloader").fadeIn();
    $('#modalForm').modal('hide');
    $.ajax({
      url:"<?php echo base_url()."transactional/order/pickup/";?>"+$('#order_id').val(),
      type:"POST",
      dataType:"json",
      data:{
        "order_pickup_info":$('#order_pickup_info').val()
      },
      success:function(jdata){
        if(jdata.status=='success'){
         alert('<?php echo $this->lang->line("data_save");?>');
          $('#dataBody').load('<?php echo base_url()."transactional/pickup/list";?> #dataBody');
          
          $(".preloader").fadeOut();
        }
      }

    })

  });
})

   
</script>