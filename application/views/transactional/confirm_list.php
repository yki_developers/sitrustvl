
<nav aria-label="breadcrumb" style="margin-top: 50px;">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('order_confirmation');?></li>
  </ol>
</nav>








<div class="card">
<div class="card-header bg-merah" id="headercard">&nbsp;
</div>
<div class="card-body">
<div class="input-group col-lg-4">
    <input type="text" id="search" name="search" class="form-control" placeholder="Pencarian">
    <div class="input-group-append"><span class="btn  btn-success" id="src"><i class="fas fa-search"></i></span></div>&nbsp;
   
</div>
</div>
<div class="card-body">
<div class="tableFixHead">
<table class="table">
<thead>


<tr class="bg-merah">
    <th scope="col"><?php echo $this->lang->line('number');?></th>
    <th scope="col"><?php echo $this->lang->line('order_number');?></th>
    <th scope="col"><?php echo $this->lang->line('order_date');?></th>
    <th scope="col"><?php echo $this->lang->line('order_origin');?></th>
    <th scope="col"><?php echo $this->lang->line('order_destination');?></th>
    <th scope="col"><?php echo $this->lang->line('order_courier');?></th>
    <th scope="col"><?php echo $this->lang->line('order_num_specimen');?></th>
    <th scope="col"><?php echo $this->lang->line('order_status');?></th>

    <th scope="col" class="text-center"><?php echo $this->lang->line('action');?></th>
</tr>
</thead>

<tbody id="dataBody">
    <?php
    $this->load->model('transactional/M_order');
    $i=1;
    foreach($orderlist as $list){ 
        $m = $this->M_reverse->hfName($list->order_hf_recipient);
        $k = $this->M_reverse->shipperName($list->order_shipper_id);
        ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->order_number?></td>
    <td scope="col"><?php echo $list->order_date;?></td>
    <td scope="col"><?php echo $list->faskes_pengirim;?></td>
    <td scope="col"><?php echo $m->hf_name;?></td>
    <td scope="col"><?php echo $k->shipper_name;?></td>
    <td scope="col"><?php echo $this->M_order->countSpecimen($list->order_id);?></td>


    <td scope="col"><?php echo specimen_vl_status($list->order_status);?></td>
    <td scope="col">
    
    <a href="#" class="btn btn-danger btn-sm" id="add" data-toggle="modal" data-target="#modalForm_<?php echo $list->order_id;?>">&nbsp;<?php echo $this->lang->line('rejected');?></a>
    &nbsp;
    <a href="#" id="btnedit" class="btn btn-success btn-sm"   onclick="approved('<?php echo $list->order_id;?>')">&nbsp;<?php echo $this->lang->line('approved');?></a>&nbsp;
   

    </td>
    
</tr>


<!---- MODAL -->

<div class="modal fade" id="modalForm_<?php echo $list->order_id;?>">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('order_rejected');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form id="confirm">
        <div class="form-row">
<div class="col-md-12">
                <div class="form-label-group">
              <input type="hidden" id="order_id" name="order_id" value="<?php echo $list->order_id;?>">
                <input type="text"  id="order_reason" name="order_reason" class="form-control" placeholder="<?php echo $this->lang->line('order_reason');?>" required="required" autofocus="autofocus">
                  <label for="order_reason"><?php echo $this->lang->line('order_reason');?></label>
            
                </div>
              </div>
</div>







<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <a class="btn btn-primary btn-block"   id="btnSubmit" onclick="tolak('<?php echo $list->order_id;?>')"><?php echo $this->lang->line('submit');?></a>
               
               
                </div>
              </div>
</div>

        </form>
        </div>
      </div>
    </div>
</div>

<!-- END MODAL -->

    <?php $i++;
} ?>
</tbody>

</table>

</div>

</div>
<div class="card-footer text-center">

</div>
</div>

<script>
function finish(id){
  if(confirm('Apakah anda yakin sudah selesai?')){
    document.location = "<?php echo base_url()."transactional/order/finish/";?>"+id;
  }
}

function rejected(id){

    if(confirm('Apakah anda yakin Menolak?')){
    document.location = "<?php echo base_url()."transactional/order/rejected/";?>"+id;
  }

}

function approved(id){

if(confirm('Apakah anda yakin Menerima?')){
document.location = "<?php echo base_url()."transactional/order/approved/";?>"+id;
}

}

function tolak(id){
  $('.modal').modal('hide');
  $(".preloader").fadeIn();
  $.ajax({
            url:"<?php echo base_url()."transactional/order/update/";?>"+id,
            type:"POST",
            dataType:"json",
            data:{
                "order_reason":$('#order_reason').val(),
                "order_approved":"2",
                "order_status":"2",
                "order_read":"0"
                
            },
            success:function(jdata){
                if(jdata.status=='success'){
                  
                  $('#dataBody').load('<?php echo base_url()."transactional/order/confirmationdata";?>',function(){
                   
                    $(".preloader").fadeOut();
                    alert(jdata.message);
                    

                  })
                    
                   // document.location = "<?php echo base_url()."transactional/order/confirmation";?>";
                }

            }
        })
}


    $('document').ready(function(){

$('#search').keyup(function(){
  $('#dataBody').load('<?php echo base_url()."transactional/order/confirmationsearch";?>',{"search":$('#search').val()});
});

        
   


    });
</script>