
<nav aria-label="breadcrumb" style="margin-top: 50px;">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-box-open"></i>&nbsp;<?php echo $this->lang->line('receive');?> EID</li>
  </ol>
</nav>





<!---- MODAL ORDER -->

<div class="modal fade" id="modalForm">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('order');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form method="POST" action="<?php echo base_url()."account/user/add";?>" id="shipper">
        <div class="form-row">
<div class="col-md-12">
                <div class="form-label-group">
              
                <input type="text" value="<?php echo date("Y-m-d H:i:s");?>" id="order_date" name="order_date" class="form-control" placeholder="<?php echo $this->lang->line('order_date');?>" required="required" autofocus="autofocus">
                  <label for="order_date"><?php echo $this->lang->line('order_date');?></label>
            
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <select name="order_hf_recipient" id="order_hf_recipient" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("order_destination");?>">
                      <option value=""><?php echo $this->lang->line("order_destination");?></option>
                  <?php

foreach($lab as $lablist){
    $qn = $this->M_reverse->hfName($lablist->labnetwork_destination)
?>
<option value="<?php echo $lablist->labnetwork_destination; ?>"><?php echo $qn->hf_name;?></option>
<?php
}
?>

</select>

            
                </div>
              </div>
</div>



<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <select name="order_shipper_id" id="order_shipper_id" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("order_destination");?>">
                      <option value=""><?php echo $this->lang->line("order_courier");?></option>
                  <?php

foreach($kurir as $list){
?>
<option value="<?php echo $list->shipper_code; ?>"><?php echo $list->shipper_name;?></option>
<?php
}
?>

</select>

            
                </div>
              </div>
</div>






<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <a class="btn btn-primary btn-block"   id="btnSubmit"><?php echo $this->lang->line('submit');?></a>
               
               
                </div>
              </div>
</div>

        </form>
        </div>
      </div>
    </div>
</div>

<!-- END MODAL -->



<!---- MODAL EDIT ORDER -->

<div class="modal fade" id="modalEdit">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('order');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form method="POST" action="<?php echo base_url()."account/user/add";?>" id="shipper">
        <div class="form-row">
<div class="col-md-12">
                <div class="form-label-group">
              
                <input type="text" value="<?php echo date("Y-m-d H:i:s");?>" id="order_date" name="order_date" class="form-control" placeholder="<?php echo $this->lang->line('order_date');?>" required="required" autofocus="autofocus">
                  <label for="order_date"><?php echo $this->lang->line('order_date');?></label>
            
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <select name="order_hf_recipient" id="order_hf_recipient" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("order_destination");?>">
                      <option value=""><?php echo $this->lang->line("order_destination");?></option>
                  <?php

foreach($lab as $lablist){
    $qn = $this->M_reverse->hfName($lablist->labnetwork_destination)
?>
<option value="<?php echo $lablist->labnetwork_destination; ?>"><?php echo $qn->hf_name;?></option>
<?php
}
?>

</select>

            
                </div>
              </div>
</div>



<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <select name="order_shipper_id" id="order_shipper_id" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("order_destination");?>">
                      <option value=""><?php echo $this->lang->line("order_courier");?></option>
                  <?php

foreach($kurir as $list){
?>
<option value="<?php echo $list->shipper_id; ?>"><?php echo $list->shipper_name;?></option>
<?php
}
?>

</select>

            
                </div>
              </div>
</div>






<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <a class="btn btn-primary btn-block"   id="btnSubmit"><?php echo $this->lang->line('submit');?></a>
               
               
                </div>
              </div>
</div>

        </form>
        </div>
      </div>
    </div>
</div>

<!-- END MODAL -->



<div class="card">
<div class="card-header bg-merah" id="headercard">&nbsp;
</div>
<div class="card-body">
<div class="input-group col-lg-4">
    <input type="text" id="search" name="search" class="form-control" placeholder="<?php echo $this->lang->line('search');?>">
    <div class="input-group-append"><span class="btn  btn-success" id="src"><i class="fas fa-search"></i></span></div>
    &nbsp;
   
</div>

</div>


<div class="card-body">

<table class="table table-stripe">
<thead>
<tr class="bg-merah">
    <th scope="col"><?php echo $this->lang->line('number');?></th>
    <th scope="col"><?php echo $this->lang->line('order_number');?></th>
    <th scope="col"><?php echo $this->lang->line('order_date');?></th>
    <th scope="col"><?php echo $this->lang->line('order_origin');?></th>
    <th scope="col"><?php echo $this->lang->line('order_destination');?></th>
    <th scope="col"><?php echo $this->lang->line('order_courier');?></th>
    <th scope="col"><?php echo $this->lang->line('total_specimen');?></th>
    <th scope="col"><?php echo $this->lang->line('order_status');?></th>

    <th scope="col"><?php echo $this->lang->line('action');?></th>
</tr>
</thead>
<tbody id="dataBody">
    <?php
    $i=1;
    foreach($orderlist as $list){ 
        $m = $this->M_reverse->hfName($list->order_hf_recipient);
        $k = $this->M_reverse->shipperName($list->order_shipper_id);
        ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->order_number?></td>
    <td scope="col"><?php echo $list->order_date;?></td>
    <td scope="col"><?php echo $list->faskes_pengirim;?></td>
    <td scope="col"><?php echo $m->hf_name;?></td>
    <td scope="col"><?php echo $k->shipper_name;?></td>
    <td scope="col"><?php echo $list->total_specimen;?></td>
    <td scope="col"><?php
    if($list->order_status!='2'){
    echo specimen_vl_status($list->order_status);
    }else{
      echo specimen_vl_approval($list->order_approved);
      if($list->order_approved=='2'){
        echo "<br>".$this->lang->line("order_reason").": ".$list->order_reason;
      }

    }
    ?></td>
    <td scope="col" class="text-center">
    
&nbsp;
    
    &nbsp;
    <?php if($list->order_status==0){?>
      <a href="<?php echo base_url()."transactional/specimen/list/".$list->order_id;?>" id="btnedit" class="btn btn-success btn-sm"   onclick="specimen('<?php echo $list->order_id;?>')"><i class="fas fa-plus-square"></i>&nbsp;<?php echo $this->lang->line('add_specimen');?></a>
      <a href="#" class="btn btn-danger btn-sm" onclick="conf('<?php echo $list->order_id; ?>')"><i class="fas fa-trash-alt"></i>&nbsp;<?php echo $this->lang->line('delete');?></a>
    <a href="#" id="btnfinish" class="btn btn-success btn-sm"   onclick="finish('<?php echo $list->order_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('finish');?></a>
    <?php }else{
      
      if($list->order_approved=="2"){
      ?>
       <a href="#" id="btnedit" class="btn btn-success btn-sm"  ata-toggle="modal" data-target="#modalEdit"  onclick="edit('<?php echo $list->order_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>
      <?php
      }
      ?>

      <a href="<?php echo base_url()."transactional/received/eidlist/".$list->order_id;?>" id="btnedit" class="btn btn-success btn-sm"  onclick=""><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('detail');?></a>
<?php
    } ?>
    </td>
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>


</div>
<div class="card-footer text-center">

</div>
</div>













<script>
function finish(id){
  if(confirm('Apakah anda yakin sudah selesai?')){
    document.location = "<?php echo base_url()."transactional/order/rec/";?>"+id;
  }
}

function edit(id){
  $('#modalEdit').modal();
  $.ajax({
    url:"<?php echo base_url()."transactional/order/detail";?>",
    type:"POST",
    dataType : "json",
    data:{
      "order_id":id
    },
    success:function(jsondata){
      $('#idkabupatenMST').val(jsondata.district_code);
      $('#idpropinsiEdit').val(jsondata.district_province);
      $('#idkabupatenEdit').val(jsondata.district_code);
      $('#nama_kabupatenEdit').val(jsondata.district_name);
    }
  })
}

    $('document').ready(function(){


$('#search').keyup(function(){
  $('#dataBody').load('<?php echo base_url()."transactional/order/search_received";?>',{"search":$('#search').val()});
})
        
    $('#btnSubmit').click(function(){
        $.ajax({
            url:"<?php echo base_url()."transactional/order/add";?>",
            type:"POST",
            dataType:"json",
            data:{
                "order_hf_sender":"<?php echo $this->session->userdata('user_unit');?>",
                "order_date":$('#order_date').val(),
                "order_hf_recipient":$("#order_hf_recipient").val(),
                "order_shipper_id":$("#order_shipper_id").val()
            },
            success:function(jdata){
                if(jdata.status=='success'){
                    alert(jdata.message);
                    document.location = "<?php echo base_url()."transactional/specimen/list/";?>"+jdata.order_id;
                }

            }
        })
    });

    });
</script>