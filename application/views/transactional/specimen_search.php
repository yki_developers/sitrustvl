<?php
    $i=1;
    foreach($specimen as $list){ 
        $m = $this->M_reverse->hfName($list->order_hf_recipient);
        $k = $this->M_reverse->shipperName($list->order_shipper_id);
        ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->order_number?></td>
    <td scope="col"><?php echo $list->order_date;?></td>
    <td scope="col"><?php echo $list->patient_name;?></td>
    <td scope="col"><?php echo $list->specimen_id;?></td>
    <td scope="col"><?php echo specimen_vl_test($list->specimen_exam_category);?></td>
    <td scope="col"><?php echo specimen_vl_type($list->specimen_type);?></td>
    <td scope="col" class="text-left">
   
&nbsp;
<?php if($list->order_status==0 || ($list->order_status==2 && $list->order_approved==2)){?>
    <a href="#" class="btn btn-danger btn-sm" onclick="conf('<?php echo $list->specimen_num_id;?>','<?php echo $list->order_id; ?>')"><i class="fas fa-trash-alt"></i>&nbsp;<?php echo $this->lang->line('delete');?></a>
    &nbsp;
    <a href="#" id="btnedit" class="btn btn-success btn-sm"   onclick="edit('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>
<?php }elseif(($list->order_status>0 && $list->order_status<6 && $list->specimen_result_flag!='1') || ($list->order_status==2 && $list->order_approved==2)) {?> 
  <a href="#" id="btnedit" class="btn btn-success btn-sm"   onclick="edit('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>
  <?php }else{?>
    <a href="#" id="btnedit" class="btn btn-success btn-sm"   onclick="edit_patient('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit_patient');?></a>
  <?php }?>
    </td>
    
</tr>

    <?php $i++;
} ?>