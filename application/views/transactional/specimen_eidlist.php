<style>.datepicker{z-index:1151 !important;}
label{
  color: red;
  margin-left: 10px;
}
</style>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb" style="margin-top: 50px;">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item"><a href="<?php echo base_url()."transactional/order/eid";?>"><i class="fas fa-plus-square"></i>&nbsp;<?php echo $this->lang->line('order');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-vial"></i>&nbsp;<?php echo $this->lang->line('specimen');?></li>
  </ol>
</nav>





<!---- MODAL ORDER -->

<div class="modal fade shadow" id="modalForm">
    <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-merah shadow">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('specimen');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form method="POST" id="specimen"  class="needs-validation" validate>
<!-- hidden area -->
        <input type="hidden" value="1" id="specimen_exam_categori">
        <input type="hidden" value="0" id="patient_id">
        <input type="hidden" value="0" id="specimen_num_id">
        <input type="hidden" value="4" id="specimen_type" name="specimen_type">
<!-- end hidden area -->

<div class="card card-body">
<span><i class="fas fa-edit"></i> ISIAN DATA IDENTITAS IBU</span>


<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('mother_name');?></span>
    </div>
    <input type="text"  id="patient_eid_mother" name="patient_eid_mother" class="form-control"  required="required">
  </div> 



  <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('mother_nid');?></span>
    </div>
    <input type="text"  id="patient_eid_mother_nid" name="patient_eid_mother_nid" class="form-control" maxlength="16"  required="required">
  </div> 


  <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('mother_regnas');?></span>
    </div>
    <input type="text"  id="patient_eid_mother_regnas" name="patient_eid_mother_regnas" class="form-control">
  </div> 




</div>


<div class="card card-body" style="margin-top: 5px;">
<span><i class="fas fa-edit"></i><?php echo $this->lang->line("child_form");?></span>


<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line("specimen_test_type");?></span>
    </div>
    <select name="specimen_eid_number" id="specimen_eid_number" class="form-control custom-select shadow"  required="required" placeholder="<?php echo $this->lang->line("specimen_test_type");?>">
                      <option value="">&nbsp;</option>
                      <?php for($i=1;$i<=3;$i++){?>
                      <option value="<?php echo $i;?>"><?php echo specimen_eid_number($i); ?></option>
                      <?php }?>
               

</select>
  </div> 





  <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('child_nid');?></span>
    </div>
                
                <input type="text"  id="patient_nid" name="patient_nid" class="form-control shadow noneid" maxlength="16" >
                
                </div>
              




              <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('patient_med_record');?></span>
    </div>
              
                <input type="text"  id="patient_med_record" name="patient_med_record" class="form-control"  required="required">              
</div>


<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('child_name');?></span>
    </div>
              
                <input type="text"  id="patient_name" name="patient_name" class="form-control"  required="required">
                  
</div>


<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line("patient_sex");?></span>
    </div>
              
                <select name="patient_sex" id="patient_sex" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("patient_sex");?>">
                      <option value=""></option>
                  <option value="1"><?php echo  $this->lang->line("sex_m");?></option>
                  <option value="2"><?php echo  $this->lang->line("sex_f");?></option>

</select>

            
               
</div>


<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line("patient_bday");?></span>
    </div>
              
                <input type="text"  id="patient_bday" name="patient_bday" class="form-control tanggal" placeholder="<?php echo $this->lang->line('patient_bday');?>" required="required">
                  
</div>

<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line("province");?></span>
    </div>
                <select name="patient_province" id="patient_province" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("province");?>">
                      <option value=""></option>
                      <?php foreach($province as $combolist){?>
                      <option value="<?php echo $combolist->province_code;?>"><?php echo $combolist->province_name; ?></option>
                      <?php }?>
               

</select>
            
</div>

<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line("district");?></span>
    </div>
              
                <select name="patient_district" id="patient_district" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("district");?>" disabled="disabled">
                      <option value=""></option>
               

</select>
            
             
</div>


<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line("subdistrict");?></span>
    </div>
              
                <select name="patient_subdistrict" id="patient_subdistrict" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("subdistrict");?>" disabled="disabled">
                      <option value=""></option>
               

</select>
            
       
</div>


<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line("patient_address");?></span>
    </div>
              
                <input type="text"  id="patient_address" name="patient_address" class="form-control"  required="required">
                  
</div>


<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line("specimen_id");?></span>
    </div>
              
                <input type="text"  id="specimen_id" name="specimen_id" class="form-control"  required="required">
                 
            
          
</div>

<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line("specimen_date_collected");?></span>
    </div>
              
                <input type="text"  id="specimen_date_collected" name="specimen_date_collected" class="form-control tanggal" required="required">
                
</div>


<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line("specimen_time_collected");?></span>
    </div>
              
                <input type="text"  id="specimen_time_collected" name="specimen_time_collected" class="form-control clockpicker" placeholder="00:00" maxlength="8" required="required">
                 
</div>


<!-- <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line("specimen_type");?></span>
    </div>
              
                <select name="specimen_type" id="specimen_type" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("specimen_type");?>">
                      <option value=""></option>
                      <?php for($i=1;$i<=4;$i++){?>
                      <option value="<?php echo $i;?>"><?php echo specimen_vl_type($i); ?></option>
                      <?php }?>
               

</select>
            
</div> -->



<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line("doctor_name");?></span>
    </div>
              
                <input type="text"  id="doctor_name" name="doctor_name" class="form-control"  required="required">
                
</div>


<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <button type="submit" class="btn btn-success btn-block" id="btnSubmit"><?php echo $this->lang->line("submit");?></button>
               
               
                </div>
              </div>
</div>

</div>

        </form>
        </div>

        <div class="modal-footer">

        
<button type="button" class="btn btn-outline-danger" id="tutup">Tutup</button>
</div>

      </div>
    </div>
</div>

<!-- END MODAL -->


<div class="card">
<div class="card-header bg-merah" id="headercard">&nbsp;
</div>

<div class="card-body">
<div class="input-group col-lg-4">
    <input type="text" id="search" name="search" class="form-control" placeholder="Pencarian">
    <div class="input-group-append"><span class="btn  btn-success" id="src"><i class="fas fa-search"></i></span></div>&nbsp;
    <?php if($order->order_status=='0' || $order->order_approved=='2'){?>
    <a href="#cardform" class="btn btn-success btn-xs" id="add" data-toggle="modal" data-target="#modalForm"  data-backdrop="static" data-keyboard="false"><i class="fas fa-plus-circle"></i>&nbsp;<?php echo $this->lang->line('add');?></a>
    <?php } ?>
</div>

</div>
<div class="card-body">

<table class="table table-stripe">
<thead>


<tr class="bg-merah">
    <th scope="col"><?php echo $this->lang->line('number');?></th>
    <th scope="col"><?php echo $this->lang->line('order_number');?></th>
    <th scope="col"><?php echo $this->lang->line('order_date');?></th>
    <th scope="col"><?php echo $this->lang->line('patient_name');?></th>
    <th scope="col"><?php echo $this->lang->line('specimen_number');?></th>
    <th scope="col"><?php echo $this->lang->line('specimen_test_type');?></th>
    <th scope="col"><?php echo $this->lang->line('specimen_type');?></th>


    <th scope="col"><?php echo $this->lang->line('action');?></th>
</tr>
</thead>
<tbody id="dataBody">
    <?php
    $i=1;
    foreach($specimen as $list){ 
        $m = $this->M_reverse->hfName($list->order_hf_recipient);
        $k = $this->M_reverse->shipperName($list->order_shipper_id);
        ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->order_number?></td>
    <td scope="col"><?php echo $list->order_date;?></td>
    <td scope="col"><?php echo $list->patient_name;?></td>
    <td scope="col"><?php echo $list->specimen_id;?></td>
    <td scope="col"><?php echo specimen_vl_test($list->specimen_exam_category);?></td>
    <td scope="col"><?php echo specimen_vl_type($list->specimen_type);?></td>
    <td scope="col" class="text-left">
   
&nbsp;
<?php if($list->order_status==0 || ($list->order_status==2 && $list->order_approved==2)){?>
    <a href="#" class="btn btn-danger btn-sm" onclick="conf('<?php echo $list->specimen_num_id;?>','<?php echo $list->order_id; ?>')"><i class="fas fa-trash-alt"></i>&nbsp;<?php echo $this->lang->line('delete');?></a>
    &nbsp;
    <a href="#" id="btnedit" class="btn btn-success btn-sm"   onclick="edit('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>
<?php }elseif(($list->order_status>0 && $list->order_status<6 && $list->specimen_result_flag!='1') || ($list->order_status==2 && $list->order_approved==2)) {?> 
  <a href="#" id="btnedit" class="btn btn-success btn-sm"   onclick="edit('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>
  <?php }else{?>
    <a href="#" id="btnedit" class="btn btn-success btn-sm"   onclick="edit_patient('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit_patient');?></a>
  <?php }?>
    </td>
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>


</div>
<div class="card-footer text-center">
<?php if($order->order_status=='0'){
  
  if($specimen){
  ?>

<a href="#" id="btnfinish" class="btn btn-success btn-sm" onclick="finish('<?php echo $this->uri->segment(4);?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('create_order');?></a>
<?php } } ?>
</div>
</div>



<!-- Edit Data Pasien -->

<div class="modal fade shadow" id="modalFormPatient">
    <div class="modal-dialog modal-dialog-centered modal-lg">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-merah shadow">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('edit_specimen');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form method="POST" id="idpatient"  class="needs-validation" validate>
<!-- hidden area -->
        <input type="hidden" value="0" id="edit_patient_id">
        
<!-- end hidden area -->

<div class="card card-body">
<span><i class="fas fa-edit"></i> ISIAN DATA IDENTITAS IBU</span>


<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('mother_name');?></span>
    </div>
    <input type="text"  id="edit_patient_eid_mother" name="edit_patient_eid_mother" class="form-control"  required="required">
  </div> 



  <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('mother_nid');?></span>
    </div>
    <input type="text"  id="edit_patient_eid_mother_nid" name="edit_patient_eid_mother_nid" class="form-control" maxlength="16"  required="required">
  </div> 


  <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('mother_regnas');?></span>
    </div>
    <input type="text"  id="edit_patient_eid_mother_regnas" name="edit_patient_eid_mother_regnas" class="form-control">
  </div> 




</div>


<div class="card card-body" style="margin-top: 5px;">
<span><i class="fas fa-edit"></i><?php echo $this->lang->line("child_form");?></span>


  <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('child_nid');?></span>
    </div>
                
                <input type="text"  id="edit_patient_nid" name="edit_patient_nid" class="form-control shadow noneid" maxlength="16" >
                
                </div>
              




              <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('patient_med_record');?></span>
    </div>
              
                <input type="text"  id="edit_patient_med_record" name="edit_patient_med_record" class="form-control"  required="required">              
</div>


<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line('child_name');?></span>
    </div>
              
                <input type="text"  id="edit_patient_name" name="edit_patient_name" class="form-control"  required="required">
                  
</div>


<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line("patient_sex");?></span>
    </div>
              
                <select name="edit_patient_sex" id="edit_patient_sex" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("patient_sex");?>">
                      <option value=""></option>
                  <option value="1"><?php echo  $this->lang->line("sex_m");?></option>
                  <option value="2"><?php echo  $this->lang->line("sex_f");?></option>

</select>

            
               
</div>


<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line("patient_bday");?></span>
    </div>
              
                <input type="text"  id="edit_patient_bday" name="edit_patient_bday" class="form-control tanggal" placeholder="<?php echo $this->lang->line('patient_bday');?>" required="required">
                  
</div>

<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line("province");?></span>
    </div>
                <select name="edit_patient_province" id="edit_patient_province" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("province");?>">
                      <option value=""></option>
                      <?php foreach($province as $combolist){?>
                      <option value="<?php echo $combolist->province_code;?>"><?php echo $combolist->province_name; ?></option>
                      <?php }?>
               

</select>
            
</div>

<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line("district");?></span>
    </div>
              
                <select name="edit_patient_district" id="edit_patient_district" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("district");?>" disabled="disabled">
                      <option value=""></option>
               

</select>
            
             
</div>


<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line("subdistrict");?></span>
    </div>
              
                <select name="edit_patient_subdistrict" id="edit_patient_subdistrict" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("subdistrict");?>" disabled="disabled">
                      <option value=""></option>
               

</select>
            
       
</div>


<div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text"><?php echo $this->lang->line("patient_address");?></span>
    </div>
              
                <input type="text"  id="edit_patient_address" name="edit_patient_address" class="form-control"  required="required">
                  
</div>

<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <button type="submit" class="btn btn-success btn-block" id="btnSubmitEdit"><?php echo $this->lang->line("submit");?></button>
               
               
                </div>
              </div>
</div>

</div>

        </form>
        </div>

        <div class="modal-footer">

        
<button type="button" class="btn btn-outline-danger" id="tutup">Tutup</button>
</div>

      </div>
    </div>
</div>



<script>


function conf(id,oid){
  if(confirm("<?php echo $this->lang->line('delete_confirmation');?>")){
    document.location = "<?php echo base_url()."transactional/specimen/delete/";?>"+id+"/"+oid;
  }
}
function dbdate(tgl){
var tg = tgl.split("-");
var dbFormat = tg[2]+"-"+tg[1]+"-"+tg[0];
return dbFormat;
}

function edit(id){
  $.ajax({
    url:"<?php echo base_url()."transactional/specimen/detail";?>",
    type:"POST",
    dataType:"json",
    data:{
      "specimen_num_id":id
    },
    success:function(jdata){
      if(jdata.status=="success"){
       $('#specimen_num_id').val(id);
       $('#patient_eid_mother').val(jdata.response.patient_eid_mother);
       $('#patient_eid_mother_nid').val(jdata.response.patient_eid_mother_nid);
       $('#patient_eid_mother_regnas').val(jdata.response.patient_eid_mother_regnas);
        $('#patient_nid').val(jdata.response.patient_nid);
        $('#patient_med_record').val(jdata.response.patient_med_record);
        $('#patient_name').val(jdata.response.patient_name);
        $('#patient_province').val(jdata.response.patient_province);
        $.ajax({
                url : '<?php echo base_url()."master/district/districtbyprovince";?>',
                type:'POST',
                dataType:'json',
                data:{
                  'province_code':jdata.response.patient_province
                  },
                  success:function(jsdata){
                    var str ='<option value=""><?php echo $this->lang->line('district');?></option>';
                    $.each(jsdata.response,function(i,item){
                     
                     if(item.district_code==jdata.response.patient_district){
                      str +='<option value="'+item.district_code+'"  selected="selected">'+item.district_name+'</option>';

                     }else{
                      str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
                     }  
                    })
                      $('#patient_district').html(str).removeAttr('disabled').val(jdata.response.patient_district);
                      //$(".preloader").fadeOut();
                      }
              });


              $.ajax({
                url : '<?php echo base_url()."master/subdistrict/bydistrict";?>',
                type:'POST',
                dataType:'json',
                data:{
                  'district_code':jdata.response.patient_district
                  },
                  success:function(jsdata){
                    var str ='<option value=""><?php echo $this->lang->line('subdistrict');?></option>';
                    $.each(jsdata.response,function(i,item){
                      str +='<option value="'+item.subdistrict_code+'">'+item.subdistrict_name+'</option>';
                      })
                      $('#patient_subdistrict').html(str).removeAttr('disabled').val(jdata.response.patient_subdistrict);
                      //$(".preloader").fadeOut();
                      }
              });
        $('#patient_address').val(jdata.response.patient_address);
        $('#patient_id').val(jdata.response.patient_id);
        $('#patient_sex').val(jdata.response.patient_sex);
        $('#patient_bday').val(dbdate(jdata.response.patient_bday));
        $('#specimen_id').val(jdata.response.specimen_id);
        $('#specimen_eid_number').val(jdata.response.specimen_eid_number);
        $('#specimen_date_collected').val(dbdate(jdata.response.date_collected));
        $('#specimen_time_collected').val(jdata.response.time_collected);
        $('#specimen_type').val(jdata.response.specimen_type);
        $('#specimen_exam_categori').val(jdata.response.specimen_exam_category);
        $('#doctor_name').val(jdata.response.specimen_doctor_name);
        $('#modalForm').modal({
          backdrop: 'static',
                            keyboard: false,
                            show:true
        });

      }
    }
  })
}





function edit_patient(id){
  $.ajax({
    url:"<?php echo base_url()."transactional/specimen/detail";?>",
    type:"POST",
    dataType:"json",
    data:{
      "specimen_num_id":id
    },
    success:function(jdata){
      if(jdata.status=="success"){
   
       $('#edit_patient_eid_mother').val(jdata.response.patient_eid_mother);
       $('#edit_patient_eid_mother_nid').val(jdata.response.patient_eid_mother_nid);
       $('#edit_patient_eid_mother_regnas').val(jdata.response.patient_eid_mother_regnas);
        $('#edit_patient_nid').val(jdata.response.patient_nid);
        $('#edit_patient_med_record').val(jdata.response.patient_med_record);
        $('#edit_patient_name').val(jdata.response.patient_name);
        $('#edit_patient_province').val(jdata.response.patient_province);
        $.ajax({
                url : '<?php echo base_url()."master/district/districtbyprovince";?>',
                type:'POST',
                dataType:'json',
                data:{
                  'province_code':jdata.response.patient_province
                  },
                  success:function(jsdata){
                    var str ='<option value=""><?php echo $this->lang->line('district');?></option>';
                    $.each(jsdata.response,function(i,item){
                     
                     if(item.district_code==jdata.response.patient_district){
                      str +='<option value="'+item.district_code+'"  selected="selected">'+item.district_name+'</option>';

                     }else{
                      str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
                     }  
                    })
                      $('#edit_patient_district').html(str).removeAttr('disabled').val(jdata.response.patient_district);
                      //$(".preloader").fadeOut();
                      }
              });


              $.ajax({
                url : '<?php echo base_url()."master/subdistrict/bydistrict";?>',
                type:'POST',
                dataType:'json',
                data:{
                  'district_code':jdata.response.patient_district
                  },
                  success:function(jsdata){
                    var str ='<option value=""><?php echo $this->lang->line('subdistrict');?></option>';
                    $.each(jsdata.response,function(i,item){
                      str +='<option value="'+item.subdistrict_code+'">'+item.subdistrict_name+'</option>';
                      })
                      $('#edit_patient_subdistrict').html(str).removeAttr('disabled').val(jdata.response.patient_subdistrict);
                      //$(".preloader").fadeOut();
                      }
              });
        $('#edit_patient_address').val(jdata.response.patient_address);
        $('#edit_patient_id').val(jdata.response.patient_id);
        $('#edit_patient_sex').val(jdata.response.patient_sex);
        $('#edit_patient_bday').val(dbdate(jdata.response.patient_bday));
        $('#modalFormPatient').modal({
          backdrop: 'static',
                            keyboard: false,
                            show:true
        });

      }
    }
  })
}

function finish(id){
  if(confirm('Apakah anda yakin sudah selesai?')){
    document.location = "<?php echo base_url()."transactional/order/eidfinish/";?>"+id;
  }
}
    $('document').ready(function(){

      $('.close,#tutup').click(function(){
        document.location = "<?php echo base_url()."transactional/specimen/eid/".$this->uri->segment(4);?>";
      })

     // $('#specimen').validate();

     // $(".eidpart").hide();



$('#src').click(function(){
  $('#dataBody').load('<?php echo base_url()."transactional/specimen/search/";?>', 
            {
                "search": $('#search').val(),
                "order_id":"<?php echo $this->uri->segment('4');?>"
             });
});


    

      $('#specimen_id').blur(function(){
        if($(this).val()!=''){
        $.ajax({
          url:"<?php echo base_url()."transactional/specimen/checkspecimen";?>",
          type:"POST",
          dataType:"json",
          data:{
            "specimen_id":$('#specimen_id').val(),
            "specimen_order_id":"<?php echo $this->uri->segment(4);?>"
          },
          success:function(jdata){
            if(jdata.status==false){
              alert('ID Specimen Ini sudah terdaftar');
              $("#specimen_id").val('').focus();
            }
          }
        })
      }
      });

$('.tanggal').attr("readonly","readonly");
      $('.clockpicker').clockpicker(
        {
          placement: 'top',
    align: 'left',
    autoclose:'true'

        }
      );

      $('.tanggal').datepicker({
  format:"dd-mm-yyyy",
  startView:"year",
  minView:"year"
}).on('changeDate',function(ev){
  $(this).blur();
  $(this).datepicker('hide');
});

       $('#patient_province').change(function(){
    $(".preloader").fadeIn();
    $.ajax({
        url : '<?php echo base_url()."master/district/districtbyprovince";?>',
        type:'POST',
        dataType:'json',
        data:{
            'province_code':$('#patient_province').val()
        },
        success:function(jdata){

            var str ='<option value="">-- Pilih Kabupaten --</option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
            })
            $('#patient_district').html(str).removeAttr('disabled');
            $(".preloader").fadeOut();
        }
    })
})




$('#patient_district').change(function(){
    $(".preloader").fadeIn();
    $.ajax({
        url : '<?php echo base_url()."master/subdistrict/bydistrict";?>',
        type:'POST',
        dataType:'json',
        data:{
            'district_code':$('#patient_district').val()
        },
        success:function(jdata){

            var str ='<option value="">-- Pilih Kecamatan --</option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.subdistrict_code+'">'+item.subdistrict_name+'</option>';
            })
            $('#patient_subdistrict').html(str).removeAttr('disabled');
            $(".preloader").fadeOut();
        }
    })
})





$('#edit_patient_province').change(function(){
    $(".preloader").fadeIn();
    $.ajax({
        url : '<?php echo base_url()."master/district/districtbyprovince";?>',
        type:'POST',
        dataType:'json',
        data:{
            'province_code':$('#edit_patient_province').val()
        },
        success:function(jdata){

            var str ='<option value="">-- Pilih Kab/Kota --</option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
            })
            $('#edit_patient_district').html(str).removeAttr('disabled');
            $(".preloader").fadeOut();
        }
    })
})




$('#edit_patient_district').change(function(){
    $(".preloader").fadeIn();
    $.ajax({
        url : '<?php echo base_url()."master/subdistrict/bydistrict";?>',
        type:'POST',
        dataType:'json',
        data:{
            'district_code':$('#edit_patient_district').val()
        },
        success:function(jdata){

            var str ='<option value="">-- Pilih Kecamatan --</option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.subdistrict_code+'">'+item.subdistrict_name+'</option>';
            })
            $('#edit_patient_subdistrict').html(str).removeAttr('disabled');
            $(".preloader").fadeOut();
        }
    })
})


$('#idpatient').validate({
  rules:{
    edit_patient_eid_mother_nid:{
required:true,
minlength:16
  },
  edit_patient_nid:{
    required: true,
    minlength:16,
  }
  },
  messages:{
    edit_patient_eid_mother:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  edit_patient_eid_mother_nid:{
    required:"<?php echo $this->lang->line('required');?>",
    minlength:"Masukkan 16 digit NIK Ibu"
  },
  edit_patient_nid:{
    required:"<?php echo $this->lang->line('required');?>",
    minlength:"Masukkan 16 digit NIK"
  },
  edit_patient_regnas:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  edit_patient_med_record:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  edit_patient_name:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  edit_patient_bday:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  edit_patient_sex:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  edit_patient_art_date:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  edit_patient_province:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  edit_patient_district:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  edit_patient_subdistrict:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  edit_patient_address:{
    required:"<?php echo $this->lang->line('required');?>"
  }
  },
  submitHandler:function(form){
    $.ajax({
            url:"<?php echo base_url()."transactional/specimen/update/";?>"+$("#edit_patient_id").val(),
            type:"POST",
            dataType:"json",
            data:{
                "patient_name":$('#edit_patient_name').val(),
                "patient_nid":$("#edit_patient_nid").val(),
                "patient_bday":dbdate($('#edit_patient_bday').val()),
                "patient_sex":$('#edit_patient_sex').val(),
                "patient_med_record":$('#edit_patient_med_record').val(),
                "patient_province":$('#edit_patient_province').val(),
                "patient_district":$('#edit_patient_district').val(),
                "patient_subdistrict":$('#edit_patient_subdistrict').val(),
                "patient_address":$('#edit_patient_address').val(),
                "patient_eid_mother":$('#edit_patient_eid_mother').val(),
                "patient_eid_mother_nid":$('#edit_patient_eid_mother_nid').val(),
                "patient_eid_mother_regnas":$('#edit_patient_eid_mother_regnas').val()
                

            },
            success:function(jdata){
                if(jdata.status=='success'){
                  alert(jdata.message);
                  document.location = "<?php echo base_url()."transactional/specimen/eid/".$this->uri->segment(4);?>";
                   
                }else{
                  alert(jdata.message);
                }

            }
        })
  }

});

$('#specimen').validate({

rules:{
  patient_eid_mother_nid:{
required:true,
minlength:16
  },
  patient_nid:{
    required: true,
    minlength:16,
  },
  specimen_id:{
    remote:{
      url:"<?php echo base_url()."transactional/specimen/checkspecimen";?>",
      type:"POST",
      dataType:"json",
      data:{
        "specimen_order_id":"<?php echo $this->uri->segment(4);?>",
        "specimen_num_id":$('#specimen_num_id').val()
      }
    }
  }
},
messages:{
  patient_eid_mother:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  patient_eid_mother_nid:{
    required:"<?php echo $this->lang->line('required');?>",
    minlength:"Masukkan 16 digit NIK Ibu"
  },
  patient_nid:{
    required:"<?php echo $this->lang->line('required');?>",
    minlength:"Masukkan 16 digit NIK"
  },
  patient_regnas:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  patient_med_record:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  patient_name:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  patient_bday:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  patient_sex:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  patient_art_date:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  patient_province:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  patient_district:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  patient_subdistrict:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  patient_address:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  specimen_id:{
    required:"<?php echo $this->lang->line('required');?>",
    remote:"ID Specimen ini sudah terdaftar"
  },
  specimen_date_collected:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  specimen_time_collected:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  specimen_type:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  doctor_name:{
    required:"<?php echo $this->lang->line('required');?>"
  },
  specimen_eid_number:{
    required:"<?php echo $this->lang->line('required');?>"
  },

},
submitHandler: function(form) {
 
$('#btnSubmit').attr("disabled","disabled");

  if($('#specimen_num_id').val()=="0"){
        $.ajax({
            url:"<?php echo base_url()."transactional/specimen/add";?>",
            type:"POST",
            dataType:"json",
            data:{
                "patient_name":$('#patient_name').val(),
                "patient_nid":$("#patient_nid").val(),
                "patient_bday":dbdate($('#patient_bday').val()),
                "patient_sex":$('#patient_sex').val(),
                "patient_regnas":$('#patient_regnas').val(),
                "patient_med_record":$('#patient_med_record').val(),
                "patient_province":$('#patient_province').val(),
                "patient_district": $('#patient_district').val(),
                "patient_subdistrict":$('#patient_subdistrict').val(),
                "patient_address":$('#patient_address').val(),
                "patient_eid_mother":$('#patient_eid_mother').val(),
                "patient_eid_mother_nid":$('#patient_eid_mother_nid').val(),
                "patient_eid_mother_regnas":$('#patient_eid_mother_regnas').val()
            },
            success:function(jdata){
                if(jdata.status=='success'){
                   
                  $.ajax({
                    url : "<?php echo base_url()."transactional/specimen/input";?>",
                    type: "POST",
                    dataType:"json",
                    data:{
                      "specimen_patient_id":jdata.response,
                      "specimen_order_id":<?php echo $this->uri->segment(4);?>,
                "specimen_id":$('#specimen_id').val(),
                "specimen_date_collected":dbdate($('#specimen_date_collected').val())+" "+$('#specimen_time_collected').val(),
                "specimen_type":$('#specimen_type').val(),
                "specimen_exam_category":$('#specimen_exam_categori').val(),
                "specimen_doctor_name":$('#doctor_name').val(),
                "specimen_eid_number":$('#specimen_eid_number').val()
                    },
                    success:function(jdata){
                      if(jdata.status=='success'){
                        alert(jdata.message);
                        document.location = "<?php echo base_url()."transactional/specimen/eid/".$this->uri->segment(4);?>";
                      }else{
                        alert(jdata.message);
                      }
                    }
                  })


                }

            }
        })
      }else{


        $.ajax({
            url:"<?php echo base_url()."transactional/specimen/update/";?>"+$("#patient_id").val(),
            type:"POST",
            dataType:"json",
            data:{
                "patient_name":$('#patient_name').val(),
                "patient_nid":$("#patient_nid").val(),
                "patient_bday":dbdate($('#patient_bday').val()),
                "patient_sex":$('#patient_sex').val(),
                "patient_med_record":$('#patient_med_record').val(),
                "patient_province":$('#patient_province').val(),
                "patient_district":$('#patient_district').val(),
                "patient_subdistrict":$('#patient_subdistrict').val(),
                "patient_address":$('#patient_address').val(),
                "patient_eid_mother":$('#patient_eid_mother').val(),
                "patient_eid_mother_nid":$('#patient_eid_mother_nid').val(),
                "patient_eid_mother_regnas":$('#patient_eid_mother_regnas').val()
                

            },
            success:function(jdata){
                if(jdata.status=='success'){
                   
                  $.ajax({
                    url : "<?php echo base_url()."transactional/specimen/updatespecimen/";?>"+$('#specimen_num_id').val(),
                    type: "POST",
                    dataType:"json",
                    data:{
                      "specimen_patient_id":$('#patient_id').val(),
                      "specimen_order_id":<?php echo $this->uri->segment(4);?>,
                "specimen_id":$('#specimen_id').val(),
                "specimen_date_collected":dbdate($('#specimen_date_collected').val())+" "+$('#specimen_time_collected').val(),
                "specimen_type":$('#specimen_type').val(),
                "specimen_exam_category":$('#specimen_exam_categori').val(),
                "specimen_doctor_name":$('#doctor_name').val(),
                "specimen_eid_number":$('#specimen_eid_number').val()
                    },
                    success:function(jdata){
                      if(jdata.status=='success'){
                        alert(jdata.message);
                        document.location = "<?php echo base_url()."transactional/specimen/eid/".$this->uri->segment(4);?>";
                      }else{
                        alert(jdata.message);
                      }
                    }
                  })


                }

            }
        })









      }




}
});
        


    });
</script>