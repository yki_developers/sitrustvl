
<nav aria-label="breadcrumb" style="margin-top: 50px;">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-clipboard-check"></i>&nbsp;<?php echo $this->lang->line('result');?> EID</li>
  </ol>
</nav>









<div class="card">
<div class="card-header bg-merah" id="headercard">&nbsp;
</div>

<div class="card-body">
<div class="input-group col-lg-4">
    <input type="text" id="search" name="search" class="form-control" placeholder="No order / Pengirim">
    <div class="input-group-append"><span class="btn  btn-success" id="src"><i class="fas fa-search"></i></span></div>&nbsp;
    
    &nbsp;Atau&nbsp;
    <a href="<?php echo base_url();?>transactional/result/searchbyspecimen"><div class="input-group-append"><span class="btn  btn-primary" id="src_pst"><i class="fas fa-search"></i>&nbsp;Cari Nama Pasien</span></div></a>&nbsp;
  
</div>


</div>

<div class="card-body">
<div class="tableFixHead" id="tabledata">
<table class="table">
<thead>


<tr class="bg-merah">
<tr class="bg-merah">
    <th scope="col"><?php echo $this->lang->line('number');?></th>
    <th scope="col"><?php echo $this->lang->line('order_number');?></th>
    <th scope="col"><?php echo $this->lang->line('order_date');?></th>
    <th scope="col"><?php echo $this->lang->line('order_origin');?></th>
    <th scope="col"><?php echo $this->lang->line('order_destination');?></th>
    <th scope="col"><?php echo $this->lang->line('order_courier');?></th>
    <th scope="col"><?php echo $this->lang->line('order_status');?></th>

    <th scope="col"><?php echo $this->lang->line('action');?></th>
</tr>
</thead>
<tbody id="dataBody">
    <?php
    $i=1;
    foreach($orderlist as $list){ 
        $m = $this->M_reverse->hfName($list->order_hf_sender);
        $k = $this->M_reverse->shipperName($list->order_shipper_id);
        ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->order_number?></td>
    <td scope="col"><?php echo $list->order_date;?></td>
    <td scope="col"><?php echo $m->hf_name;?></td>
    <td scope="col"><?php echo $list->faskes_penerima;?></td>
    <td scope="col"><?php echo $k->shipper_name;?></td>
    <td scope="col"><?php
    if($list->order_status!='2'){
    echo specimen_vl_status($list->order_status);
    }else{
      echo specimen_vl_approval($list->order_approved);
      if($list->order_approved=='2'){
        echo "<br>".$this->lang->line("order_reason").": ".$list->order_reason;
      }

    }
    ?></td>
    <td scope="col" class="text-center d-none d-sm-block">
    
&nbsp;
    
    &nbsp;
    <?php if($list->order_status==0){?>
      <a href="<?php echo base_url()."transactional/specimen/list/".$list->order_id;?>" id="btnedit" class="btn btn-success btn-sm"   onclick="specimen('<?php echo $list->order_id;?>')"><i class="fas fa-plus-square"></i>&nbsp;<?php echo $this->lang->line('add_specimen');?></a>
      <a href="#" class="btn btn-danger btn-sm" onclick="conf('<?php echo $list->order_id; ?>')"><i class="fas fa-trash-alt"></i>&nbsp;<?php echo $this->lang->line('delete');?></a>
    <a href="#" id="btnfinish" class="btn btn-success btn-sm"   onclick="finish('<?php echo $list->order_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('finish');?></a>
    <?php }else{
      
      if($list->order_approved=="2"){
      ?>
       <a href="#" id="btnedit" class="btn btn-success btn-sm"  ata-toggle="modal" data-target="#modalEdit"  onclick="edit('<?php echo $list->order_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>
      <?php
      }

      ?>

      <a href="<?php echo base_url()."transactional/result/eiddetail/".$list->order_id;?>" id="btnedit" class="btn btn-success btn-sm"  onclick=""><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('detail');?></a>
<?php
    } ?>
    </td>
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>
</div>

</div>
<div class="card-footer text-center">

</div>
</div>













<script>
function finish(id){
  if(confirm('Apakah anda yakin sudah selesai?')){
    document.location = "<?php echo base_url()."transactional/result/eidfinish/";?>"+id;
  }
}

function edit(id){
  $('#modalEdit').modal();
  $.ajax({
    url:"<?php echo base_url()."transactional/order/detail";?>",
    type:"POST",
    dataType : "json",
    data:{
      "order_id":id
    },
    success:function(jsondata){
      $('#idkabupatenMST').val(jsondata.district_code);
      $('#idpropinsiEdit').val(jsondata.district_province);
      $('#idkabupatenEdit').val(jsondata.district_code);
      $('#nama_kabupatenEdit').val(jsondata.district_name);
    }
  })
}

    $('document').ready(function(){

      $('#search').keyup(function(){
        $('#dataBody').load('<?php echo base_url()."transactional/result/search";?>',{"search":$('#search').val()});
      })

     /* $('#search_pst').keyup(function(){
        $('#tabledata').load('<?php echo base_url()."transactional/result/searchbyspecimen";?>',{"search":$('#search_pst').val()});
      })
*/

        
    $('#btnSubmit').click(function(){
        $.ajax({
            url:"<?php echo base_url()."transactional/order/add";?>",
            type:"POST",
            dataType:"json",
            data:{
                "order_hf_sender":"<?php echo $this->session->userdata('user_unit');?>",
                "order_date":$('#order_date').val(),
                "order_hf_recipient":$("#order_hf_recipient").val(),
                "order_shipper_id":$("#order_shipper_id").val()
            },
            success:function(jdata){
                if(jdata.status=='success'){
                    alert(jdata.message);
                    document.location = "<?php echo base_url()."transactional/specimen/list/";?>"+jdata.order_id;
                }

            }
        })
    });

    });
</script>