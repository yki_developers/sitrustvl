
<style>
.scroll {
    max-height: 500px;
    overflow-y: scroll;
}

</style>
<nav aria-label="breadcrumb" style="margin-top: 50px;">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item" aria-current="page"><a href="<?php echo base_url()."transactional/result/list";?>"><i class="fas fa-clipboard-check"></i>&nbsp;<?php echo $this->lang->line('result');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('specimen');?></li>

  </ol>
</nav>







<div class="card">
<div class="card-header bg-merah" id="headercard">&nbsp;
</div>
<div class="card-body">
<div class="input-group col-lg-4">
    <input type="text" id="search" name="search" class="form-control" placeholder="Pencarian">
    <div class="input-group-append"><span class="btn  btn-success" id="src"><i class="fas fa-search"></i></span></div>&nbsp;
   
</div>
</div>

<div class="card-body">
<div class="tableFixHead">
<table class="table  table-bordered">
<thead>

<tr class="bg-merah">
    <th scope="col"><?php echo $this->lang->line('number');?></th>
    <th scope="col"><?php echo $this->lang->line('order_number');?></th>
    <th scope="col"><?php echo $this->lang->line('order_date');?></th>
    <th scope="col"><?php echo $this->lang->line('patient_name');?></th>
    <th scope="col"><?php echo $this->lang->line('specimen_number');?></th>
    <th scope="col"><?php echo $this->lang->line('specimen_test_type');?></th>
    <th scope="col"><?php echo $this->lang->line('specimen_type');?></th>


    <th scope="col" class="text-left"><?php echo $this->lang->line('action');?></th>
    <th scope="col" class="text-left"><?php echo $this->lang->line('document');?></th>
</tr>
</thead>
<tbody id="dataBody">
    <?php
    $i=1;
    foreach($specimen as $list){ 
      
        $m = $this->M_reverse->hfName($list->order_hf_recipient);
        $k = $this->M_reverse->shipperName($list->order_shipper_id);
        ?>
    <tr>
    <td scope="col"><?php echo $i;?></td>
    <td scope="col"><?php echo $list->order_number?></td>
    <td scope="col"><?php echo $list->order_date;?></td>
    <td scope="col"><?php echo $list->patient_name;?></td>
    <td scope="col"><?php echo $list->specimen_id;?></td>
    <td scope="col"><?php echo specimen_vl_test($list->specimen_exam_category);?></td>
    <td scope="col"><?php echo specimen_vl_type($list->specimen_type);?></td>
    <td scope="col"><?php
    if($list->specimen_result_flag!='1' && $this->session->userdata("user_hf_group")=='5' && $list->specimen_exam_category=='2'){
        ?>


        <a href="<?php echo base_url()."transactional/result/vlresult/".$list->specimen_num_id;?>" class="btn btn-success" onclick="result('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('result');?></a>
        <?php 
      
      
      }elseif($list->specimen_result_flag!='1' && $this->session->userdata("user_hf_group")=='5' && $list->specimen_exam_category=='1'){?>
      <a href="<?php echo base_url()."transactional/result/eidresult/".$list->specimen_num_id;?>"  class="btn btn-primary" onclick="result('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('result');?></a>
      <?php 
    
    }elseif($list->specimen_result_flag=='1' &&  $this->session->userdata("user_hf_group")=='5' && $list->specimen_exam_category=='2' &&  $list->vl_result_dokumen==null){?>
        <a href="#" class="btn btn-success" onclick="resultview('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-eye"></i>&nbsp;<?php echo $this->lang->line('result_view');?></a>
        <a href="#" class="btn btn-success" onclick="upload('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-images"></i>&nbsp;<?php echo $this->lang->line('result_document');?></a>

      <?php 
    
    }elseif($list->specimen_result_flag=='1' &&  $this->session->userdata("user_hf_group")=='5' && $list->specimen_exam_category=='2' &&  $list->vl_result_dokumen!=null){?>
      <a href="#" class="btn btn-success" onclick="resultview('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-eye"></i>&nbsp;<?php echo $this->lang->line('result_view');?></a>
      <a href="#" class="btn btn-success" onclick="upload('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-images"></i>&nbsp;<?php echo $this->lang->line('change_result_document');?></a>

    <?php 
  
  }elseif($list->specimen_result_flag=='1' &&  $this->session->userdata("user_hf_group")=='5' && $list->specimen_exam_category=='1' &&  $list->eid_result_dokumen==null){?>
       <a href="#" class="btn btn-success" onclick="eidresultview('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-eye"></i>&nbsp;<?php echo $this->lang->line('result_view');?></a>
        <a href="#" class="btn btn-success" onclick="eidupload('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-images"></i>&nbsp;<?php echo $this->lang->line('result_document');?></a>
      
      
        <?php }elseif($list->specimen_result_flag=='1' &&  $this->session->userdata("user_hf_group")=='5' && $list->specimen_exam_category=='1' &&  $list->eid_result_dokumen!=null){?>
          <a href="#" class="btn btn-success" onclick="eidresultview('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-eye"></i>&nbsp;<?php echo $this->lang->line('result_view');?></a>
          <a href="#" class="btn btn-success" onclick="eidupload('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('change_result_document');?></a>
      
      
      
        <?php }elseif($list->specimen_result_flag=='1' &&  $this->session->userdata("user_hf_group")=='2' &&  $list->vl_result_dokumen!=null && $list->specimen_exam_category=='2'){?>
        <a href="<?php echo base_url()."assets/resultdoc/".$list->vl_result_dokumen;?>" class="btn btn-success" target="_blank" rel="noopener noreferrer"><i class="fas fa-images"></i>&nbsp;<?php echo $this->lang->line('view_result_document');?></a>
      <?php }elseif($list->specimen_result_flag=='1' &&   $this->session->userdata("user_hf_group")=='2' &&  $list->vl_result_dokumen==null && $list->specimen_exam_category=='2'){?>
        <a href="#" class="btn btn-success" onclick="resultview('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-eye"></i>&nbsp;<?php echo $this->lang->line('result_view');?></a>


      <?php }elseif($list->specimen_result_flag=='1' &&  $this->session->userdata("user_hf_group")=='2' &&  $list->eid_result_dokumen!=null && $list->specimen_exam_category=='1'){?>
        <a href="#" class="btn btn-success" onclick="eidresultview('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-eye"></i>&nbsp;<?php echo $this->lang->line('result_view');?></a>
      <?php }elseif($list->specimen_result_flag=='1' &&   $this->session->userdata("user_hf_group")=='2' &&  $list->eid_result_dokumen==null && $list->specimen_exam_category=='1'){?>
        <a href="#" class="btn btn-success" onclick="eidresultview('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-eye"></i>&nbsp;<?php echo $this->lang->line('result_view');?></a>
      <?php }

    ?>

    &nbsp;
   
    </td>
    <td>
      <?php
      if($list->specimen_exam_category=='2' && $list->vl_result_dokumen!=null){?>
        <a href="<?php echo base_url()."transactional/result/files/".$list->vl_result_id;?>" class="btn btn-success"  target="_blank" rel="noopener noreferrer"><i class="fas fa-images"></i>&nbsp;<?php echo $this->lang->line('view_result_document');?></a>
       <?php }elseif($list->specimen_exam_category=='1' && $list->eid_result_dokumen!=null){?>
        <a href="<?php echo base_url()."transactional/result/eidfiles/".$list->eid_result_id;?>" class="btn btn-success"   target="_blank" rel="noopener noreferrer"><i class="fas fa-images"></i>&nbsp;<?php echo $this->lang->line('view_result_document');?></a>
      <?php }?>
    </td>
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>

</div>
</div>
<div class="card-footer text-center">&nbsp;
  <?php
  if($orderinfo->order_kategori=='2'){
    $val = @$vlchecked->dokumen;
  }else{
    $val = @$eidchecked->dokumen;
  }
  if($this->session->userdata("user_hf_group")=='5' && $resultchecked->noresult=='0' && $val=='0'){
    ?>

<a href="#" id="btnfinish" class="btn btn-success btn-sm" onclick="finish('<?php echo $this->uri->segment(4);?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('finish');?></a>
  <?php } ?>
</div>
</div>





<!-- Update Foto -->
<div class="modal fade" id="modalFoto">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-merah">
          <h4 class="modal-title"><i class="fas fa-images"></i>&nbsp;<?php echo $this->lang->line('result_document');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
        <form id="formFoto">
        <input type="hidden" id="specimen_num_id" name="specimen_num_id" value="">
        <div class="form-row">
<div class="col-md-12 text-center" style="padding-top:10px">
                <img src="" id="preview" style="width:250px">
              </div>
</div>

        <div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <input type="file" id="foto" name="foto" class="custom-file-input" placeholder="<?php echo $this->lang->line('result_document');?>"  autofocus="autofocus">
                <label for="foto" class="custom-file-label"><?php echo $this->lang->line('result_document');?></label>
                </div>
              </div>
</div>



<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <a class="btn btn-primary btn-block"   id="btnUpload">Upload</a>
               
               
                </div>
              </div>
</div>

        </form>

        </div>
         <!-- Modal footer -->
         <div class="modal-footer bg-merah">&nbsp;
          &nbsp;
        </div>
        
      </div>
    </div>
  </div>

  



  <!-- Update Foto  EID-->
<div class="modal fade" id="modalFotoEid">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-merah">
          <h4 class="modal-title"><i class="fas fa-images"></i>&nbsp;<?php echo $this->lang->line('result_document');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
        <form id="formFotoEid">
        <input type="hidden" id="specimen_num_id_eid" name="specimen_num_id" value="">
        <div class="form-row">
<div class="col-md-12 text-center" style="padding-top:10px">
                <img src="" id="preview" style="width:250px">
              </div>
</div>

        <div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <input type="file" id="foto" name="foto" class="custom-file-input" placeholder="<?php echo $this->lang->line('result_document');?>"  autofocus="autofocus">
                <label for="foto" class="custom-file-label"><?php echo $this->lang->line('result_document');?></label>
                </div>
              </div>
</div>



<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <a class="btn btn-primary btn-block"   id="btnUploadEid">Upload</a>
               
               
                </div>
              </div>
</div>

        </form>

        </div>
         <!-- Modal footer -->
         <div class="modal-footer bg-merah">&nbsp;
          &nbsp;
        </div>
        
      </div>
    </div>
  </div>




  <div class="modal fade" id="modalView">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-hijau">
          <h4 class="modal-title"><i class="fas fa-eye"></i>&nbsp;<?php echo $this->lang->line('result_view');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">

        <div class="form-row">
<div class="col-md-12">
                <div class="form-label-group">
              
              
                <input type="text" value=""  id="patient_regnas" name="patient_regnas" class="form-control"  required="required" autofocus="autofocus" readonly="readonly">

                  
                <label for="patient_regnas"><?php echo $this->lang->line('patient_regnas');?></label>
            
                </div>
              </div>
</div>



<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
                <div class="form-label-group">
              
               
                <input type="text" value=""  id="specimen_id" name="specimen_id" class="form-control"  required="required" autofocus="autofocus" readonly="readonly">

                  
                <label for="specimen_id"><?php echo $this->lang->line('specimen_id');?></label>
            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
                <div class="form-label-group">
              
               
                <input type="text"  id="specimen_exam_date" name="specimen_exam_date" class="form-control tanggal"  required="required" autofocus="autofocus" readonly="readonly">

                  
                <label for="specimen_exam_date"><?php echo $this->lang->line('specimen_exam_date');?></label>
            
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
                <div class="form-label-group">
              
               
                <input type="text"  id="specimen_date_release" name="specimen_date_release" class="form-control tanggal"  required="required" autofocus="autofocus" readonly="readonly">

                  
                <label for="specimen_date_release"><?php echo $this->lang->line('specimen_date_release');?></label>
            
                </div>
              </div>
</div>



<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
             
                </div>
              </div>
<div class="form-row">

<div class="col-md-12"  style="margin-top: 10px;">
                <div class="form-check" style="margin-left:20px">
                
               
                
            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
<div class="form-label-group">
                
<input type="text"  id="specimen_vl_result" name="specimen_vl_result" class="form-control"   autofocus="autofocus" readonly="readonly">         
<label for="specimen_vl_result" id="lab_result"> <?php echo $this->lang->line("result_vl_test");?></label>
               
                </div>
              </div>
</div>

        
        </div>
         <!-- Modal footer -->
         <div class="modal-footer bg-hijau">&nbsp;

         <?php if($this->session->userdata("user_hf_group")=='5'){ ?>
<a href="<?php echo base_url()."transactional/result/vlresultedit/".$list->specimen_num_id;?>" class="btn btn-danger btn-block btedit" onclick="result('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>
         <?php }?>
          &nbsp;
        </div>
        
      </div>
    </div>
  </div>


  <!--- view EID REsult-->
  <div class="modal fade" id="modalViewEid">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-hijau">
          <h4 class="modal-title"><i class="fas fa-eye"></i>&nbsp;<?php echo $this->lang->line('result_view');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">

        <div class="form-row">
<div class="col-md-12">
                <div class="form-label-group">
              
              
                <input type="text" value=""  id="patient_eid_mother_regnas"  class="form-control"  required="required" autofocus="autofocus" readonly="readonly">

                  
                <label for="patient_eid_mother_regnas"><?php echo $this->lang->line('mother_regnas');?></label>
            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12"   style="margin-top: 10px;">
                <div class="form-label-group">
              
              
                <input type="text" value=""  id="patient_eid_mother_nid"  class="form-control"  required="required" autofocus="autofocus" readonly="readonly">

                  
                <label for="patient_eid_mother_nid"><?php echo $this->lang->line('mother_nid');?></label>
            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
                <div class="form-label-group">
              
               
                <input type="text" value=""  id="specimen_eid_id" name="specimen_eid_id" class="form-control"  required="required" autofocus="autofocus" readonly="readonly">

                  
                <label for="specimen_id"><?php echo $this->lang->line('specimen_id');?></label>
            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
                <div class="form-label-group">
              
               
                <input type="text"  id="specimen_eid_exam_date" name="specimen_exam_date" class="form-control tanggal"  required="required" autofocus="autofocus" readonly="readonly">

                  
                <label for="specimen_exam_date"><?php echo $this->lang->line('specimen_exam_date');?></label>
            
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
                <div class="form-label-group">
              
               
                <input type="text"  id="specimen_eid_date_release" name="specimen_date_release" class="form-control tanggal"  required="required" autofocus="autofocus" readonly="readonly">

                  
                <label for="specimen_date_release"><?php echo $this->lang->line('specimen_date_release');?></label>
            
                </div>
              </div>
</div>



<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
             
                </div>
              </div>
<div class="form-row">

<div class="col-md-12"  style="margin-top: 10px;">
                <div class="form-check" style="margin-left:20px">
                
               
                
            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
<div class="form-label-group">
                
<input type="text"  id="specimen_eid_result" name="specimen_eid_result" class="form-control"   autofocus="autofocus" readonly="readonly">         
<label for="specimen_vl_result" id="lab_result"> <?php echo $this->lang->line("result");?></label>
               
                </div>
              </div>
</div>

        
        </div>
         <!-- Modal footer -->
         <div class="modal-footer bg-hijau">&nbsp;

         <?php if($this->session->userdata("user_hf_group")=='5'){ ?>
<a href="<?php echo base_url()."transactional/result/vlresultedit/".$list->specimen_num_id;?>" class="btn btn-danger btn-block btedit" onclick="result('<?php echo $list->specimen_num_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>
         <?php }?>
          &nbsp;
        </div>
        
      </div>
    </div>
  </div>
  <!-- end EID Result -->
<script>

function upload(id){
  $('#modalFoto').modal({ backdrop: 'static',
                            keyboard: false,
                            show:true});
   $('#specimen_num_id').val(id);
}




function eidupload(id){
  $('#modalFotoEid').modal({ backdrop: 'static',
                            keyboard: false,
                            show:true});
   $('#specimen_num_id_eid').val(id);
}


function resultview(id){
  $.ajax({
    url:"<?php echo base_url()."transactional/result/viewresult/";?>"+id,
    type:"POST",
    dataType:"json",
    success:function(jdata){
      $('#patient_regnas').val(jdata.response.patient_regnas);
      $('#specimen_id').val(jdata.response.specimen_id);
      $('#specimen_exam_date').val(formatDate(jdata.response.vl_result_exam_date));
      $('#specimen_date_release').val(formatDate(jdata.response.vl_result_date_release));
      $('.btedit').attr("href","<?php echo base_url()."transactional/result/vlresultedit/";?>"+jdata.response.vl_result_specimen_num_id);
      if(jdata.response.vl_result_vlresult_optional=='3'){
        $("#specimen_vl_result").val(jdata.response.vl_result_vlresult);
      }else if(jdata.response.vl_result_vlresult_optional=='1'){
        $("#specimen_vl_result").val(">10.000.000");
      }else if(jdata.response.vl_result_vlresult_optional=='2'){
        $("#specimen_vl_result").val("<40");
      }else if(jdata.response.vl_result_vlresult_optional=='4'){
        $("#specimen_vl_result").val("Tidak Terdeteksi");
      }else if(jdata.response.vl_result_vlresult_optional=='5'){
        $("#specimen_vl_result").val("INVALID");
      }else if(jdata.response.vl_result_vlresult_optional=='6'){
        $("#specimen_vl_result").val("ERROR");
      }else if(jdata.response.vl_result_vlresult_optional=='7'){
        $("#specimen_vl_result").val("Tidak ada Hasil");
      }
      $('#modalView').modal({ backdrop: 'static',
                            keyboard: false,
                            show:true});
    }
  })
  
}

function eidresultview(id){
  $.ajax({
    url:"<?php echo base_url()."transactional/result/eidviewresult/";?>"+id,
    type:"POST",
    dataType:"json",
    success:function(jdata){
      $('#patient_eid_mother_regnas').val(jdata.response.patient_eid_mother_regnas);
      $('#patient_eid_mother_nid').val(jdata.response.patient_eid_mother_nid);
      $('#specimen_eid_id').val(jdata.response.specimen_id);
      $('#specimen_eid_exam_date').val(formatDate(jdata.response.eid_result_exam_date));
      $('#specimen_eid_date_release').val(formatDate(jdata.response.eid_result_date_release));
      $('.btedit').attr("href","<?php echo base_url()."transactional/result/eidresultedit/";?>"+jdata.response.eid_result_specimen_num_id);

      
      if(jdata.response.eid_result_eidresult=='1'){
        $("#specimen_eid_result").val("Tidak Terdeteksi");
      }else if(jdata.response.eid_result_eidresult=='2'){
        $("#specimen_eid_result").val("Terdeteksi");
      }else if(jdata.response.eid_result_eidresult=='3'){
        $("#specimen_eid_result").val("Invalid");
      }else if(jdata.response.eid_result_eidresult=='4'){
        $("#specimen_eid_result").val("ERROR");
      }else if(jdata.response.eid_result_eidresult=='5'){
        $("#specimen_eid_result").val("Tidak ada hasil");
      }
      $('#modalViewEid').modal({ backdrop: 'static',
                            keyboard: false,
                            show:true});
    }
  })
  
}

function finish(id){
  if(confirm('Apakah anda yakin sudah selesai?')){
    document.location = "<?php echo base_url()."transactional/result/finish/";?>"+id;
  }
}
    $('document').ready(function(){
$('#search').keyup(function(){
  $('#dataBody').load('<?php echo base_url()."transactional/result/searchspecimen/".$this->uri->segment('4');?>',{"search":$('#search').val()});
})

      $('#btnUpload').click(function(){
  $(".preloader").fadeIn();
  $('#modalFoto').hide();
  var dataForm = new FormData($('#formFoto')[0]);
  $.ajax({
  url:"<?php echo base_url()."transactional/result/vlupdate";?>",
  type:'POST',
  dataType:'json',
  cache: false,
  contentType: false,
  processData: false,
  data: dataForm,
  success:function(jdata){
      $(".preloader").fadeOut();
      if (jdata.status=='200') {
        
          alert(jdata.message);
          document.location="<?php echo base_url()."transactional/result/detail/".$this->uri->segment('4');?>";
          
      }else{
        $(".preloader").fadeOut();
          alert(jdata.error);

      }
  }
  
       });
});



$('#btnUploadEid').click(function(){
  $(".preloader").fadeIn();
  $('#modalFotoEid').hide();
  var dataForm = new FormData($('#formFotoEid')[0]);
  $.ajax({
  url:"<?php echo base_url()."transactional/result/eidupdate";?>",
  type:'POST',
  dataType:'json',
  cache: false,
  contentType: false,
  processData: false,
  data: dataForm,
  success:function(jdata){
      $(".preloader").fadeOut();
      if (jdata.status=='200') {
        
          alert(jdata.message);
          document.location="<?php echo base_url()."transactional/result/detail/".$this->uri->segment('4');?>";
          
      }else{
        $(".preloader").fadeOut();
          alert(jdata.error);

      }
  }
  
       });
});



        $('#patient_province').change(function(){
    $(".preloader").fadeIn();
    $.ajax({
        url : '<?php echo base_url()."master/district/districtbyprovince";?>',
        type:'POST',
        dataType:'json',
        data:{
            'province_code':$('#patient_province').val()
        },
        success:function(jdata){

            var str ='<option value=""><?php echo $this->lang->line('district');?></option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
            })
            $('#patient_district').html(str);
            $(".preloader").fadeOut();
        }
    })
})





        
    $('#btnSubmit').click(function(){
        $.ajax({
            url:"<?php echo base_url()."transactional/specimen/add";?>",
            type:"POST",
            dataType:"json",
            data:{
                "patient_name":$('#patient_name').val(),
                "patient_nid":$("#patient_nid").val(),
                "patient_bday":$('#patient_bday').val(),
                "patient_sex":$('#patient_sex').val(),
                "patient_regnas":$('#patient_regnas').val(),
                "specimen_art_date":$('#specimen_art_date').val(),
                "specimen_order_id":<?php echo $this->uri->segment(4);?>,
                "specimen_id":$('#specimen_id').val(),
                "specimen_date_collected":$('#specimen_date_collected').val(),
                "specimen_type":$('#specimen_type').val(),
                "specimen_exam_category":$('#specimen_exam_categori').val()
            },
            success:function(jdata){
                if(jdata.status=='success'){
                    alert(jdata.message);
                    document.location = "<?php echo base_url()."transactional/specimen/list/";?>"+jdata.orderid;
                }

            }
        })
    });
    

    });
</script>