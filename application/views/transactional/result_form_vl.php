<style>.datepicker{z-index:1151 !important;}

label{
  color: red;
  margin-left: 10px;
}
.form-check-label{
  color:black;
  margin-left: 10px;
}
</style>
<nav aria-label="breadcrumb" style="margin-top: 50px;">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item"><a href="<?php echo base_url()."transactional/result/detail/".$specimen->specimen_order_id;?>"><i class="fas fa-vial"></i>&nbsp;<?php echo $this->lang->line('result');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('result');?></li>
  </ol>
</nav>

<div class="card  mx-auto"  style="width:600px">
<div class="card-header bg-merah" id="headercard">&nbsp;<h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('result');?></h4>
</div>
<div class="card-body col-md-12">
<form id="result_vl" method="POST">


<div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text"><?php echo $this->lang->line('patient_regnas');?></span>
    </div>
              
                <input type="hidden" value="<?php echo $specimen->patient_id;?>"  id="patient_id" name="patient_id">
                <input type="hidden" value="<?php echo $specimen->specimen_num_id;?>"  id="specimen_num_id" name="specimen_num_id">
               
                <input type="text" value="<?php echo $specimen->patient_regnas;?>"  id="patient_regnas" name="patient_regnas" class="form-control"  required="required" autofocus="autofocus" readonly="readonly">
            
</div>



<div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text"><?php echo $this->lang->line('specimen_id');?></span>
    </div>
               
                <input type="text" value="<?php echo $specimen->specimen_id;?>"  id="specimen_id" name="specimen_id" class="form-control"  required="required" autofocus="autofocus" readonly="readonly">

                  
               
</div>


<div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text"><?php echo $this->lang->line('specimen_exam_date');?></span>
    </div>
               
                <input type="text"  id="specimen_exam_date" name="specimen_exam_date" class="form-control tanggal"  required="required" autofocus="autofocus">

                  
               
</div>

<div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text"><?php echo $this->lang->line('specimen_date_release');?></span>
    </div>
                <input type="text"  id="specimen_date_release" name="specimen_date_release" class="form-control tanggal"  required="required" autofocus="autofocus">

                  
              
</div>

<div class="card card-body">
<span><i class="fas fa-edit"></i> <?php echo $this->lang->line("result");?> </span> 



<div class="form-row">

<div class="col-md-12"  style="margin-top: 10px;">
                <div class="form-check" style="margin-left:20px">
                <input class="form-check-input" type="radio" name="specimen_vl_result_option" id="specimen_vl_result_option3" value="3">
  <label class="form-check-label" for="specimen_vl_result_option3">Input Angka Absolute</label>
              <br>
                <input class="form-check-input" type="radio" name="specimen_vl_result_option" id="specimen_vl_result_option1" value="1">
  <label class="form-check-label" for="specimen_vl_result_option1">DETECTED &gt; 10<sup>7</sup> copies/mL</label><br>

  <input class="form-check-input" type="radio" name="specimen_vl_result_option" id="specimen_vl_result_option2" value="2">
  <label class="form-check-label" for="specimen_vl_result_option2">DETECTED &lt; 40 copies/mL</label>
              <br>

              <input class="form-check-input" type="radio" name="specimen_vl_result_option" id="specimen_vl_result_option4" value="4">
  <label class="form-check-label" for="specimen_vl_result_option4">NOT DETECTED</label>
              <br>

              <input class="form-check-input" type="radio" name="specimen_vl_result_option" id="specimen_vl_result_option5" value="5">
  <label class="form-check-label" for="specimen_vl_result_option5">INVALID</label>
              <br>
              <input class="form-check-input" type="radio" name="specimen_vl_result_option" id="specimen_vl_result_option5" value="6">
  <label class="form-check-label" for="specimen_vl_result_option6">ERROR</label>
              <br>

              <input class="form-check-input" type="radio" name="specimen_vl_result_option" id="specimen_vl_result_option7" value="7">
  <label class="form-check-label" for="specimen_vl_result_option7">NO RESULT</label>
              <br>
               
                
            
                </div>
              </div>
</div>






<div class="input-group mb-3 lab_result" style="display:none">
    <div class="input-group-prepend">
      <span class="input-group-text"><?php echo $this->lang->line('result_absolut');?></span>
    </div>
                
<input type="text"  id="specimen_vl_result" name="specimen_vl_result" class="form-control"    autofocus="autofocus">         
               
</div>

</div>





<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
<div class="form-label-group">
                
                <!-- <a class="btn btn-success btn-block"   id="btnSubmit"><?php echo $this->lang->line('submit');?></a>-->
                <button id="btnSubmit" class="btn btn-success btn-block" type="submit"><?php echo $this->lang->line("submit");?></button>
               
               
                </div>
              </div>
</div>


</form>


</div>
<div class="card-footer bg-merah"> &nbsp;
</div>
</div>









<!-- Update Foto -->
<div class="modal fade" id="modalFoto">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-merah">
          <h4 class="modal-title"><i class="fas fa-images"></i>&nbsp;<?php echo $this->lang->line('result_document');?></h4>
          <button type="button" class="close" id="closeBtn">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
        <form id="formFoto">
        <input type="hidden" id="specimen_num_id" name="specimen_num_id" value="<?php echo $specimen->specimen_num_id;?>">
        <div class="form-row">
<div class="col-md-12 text-center" style="padding-top:10px">
                <img src="" id="preview" style="width:250px">
              </div>
</div>

        <div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <input type="file" id="foto" name="foto" class="custom-file-input" placeholder="<?php echo $this->lang->line('result_document');?>"  autofocus="autofocus">
                <label for="foto" class="custom-file-label"><?php echo $this->lang->line('result_document');?></label>
                </div>
              </div>
</div>



<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <a class="btn btn-primary btn-block"   id="btnUpload">Upload</a>
               
               
                </div>
              </div>
</div>

        </form>

        </div>
         <!-- Modal footer -->
         <div class="modal-footer bg-merah">&nbsp;
        </div>
        
      </div>
    </div>
  </div>





<script>
    $('document').ready(function(){

      $("#specimen_vl_result").keypress(function (e) {
     //if the letter is not digit then display error and don't type anything
     if (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) {
        //display error message
               return false;
    }
   });

   $('.tanggal').attr("readonly","readonly");
      $('.tanggal').datepicker({
  format:"dd-mm-yyyy",
  startView:"year",
  minView:"year"
}).on('changeDate',function(ev){
  $(this).blur();
  $(this).datepicker('hide');
});

$('input[name="specimen_vl_result_option"]').change(function(){
  if($(this).val()=='3'){


    $('#specimen_vl_result').show().attr("required","required").focus();
    $('.lab_result').show();
  }else{
    $('#specimen_vl_result').hide().removeAttr("required");
    $('.lab_result').hide();
  }
})

$('#closeBtn').click(function(){
  $('#modalFoto').modal('hide');
  document.location="<?php echo base_url()."transactional/result/detail/".$specimen->specimen_order_id;?>";
})       


$('#result_vl').validate({
  rules:{
    specimen_vl_result_option:{
      required:true
    },
    specimen_vl_result:{
      required:'#specimen_vl_result_option3:checked',
      number:true
    }
  },
  messages:{
    specimen_vl_result_option:"<?php echo $this->lang->line("required");?>",
    specimen_vl_result:"<?php echo $this->lang->line("required");?>",
    specimen_exam_date:"<?php echo $this->lang->line("required");?>",
    specimen_date_release:"<?php echo $this->lang->line("required");?>"
  },
  submitHandler:function(form){
    $('#btnSubmit').attr("disabled","disabled");
    $.ajax({
                url:"<?php echo base_url()."transactional/result/vlinsert";?>",
                type:"POST",
                dataType:"json",
                data:{
                    "vl_result_patient_id":$('#patient_id').val(),
                    "vl_result_specimen_num_id":$('#specimen_num_id').val(),
                    "vl_result_specimen_id":$('#specimen_id').val(),
                    "vl_result_exam_date":formatDate($('#specimen_exam_date').val()),
                    "vl_result_date_release":formatDate($('#specimen_date_release').val()),
                    "vl_result_vlresult":$('#specimen_vl_result').val(),
                    "vl_result_vlresult_optional":$('input[name="specimen_vl_result_option"]:checked').val()
                },
                success:function(jdata){
                    if(jdata.status=='success'){
                        //alert(jdata.message);
                        if(confirm(jdata.message+" Apakah anda Akan mengupload scan hasil pemeriksaan sekarang?")){
                          $('#modalFoto').modal({
                            backdrop: 'static',
                            keyboard: false,
                            show:true
                          });
                          $('#specimen_num_id').val('<?php echo $specimen->specimen_num_id;?>');
                        }else{
                        document.location="<?php echo base_url()."transactional/result/detail/".$specimen->specimen_order_id;?>";
                        }
                      }
                }
            })
  }
});
$('#btnSubmits').click(function(){
          $(this).attr('disabled','disabled');
            $.ajax({
                url:"<?php echo base_url()."transactional/result/vlinsert";?>",
                type:"POST",
                dataType:"json",
                data:{
                    "vl_result_patient_id":$('#patient_id').val(),
                    "vl_result_specimen_num_id":$('#specimen_num_id').val(),
                    "vl_result_specimen_id":$('#specimen_id').val(),
                    "vl_result_exam_date":formatDate($('#specimen_exam_date').val()),
                    "vl_result_date_release":formatDate($('#specimen_date_release').val()),
                    "vl_result_vlresult":$('#specimen_vl_result').val(),
                    "vl_result_vlresult_optional":$('input[name="specimen_vl_result_option"]:checked').val()
                },
                success:function(jdata){
                    if(jdata.status=='success'){
                        //alert(jdata.message);
                        if(confirm(jdata.message+" Apakah anda Akan mengupload scan hasil pemeriksaan sekarang?")){
                          $('#modalFoto').modal({
                            backdrop: 'static',
                            keyboard: false,
                            show:true
                          });
                          $('#specimen_num_id').val('<?php echo $specimen->specimen_num_id;?>');
                        }else{
                        document.location="<?php echo base_url()."transactional/result/detail/".$specimen->specimen_order_id;?>";
                        }
                      }
                }
            })
        });







        $('#btnUpload').click(function(){
  $(".preloader").fadeIn();
  $('#modalFoto').hide();
  var dataForm = new FormData($('#formFoto')[0]);
  $.ajax({
  url:"<?php echo base_url()."transactional/result/vlupdate";?>",
  type:'POST',
  dataType:'json',
  cache: false,
  contentType: false,
  processData: false,
  data: dataForm,
  success:function(jdata){
      $(".preloader").fadeOut();
      if (jdata.status=='200') {
        
          alert(jdata.message);
          document.location="<?php echo base_url()."transactional/result/detail/".$specimen->specimen_order_id;?>";
          
      }else{
        $(".preloader").fadeOut();
          alert(jdata.error);

      }
  }
  
       });
});



    });
    </script>