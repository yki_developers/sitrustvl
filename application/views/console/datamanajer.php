

    <style>
.datepicker{z-index:1151 !important;}
label{
  color: red;
  margin-left: 10px;
}
</style>

    <div class="card text-center">
      <div class="card-header  bg-merah" style="font-size:20pt;margin-top:50px"><?php echo $this->lang->line('welcome')."&nbsp; ".$this->session->userdata('user_fname')." ".$this->session->userdata('user_lname');?></div>
      <div class="card-header">
      <ul class="nav nav-tabs card-header-tabs">

      
     <li class="nav-item">
        <a class="nav-link menu" id="home" href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a>
      </li>


<?php if($this->session->userdata("user_group")=='6'){ ?>
     <li class="nav-item">
        <a class="nav-link  menu" id="user" href="<?php echo base_url()."dashboard/faskes";?>"><i class="fas fa-bar"></i>&nbsp;<?php echo $this->lang->line('dashboard');?></a>
      </li>
<?php  } ?>

<?php if($this->session->userdata("user_adminlevel")=='1' && $this->session->userdata("user_group")!='1'){?>

  <li class="nav-item">
        <a class="nav-link  menu" id="user" href="<?php echo base_url()."account/user/list/1";?>"><i class="fas fa-user"></i>&nbsp;<?php echo $this->lang->line('user');?></a>
      </li>

<?php }?>
<?php if($this->session->userdata("user_group")=='8'){?>

  <li class="nav-item">
       <a class="nav-link  active" id="datamanajer"><i class="fas fa-tools"></i>&nbsp;<?php echo $this->lang->line('datacontrol');?></a>
     </li>

<?php }?>

<li class="nav-item">
        <a class="nav-link  menu" id="chpassword" href="<?php echo base_url()."account/profile";?>"><i class="fas fa-user"></i>&nbsp;<?php echo $this->lang->line('change_password');?></a>
      </li>

      <li class="nav-item">
        <a class="nav-link  menu" id="dataartikel" href="<?php echo base_url();?>login/logout"><i class="fas fa-sign-out-alt"></i>&nbsp;<?php echo $this->lang->line('logout');?></a>
      </li>
   



      
    </ul>

      </div>


     
          


         
          <div class="card">
              <div class="card-header bg-hijau">&nbsp;</div>
              <div class="card-body">

          <div class="card-body">
       
          <a href="#">
          <div class="btn btn-outline-info col-md-2 text-center " id="merger" style="margin:5px">
            <i class="far fa-edit fa-5x"></i><br>Penggabungan Order
          </div></a>

          <a href="#">
          <div class="btn btn-outline-info col-md-2 text-center disabled" id="edits" style="margin:5px">
            <i class="far fa-edit fa-5x"></i><br>Edit Order&nbsp; <span class="badge badge-danger">Disabled</span>
          </div>
         
        </a>

          </div>
              </div>
              <div class="card-footer">&nbsp;</div>
            </div>
          </div>

         
</div>

    
      

</div>
</div>


<!--- Modal Merger -->

<div class="modal fade" id="modalMerge">
    <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">

        <div class="modal-header bg-hijau">
        <i class="fas fa-edit"></i>&nbsp;Penggabungan Order</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <div class="modal-body">

        <form id="mergerForm">

        <div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text">Order id</span>
    </div>
    <input type="text" value="" id="order_origin" name="order_origin" class="form-control" placeholder="Gunakan tanda koma (,) untuk lebih dari satu order" required="required" autofocus="autofocus">&nbsp;
  </div>



  <div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text">Order Tujuan</span>
    </div>
    <input type="text" value="" id="order_dest" name="order_dest" class="form-control"  required="required" autofocus="autofocus">&nbsp;
  </div>


  <div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                  <button type="submit" class="btn btn-outline-success" id="btnSubmit"><?php echo $this->lang->line('submit');?></button>
                </div>
              </div>
</div>

        </form>

        </div>
        <div class="modal-footer">
            &nbsp;
        </div>

    </div>
    </div>
</div>



<!---- MODAL ORDER -->


<div class="modal fade" id="modalOrder">
    <div class="modal-dialog modal-dialog-centered modal-lg">
    <div class="modal-content">

        <div class="modal-header bg-hijau">
        <i class="fas fa-edit"></i>&nbsp;Edit Order</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>

        <div class="modal-body">

        <form id="orderIdform">

        <div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text"><i class="fas fa-search"></i>&nbsp;Order id</span>
    </div>
    <input type="text" value="" id="order_id" name="order_id" class="form-control"  required="required" autofocus="autofocus">&nbsp;

    <div class="input-group-append">
     <button type="button" class="btn btn-outline-success" id="btnSearch"><?php echo $this->lang->line('search');?></button>
    </div>
  </div>



        </form>

        </div>
        <div class="modal-footer">
            &nbsp;
        </div>

    </div>
    </div>
</div>


<!-- Modal Edit ORder -->

<div class="modal fade" id="modalForm">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-merah">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('order');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form method="POST"  id="orderForm">
        <input type="hidden" id="order_reorder" value="0">
        <input type="hidden" id="order_id" name="order_id">
        <input type="hidden" id="order_approved" name="order_approved">
        <input type="hidden" id="order_status" name="order_status">

        <div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text"><?php echo $this->lang->line('order_date');?></span>
    </div>
    <input type="text" value="<?php echo date("d-m-Y");?>" id="order_date" name="order_date" class="form-control tanggal" required="required" autofocus="autofocus">&nbsp;
  </div>


  <div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text"><?php echo $this->lang->line("order_destination");?></span>
    </div>
    <select name="order_hf_recipient" id="order_hf_recipient" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("order_destination");?>">
                      <option value="">&nbsp;</option>
                  <?php

foreach($lab as $lablist){
    $qn = $this->M_reverse->hfName($lablist->labnetwork_destination)
?>
<option value="<?php echo $lablist->labnetwork_destination; ?>"><?php echo $qn->hf_name;?></option>
<?php
}
?>

</select>
  </div>



  <div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text"><?php echo $this->lang->line("order_courier");?></span>
    </div>
    <select name="order_shipper_id" id="order_shipper_id" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("order_destination");?>">
                      <option value="">&nbsp;</option>
                  <?php

foreach($kurir as $list){
?>
<option value="<?php echo $list->shipper_code; ?>"><?php echo $list->shipper_name;?></option>
<?php
}
?>

</select>
  </div>

        





<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                  <button type="submit" class="btn btn-outline-success" id="btnSubmit"><?php echo $this->lang->line('submit');?></button>
                  <button type="button" class="btn btn-outline-danger" id="tutup">Tutup</button>
              <!--  <a class="btn btn-success btn-block"   id="btnSubmit"><?php echo $this->lang->line('submit');?></a> -->
               
               
                </div>
              </div>
</div>

        </form>
        </div>
      </div>
    </div>
</div>

<!-- END MODAL -->

<script>
$(document).ready(function(){

    $('#edit').click(function(){
        $('#modalOrder').modal({
          backdrop:"static",
          keyboard:false,
          show:true
      })

    });

  $('#merger').click(function(){
      $('#modalMerge').modal({
          backdrop:"static",
          keyboard:false,
          show:true
      })

  })

  $('#mergerForm').validate({
      messages:{
          order_origin:"<?php echo $this->lang->line("required");?>",
          order_dest:"<?php echo $this->lang->line("required");?>"
      },
      submitHandler:function(form){
          $.ajax({
              url:"<?php echo base_url()."console/datamanajer/mergersubmit";?>",
              type:"POST",
              dataType:"json",
              data:{
                  "order_origin":$('#order_origin').val(),
                  "order_dest":$('#order_dest').val()
              },
              success:function(jdata){
                if(jdata.status=='success'){  
                alert(jdata.message);
                $('#modalMerge').modal('hide');
                }else{
                    alert(jdata.message);
                }

              }
          })
      }
  });
})
</script>