<nav aria-label="breadcrumb"   style="margin-top:50px">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('subdistrict');?></li>
  </ol>
</nav>



<!---- Form Add New Kecamatan -->
<div class="modal fade" id="modalForm">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-merah">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('data_form');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form method="POST" action="<?php echo base_url()."master/subdistrict/add";?>" id="newkec">
        <div class="form-row">
<div class="col-md-12">
                <div class="form-label-group">
              
                  <select name="province_code" id="province_code" class="form-control custom-select"  required="required" placeholder="Nama Propinsi">
                      <option value=""><?php echo $this->lang->line('province');?></option>
                  <?php

foreach($province as $lpropinsi){
?>
<option value="<?php echo $lpropinsi->province_code;?>"><?php echo $lpropinsi->province_name;?></option>
<?php
}
?>

</select>

            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12"  style="padding-top:10px">
                <div class="form-label-group">
              
                  <select name="district_code" id="district_code" class="form-control custom-select"  required="required" placeholder="Nama Kabupaten">
                      <option value=""><?php echo $this->lang->line('district');?></option>
                    </select>

            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                  <input type="text" id="subdistrict_code" name="subdistrict_code" class="form-control" placeholder="<?php echo $this->lang->line('subdistrict_code');?>" required="required" autofocus="autofocus">
                  <label for="subdistrict_code"><?php echo $this->lang->line('subdistrict_code');?></label>
                </div>
              </div>
</div>
<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                  <input type="text" id="subdistrict_name" name="subdistrict_name" class="form-control" placeholder="<?php echo $this->lang->line('subdistrict_name');?>" required="required" autofocus="autofocus">
                  <label for="subdistrict_name"><?php echo $this->lang->line('subdistrict_name');?></label>
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <a class="btn btn-primary btn-block"   id="btnSubmit"><?php echo $this->lang->line('submit');?></a>
               
               
                </div>
              </div>
</div>


</form>

        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer bg-merah">
          &nbsp;
        </div>
        
      </div>
    </div>
  </div>

<!---- End Form -->







<!---- Form EDIT Kecamatan -->
<div class="modal fade" id="modalEdit">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-merah">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('data_form');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form method="POST" action="<?php echo base_url()."master/subdistrict/update";?>" id="newkec">

        <input type="hidden" name="idkecamatanMST" id="idkecamatanMST" value="">
        <div class="form-row">
<div class="col-md-12">
                <div class="form-label-group">
              
                  <select name="idpropinsiEdit" id="idpropinsiEdit" class="form-control custom-select"  required="required" placeholder="Nama Propinsi">
                      <option value=""><?php echo $this->lang->line('province');?></option>
                  <?php

foreach($province as $lpropinsi){
?>
<option value="<?php echo $lpropinsi->province_code;?>"><?php echo $lpropinsi->province_name;?></option>
<?php
}
?>

</select>

            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12"  style="padding-top:10px">
                <div class="form-label-group">
              
                  <select name="idkabupatenEdit" id="idkabupatenEdit" class="form-control custom-select"  required="required" placeholder="Nama Kabupaten">
                      <option value=""><?php echo $this->lang->line('district');?></option>
                    </select>

            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                  <input type="text" id="idkecamatanEdit" name="idkecamatanEdit" class="form-control" placeholder="Kode Kecamatan" required="required" autofocus="autofocus">
                  <label for="idkecamatan"><?php echo $this->lang->line('subdistrict_code');?></label>
                </div>
              </div>
</div>
<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                  <input type="text" id="nama_kecamatanEdit" name="nama_kecamatanEdit" class="form-control" placeholder="Nama kecamatan" required="required" autofocus="autofocus">
                  <label for="nama_kecamatan"><?php echo $this->lang->line('subdistrict_name');?></label>
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <a class="btn btn-primary btn-block"   id="btnUpdate">Update</a>
               
               
                </div>
              </div>
</div>


</form>

        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer bg-merah">
          &nbsp;
        </div>
        
      </div>
    </div>
  </div>

<!---- End Form -->

<div class="card">
<div class="card-header bg-merah" id="headercard">
<?php
$max = ceil($total/50);
  if($this->uri->segment(4)>5 && ($this->uri->segment(4)+5)>6){

  
   if($this->uri->segment(4)==$max || ($this->uri->segment(4)+2)>$max){ 
     $maxnum = 1+$this->uri->segment(4);
  }else{ 
    $maxnum = 3+$this->uri->segment(4);
  }
  
  $start = $maxnum-5;
  $prev = $start-1; 
  

}else{
    
      $maxnum = 6;
      $start = 1;
      $prev = 1;

  }
 // $ix = (($this->uri->segment('3')*50)+1)-50;
  ?>
<ul class="pagination pagination-sm justify-content-center">
  <li class="page-item prev"><a class="page-link fas fa-arrow-circle-left" href="<?php echo base_url()."master/subdistrict/list/".$prev;?>">&nbsp;</a></li>
  

<?php
//echo $max;
  for($i=$start;$i<=($maxnum-1);$i++){

    if($i==$this->uri->segment(4)){
        ?>
        <li class="page-item active" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo base_url()."master/subdistrict/list/".$i;?>"><?php echo $i;?></a></li>
        <?php

    }else{

      ?>
<li class="page-item" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo base_url()."master/subdistrict/list/".$i;?>"><?php echo $i;?></a></li>

      <?php
    }

  }
  ?>
  <li class="page-item next"><a class="page-link fas fa-arrow-circle-right" href="<?php echo base_url()."master/subdistrict/list/".$maxnum; ?>">&nbsp;</a></li>
</ul>


</div>
<div class="card-body">

<table class="table table-stripe">
<thead>
<tr class=>
    <th scope="col" colspan="4">
    <div class="input-group input-group-sm">
    <input type="text" id="search" name="search" class="form-control" placeholder="Pencarian">
    <div class="input-group-append"><span class="btn  btn-success" id="src"><i class="fas fa-search"></i></span></div>
</div>
</th>
    <th scope="col" colspan="2" class="text-right">
    &nbsp;
    <a href="#cardform" class="btn btn-success btn-xs" id="add" data-toggle="modal" data-target="#modalForm"><i class="fas fa-plus-circle"></i>&nbsp;<?php echo $this->lang->line('add');?></a>
</th>

</tr>

<tr>
    <th scope="col"><?php echo $this->lang->line('number');?></th>
    <th scope="col"><?php echo $this->lang->line('subdistrict_code');?></th>
    <th scope="col"><?php echo $this->lang->line('province_name');?></th>
    <th scope="col"><?php echo $this->lang->line('district_name');?></th>
    <th scope="col"><?php echo $this->lang->line('subdistrict_name');?></th>
    <th scope="col" class="d-none d-sm-block"><?php echo $this->lang->line('action');?></th>
</tr>
</thead>
<tbody id="dataBody">
    <?php

if($this->uri->segment(4)!='1'){    
$xi = ($this->uri->segment(4)-1)*50;
}else{
    $xi = 0;
}

    $i=1;
    foreach($response as $list){ ?>
    <tr>
    <th scope="row"><?php echo $i+$xi;?></th>
    <td scope="col"><?php echo $list->subdistrict_code;?></td>
    <td scope="col"><?php echo $list->province_name;?></td>
    <td scope="col"><?php echo $list->district_name;?></td>
    <td scope="col"><?php echo $list->subdistrict_name;?></td>
    <td scope="col" class="text-center d-none d-sm-block">
    <a href="#" class="btn btn-danger btn-sm" onclick="conf('<?php echo $list->subdistrict_code;?>')"><i class="fas fa-trash-alt"></i>&nbsp;<?php echo $this->lang->line('delete');?></a>
    &nbsp;
    <a href="#cardedit" id="btnedit" class="btn btn-success btn-sm"   onclick="edit('<?php echo $list->subdistrict_code;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>
    </td>
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>


</div>
<div class="card-footer text-center bg-merah">
&nbsp;
</div>
</div>

<script>

function edit(idkecamatan){
    $(".preloader").fadeIn();
  
  $.ajax({
    url:"<?php echo base_url()."master/subdistrict/detail/";?>"+idkecamatan,
    type:"POST",
    dataType : "json",
    data:{
      "subdistrict_code":idkecamatan
    },
    success:function(jsondata){
      $('#idkecamatanMST').val(jsondata.subdistrict_code);
      $('#idpropinsiEdit').val(jsondata.subdistrict_province);
      $('#idkecamatanEdit').val(jsondata.subdistrict_code);
      $('#nama_kecamatanEdit').val(jsondata.subdistrict_name);
      $('#idkabupatenEdit').val(jsondata.subdistrict_district);
      $.ajax({
        url : '<?php echo base_url()."master/district/districtbyprovince";?>',
        type:'POST',
        dataType:'json',
        data:{
            'province_code':jsondata.subdistrict_province
        },
        success:function(jdata){

            var str ='<option value=""><?php echo $this->lang->line('district');?></option>';
            $.each(jdata.response,function(i,item){
              if(item.district_code==jsondata.subdistrict_district){
                str +='<option value="'+item.district_code+'" selected="selected">'+item.district_name+'</option>';
              }else{
                str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
              }
            })
            $('#idkabupatenEdit').html(str);
            $('#modalEdit').modal('show');
            $(".preloader").fadeOut();
        }
    });
    }
  })
}


function conf(id){
  var n = confirm('<?php echo $this->lang->line("delete_confirmation");?>');
  
  if(n==true){
    $(".preloader").fadeIn();
document.location = "<?php echo base_url()."master/subdistrict/delete/";?>"+id;
  }
}

$(document).ready(function(){
var page = "<?php echo $this->uri->segment(4);?>";
var maxn = "<?php echo $max;?>";
var maxnum = "<?php echo $maxnum;?>";

$('#src').click(function(){
    $(".preloader").fadeIn();
    var search = $('#search').val();
    $('#dataBody').load('<?php echo base_url()."master/subdistrict/search";?>', 
            {
                "search": search

             });
})


$('#province_code').change(function(){
    $(".preloader").fadeIn();
    $.ajax({
        url :'<?php echo base_url()."master/district/districtbyprovince";?>',
        type:'POST',
        dataType:'json',
        data:{
            'province_code':$('#province_code').val()
        },
        success:function(jdata){

            var str ='<option value=""><?php echo $this->lang->line('district');?></option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
            })
            $('#district_code').html(str);
            $(".preloader").fadeOut();
        }
    })
});


$('#idpropinsiEdit').change(function(){
    $(".preloader").fadeIn();
    $.ajax({
        url : '<?php echo base_url()."master/district/districtbyprovince";?>',
        type:'POST',
        dataType:'json',
        data:{
            'province_code':$('#idpropinsiEdit').val()
        },
        success:function(jdata){

            var str ='<option value=""><?php echo $this->lang->line('district');?></option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
            })
            $('#idkabupatenEdit').html(str);
            $(".preloader").fadeOut();
        }
    })
});


$('#btnSubmit').click(function(){
    $(".preloader").fadeIn();
    $.ajax({
        url:'<?php echo base_url()."master/subdistrict/add";?>',
        type:'POST',
        dataType:'json',
        data:{
            'subdistrict_province':$('#province_code').val(),
            'subdistrict_district':$('#district_code').val(),
            'subdistrict_code':$('#subdistrict_code').val(),
            'subdistrict_name':$('#subdistrict_name').val()
        },
        success:function(jdata){
            if(jdata.status=='success'){
                alert(jdata.message);
                document.location = "<?php echo base_url()."master/subdistrict/list/1";?>";
            }else{
                alert('error');
            }
        }
    })
});


$('#btnUpdate').click(function(){
    //alert('bla');
  $(".preloader").fadeIn();
  $.ajax({
    url : "<?php echo base_url()."master/subdistrict/update/";?>"+$('#idkecamatanMST').val(),
    type : "POST",
    dataType :"json",
    data:{
    
      "subdistrict_province":$('#idpropinsiEdit').val(),
      "subdistrict_code":$('#idkecamatanEdit').val(),
      "subdistrict_district":$('#idkabupatenEdit').val(),
      "subdistrict_name":$('#nama_kecamatanEdit').val(),
      "id":$('#idkecamatanMST').val()
    },
    success:function(jdata){
        $(".preloader").fadeOut();
      if(jdata.status=='success'){
       
        alert(jdata.message);
        document.location = "<?php echo base_url()."master/subdistrict/list/1";?>";
      }else{
        alert(jdata.error);
      }

    },
    error:function(){
      alert('Internal ERROR');
      $(".preloader").fadeOut();
    }
  })

});


if(page=='' || page=='1'){
    $('.prev').addClass('disabled');
}

var m = parseInt(maxn)<parseInt(maxnum);
if(m==true){
    $('.next').addClass('disabled');
}

})
</script>