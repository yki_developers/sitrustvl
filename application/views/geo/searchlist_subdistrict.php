<?php 


    $i=1;
    foreach($response as $list){ ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->subdistrict_code;?></td>
    <td scope="col"><?php echo $list->province_name;?></td>
    <td scope="col"><?php echo $list->district_name;?></td>
    <td scope="col"><?php echo $list->subdistrict_name;?></td>
    <td scope="col" class="text-center d-none d-sm-block">
    <a href="#" class="btn btn-danger btn-sm" onclick="conf('<?php echo $list->subdistrict_code;?>')"><i class="fas fa-trash-alt"></i>&nbsp;<?php echo $this->lang->line('delete');?></a>
    &nbsp;
    <a href="#cardedit" id="btnedit" class="btn btn-success btn-sm"   onclick="edit('<?php echo $list->subdistrict_code;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>
    </td>
    
</tr>

    <?php $i++;
} ?>



<script>
$('document').ready(function(){
    $(".preloader").fadeOut();
})
</script>