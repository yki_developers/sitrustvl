<nav aria-label="breadcrumb"  style="margin-top:50px">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('province');?></li>
  </ol>
</nav>



<div class="card  card-register mx-auto mt-1 collapse " id="cardform" style="margin-bottom:10px;color:#ffffff">
<div class="card-header  bg-merah"><?php echo $this->lang->line('data_form');?></div>
<div class="card-body">

<form method="POST" action="<?php echo base_url()."wilayah/propinsi/add";?>" id="newprop">
<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                  <input type="text" id="province_code" name="province_code" class="form-control" placeholder="<?php echo $this->lang->line('province_code');?>" required="required" autofocus="autofocus">
                  <label for="province_code"><?php echo $this->lang->line('province_code');?></label>
                </div>
              </div>
</div>
<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                  <input type="text" id="province_name" name="province_name" class="form-control" placeholder="<?php echo $this->lang->line('province_name');?>" required="required" autofocus="autofocus">
                  <label for="province_name"><?php echo $this->lang->line('province_name');?></label>
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12" style="padding-top:10px;color:#ffffff">
                <div class="form-label-group">
                
                <a class="btn btn-success btn-block"   id="btnSubmit"><?php echo $this->lang->line('submit');?></a>
               
               
                </div>
              </div>
</div>


</form>



</div>

<div class="card-footer   bg-merah">
&nbsp;
</div>
</div>





<div class="card  card-register mx-auto mt-1 collapse" id="cardedit" style="margin-bottom:10px">
<div class="card-header   bg-merah"><?php echo $this->lang->line('data_form');?></div>
<div class="card-body">

<form method="POST" action="<?php echo base_url()."master/province/update/";?>" id="editprop">
<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                  <input type="text" id="editidpropinsi" name="province_code" class="form-control" placeholder="<?php echo $this->lang->line('province_code');?>" required="required" autofocus="autofocus" readonly="readonly">
                  <label for="editidpropinsi"><?php echo $this->lang->line('province_code');?></label>
                </div>
              </div>
</div>
<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                  <input type="text" id="editnama_propinsi" name="province_name" class="form-control" placeholder="><?php echo $this->lang->line('province_name');?>" required="required" autofocus="autofocus">
                  <label for="editnama_propinsi"><?php echo $this->lang->line('province_name');?></label>
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12" style="padding-top:10px;color:#ffffff">
                <div class="form-label-group">
                
                <a class="btn btn-success btn-block"   id="updateBtn"><?php echo $this->lang->line('update');?></a>
               
               
                </div>
              </div>
</div>


</form>



</div>

<div class="card-footer   bg-merah">
&nbsp;
</div>
</div>


<div class="card ">
<div class="card-header  bg-merah" id="headercard">

</div>
<div class="card-body">

<table class="table table-stripe">
<thead>
<tr>
    <th scope="col" colspan="3">
    <div class="input-group input-group-sm">
    <input type="text" id="search" name="search" class="form-control" placeholder="Pencarian">
    <div class="input-group-append"><span class="btn  btn-success" id="src"><i class="fas fa-search"></i></span></div>
</div>
</th>
    <th scope="col" class="text-right">
    &nbsp;
    <a href="#cardform" class="btn btn-success btn-xs" id="add" data-toggle="collapse"><i class="fas fa-plus-circle"></i>&nbsp;<?php echo $this->lang->line('add');?></a>
</th>

</tr>

<tr class="bg-merah" style="color:#ffffff">
    <th scope="col"><?php echo $this->lang->line('number');?></th>
    <th scope="col"><?php echo $this->lang->line('province_code');?></th>
    <th scope="col"><?php echo $this->lang->line('province_name');?></th>
    <th scope="col" class="d-none d-sm-block"><?php echo $this->lang->line('action');?></th>
</tr>
</thead>
<tbody id="dataBody">
    <?php 
    $i=1;
    foreach($prov as $list){ ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->province_code?></td>
    <td scope="col"><?php echo $list->province_name?></td>
    <td scope="col" class="text-center d-none d-sm-block">
    <a href="#" class="btn btn-danger btn-sm" onclick="conf('<?php echo $list->province_code?>')"><i class="fas fa-trash-alt"></i>&nbsp;<?php echo $this->lang->line('delete');?></a>
    &nbsp;
    <a href="#cardedit" id="btnedit" class="btn btn-success btn-sm"   onclick="getDetail('<?php echo $list->province_code;?>','<?php echo $list->province_name;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>
    </td>
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>


</div>
<div class="card-footer bg-merah"> &nbsp;</div>
</div>


<script>
function conf(id){
  var n = confirm('<?php echo $this->lang->line("delete_confirmation");?>');
  
  if(n==true){
   
document.location = "<?php echo base_url()."master/province/delete/";?>"+id;
  }
}

function getDetail(id,nama){
  $('#cardedit').show('slow');
$('#editidpropinsi').val(id);
$('#editnama_propinsi').val(nama);
}
    $(document).ready(function(){

      $('#btnedit').click(function(){
        $('#cardform').collapse('hide');
      })

      $('#add').click(function(){
        $('#cardedit').hide('slow');
       
      })
       
        $("#src").click(function(){
            var search = $('#search').val();
            
            $('#dataBody').load('<?php echo base_url()."master/province/search/";?>'+search);
        });



        $('#src').click(function(){
    $(".preloader").fadeIn();
    var search = $('#search').val();
    $('#dataBody').load('<?php echo base_url()."master/province/search/";?>', 
            {
                "search": search

             });
})



$('#updateBtn').click(function(){
  $(".preloader").fadeIn();
  $.ajax({
    type:"POST",
    dataType:"json",
    url:"<?php echo base_url()."master/province/update/";?>"+$('#editidpropinsi').val(),
    data:{
      "province_code":$('#editidpropinsi').val(),
      "province_name":$('#editnama_propinsi').val()
    },
    success:function(jdata){
            alert(jdata.message);
            $('#cardedit').slideUp('slow');
            $('#dataBody').load('<?php echo base_url()."master/province/updatelist";?>');
          },
          error:function(){
            alert('Internal Server Error');
          }
  })

});

$('#btnSubmit').click(function(){
  $(".preloader").fadeIn();
        $.ajax({
          type:"POST",
          dataType:"json",
          url:"<?php echo base_url()."master/province/add";?>",
          data:{
            "province_code":$('#province_code').val(),
            "province_name":$('#province_name').val()
          },
          success:function(jdata){
            $('#cardform').collapse('hide');
            alert(jdata.message);
            $('#dataBody').load('<?php echo base_url()."master/province/updatelist";?>');
          },
          error:function(){
            alert('Internal Server Error');
          }
        });


    });


    

    });
    </script>