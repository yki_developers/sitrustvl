<nav aria-label="breadcrumb"   style="margin-top:50px">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line("district");?></li>
  </ol>
</nav>


<!---- Form Add New Kabupaten -->
<div class="modal fade" id="modalForm">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header  bg-merah">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line("data_form");?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form method="POST" action="<?php echo base_url()."master/district/add";?>" id="newkab">
        <div class="form-row">
<div class="col-md-12">
                <div class="form-label-group">
              
                  <select name="province_code" id="province_code" class="form-control custom-select"  required="required">
                      <option value=""><?php echo $this->lang->line('province');?></option>
                  <?php

foreach($province as $lprop){
?>
<option value="<?php echo $lprop->province_code;?>"><?php echo $lprop->province_name;?></option>
<?php
}
?>

</select>

            
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                  <input type="text" id="district_code" name="district_code" class="form-control" placeholder="<?php echo $this->lang->line("district_code");?>" required="required" autofocus="autofocus">
                  <label for="district_code"><?php echo $this->lang->line("district_code");?></label>
                </div>
              </div>
</div>
<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                  <input type="text" id="district_name" name="district_name" class="form-control" placeholder="Nama kabupaten" required="required" autofocus="autofocus">
                  <label for="district_name"><?php echo $this->lang->line("district_name");?></label>
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12" style="padding-top:10px;color:#ffffff">
                <div class="form-label-group">
                
                <a class="btn btn-success btn-block"   id="btnSubmit"><?php echo $this->lang->line("submit");?></a>
               
               
                </div>
              </div>
</div>


</form>

        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer  bg-merah">
          &nbsp;
        </div>
        
      </div>
    </div>
  </div>

<!---- End Form -->



<!--- start Form Edit -->
<div class="modal fade" id="modalEdit">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header  bg-merah">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line("data_form");?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form>
        <input type="hidden" name="idkabupatenMST" id="idkabupatenMST" value="">
        <div class="form-row">
<div class="col-md-12">
                <div class="form-label-group">
              
                  <select name="idpropinsiEdit" id="idpropinsiEdit" class="form-control custom-select"  required="required" placeholder="Nama Propinsi">
                      <option value=""><?php echo $this->lang->line("province");?></option>
                  <?php

foreach($province as $lpropinsi){
?>
<option value="<?php echo $lpropinsi->province_code;?>"><?php echo $lpropinsi->province_name;?></option>
<?php
}
?>

</select>

            
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                  <input type="text" id="idkabupatenEdit" name="idkabupatenEdit" class="form-control" placeholder="Kode kabupaten" required="required" autofocus="autofocus">
                  <label for="idkabupaten"><?php echo $this->lang->line("district_code");?></label>
                </div>
              </div>
</div>
<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                  <input type="text" id="nama_kabupatenEdit" name="nama_kabupatenEdit" class="form-control" placeholder="Nama kabupaten" required="required" autofocus="autofocus">
                  <label for="nama_kabupaten"><?php echo $this->lang->line("district_name");?></label>
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12" style="padding-top:10px;color:#ffffff">
                <div class="form-label-group">
                
                <a class="btn btn-success btn-block"   id="editSubmit"><?php echo $this->lang->line("update");?></a>
               
               
                </div>
              </div>
</div>


</form>

        </div>
        
        <!-- Modal footer -->
        <div class="modal-footer  bg-merah">
          &nbsp;
        </div>
        
      </div>
    </div>
  </div>


<!-- end Form -->
<div class="card">
<div class="card-header bg-merah" id="headercard">
<?php
$max = ceil($total/50);
  if($this->uri->segment(4)>5 && ($this->uri->segment(4)+5)>6){

  
   if($this->uri->segment(4)==$max || ($this->uri->segment(4)+2)>$max){ 
     $maxnum = 1+$this->uri->segment(4);
  }else{ 
    $maxnum = 3+$this->uri->segment(4);
  }
  
  $start = $maxnum-5;
  $prev = $start-1; 

}else{
      $maxnum = 6;
      $start = 1;
      $prev = 1;

  }
  ?>
<ul class="pagination pagination-sm justify-content-center">
  <li class="page-item prev"><a class="page-link fas fa-arrow-circle-left" href="<?php echo $prev;?>">&nbsp;</a></li>
  

<?php
//echo $max;
  for($i=$start;$i<=($maxnum-1);$i++){

    if($i==$this->uri->segment(4)){
        ?>
        <li class="page-item active" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo $i;?>"><?php echo $i;?></a></li>
        <?php

    }else{

      ?>
<li class="page-item" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo $i;?>"><?php echo $i;?></a></li>

      <?php
    }

  }
  ?>
  <li class="page-item next"><a class="page-link fas fa-arrow-circle-right" href="<?php echo $maxnum; ?>">&nbsp;</a></li>
</ul>


</div>
<div class="card-body">

<table class="table table-stripe">
<thead>
<tr>
    <th scope="col" colspan="3">
    <div class="input-group input-group-sm">
    <input type="text" id="search" name="search" class="form-control" placeholder="<?php echo $this->lang->line('search');?>">
    <div class="input-group-append"><span class="btn  btn-success" id="src"><i class="fas fa-search"></i></span></div>
</div>
</th>
    <th scope="col" colspan="2" class="text-right">
    &nbsp;
    <a href="#cardform" class="btn  btn-xs btn-success" id="add" data-toggle="modal" data-target="#modalForm"><i class="fas fa-plus-circle"></i>&nbsp;<?php echo $this->lang->line('add');?></a>
</th>

</tr>

<tr class="bg-merah" style="color:#ffffff">
    <th scope="col"><?php echo $this->lang->line("number");?></th>
    <th scope="col"><?php echo $this->lang->line("district_code");?></th>
    <th scope="col"><?php echo $this->lang->line("province_name");?></th>
    <th scope="col"><?php echo $this->lang->line("district_name");?></th>
    <th scope="col"><?php echo $this->lang->line("action");?></th>
</tr>
</thead>
<tbody id="dataBody">
    <?php 
    $i=1;
    foreach($response as $list){ ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list ->district_code;?></td>
    <td scope="col"><?php echo $list->province_name;?></td>
    <td scope="col"><?php echo $list->district_name;?></td>
    <td scope="col" class="text-center d-none d-sm-block">
    <a href="#" class="btn btn-danger btn-sm" onclick="conf('<?php echo $list ->district_code;?>')"><i class="fas fa-trash-alt"></i>&nbsp;<?php echo $this->lang->line("delete");?></a>
    &nbsp;
    <a href="#cardedit" id="btnedit" class="btn btn-success btn-sm"   onclick="edit('<?php echo $list ->district_code;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line("edit");?></a>
    </td>
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>


</div>
<div class="card-footer text-center bg-merah"> &nbsp;
<!-- 
<ul class="pagination pagination-sm">
  <li class="page-item"><a class="page-link" href="#">Previous</a></li>
  <li class="page-item"><a class="page-link" href="#">1</a></li>
  <li class="page-item"><a class="page-link" href="#">2</a></li>
  <li class="page-item"><a class="page-link" href="#">3</a></li>
  <li class="page-item"><a class="page-link" href="#">Next</a></li>
</ul>
-->

</div>
</div>

<script>


function edit(idkabupaten){
  $('#modalEdit').modal();
  $.ajax({
    url:"<?php echo base_url()."master/district/detail/";?>",
    type:"POST",
    dataType : "json",
    data:{
      "district_code":idkabupaten
    },
    success:function(jsondata){
      $('#idkabupatenMST').val(jsondata.district_code);
      $('#idpropinsiEdit').val(jsondata.district_province);
      $('#idkabupatenEdit').val(jsondata.district_code);
      $('#nama_kabupatenEdit').val(jsondata.district_name);
    }
  })
}

function conf(id){
  var n = confirm("<?php echo $this->lang->line("delete_confirmation");?>");
  
  if(n==true){
    $(".preloader").fadeIn();
document.location = "<?php echo base_url()."master/district/delete/";?>"+id;
  }
}

$(document).ready(function(){
var page = "<?php echo $this->uri->segment(4);?>";
var maxn = "<?php echo $max;?>";
var maxnum = "<?php echo $maxnum;?>";



$('#btnSubmit').click(function(){
  $(".preloader").fadeIn();
  $.ajax({
    url : "<?php echo base_url()."master/district/add";?>",
    type : "POST",
    dataType :"json",
    data:{
      "district_province":$('#province_code').val(),
      "district_code":$('#district_code').val(),
      "district_name":$('#district_name').val()
    },
    success:function(jdata){
      if(jdata.status=='success'){
        alert(jdata.message);
        document.location = "<?php echo base_url()."master/district/list/1";?>";
      }else{
        alert(jdata.error);
      }

    },
    error:function(){
      alert('Internal ERROR');
    }
  })

});



$('#editSubmit').click(function(){
  $(".preloader").fadeIn();
  $.ajax({
    url : "<?php echo base_url()."master/district/update/";?>"+$('#idkabupatenMST').val(),
    type : "POST",
    dataType :"json",
    data:{
      "district_province":$('#idpropinsiEdit').val(),
      "district_code":$('#idkabupatenEdit').val(),
      "district_name":$('#nama_kabupatenEdit').val()
    },
    success:function(jdata){
      if(jdata.status=='success'){
        alert(jdata.message);
        document.location = "<?php echo base_url()."master/district/list/1";?>";
      }else{
        alert(jdata.message);
      }

    },
    error:function(){
      alert('Internal ERROR');
    }
  })

});


$('#src').click(function(){
    $(".preloader").fadeIn();
    var search = $('#search').val();
    $('#dataBody').load('<?php echo base_url()."master/district/search/";?>', 
            {
                "search": search

             });

});



if(page=='' || page=='1'){
    $('.prev').addClass('disabled');
}

var m = parseInt(maxn)<parseInt(maxnum);
if(m==true){
    $('.next').addClass('disabled');
}

   

})
</script>