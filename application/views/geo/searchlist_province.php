<?php 
    $i=1;
    if($prov!=null){
    foreach($prov as $list){ ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->province_code;?></td>
    <td scope="col"><?php echo $list->province_name;?></td>
    <td scope="col" class="text-center d-none d-sm-block">
    <a href="#" class="btn btn-danger btn-sm" onclick="conf('<?php echo $list->province_code;?>')"><i class="fas fa-trash-alt"></i>&nbsp;<?php echo $this->lang->line('delete');?></a>
    &nbsp;
    <a href="#cardedit" id="btnedit" class="btn btn-success btn-sm"   onclick="getDetail('<?php echo $list->province_code;?>','<?php echo $this->lang->line('province_name');?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>
    </td>
</tr>

    <?php $i++;
} 
}else{
    ?>
    <tr>
    <td colspan="4" scope="col" class="text-center"><?php echo $this->lang->line('not_found');?></td>
    </tr>
    <?php
}
?>


<script>
$(document).ready(function(){
    $(".preloader").fadeOut();
})
</script>