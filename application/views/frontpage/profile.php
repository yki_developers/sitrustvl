<div class="card text-center">
      <div class="card-header bg-merah" style="color:#ffffff;font-style:bold;font-size:15pt"><?php echo $this->lang->line('welcome')."&nbsp; ".$this->session->userdata('user_fname')." ".$this->session->userdata('user_lname');?></div>
      <div class="card-header">
      <ul class="nav nav-tabs card-header-tabs">

      <?php
     // foreach($parent as $modparent){
        ?>
      <!--
      <li class="nav-item">
        <a class="nav-link bg-light" id="<?php echo $modparent->module_id;?>" href="<?php echo base_url().$modparent->module_path;?>"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line($modparent->module_name);?></a>
      </li>
      <?php// } ?>
    
      <li class="nav-item">
        <a class="nav-link  menu" id="datapengguna" href="#"><i class="fas fa-users"></i>&nbsp;<?php echo $this->lang->line('UserData');?></a>
      </li>
     -->
     <li class="nav-item">
        <a class="nav-link  menu" id="datapengguna" href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a>
      </li>


<?php if($this->session->userdata("user_group")=='6'){ ?>
     <li class="nav-item">
        <a class="nav-link  menu" id="datapengguna" href="<?php echo base_url()."dashboard/faskes";?>"><i class="fas fa-bar"></i>&nbsp;<?php echo $this->lang->line('dashboard');?></a>
      </li>
<?php  } ?>

<li class="nav-item">
        <a class="nav-link  active" id="datapengguna" href="<?php echo base_url()."account/profile";?>"><i class="fas fa-user"></i>&nbsp;<?php echo $this->lang->line('change_password');?></a>
      </li>

      <li class="nav-item">
        <a class="nav-link  menu" id="dataartikel" href="<?php echo base_url();?>login/logout"><i class="fas fa-sign-out-alt"></i>&nbsp;<?php echo $this->lang->line('logout');?></a>
      </li>
   



      
    </ul>

      </div>
</div>

<div class="card mx-auto " style="margin-top:30px">

<div class="card card-header bg-merah " style="font-size:10pt"><?php echo $this->lang->line('change_password');?></div>
<div class="card card-body">

<table class="table table-stripe">
<thead>
<tr>
<th><?php echo $this->lang->line("user_fullname");?></th><th>: <?php echo $this->session->userdata('user_fname');?></th>
</tr>
<tr>
<th><?php echo $this->lang->line('username');?></th><th>: <?php echo $this->session->userdata('user_name');?></th>
</tr>

</thead>
<tbody>
<form>
<tr>
<td><?php echo $this->lang->line('old_password');?></td><td><input type="password" id="old_password" name="old_password">
</tr>

<tr>
<td><?php echo $this->lang->line('new_password');?></td><td><input type="password" id="new_password" value="" name="new_password"></td>
</tr>
<tr>
<td colspan="2"><input id="btnSubmit" class="btn btn-success" value="<?php echo $this->lang->line('change_password');?>"></td>
</tr>
</form>
</tbody>
</table>


</div>
<div class="card card-footer bg-merah">&nbsp;</div>
</div>


<script>

$('document').ready(function(){

$('#btnSubmit').click(function(){
    $.ajax({
        url:"<?php echo base_url()."account/profile/chpassword";?>",
        type:"POST",
        dataType:"json",
        data:{
            "user_name":"<?php echo $this->session->userdata('user_name');?>",
            "newpassword":$('#new_password').val()
        },
        success:function(jdata){
            if(jdata.status=="success"){
                alert("Password Berhasil dirubah");
                document.location = "<?php echo base_url()."login/logout";?>";
            }
        }
    })
})

});
</script>