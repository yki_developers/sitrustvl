

    <div class="card text-center">
      <div class="card-header  bg-merah" style="font-size:20pt;margin-top:50px">..:: <?php echo $this->lang->line('welcome')."&nbsp; ".$this->session->userdata('user_fname')." ".$this->session->userdata('user_lname');?> ::..</div>
      <div class="card-header">
      <ul class="nav nav-tabs card-header-tabs">

     
     <li class="nav-item">
        <a class="nav-link  active" id="datapengguna" href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a>
      </li>


<?php if($this->session->userdata("user_group")=='6'){ ?>
     <li class="nav-item">
        <a class="nav-link  menu" id="dashboard_faskes" href="<?php echo base_url()."dashboard/fasyankes";?>"><i class="fas fa-chart-pie"></i>&nbsp;<?php echo $this->lang->line('dashboard');?></a>
      </li>
<?php  }elseif($this->session->userdata("user_group")=='2'){?>
  <li class="nav-item">
  <a class="nav-link  menu" id="dashboard_nasional" href="<?php echo base_url()."dashboard/nasional";?>"><i class="fas fa-chart-pie"></i>&nbsp;<?php echo $this->lang->line('dashboard');?></a>
</li>
<?php }elseif($this->session->userdata("user_group")=='3'){?>
  <li class="nav-item">
  <a class="nav-link  menu" id="dashboard_propinsi" href="<?php echo base_url()."dashboard/propinsi";?>"><i class="fas fa-chart-pie"></i>&nbsp;<?php echo $this->lang->line('dashboard');?></a>
</li>
<?php }elseif($this->session->userdata("user_group")=='4'){?>
  <li class="nav-item">
  <a class="nav-link  menu" id="dashboard_kabupaten" href="<?php echo base_url()."dashboard/kabupaten";?>"><i class="fas fa-chart-pie"></i>&nbsp;<?php echo $this->lang->line('dashboard');?></a>
</li>
<?php }elseif($this->session->userdata("user_group")=='1'){?>

  <li class="nav-item">
  <a class="nav-link  menu" id="dashboard_nasional" href="<?php echo base_url()."dashboard/nasional";?>"><i class="fas fa-chart-pie"></i>&nbsp;<?php echo $this->lang->line('dashboard');?></a>
</li>

<?php }

?>

<?php if($this->session->userdata("user_adminlevel")=='1' && $this->session->userdata("user_group")!='1'){?>

  <li class="nav-item">
        <a class="nav-link  menu" id="user" href="<?php echo base_url()."account/user/list/1";?>"><i class="fas fa-user"></i>&nbsp;<?php echo $this->lang->line('user');?></a>
      </li>

<?php }?>
<?php if($this->session->userdata("user_group")=='8'){?>

  <li class="nav-item">
       <a class="nav-link  menu" id="user" href="<?php echo base_url()."console/datamanajer";?>"><i class="fas fa-tools"></i>&nbsp;<?php echo $this->lang->line('datacontrol');?></a>
     </li>

<?php }?>

<li class="nav-item">
        <a class="nav-link  menu" id="datapengguna" href="<?php echo base_url()."account/profile";?>"><i class="fas fa-user"></i>&nbsp;<?php echo $this->lang->line('change_password');?></a>
      </li>

      <li class="nav-item">
        <a class="nav-link  menu" id="dataartikel" href="<?php echo base_url();?>login/logout"><i class="fas fa-sign-out-alt"></i>&nbsp;<?php echo $this->lang->line('logout');?></a>
      </li>
   



      
    </ul>

      </div>


      <?php
     
     if($this->session->userdata('user_group')!='6'){
         
      foreach($parent as $modparent){
        ?>
      
      <div class="card">
          <div class="card-header bg-merah" style="margin-top:10px;font-size:15pt;color:#ffffff"><?php echo $this->lang->line($modparent->module_name);?></div>
          <div class="card-body">

          <?php
          $this->load->model("sys/M_module");
          $rdata = array(
            "role_group"=>$this->session->userdata('user_group'),
            "module_parent"=>$modparent->module_id
          );
          
          $mlist = $this->M_module->getChildModule($rdata);
          foreach($mlist as $modList){
            
            
            ?>
            <a href="<?php echo base_url().$modList->module_path;?>">
            <div class="btn btn-outline-info col-md-2 text-center " id="<?php echo $modList->module_id;?>" style="margin:5px">
            <i class="<?php echo $modList->module_icon;?>"></i><br><?php echo $this->lang->line($modList->module_name);?>
            <?php if($modList->module_id=='22' && $neworder->neworder>0){?>&nbsp;<span class="badge badge-danger"><?php echo $neworder->neworder;?></span>
              <?php }elseif($modList->module_id=='18' && $pickup->neworder>0) {?>  &nbsp;<span class="badge badge-danger"><?php echo $pickup->neworder;?></span>
                <?php }elseif($modList->module_id=='28' && $ondelivery->neworder>0) {?>  &nbsp;<span class="badge badge-danger"><?php echo $ondelivery->neworder;?></span>
                  <?php }elseif($modList->module_id=='17' && $delivered->neworder>0) {?>  &nbsp;<span class="badge badge-danger"><?php echo $delivered->neworder;?></span>
                    <?php }elseif($modList->module_id=='23' && $result->neworder>0) {?>  &nbsp;<span class="badge badge-danger"><?php echo $result->neworder;?></span>
                      <?php }elseif($modList->module_id=='16' && $orderread->neworder>0) {?>  &nbsp;<span class="badge badge-danger">Status Update ( <?php echo $orderread->neworder;?>)</span>
                    <?php }?>
              
              </div></a>
          <?php
            }
          ?>
          </div>
<div class="card-footer  bg-merah"">&nbsp;
</div>
</div>
      
      <?php
      }
    }else{
      ?>
       <div class="card">
          <div class="card-header bg-merah" style="margin-top:10px"><?php $fs = $this->M_reverse->hfName($this->session->userdata('user_unit')); echo $fs->hf_name;?></div>
          <div class="card-body">

          <?php if($this->session->userdata("user_hf_group")=='1' || $this->session->userdata("user_hf_group")=='3' ){

            ?>
            <div class="card">
              <div class="card-header bg-hijau" >&nbsp;</div>
              <div class="card-body">
              <a href="<?php echo base_url()."transactional/order/list";?>">
<div class="btn btn-outline-info col-md-2 text-center " id="order" style="margin:5px">
            <i class="fas fa-paper-plane fa-5x"></i><br><?php echo $this->lang->line("order");?> VL
            </div></a>
           
              

            <a href="<?php echo base_url()."transactional/order/eid";?>">
<div class="btn btn-outline-info col-md-2 text-center " id="order" style="margin:5px">
            <i class="fas fa-plus-square fa-5x"></i><br><?php echo $this->lang->line("order_eid");?><!--
            <?php if($orderread->neworder>0) {?>  &nbsp;<span class="badge badge-danger">Status Update ( <?php echo $orderread->neworder;?>)</span>
                    <?php }?> -->
            </div></a>

             

            <a href="<?php echo base_url()."report/exam_report/vlmonitoring/1";?>">
              <div class="btn btn-outline-info col-md-2 text-center " id="monitoring" style="margin:5px">
            <i class="fas fa-desktop fa-5x"></i><br><?php echo $this->lang->line('vl_monitoring');?>
           
              
              </div>
              </a>


              <a href="<?php echo base_url()."report/exam_report/eidmonitoring/1";?>">
              <div class="btn btn-outline-info col-md-2 text-center " id="monitoring" style="margin:5px">
            <i class="fas fa-desktop fa-5x"></i><br><?php echo $this->lang->line('eid_monitoring');?>
           
              
              </div>
              </a>

              </div>
              <div class="card-footer">&nbsp;</div>
            </div>
          



            <?php
          }elseif($this->session->userdata("user_hf_group")=='2' || $this->session->userdata("user_hf_group")=='4'){?>





            <div class="card">
              <div class="card-header bg-hijau">&nbsp; </div>
              <div class="card-body">
              <a href="<?php echo base_url()."report/exam_report/vlmonitoring/1";?>">
           <div class="btn btn-outline-info col-md-2 text-center " id="monitoring" style="margin:5px">
            <i class="fas fa-desktop fa-5x"></i><br><?php echo $this->lang->line('vl_monitoring');?>

            </div></a>



            <a href="<?php echo base_url()."report/exam_report/eidmonitoring/1";?>">
           <div class="btn btn-outline-info col-md-2 text-center " id="monitoring" style="margin:5px">
            <i class="fas fa-desktop fa-5x"></i><br><?php echo $this->lang->line('eid_monitoring');?>

            </div></a>
              </div>
              <div class="card-footer">&nbsp;</div>
            </div>


       
        <?php }elseif($this->session->userdata("user_hf_group")=='5'){?>


          <div class="card">
              <div class="card-header bg-hijau" >&nbsp; Input Permintaan Pengiriman Spesimen</div>
              <div class="card-body">
              <a href="<?php echo base_url()."transactional/order/list";?>">
<div class="btn btn-outline-info col-md-2 text-center " id="order" style="margin:5px">
            <i class="fas fa-paper-plane fa-5x"></i><br><?php echo $this->lang->line("order");?> VL
            </div></a>
           
              

            <a href="<?php echo base_url()."transactional/order/eid";?>">
<div class="btn btn-outline-info col-md-2 text-center " id="order" style="margin:5px">
            <i class="fas fa-plus-square fa-5x"></i><br><?php echo $this->lang->line("order_eid");?><!--
            <?php if($orderread->neworder>0) {?>  &nbsp;<span class="badge badge-danger">Status Update ( <?php echo $orderread->neworder;?>)</span>
                    <?php }?> -->
            </div></a>

             

            <a href="<?php echo base_url()."report/exam_report/vlmonitoring/1";?>">
              <div class="btn btn-outline-info col-md-2 text-center " id="monitoring" style="margin:5px">
            <i class="fas fa-desktop fa-5x"></i><br><?php echo $this->lang->line('vl_monitoring');?>
           
              
              </div>
              </a>


              <a href="<?php echo base_url()."report/exam_report/eidmonitoring/1";?>">
              <div class="btn btn-outline-info col-md-2 text-center " id="monitoring" style="margin:5px">
            <i class="fas fa-desktop fa-5x"></i><br><?php echo $this->lang->line('eid_monitoring');?>
           
              
              </div>
              </a>

              </div>
              <div class="card-footer">&nbsp;</div>
            </div>


          <div class="card">
              <div class="card-header bg-hijau">&nbsp; Input Konfirmasi dan Hasil pemeriksaan VL</div>
              <div class="card-body">
              <a href="<?php echo base_url()."transactional/order/confirmation";?>">
          <div class="btn btn-outline-info col-md-2 text-center " id="monitoring" style="margin:5px">
            <i class="far fa-check-square fa-5x"></i><br><?php echo $this->lang->line('confirmation');?> VL
        &nbsp;<?php if($neworder->neworder>0) {?>  &nbsp;<span class="badge badge-danger"><?php echo $neworder->neworder;?></span><?php }?>
          </div></a>

          <a href="<?php echo base_url()."transactional/order/received";?>">
            <div class="btn btn-outline-info col-md-2 text-center " id="monitoring" style="margin:5px">
            <i class="fas fa-box-open fa-5x"></i><br><?php echo $this->lang->line('receive');?> VL
            &nbsp;<?php if($delivered->neworder>0) {?>  &nbsp;<span class="badge badge-danger"><?php echo $delivered->neworder;?></span><?php }?>
            </div></a>
            <a href="<?php echo base_url()."transactional/result/list";?>">
            <div class="btn btn-outline-info col-md-2 text-center " id="monitoring" style="margin:5px">
            <i class="fas fa-clipboard-check fa-5x"></i><br><?php echo $this->lang->line('result');?>&nbsp;VL&nbsp;<?php if($result->neworder>0) {?>  &nbsp;<span class="badge badge-danger"><?php echo $result->neworder;?></span><?php }?></div></a>




            </div>
              
              <div class="card-footer">&nbsp;</div>
            </div>


            


            <div class="card">
              <div class="card-header bg-hijau">&nbsp; Input Konfirmasi dan Hasil pemeriksaan EID</div>
              <div class="card-body">
              <a href="<?php echo base_url()."transactional/order/eidconfirmation";?>">
          <div class="btn btn-outline-info col-md-2 text-center " id="monitoring" style="margin:5px">
            <i class="far fa-check-square fa-5x"></i><br><?php echo $this->lang->line('confirmation');?> EID
        &nbsp;<?php if($neworderEid->neworder>0) {?>  &nbsp;<span class="badge badge-danger"><?php echo $neworderEid->neworder;?></span><?php }?>
          </div></a>

          <a href="<?php echo base_url()."transactional/order/eidreceived";?>">
            <div class="btn btn-outline-info col-md-2 text-center " id="monitoring" style="margin:5px">
            <i class="fas fa-box-open fa-5x"></i><br><?php echo $this->lang->line('receive');?> EID
            &nbsp;<?php if($deliveredEid->neworder>0) {?>  &nbsp;<span class="badge badge-danger"><?php echo $deliveredEid->neworder;?></span><?php }?>
            </div></a>
            <a href="<?php echo base_url()."transactional/result/eidlist";?>">
            <div class="btn btn-outline-info col-md-2 text-center " id="monitoring" style="margin:5px">
            <i class="fas fa-clipboard-check fa-5x"></i><br><?php echo $this->lang->line('result');?>&nbsp;EID&nbsp;<?php if($resultEid->neworder>0) {?>  &nbsp;<span class="badge badge-danger"><?php echo $resultEid->neworder;?></span><?php }?></div></a>




            </div>
              
              <div class="card-footer">&nbsp;</div>
            </div>



            <div class="card">
              <div class="card-header bg-hijau">&nbsp;Input Permintaan Pemeriksaan Internal</div>
              <div class="card-body">

              <a href="<?php echo base_url()."intern/eidinternal";?>">
          <div class="btn btn-outline-info col-md-2 text-center " id="int_pdp" style="margin:5px">
            <i class="fas fa-tasks fa-5x"></i><br><?php echo $this->lang->line('internal_pdp')." EID";?></div></a>

            <a href="<?php echo base_url()."intern/internal/result_eid";?>">
            <div class="btn btn-outline-info col-md-2 text-center " id="int_lab" style="margin:5px">
            <i class="fas fa-clipboard-check fa-5x"></i><br><?php echo $this->lang->line('internal_result');?>&nbsp;EID</div></a>



              <a href="<?php echo base_url()."intern/internal";?>">
          <div class="btn btn-outline-info col-md-2 text-center " id="int_pdp" style="margin:5px">
            <i class="fas fa-tasks fa-5x"></i><br><?php echo $this->lang->line('internal_pdp')." VL";?></div></a>

  
            <a href="<?php echo base_url()."intern/internal/result";?>">
            <div class="btn btn-outline-info col-md-2 text-center " id="int_lab" style="margin:5px">
            <i class="fas fa-clipboard-check fa-5x"></i><br><?php echo $this->lang->line('internal_result');?> VL</div></a>




            </div>
              </div>
              <div class="card-footer">&nbsp;</div>
            </div>



          <?php } ?>
          


         
          <div class="card">
              <div class="card-header bg-hijau">&nbsp;Laporan</div>
              <div class="card-body">

          <div class="card-body">
            
         <?php  if($this->session->userdata('user_hf_group')=='5'){?>
          <a href="<?php echo base_url()."report/exam_report/internalvlreport";?>">
          <div class="btn btn-outline-info col-md-2 text-center " id="vlreport" style="margin:5px">
            <i class="far fa-file-alt fa-5x"></i><br>Lap. Pemeriksaan Internal VL
          </div></a>

       <!--   <a href="<?php echo base_url()."report/exam_report/internaleidreport";?>">
          <div class="btn btn-outline-info col-md-2 text-center " id="vlreport" style="margin:5px">
            <i class="far fa-file-alt fa-5x"></i><br>Rekap Pemeriksaan Internal EID
          </div></a>
-->
          <?php } ?> 

          <a href="<?php echo base_url()."report/exam_report/vlreport/1";?>">
          <div class="btn btn-outline-info col-md-2 text-center " id="vlreport" style="margin:5px">
            <i class="far fa-file-alt fa-5x"></i><br><?php echo $this->lang->line('vl_report');?>
          </div></a>
          <a href="<?php echo base_url()."report/exam_report/eidreport/1";?>">
          <div class="btn btn-outline-info col-md-2 text-center " id="eidreport" style="margin:5px">
            <i class="far fa-file-alt fa-5x"></i><br><?php echo $this->lang->line('eid_report');?>
          </div>
          </a>


          </div>
              </div>
              <div class="card-footer">&nbsp;</div>
            </div>
          </div>

         
</div>

      <?php
    }
      ?>
      

</div>
</div>

<script>
$(document).ready(function(){
  $('.menu').click(function(){
    $(".preloader").fadeIn();
  })

$('#1').addClass('active')
})
</script>