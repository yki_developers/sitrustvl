<script>
  $(".preloader").fadeIn();
  </script>
<nav aria-label="breadcrumb"  style="margin-top:50px">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><a href="<?php echo base_url()."master/hf/list/1";?>">&nbsp;<i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('health_facility');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('network_lab');?></li>
  </ol>
</nav>




<div class="card">
<div class="card-header  bg-merah" id="headercard">


<?php
$max = ceil($total/50);
  if($this->uri->segment(4)>5 && ($this->uri->segment(4)+5)>6){

  
   if($this->uri->segment(4)==$max || ($this->uri->segment(4)+2)>$max){ 
     $maxnum = 1+$this->uri->segment(4);
  }else{ 
    $maxnum = 3+$this->uri->segment(4);
  }
  
  $start = $maxnum-5;
  $prev = $start-1; 

}else{
      $maxnum = 6;
      $start = 1;
      $prev = 1;

  }
  ?>
<ul class="pagination pagination-sm justify-content-center">
  <li class="page-item prev"><a class="page-link fas fa-arrow-circle-left" href="<?php echo $prev;?>">&nbsp;</a></li>
  

<?php
//echo $max;
  for($i=$start;$i<=($maxnum-1);$i++){

    if($i==$this->uri->segment(4)){
        ?>
        <li class="page-item active" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo $i;?>"><?php echo $i;?></a></li>
        <?php

    }else{

      ?>
<li class="page-item" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo $i;?>"><?php echo $i;?></a></li>

      <?php
    }

  }
  ?>
  <li class="page-item next"><a class="page-link fas fa-arrow-circle-right" href="<?php echo $maxnum; ?>">&nbsp;</a></li>
</ul>
</div>

<div class="card-body">
<div class="input-group col-lg-4">
    <input type="text" id="search" name="search" class="form-control" placeholder="<?php echo $this->lang->line('search');?>" aria-describedby="src">
    <div class="input-group-append"><span class="btn  btn-success " id="src"><i class="fas fa-search"></i></span></div>&nbsp
   <!--  <a href="#cardform" class="btn btn-success btn-xs" id="add" data-toggle="modal" data-target="#modalForm"><i class="fas fa-plus-circle"></i>&nbsp;<?php echo $this->lang->line('add');?></a> -->
</div>

</div>

<div class="card-body">
<div class="tableFixHead">
<table class="table table-stripe">
<thead>
<tr>
    <th scope="col"><?php echo $this->lang->line('number');?></th>
    <th scope="col"><?php echo $this->lang->line('network_origin');?></th>
    <th scope="col"><?php echo $this->lang->line('network_destination');?></th>
    <th scope="col"><?php echo $this->lang->line('action');?></th>
</tr>
</thead>
<tbody id="dataBody">
    <?php

if($this->uri->segment(4)!=''){    
$xi = ($this->uri->segment(4)-1)*50;
}else{
    $xi = 0;
}
    $i=1;
    foreach($response as $list){ ?>
    <tr>
    <th scope="row"><?php echo $i+$xi;?></th>
    <td scope="col"><?php echo $list->origin_name?></td>
    <td scope="col"><?php echo $list->destination_name;?></td>
    <td scope="col" class="text-center d-none d-sm-block">
    <a href="#" class="btn btn-danger btn-sm" onclick="conf('<?php echo $list->hf_code.'_'.$list->destination_code;?>')"><i class="fas fa-trash-alt"></i>&nbsp;<?php echo $this->lang->line('delete');?></a>
    &nbsp;
   <!-- <a href="#" id="btnedit" class="btn btn-success btn-sm"   onclick="edit('<?php echo $list->hf_code?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a> -->

    </td>
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>
</div>


</div>
<div class="card-footer text-center">
</div>
</div>



<script>
    function conf(id){
        if(confirm('Apakah anda akan menghapus data ini?')){
            document.location='<?php echo base_url()."network/laboratorium/delete/";?>'+id;
        }
    }
    $('document').ready(function(){
        $('#src').click(function(){
            $('#dataBody').load('<?php echo base_url()."network/laboratorium/search";?>',{"search":$('#search').val()});
        })


        $('#search').keyup(function(){
            $('#dataBody').load('<?php echo base_url()."network/laboratorium/search";?>',{"search":$('#search').val()});
        })
    })
    </script>