<script>
  $(".preloader").fadeIn();
  </script>
<nav aria-label="breadcrumb"  style="margin-top:50px">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item"><a href="<?php echo base_url()."master/hf/list/1";?>"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('health_facility');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('network_lab');?></li>
  </ol>
</nav>



<!-- Form New NEtwork -->
<div class="modal fade" id="modalForm">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('data_form');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form method="POST" action="<?php echo base_url()."network/laboratorium/add";?>" id="newkec">

        <div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                  <input type="text" id="origin_name" name="origin_name"  value="<?php echo $origin->hf_name;?>" class="form-control" placeholder="faskes asal <?php echo $this->lang->line('origin_name');?>" required="required" autofocus="autofocus" readonly="readonly">
                  <input type="hidden" id="labnetwork_origin" value="<?php echo $origin->hf_code;?>" name="labnetwork_origin">
                  <label for="origin_name"><?php echo $this->lang->line('network_origin');?></label>
                </div>
              </div>
</div>

        <div class="form-row">
<div class="col-md-12"  style="padding-top:10px">
                <div class="form-label-group">
              
                  <select name="province_code" id="province_code" class="form-control custom-select"  required="required">
                      <option value=""><?php echo $this->lang->line('province');?></option>
                  <?php

foreach($province as $lpropinsi){
?>
<option value="<?php echo $lpropinsi->province_code;?>"><?php echo $lpropinsi->province_name;?></option>
<?php
}
?>

</select>

            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                  <select name="district_code" id="district_code" class="form-control custom-select"  required="required">
                      <option value=""><?php echo $this->lang->line('district');?></option>
                    </select>

            
                </div>
              </div>
</div>





<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
                <div class="form-label-group">
              
                  <select name="labnetwork_destination" id="labnetwork_destination" class="form-control custom-select"  required="required">
                      <option value=""><?php echo $this->lang->line('networkn');?></option>
                    </select>

            
                </div>
              </div>
</div>






<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <a class="btn btn-primary btn-block"   id="btnSubmit"><?php echo $this->lang->line('submit');?></a>
               
               
                </div>
              </div>
</div>

        </form>
        </div>
      </div>
    </div>
</div>

<!-- End Form -->




<div class="card">
<div class="card-header bg-merah" id="headercard">





</div>
<div class="card-body">

<table class="table table-stripe">
<thead>
<tr>
    <th scope="col" colspan="3">
    <div class="input-group input-group-sm">
    <!-- <input type="text" id="search" name="search" class="form-control" placeholder="Pencarian">
    <div class="input-group-append"><span class="btn  btn-primary" id="src"><i class="fas fa-search"></i></span>
    
    </div> -->
</div>
</th>
    <th scope="col" colspan="2" class="text-right">
    &nbsp;
    <a href="#cardform" class="btn btn-success btn-xs" id="add" data-toggle="modal" data-target="#modalForm"><i class="fas fa-plus-circle"></i>&nbsp;<?php echo $this->lang->line('add');?></a>
</th>

</tr>

<tr class="bg-merah">
    <th scope="col"><?php echo $this->lang->line('number');?></th>
    <th scope="col"><?php echo $this->lang->line('province_name');?></th>
    <th scope="col"><?php echo $this->lang->line('district_name');?></th>
    <th scope="col"><?php echo $this->lang->line('network_destination');?></th>
    <th scope="col" class="d-none d-sm-block"><?php echo $this->lang->line('action');?></th>
</tr>
</thead>
<tbody id="dataBody">
    <?php


    $i=1;
    foreach($response as $list){ ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->province_name?></td>
    <td scope="col"><?php echo $list->district_name?></td>
    <td scope="col"><?php echo $list->hf_name;?></td>
    <td scope="col" class="text-center d-none d-sm-block">
    <a href="#" class="btn btn-danger btn-sm" onclick="conf('<?php echo $list->labnetwork_origin.'_'.$list->labnetwork_destination; ?>')"><i class="fas fa-trash-alt"></i>&nbsp;<?php echo $this->lang->line('delete');?></a>
    &nbsp;
    

    </td>
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>


</div>
<div class="card-footer text-center  bg-merah">
</div>
</div>


<script>

    function conf(id){
        if(confirm('<?php echo $this->lang->line('delete_confirmation');?>')){
            document.location = "<?php echo base_url()."network/laboratorium/delete/";?>"+id;
        }
    }

    $('document').ready(function(){

        $('#province_code').change(function(){
    $(".preloader").fadeIn();
    $.ajax({
        url : '<?php echo base_url()."master/district/districtbyprovince";?>',
        type:'POST',
        dataType:'json',
        data:{
            'province_code':$('#province_code').val()
        },
        success:function(jdata){

            var str ='<option value=""><?php echo $this->lang->line('district');?></option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
            })
            $('#district_code').html(str);
            $(".preloader").fadeOut();
        }
    })
});



$('#district_code').change(function(){
    $(".preloader").fadeIn();
    $.ajax({
        url : '<?php echo base_url()."master/hf/reflist";?>',
        type:'POST',
        dataType:'json',
        data:{
            'hf_district':$('#district_code').val(),
            'hf_code':$('#labnetwork_origin').val()
        },
        success:function(jdata){

            var str ='<option value=""><?php echo $this->lang->line('network');?></option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.hf_code+'">'+item.hf_name+'</option>';
            })
            $('#labnetwork_destination').html(str);
            $(".preloader").fadeOut();
        }
    })
})


$('#btnSubmit').click(function(){
    $.ajax({
        url:"<?php echo base_url()."network/laboratorium/add";?>",
        type:"POST",
        dataType:"json",
        data:{
            "labnetwork_origin":$('#labnetwork_origin').val(),
            "labnetwork_destination":$('#labnetwork_destination').val()
        },
        success:function(jdata){
            if(jdata.status=='success'){
                alert(jdata.message);
                document.location="<?php echo base_url()."network/laboratorium/new/";?>"+$('#labnetwork_origin').val();
            }
        }
    })
})



    })
    </script>