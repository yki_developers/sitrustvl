<script>
  $(".preloader").fadeIn();
  </script>
<nav aria-label="breadcrumb"   style="margin-top:50px">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><i class="fas fa-home"></i><a href="<?php echo base_url()."home";?>">&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('network_lab');?></li>
  </ol>
</nav>






<!-- Form New NEtwork -->
<div class="modal fade" id="modalForm">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('data_form');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form method="POST" action="<?php echo base_url()."network/laboratorium/add";?>" id="newkec">

        <div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                  <input type="text" id="origin_name" name="origin_name"  value="<?php echo $origin->hf_name;?>" class="form-control" placeholder="faskes asal <?php echo $this->lang->line('origin_name');?>" required="required" autofocus="autofocus" readonly="readonly">
                  <input type="hidden" id="labnetwork_origin" value="<?php echo $origin->hf_code;?>" name="labnetwork_origin">
                  <label for="origin_name"><?php echo $this->lang->line('network_origin');?></label>
                </div>
              </div>
</div>

        <div class="form-row">
<div class="col-md-12"  style="padding-top:10px">
                <div class="form-label-group">
              
                  <select name="province_code" id="province_code" class="form-control custom-select"  required="required">
                      <option value=""><?php echo $this->lang->line('province');?></option>
                  <?php

foreach($province as $lpropinsi){
?>
<option value="<?php echo $lpropinsi->province_code;?>"><?php echo $lpropinsi->province_name;?></option>
<?php
}
?>

</select>

            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                  <select name="district_code" id="district_code" class="form-control custom-select"  required="required">
                      <option value=""><?php echo $this->lang->line('district');?></option>
                    </select>

            
                </div>
              </div>
</div>





<div class="form-row">
<div class="col-md-12"  style="margin-top: 10px;">
                <div class="form-label-group">
              
                  <select name="labnetwork_destination" id="labnetwork_destination" class="form-control custom-select"  required="required">
                      <option value=""><?php echo $this->lang->line('networkn');?></option>
                    </select>

            
                </div>
              </div>
</div>






<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <a class="btn btn-primary btn-block"   id="btnSubmit"><?php echo $this->lang->line('submit');?></a>
               
               
                </div>
              </div>
</div>

        </form>
        </div>
      </div>
    </div>
</div>

<!-- End Form -->







<div class="card">
<div class="card-header" id="headercard">

<?php
$max = ceil($total/50);
  if($this->uri->segment(4)>5 && ($this->uri->segment(4)+5)>6){

  
   if($this->uri->segment(4)==$max || ($this->uri->segment(4)+2)>$max){ 
     $maxnum = 1+$this->uri->segment(4);
  }else{ 
    $maxnum = 3+$this->uri->segment(4);
  }
  
  $start = $maxnum-5;
  $prev = $start-1; 

}else{
      $maxnum = 6;
      $start = 1;
      $prev = 1;

  }
  ?>
<ul class="pagination pagination-sm justify-content-center">
  <li class="page-item prev"><a class="page-link fas fa-arrow-circle-left" href="<?php echo $prev;?>">&nbsp;</a></li>
  

<?php
//echo $max;
  for($i=$start;$i<=($maxnum-1);$i++){

    if($i==$this->uri->segment(4)){
        ?>
        <li class="page-item active" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo $i;?>"><?php echo $i;?></a></li>
        <?php

    }else{

      ?>
<li class="page-item" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo $i;?>"><?php echo $i;?></a></li>

      <?php
    }

  }
  ?>
  <li class="page-item next"><a class="page-link fas fa-arrow-circle-right" href="<?php echo $maxnum; ?>">&nbsp;</a></li>
</ul>



</div>

<div class="card-body">
<div class="input-group col-lg-4">
    <input type="text" id="search" name="search" class="form-control" placeholder="<?php echo $this->lang->line('search');?>" aria-describedby="src">
    <div class="input-group-append"><span class="btn  btn-success " id="src"><i class="fas fa-search"></i></span></div>&nbsp
    <a href="#cardform" class="btn btn-success btn-xs" id="add" data-toggle="modal" data-target="#modalForm"><i class="fas fa-plus-circle"></i>&nbsp;<?php echo $this->lang->line('add');?></a>
</div>

</div>

<div class="card-body">
<div class="tableFixHead">
<table class="table table-stripe">
<thead>
<tr>
    <th scope="col"><?php echo $this->lang->line('number');?></th>
    <th scope="col"><?php echo $this->lang->line('network_origin');?></th>
    <th scope="col"><?php echo $this->lang->line('network_destination');?></th>
    <th scope="col"><?php echo $this->lang->line('action');?></th>
</tr>
</thead>
<tbody id="dataBody">
    <?php

if($this->uri->segment(4)!=''){    
$xi = ($this->uri->segment(4)-1)*50;
}else{
    $xi = 0;
}
    $i=1;
    foreach($response as $list){ ?>
    <tr>
    <th scope="row"><?php echo $i+$xi;?></th>
    <td scope="col"><?php echo $list->origin_name?></td>
    <td scope="col"><?php echo $list->destination_name;?></td>
    <td scope="col">
    <a href="#" class="btn btn-danger btn-sm" onclick="conf('<?php echo $list->hf_code?>')"><i class="fas fa-trash-alt"></i>&nbsp;<?php echo $this->lang->line('delete');?></a>
    &nbsp;
    <a href="#" id="btnedit" class="btn btn-success btn-sm"   onclick="edit('<?php echo $list->hf_code?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>

    </td>
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>
</div>

</div>
<div class="card-footer text-center">
</div>
</div>








<script>

function conf(id,code){
    if(confirm('<?php echo $this->lang->line('delete_confirmation');?>')){
        document.location = "<?php echo base_url()."network/laboratorium/delete/";?>"+id+"/"+code;
    }
}

function edit(id){
    document.location="<?php echo base_url()."network/laboratorium/new/";?>"+id;
}

$('document').ready(function(){

    $('#province_code').change(function(){
$(".preloader").fadeIn();
$.ajax({
    url : '<?php echo base_url()."master/district/districtbyprovince";?>',
    type:'POST',
    dataType:'json',
    data:{
        'province_code':$('#province_code').val()
    },
    success:function(jdata){

        var str ='<option value=""><?php echo $this->lang->line('district');?></option>';
        $.each(jdata.response,function(i,item){
            str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
        })
        $('#district_code').html(str);
        $(".preloader").fadeOut();
    }
})
});



$('#district_code').change(function(){
$(".preloader").fadeIn();
$.ajax({
    url : '<?php echo base_url()."master/hf/reflist";?>',
    type:'POST',
    dataType:'json',
    data:{
        'hf_district':$('#district_code').val(),
        'hf_code':$('#labnetwork_origin').val()
    },
    success:function(jdata){

        var str ='<option value=""><?php echo $this->lang->line('network');?></option>';
        $.each(jdata.response,function(i,item){
            str +='<option value="'+item.hf_code+'">'+item.hf_name+'</option>';
        })
        $('#labnetwork_destination').html(str);
        $(".preloader").fadeOut();
    }
})
})


$('#btnSubmit').click(function(){
$.ajax({
    url:"<?php echo base_url()."network/laboratorium/add";?>",
    type:"POST",
    dataType:"json",
    data:{
        "labnetwork_origin":$('#labnetwork_origin').val(),
        "labnetwork_destination":$('#labnetwork_destination').val()
    },
    success:function(jdata){
        if(jdata.status=='success'){
            alert(jdata.message);
            document.location="<?php echo base_url()."network/laboratorium"?>";
        }
    }
})
})



})
</script>