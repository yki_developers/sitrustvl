<script>
  $(".preloader").fadeIn();
  </script>
<nav aria-label="breadcrumb"  style="margin-top:50px">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><a href="<?php echo base_url()."master/hf/list/1";?>">&nbsp;<i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('health_facility');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;Jejaring Kurir</li>
  </ol>
</nav>

<div class="card">
<div class="card-header  bg-merah" id="headercard">
    &nbsp;
</div>

<div class="card-body">
<div class="input-group col-lg-4">
   <!-- <input type="text" id="search" name="search" class="form-control" placeholder="<?php echo $this->lang->line('search');?>" aria-describedby="src">
    <div class="input-group-append"><span class="btn  btn-success " id="src"><i class="fas fa-search"></i></span></div>&nbsp -->
   <a href="#cardform" class="btn btn-success btn-xs" id="add" data-toggle="modal" data-target="#modalForm"  data-backdrop="static" data-keyboard="false"><i class="fas fa-plus-circle"></i>&nbsp;<?php echo $this->lang->line('add');?></a>
</div>
</div>


<div class="card-body">

<div class="tableFixHead">
<table class="table table-stripe">
<thead>
<tr>
    <th scope="col"><?php echo $this->lang->line('number');?></th>
    <th scope="col">Nama Kurir</th>
    <th scope="col">Nama Petugas Kurir</th>
    <th scope="col">User Name Kurir</th>
    <th scope="col"><?php echo $this->lang->line('action');?></th>
</tr>
</thead>
<tbody id="dataBody">
    <?php 
    $i=1;
    foreach($list as $db){?>
        <tr>
            <td><?php echo $i; ?></td>
    <td><?php echo $db->shipper_name;?></td>
    <td><?php echo $db->user_fname;?></td>
    <td><?php echo $db->user_name;?></td>
<td>
<a href="#" onclick="hapus('<?php echo $db->hf_code;?>','<?php echo $db->user_name;?>')" class="btn btn-danger btn-sm"><i class="fas fa-trash-alt"></i>&nbsp;<?php echo $this->lang->line('delete');?></a>
</td>
    
    
    </tr>
    <?php $i++; } ?>
</tbody>
</table>

</div>


</div>


<div class="card-footer text-center">&nbsp;
</div>
</div>




<div class="modal fade" id="modalForm">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-hijau">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;Jejaring Kurir</h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <!-- Modal body -->
        <div class="modal-body">
          
        <form method="POST" action="<?php echo base_url()."network/kurir/add";?>" id="newkec">



    <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text">Kurir</span>
    </div>
   
    <select name="shipper_code" id="shipper_code" class="form-control custom-select"  required="required">
                      <option value=""></option>
                  <?php

foreach($shipperlist as $list){
?>
<option value="<?php echo $list->shipper_code;?>"><?php echo $list->shipper_name;?></option>
<?php
}
?>

</select>
  
  </div>

  <div class="input-group mb-3">
    <div class="input-group-prepend">
    <span class="input-group-text">Nama Petugas</span>
    </div>
   
    <select name="user_kurir" id="user_kurir" class="form-control custom-select"  required="required">
                      <option value=""></option>

</select>
  
  </div>
        
  <div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <a class="btn btn-primary btn-block"   id="btnSubmit"><?php echo $this->lang->line('submit');?></a>
               
               
                </div>
              </div>
</div>



        </form>
        </div>
      </div>
    </div>
</div>

<script>
  function hapus(hfkode,username){
    var del = confirm("Apakah Anda Akan Menghapus Jejaring Kurir ini?");
    if(del){
      $.ajax({
        url:"<?php echo base_url()."network/kurirnetwork/delete/";?>"+hfkode,
        type:"POST",
        dataType:"json",
        data:{
          "ref_int_username":username,
          "ref_int_hf_code":hfkode
        },
        success:function(jdata){
          if(jdata.status=='success'){

            $('#dataBody').load('<?php echo base_url()."network/kurir/datalist/";?>'+hfkode);
          alert('data berhasil dihapus');

          }
        }
      })
    }
  }
$('document').ready(function(){
$('#shipper_code').change(function(){
    $.ajax({
      url:"<?php echo base_url()."network/kurirnetwork/userkurir";?>",
      type:"POST",
      dataType:"json",
      data:{
        "shipper_code":$('#shipper_code').val(),
        "district_code":"<?php echo $faskes->hf_district;?>"
      },
      success:function(jdata){

var str ='<option value=""> --  Nama Petugas --</option>';
$.each(jdata.response,function(i,item){
    str +='<option value="'+item.user_name+'">'+item.user_fname+'</option>';
})
$('#user_kurir').html(str);
$(".preloader").fadeOut();
}

    })
  });

  $('#btnSubmit').click(function(){
   
    $.ajax({
      url:"<?php echo base_url()."network/kurir/add";?>",
      type:"POST",
      dataType:"json",
      data:{
        "ref_int_hf_code":"<?php echo $faskes->hf_code;?>",
        "ref_int_username":$('#user_kurir').val()
      },
      success:function(jsdata){
        if(jsdata.status=='success'){
          $('#dataBody').load('<?php echo base_url()."network/kurir/datalist/".$faskes->hf_code;?>');
          $('#modalForm').modal('hide');
          alert('data berhasil disimpan');
        }
      }

    });
  })


});
  </script>