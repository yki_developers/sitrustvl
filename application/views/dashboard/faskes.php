<div class="card text-center">
      <div class="card-header  bg-merah" style="font-size:20pt;margin-top:50px">Dashboard faskes</div>
      <div class="card-header">
      <ul class="nav nav-tabs card-header-tabs">

     
     <li class="nav-item">
        <a class="nav-link menu" id="datapengguna" href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a>
      </li>


<?php if($this->session->userdata("user_group")=='6'){ ?>
     <li class="nav-item">
        <a class="nav-link  active" id="dashboard_faskes" href="<?php echo base_url()."dashboard/faskes/".$this->session->userdata('user_unit');?>"><i class="fas fa-bar"></i>&nbsp;<?php echo $this->lang->line('dashboard');?></a>
      </li>
<?php  }elseif($this->session->userdata("user_group")=='2'){?>
  <li class="nav-item">
  <a class="nav-link  active" id="dashboard_nasional" href="<?php echo base_url()."dashboard/nasional";?>"><i class="fas fa-bar"></i>&nbsp;<?php echo $this->lang->line('dashboard');?></a>
</li>
<?php } ?>

<?php if($this->session->userdata("user_adminlevel")=='1' && $this->session->userdata("user_group")!='1'){?>

  <li class="nav-item">
        <a class="nav-link  menu" id="user" href="<?php echo base_url()."account/user/list/1";?>"><i class="fas fa-user"></i>&nbsp;<?php echo $this->lang->line('user');?></a>
      </li>

<?php }?>
<?php if($this->session->userdata("user_group")=='8'){?>

  <li class="nav-item">
       <a class="nav-link  menu" id="user" href="<?php echo base_url()."console/datamanajer";?>"><i class="fas fa-tools"></i>&nbsp;<?php echo $this->lang->line('datacontrol');?></a>
     </li>

<?php }?>

<li class="nav-item">
        <a class="nav-link  menu" id="datapengguna" href="<?php echo base_url()."account/profile";?>"><i class="fas fa-user"></i>&nbsp;<?php echo $this->lang->line('change_password');?></a>
      </li>

      <li class="nav-item">
        <a class="nav-link  menu" id="dataartikel" href="<?php echo base_url();?>login/logout"><i class="fas fa-sign-out-alt"></i>&nbsp;<?php echo $this->lang->line('logout');?></a>
      </li>
   



      
    </ul>

      </div>


 <div class="card-body">&nbsp;


 <div class="row">

 <div class="col-md-7">
 

<div class="card">
<div class="card-header bg-merah">&nbsp;</div>
<div class="card-body" id="cascade">

</div>
</div>



<div class="card">
<div class="card-header bg-merah">&nbsp;</div>
<div class="card-body" id="condition">

</div>
</div>




<div class="card">
<div class="card-header bg-merah">&nbsp;</div>
<div class="card-body" id="result">

</div>
</div>



</div>


<div class="col-md-5">

<div class="card">
  <div class="card-header bg-merah">&nbsp;</div>
  <div class="card-body" id="trend" style="height: 1000px;"></div>
</div>



</div>

 </div>


<div class="row">
<div class="col-md-12">
<div class="card">
<div class="card-header bg-hijau">Turn Arround Time</div>

<div class="card-body">error data..</div>

</div>

</div>
</div>

 </div>

 <div class="card-footer bg-merah">&nbsp;</div>
</div>

<script>
$("document").ready(function(){
    mchart("MSColumn2D.swf","<?php echo base_url()."dashboard/chart/chart_pengiriman_vl/faskes/".$this->session->userdata("user_unit");?>","cascade","tc1","100%","400");
   stackChart("<?php echo base_url()."dashboard/chart/chart_trend_pengiriman_vl/faskes/".$this->session->userdata("user_unit");?>","trend","tc2","100%","100%");

   pieChart("<?php echo base_url()."dashboard/chart/chart_specimen_condition/faskes/".$this->session->userdata("user_unit");?>","condition","tc3","100%","400");


   mchart("Column2D.swf","<?php echo base_url()."dashboard/chart/chart_exam_result/faskes/".$this->session->userdata("user_unit");?>","result","tc1","100%","400");
   //mchart("StackedBar2D.swf","<?php echo base_url()."dashboard/chart/chart_trend_pengiriman_vl/faskes";?>","trend","800","800");


})
</script>