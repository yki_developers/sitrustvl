<div class="col-md-12 row">
<div class="col-md-12">
<div class="card">
  <div class="card-header bg-hijau">DASHBOARD PERNGIRIMAN VIRAL LOAD (VL)</div>  
</div>
</div>
</div>
<div class="col-md-12 row"  style="margin-top: 10px;">
    <div class="col-md-3">

<div class="card">
    <div class="card-header bg-hijau">Spesimen Dikirim</div>
    <div class="card-body">
        <h1><?php echo number_format($specimen_status->specimen_dikirim,0,",",".");?></h1>
    </div>
    <div class="card-footer">&nbsp;</div>
</div>

    </div>


    <div class="col-md-3">

<div class="card">
    <div class="card-header bg-hijau">Spesimen Diterima</div>
    <div class="card-body">  <h1><?php echo number_format($specimen_status->specimen_diterima,0,",",".");?></h1></div>
    <div class="card-footer">&nbsp;</div>
</div>

    </div>
<div class="col-md-3">

<div class="card">
    <div class="card-header bg-hijau">Spesimen Sudah ada Hasil</div>
    <div class="card-body"><h1><?php echo number_format($specimen_hasil->specimen_adahasil,0,",",".");?></h1></div>
    <div class="card-footer">&nbsp;</div>
</div>
</div>

<div class="col-md-3">


<div class="card">
    <div class="card-header bg-hijau">Hasil Tersupresi</div>
    <div class="card-body"><h1><?php echo number_format($specimen_hasil->specimen_tersupresi,0,",",".");?></h1></div>
    <div class="card-footer">&nbsp;</div>
</div>
</div>
</div>



<div class="col-md-12 row" style="margin-top: 10px;">
    <div class="col-md-3">
        <div class="card">
            <div class="card-body"><h4>TAT : <?php echo number_format($specimen_tat->dash_total,"2",",",".");?>&nbsp;Hari</h4>&nbsp;(N= <?php echo $specimen_tat->specimen;?>)</div>
           



        </div>
    </div>
    <div class="col-md-3">&nbsp;</div>
    <div class="col-md-3">&nbsp;</div>
    <div class="col-md-3">&nbsp;</div>
</div>


<div class="col-md-12 row" style="margin-top: 10px;">

<div class="col-md-6">
<div class="card">
    <div class="card-header bg-hijau"></div>
    <div class="card-body" id="trend_vl"></div>
    <div class="card-footer"></div>
</div>

</div>

<div class="col-md-6">

<div class="card">
<div class="card-header bg-hijau"></div>
<div class="card-body" id="condition_vl"></div>
<div class="card-footer"></div>

</div>
</div>

</div>

<div class="col-md-12 row" style="margin-top: 10px;">
<div class="col-md-12">
<div class="card">
    <div class="card-header bg-hijau"></div>
    <div class="card-body" id="pengiriman_vl"></div>
    <div class="card-footer"></div>
</div>
</div>
</div>








<div class="col-md-12 row" style="margin-top: 10px;">
<div class="col-md-12">
<div class="card">
  <div class="card-header bg-merah">DASHBOARD PERNGIRIMAN EID</div>  
</div>
</div>
</div>
<div class="col-md-12 row"  style="margin-top: 10px;">
    <div class="col-md-3">

<div class="card">
    <div class="card-header bg-merah">Spesimen Dikirim</div>
    <div class="card-body">
        <h1><?php echo number_format($eid_status->eid_dikirim,0,",",".");?></h1>
    </div>
    <div class="card-footer">&nbsp;</div>
</div>

    </div>


    <div class="col-md-3">

<div class="card">
    <div class="card-header bg-merah">Spesimen Diterima</div>
    <div class="card-body">  <h1><?php echo number_format($eid_status->eid_diterima,0,",",".");?></h1></div>
    <div class="card-footer">&nbsp;</div>
</div>

    </div>
<div class="col-md-3">

<div class="card">
    <div class="card-header bg-merah">Spesimen Sudah ada Hasil</div>
    <div class="card-body"><h1><?php echo number_format($eid_hasil->eid_adahasil,0,",",".");?></h1></div>
    <div class="card-footer">&nbsp;</div>
</div>
</div>

<div class="col-md-3">


<div class="card">
    <div class="card-header bg-merah">Hasil Terdeteksi</div>
    <div class="card-body"><h1><?php echo number_format($eid_hasil->eid_detected,0,",",".");?></h1></div>
    <div class="card-footer">&nbsp;</div>
</div>
</div>
</div>



<div class="col-md-12 row" style="margin-top: 10px;">
    <div class="col-md-3">
        <div class="card">
            <div class="card-body"><h4>TAT : <?php echo number_format($eid_tat->dash_total,"2",",",".");?>&nbsp;Hari</h4>&nbsp;(N= <?php echo $eid_tat->specimen;?>)</div>
           



        </div>
    </div>
    <div class="col-md-3">&nbsp;</div>
    <div class="col-md-3">&nbsp;</div>
    <div class="col-md-3">&nbsp;</div>
</div>


<div class="col-md-12 row" style="margin-top: 10px;">

<div class="col-md-6">
<div class="card">
    <div class="card-header bg-merah"></div>
    <div class="card-body" id="trend_eid"></div>
    <div class="card-footer"></div>
</div>

</div>

<div class="col-md-6">

<div class="card">
<div class="card-header bg-merah"></div>
<div class="card-body" id="condition_eid"></div>
<div class="card-footer"></div>

</div>
</div>

</div>

<div class="col-md-12 row" style="margin-top: 10px;">
<div class="col-md-12">
<div class="card">
    <div class="card-header bg-merah"></div>
    <div class="card-body" id="pengiriman_eid"></div>
    <div class="card-footer"></div>
</div>
</div>




<script>
    $('document').ready(function(){

        

        <?php if($level=="nasional"){?>
           

            pieChart("<?php echo base_url()."dashboard/chart/chart_specimen_condition/".$periode->start_date."/".$periode->end_date;?>","condition_vl","tcl_".$level,"100%","400");
        mchart("Line.swf","<?php echo base_url()."dashboard/chart/chart_trend_nasional/".$periode->hari."/".$periode->start_date."/".$periode->end_date;?>","trend_vl","tc_".$level,"100%","400");
         mchart("Column2D.swf","<?php echo base_url()."dashboard/chart/chart_pengiriman_nasional/".$periode->start_date."/".$periode->end_date;?>","pengiriman_vl","tcx_".$level,"100%","400");


         mchart("Line.swf","<?php echo base_url()."dashboard/chart_eid/chart_trend_nasional/".$periode->hari."/".$periode->start_date."/".$periode->end_date;?>","trend_eid","tc_eid_".$level,"100%","400");
         pieChart("<?php echo base_url()."dashboard/chart_eid/chart_specimen_condition/".$periode->start_date."/".$periode->end_date;?>","condition_eid","tcl_eid_".$level,"100%","400");
         mchart("Column2D.swf","<?php echo base_url()."dashboard/chart_eid/chart_pengiriman_nasional/".$periode->start_date."/".$periode->end_date;?>","pengiriman_eid","tcx_eid_".$level,"100%","400");


        <?php }elseif($level=="propinsi"){ ?>
            pieChart("<?php echo base_url()."dashboard/chart/chart_specimen_condition_regional/".$level."/".$province."/".$periode->start_date."/".$periode->end_date;?>","condition_vl","tc_nas_".$level,"100%","400");
            mchart("Line.swf","<?php echo base_url()."dashboard/chart/chart_trend_propinsi/".$province."/".$periode->hari."/".$periode->start_date."/".$periode->end_date;?>","trend_vl","tc_".$level,"100%","400");
            mchart("Column2D.swf","<?php echo base_url()."dashboard/chart/chart_pengiriman_regional/".$level."/".$province."/".$periode->start_date."/".$periode->end_date;?>","pengiriman_vl","tcx_".$level,"100%","400");



            pieChart("<?php echo base_url()."dashboard/chart_eid/chart_specimen_condition_regional/".$level."/".$province."/".$periode->start_date."/".$periode->end_date;?>","condition_eid","tc_nas_eid_".$level,"100%","400");
            mchart("Line.swf","<?php echo base_url()."dashboard/chart_eid/chart_trend_propinsi/".$province."/".$periode->hari."/".$periode->start_date."/".$periode->end_date;?>","trend_eid","tc_eid_".$level,"100%","400");
            mchart("Column2D.swf","<?php echo base_url()."dashboard/chart_eid/chart_pengiriman_regional/".$level."/".$province."/".$periode->start_date."/".$periode->end_date;?>","pengiriman_eid","tcx_eid_".$level,"100%","400");
           


            <?php }elseif($level=="kabupaten"){ ?>
                pieChart("<?php echo base_url()."dashboard/chart/chart_specimen_condition_regional/".$level."/".$district."/".$periode->start_date."/".$periode->end_date;?>","condition_vl","tc_nas_".$level,"100%","400");
                mchart("Line.swf","<?php echo base_url()."dashboard/chart/chart_trend_kabupaten/".$district."/".$periode->hari."/".$periode->start_date."/".$periode->end_date;?>","trend_vl","tc_".$level,"100%","400");
                mchart("Column2D.swf","<?php echo base_url()."dashboard/chart/chart_pengiriman_regional/".$level."/".$district."/".$periode->start_date."/".$periode->end_date;?>","pengiriman_vl","tcx_".$level,"100%","400");

                mchart("Line.swf","<?php echo base_url()."dashboard/chart_eid/chart_trend_kabupaten/".$district."/".$periode->hari."/".$periode->start_date."/".$periode->end_date;?>","trend_eid","tc_eid_".$level,"100%","400");

                pieChart("<?php echo base_url()."dashboard/chart_eid/chart_specimen_condition_regional/".$level."/".$district."/".$periode->start_date."/".$periode->end_date;?>","condition_eid","tc_nas_eid_".$level,"100%","400");
                mchart("Column2D.swf","<?php echo base_url()."dashboard/chart_eid/chart_pengiriman_regional/".$level."/".$district."/".$periode->start_date."/".$periode->end_date;?>","pengiriman_eid","tcx_eid_".$level,"100%","400");
                

                

<?php }elseif($level=="fasyankes"){ ?>
    pieChart("<?php echo base_url()."dashboard/chart/chart_specimen_condition_regional/".$level."/".$fasyankes."/".$periode->start_date."/".$periode->end_date;?>","condition_vl","tc_nas_".$level,"100%","400");
mchart("Line.swf","<?php echo base_url()."dashboard/chart/chart_trend_fasyankes/".$fasyankes."/".$periode->hari."/".$periode->start_date."/".$periode->end_date;?>","trend_vl","tc_".$level,"100%","400");
//mchart("Column2D.swf","<?php echo base_url()."dashboard/chart/chart_pengiriman_regional/".$level."/".$fasyankes."/".$periode->start_date."/".$periode->end_date;?>","pengiriman_vl","tcx_".$level,"100%","400");



mchart("Line.swf","<?php echo base_url()."dashboard/chart_eid/chart_trend_fasyankes/".$fasyankes."/".$periode->hari."/".$periode->start_date."/".$periode->end_date;?>","trend_eid","tc_eid_".$level,"100%","400");

pieChart("<?php echo base_url()."dashboard/chart_eid/chart_specimen_condition_regional/".$level."/".$fasyankes."/".$periode->start_date."/".$periode->end_date;?>","condition_eid","tc_nas_eid_".$level,"100%","400");
               // mchart("Column2D.swf","<?php echo base_url()."dashboard/chart_eid/chart_pengiriman_regional/".$level."/".$fasyankes."/".$periode->start_date."/".$periode->end_date;?>","pengiriman_eid","tcx_eid_".$level,"100%","400");



<?php } ?>

    })
</script>