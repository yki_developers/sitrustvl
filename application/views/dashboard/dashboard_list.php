
    <div class="card text-center">
      <div class="card-header bg-danger" style="color:#ffffff;font-style:bold;font-size:20pt"><?php echo $this->lang->line('welcome')."&nbsp;<br> ".$this->session->userdata('user_fname')." ".$this->session->userdata('user_lname');?></div>
      <div class="card-header">
      <ul class="nav nav-tabs card-header-tabs">

      <?php
     // foreach($parent as $modparent){
        ?>
      <!--
      <li class="nav-item">
        <a class="nav-link bg-light" id="<?php echo $modparent->module_id;?>" href="<?php echo base_url().$modparent->module_path;?>"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line($modparent->module_name);?></a>
      </li>
      <?php// } ?>
    
      <li class="nav-item">
        <a class="nav-link  menu" id="datapengguna" href="#"><i class="fas fa-users"></i>&nbsp;<?php echo $this->lang->line('UserData');?></a>
      </li>
     -->
     <li class="nav-item">
        <a class="nav-link menu" id="datapengguna" href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;HOME</a>
      </li>


<?php if($this->session->userdata("user_group")=='6'){ ?>
     <li class="nav-item">
        <a class="nav-link  active" id="datapengguna" href="<?php echo base_url()."dashboard/faskes";?>"><i class="fas fa-bar"></i>&nbsp;Dashboard</a>
      </li>
<?php  } ?>

      <li class="nav-item">
        <a class="nav-link  menu" id="dataartikel" href="<?php echo base_url();?>login/logout"><i class="fas fa-sign-out-alt"></i>&nbsp;<?php echo $this->lang->line('logout');?></a>
      </li>
   



      
    </ul>

      </div>


      <div class="card">
          <div class="card-header bg-danger">Dashboard</div>
          <div class="card-body">
          <div class="col-md-12">
<div class="col-md-2">
          <div class="card" >
          <div class="card-header bg-primary">
          <?php echo $this->lang->line('dashboard_total_specimen');?>
          </div>
          <div class="card-body" style="font-size: 20pt;">
          <?php echo $list->specimen_total; ?>
          </div>
          </div>
</div>
<div class="col-md-2">
          <div class="card" >
          <div class="card-header bg-primary">
          <?php echo $this->lang->line('dashboard_total_specimen_picked_up');?>
          </div>
          <div class="card-body" style="font-size: 20pt;">
          <?php echo $list->specimen_pickedup; ?>
          </div>
          </div>
</div>


<div class="col-md-2">
          <div class="card" >
          <div class="card-header bg-primary">
          <?php echo $this->lang->line('dashboard_total_specimen_arrived');?>
          </div>
          <div class="card-body" style="font-size: 20pt;">
          <?php echo $list->specimen_delivered; ?>
          </div>
          </div>
</div>

<div class="col-md-2">
          <div class="card" >
          <div class="card-header bg-primary">
          <?php echo $this->lang->line('dashboard_total_specimen_result');?>
          </div>
          <div class="card-body" style="font-size: 20pt;">
          <?php echo $list->specimen_result; ?>
          </div>
          </div>
</div>





          </div>

          </div>
<div class="card-footer">&nbsp;
</div>
</div>
    </div>
    

