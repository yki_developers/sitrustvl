<style>
.datepicker{z-index:1151 !important;}
label{
  color: red;
  margin-left: 10px;
}
</style>
<div class="card text-center">
      <div class="card-header" style="margin-top:50px">
    
      <ul class="nav nav-tabs card-header-tabs">

     
<li class="nav-item">
   <a class="nav-link menu" id="datapengguna" href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a>
 </li>


<?php if($this->session->userdata("user_group")=='6'){ ?>
<li class="nav-item">
   <a class="nav-link  active" id="dashboard_faskes" href="<?php echo base_url()."dashboard/faskes/".$this->session->userdata('user_unit');?>"><i class="fas fa-bar"></i>&nbsp;<?php echo $this->lang->line('dashboard');?></a>
 </li>
<?php  }elseif($this->session->userdata("user_group")=='2'){?>
<li class="nav-item">
<a class="nav-link  active" id="dashboard_nasional" href="<?php echo base_url()."dashboard/nasional";?>"><i class="fas fa-bar"></i>&nbsp;<?php echo $this->lang->line('dashboard');?></a>
</li>
<?php } ?>

<?php if($this->session->userdata("user_adminlevel")=='1' && $this->session->userdata("user_group")!='1'){?>

<li class="nav-item">
   <a class="nav-link  menu" id="user" href="<?php echo base_url()."account/user/list/1";?>"><i class="fas fa-user"></i>&nbsp;<?php echo $this->lang->line('user');?></a>
 </li>

<?php }?>
<?php if($this->session->userdata("user_group")=='8'){?>

<li class="nav-item">
  <a class="nav-link  menu" id="user" href="<?php echo base_url()."console/datamanajer";?>"><i class="fas fa-tools"></i>&nbsp;<?php echo $this->lang->line('datacontrol');?></a>
</li>

<?php }?>

<li class="nav-item">
   <a class="nav-link  menu" id="datapengguna" href="<?php echo base_url()."account/profile";?>"><i class="fas fa-user"></i>&nbsp;<?php echo $this->lang->line('change_password');?></a>
 </li>

 <li class="nav-item">
   <a class="nav-link  menu" id="dataartikel" href="<?php echo base_url();?>login/logout"><i class="fas fa-sign-out-alt"></i>&nbsp;<?php echo $this->lang->line('logout');?></a>
 </li>




 
</ul>

    
    </div>
      <div class="card-header  text-left">
     




      

    <a href="#cardform" class="btn btn-success btn-xs" id="add" data-toggle="modal" data-target="#modalForm" data-backdrop="static" data-keyboard='false'><i class="fas fa-filter"></i>&nbsp;Filter</a>






      </div>
      <div class="card-body" id="vlbody">
         
          
      </div>
      <div class="card-footer"></div>
</div>








<!---- MODAL ORDER -->

<div class="modal fade" id="modalForm">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-hijau">
         <i class="fas fa-filter"></i>&nbsp;Filter
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
    <div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text"><?php echo $this->lang->line("province");?></span>
    </div>
    <select name="propinsi" id="propinsi" class="form-control custom-select"  required="required">
                      <option value="999">Nasional</option>
                  <?php

foreach($prop as $proplist){
   
?>
<option value="<?php echo $proplist->province_code; ?>"><?php echo $proplist->province_name;?></option>
<?php
}
?>

</select>
  </div>
      
  


  <div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text"><?php echo $this->lang->line("district");?></span>
    </div>
    <select name="kabupaten" id="kabupaten" class="form-control custom-select" disabled="disabled">
                     
                 

</select>
  </div>



  <div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text">Fasyankes</span>
    </div>
    <select name="fasyankes" id="fasyankes" class="form-control custom-select" disabled="disabled">
                     
                 

</select>
  </div>




  <div class="input-group mb-3">
    <div class="input-group-prepend">
      <span class="input-group-text">Periode</span>
    </div>
    <input type="text" id="start_date" name="start_date" class="form-control tanggal" required="required" autofocus="autofocus" readonly="readonly" value="01-01-2019" >&nbsp; s/d &nbsp;
    <input type="text"  id="end_date" name="end_date" class="form-control tanggal" required="required" autofocus="autofocus" value="<?php echo date("d-m-Y");?>" readonly="readonly">
  </div>



  </div>

        
<div class="modal-footer">

<div class="btn btn-primary" id="btnFilter">OK</div>
</div>





        </div>
      </div>
    </div>
</div>

<!-- END MODAL -->

<script>
  function dbdate(tgl){
var tg = tgl.split("-");
var dbFormat = tg[2]+"-"+tg[1]+"-"+tg[0];
return dbFormat;
}
    $('document').ready(function(){





      $('#propinsi').change(function(){
        $('#fasyankes').attr('disabled','disabled').val("P9");
    $(".preloader").fadeIn();
    $.ajax({
        url : '<?php echo base_url()."master/district/districtbyprovince";?>',
        type:'POST',
        dataType:'json',
        data:{
            'province_code':$('#propinsi').val()
        },
        success:function(jdata){

            var str ='<option value="99999">Semua Kab/Kota</option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
            })
            $('#kabupaten').html(str).removeAttr('disabled');
           
            $(".preloader").fadeOut();
        }
    })
});



$('#kabupaten').change(function(){
    $(".preloader").fadeIn();
    $.ajax({
        url : '<?php echo base_url()."master/hf/hfbydistrict";?>',
        type:'POST',
        dataType:'json',
        data:{
            'hf_district':$('#kabupaten').val()
        },
        success:function(jdata){

            var str ='<option value="P9">Semua Fasyankes</option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.hf_code+'">'+item.hf_name+'</option>';
            })
            $('#fasyankes').html(str).removeAttr('disabled');
            $(".preloader").fadeOut();
        }
    })
})





$('#btnFilter').click(function(){
  $('#modalForm').modal('hide');
  $('#vlbody').load("<?php echo base_url()."dashboard/newdashboard/datanasional";?>",
  {
    "propinsi":$('#propinsi').val(),
    "kabupaten":$('#kabupaten').val(),
    "fasyankes":$('#fasyankes').val(),
    "periode_start":dbdate($('#start_date').val()),
    "periode_end":dbdate($('#end_date').val())
  })
})


      $('.tanggal').datepicker({
  format:"dd-mm-yyyy",
  startView:"month",
 minView:"year"
}).on('changeDate',function(ev){
  $(this).blur();
  $(this).datepicker('hide');
});

        $('#modalForm').modal();
    })
</script>