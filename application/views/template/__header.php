<html>
    <head>
        <title><?php echo SITE_NAME;?></title>
        <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <meta name="description" content="" />
    <meta name="author" content="Harmi Prasetyo" />

   


    <link rel="icon" href="<?=base_url()?>/assets/favicon.ico" type="image/ico">
<link href="<?php echo base_url()."assets/bs4/css/bootstrap.css";?>" rel="stylesheet" />
<link href="<?php echo base_url()."assets/bs4/plugins/css/datepicker.css";?>" rel="stylesheet" /> 
<!-- <link href="<?php echo base_url()."assets/bs4/plugins/css/bootstrap-datepicker.css";?>" rel="stylesheet" /> -->

<link href="<?php echo base_url()."assets/sbadmin/vendor/fontawesome-free/css/all.min.css";?>" rel="stylesheet" type="text/css" />
<link href="<?php echo base_url()."assets/clockpicker/bootstrap-clockpicker.css";?>" rel="stylesheet" /> 

  <!-- Page level plugin CSS-->
  <link href="<?php echo base_url()."assets/sbadmin/vendor/datatables/dataTables.bootstrap4.css";?>" rel="stylesheet" />

  <!-- Custom styles for this template-->
  <link href="<?php echo base_url()."assets/sbadmin/css/sb-admin.css";?>" rel="stylesheet" />
 <!--  <link href="<?php echo base_url()."assets/editor/editor.css";?>" rel="stylesheet" />
  <link href="<?php echo base_url()."assets/dist/summernote-bs4.css";?>" rel="stylesheet" /> -->

<script src="<?php echo base_url()."assets/jquery/js/jquery-3.4.1.js";?>"></script>
<script src="<?php echo base_url()."assets/jquery/js/jquery.validate.js";?>"></script>
<script src="<?php echo base_url()."assets/jquery/js/popper.min.js";?>"></script>
<script src="<?php echo base_url()."assets/bs4/js/bootstrap.js";?>"></script>
<script src="<?php echo base_url();?>assets/bs4/js/bootstrap.bundle.min.js"></script>
<script src="<?php echo base_url()."assets/bs4/plugins/js/bootstrap-datepicker-id.js";?>"></script>
<!-- <script src="<?php echo base_url()."assets/bs4/plugins/js/bootstrap-datepicker.js";?>"></script>
<script src="<?php echo base_url()."assets/bs4/plugins/js/bootstrap-datepicker.id.min.js";?>"></script> -->


<script src="<?php echo base_url()."assets/clockpicker/bootstrap-clockpicker.js";?>"></script>

<script src="<?php echo base_url();?>assets/sbadmin/vendor/jquery-easing/jquery.easing.min.js"></script>


<!-- Quixlab component -->
<!-- <script src="<?php echo base_url();?>assets/quixlab/plugins/common/common.min.js"></script>
    <script src="<?php echo base_url();?>assets/quixlab/js/custom.min.js"></script>
    <script src="<?php echo base_url();?>assets/quixlab/js/settings.js"></script>
    <script src="<?php echo base_url();?>assets/quixlab/js/gleek.js"></script>
    <script src="<?php echo base_url();?>assets/quixlab/js/styleSwitcher.js"></script>
-->
<!-- Semantic CSS Component -->

<!-- <link href="<?php echo base_url()."assets/semantic/semantic.css";?>" rel="stylesheet" />-->
<link href="<?php echo base_url()."assets/semantic/components/flag.css";?>" rel="stylesheet" /> 



  <!-- Page level plugin JavaScript-->
  <script src="<?php echo base_url();?>assets/sbadmin/vendor/chart.js/Chart.min.js"></script>
  <script src="<?php echo base_url();?>assets/sbadmin/vendor/datatables/jquery.dataTables.js"></script>
  <script src="<?php echo base_url();?>assets/sbadmin/vendor/datatables/dataTables.bootstrap4.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="<?php echo base_url();?>assets/sbadmin/js/sb-admin.min.js"></script>




<script src="<?php echo base_url()."assets/charts/FusionCharts.js";?>"></script>


  <style type="text/css">
    .preloader {
      position: fixed;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
      z-index: 19999;
      background-color:#4DB6AC;
    }
    .preloader .loading {
      position: absolute;
      left: 50%;
      top: 30%;
      transform: translate(-50%,-50%);
      font: 14px arial;
      z-index:9999;
      background-color:#4DB6AC;
    }

    .breadcrumb {
      background-color: #f2ebf0;
      color:#ffffff
    }

    .bg-hijau {
      background-color: #4DB6AC;
      color:#ffffff;
    }

    .bg-merah {
      background-color: #EF5350;
      color: #ffffff;
    }

    .bg-biru {
      background-color: #42A5F5;
      color: #ffffff;
    }

    a:link {
      color:#42A5F5;
    }
    a:visited{
      color:#42A5F5;
    }
    a:hover{
      color:#42A5F5;
    }

  
  
    .btn:link{
      color:#ffffff;
    }
    .btn:visited{
      color:#ffffff;
    }



  
.scroll {
    max-height: 500px;
    overflow-y: scroll;
}





.tableFixHead,
.tableFixHead td {
  box-shadow: inset 1px -1px #000;
}
.tableFixHead th {
  box-shadow: inset 1px 1px #000, 0 1px #000;
}

.tableFixHead          { overflow-y: auto; height: 500px; }
.tableFixHead thead th { position: sticky; top: 0; }

/* Just common table stuff. Really. */
table  { border-collapse: collapse; width: 100%; }
th, td { padding: 8px 16px; }
th     { background:#EF5350; }

/* Borders (if you need them) */
.tableFixHead,
.tableFixHead td {
  box-shadow: inset 1px -1px #000;
}
.tableFixHead th {
  box-shadow: inset 1px 1px #000, 0 1px #000;
}


.input-group>.input-group-prepend {
    flex: 0 0 40%;
}
.input-group .input-group-text {
    width: 100%;
}

.input-group>.input-group-text>.radio {
  width:5%;
}

    </style>


</head>
<script>
function formatDate(tgl){
var tg = tgl.split("-");
var dbFormat = tg[2]+"-"+tg[1]+"-"+tg[0];
return dbFormat;
}



		function number(evt) {
		  var charCode = (evt.which) ? evt.which : event.keyCode
		   if (charCode > 31 && (charCode < 48 || charCode > 57))
 
		    return false;
		  return true;
		}



    function mchart(ctype,url,iddiv,id,width,height){
      var lChart = new FusionCharts("<?php echo base_url()."assets/charts/";?>"+ctype,id, width, height, "0", "1");
                                lChart.setJSONUrl(url);
                                lChart.render(iddiv);
                               /* FusionCharts(id).configureLink({
                                  overlayButton:{ 
                                    message:"X",
                                    fontColor : '880000',
                                    bgColor:'FFEEEE',
                                    borderColor: '660000'
                                   }
                                })*/

//alert(url);
    }

function stackChart(url,iddiv,id,w,h){
  var mchart = new FusionCharts("<?php echo base_url()."assets/chart/StackedBar2D.swf";?>",id,w,h,"0","1");
  mchart.setJSONUrl(url);
  mchart.render(iddiv);
  FusionCharts(id).configureLink({
                                  overlayButton:{ 
                                    message:"X",
                                    fontColor : '880000',
                                    bgColor:'FFEEEE',
                                    borderColor: '660000'
                                   }
                                })
}

function pieChart(url,iddiv,id,w,h){
  var mchart = new FusionCharts("<?php echo base_url()."assets/chart/Pie2D.swf";?>",id,w,h,"0","1");
  mchart.setJSONUrl(url);
  mchart.render(iddiv);
}

function barChart(url,iddiv,id,w,h){
  var mchart = new FusionCharts("<?php echo base_url()."assets/chart/Bar2D.swf";?>",id,w,h,"0","1");
  mchart.setJSONUrl(url);
  mchart.render(iddiv);
}


function scrollLine(url,iddiv,id,w,h){
  var mchart = new FusionCharts("<?php echo base_url()."assets/chart/scrollline2d.swf";?>",id,w,h,"0","1");
  mchart.setJSONUrl(url);
  mchart.render(iddiv);
}

</script>