<?php
echo $header;
?>
<body id="page-top">
<div class="preloader">
      <div class="loading">
        <img src="<?php echo base_url()."assets/images/loader.gif";?>" width="100%">
      </div>
</div>

<?php
echo @$navbar;
?>

 <div class="row col-md-12 mx-auto">
      <!-- Sitebar -->
<div class="col-md-1">
      <?php  //echo @$sidebar; ?>
</div> 
<div class="col-md-10" style="padding-top:10px">

<?php echo @$content; ?>


</div>

<div class="col-md-1">
&nbsp;
</div>

<!-- end Wrapper -->



</div>

<?php echo @$footer; ?>
</body>
<script>
      $(document).ready(function(){
            $(".preloader").fadeOut('slow');
      })
      </script>
