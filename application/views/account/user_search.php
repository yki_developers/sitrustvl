<?php
    $i=1;
    foreach($response as $list){ ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->user_name?></td>
    <td scope="col"><?php echo $list->user_fname?></td>
    <td scope="col"><?php echo $this->lang->line($list->group_name);?></td>
    <td scope="col" class="text-center">
    <a href="#" class="btn btn-danger btn-sm" onclick="conf('<?php echo $list->user_id; ?>')"><i class="fas fa-trash-alt"></i>&nbsp;<?php echo $this->lang->line('delete');?></a>
    &nbsp;
    <a href="#" id="btnedit" class="btn btn-success btn-sm"   onclick="edit('<?php echo $list->user_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>
    &nbsp;
    <a href="#" id="chpasswd" class="btn btn-success btn-sm"   onclick="chpass('<?php echo $list->user_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('change_password');?></a>
    </td>
    
</tr>

    <?php $i++;
} ?>