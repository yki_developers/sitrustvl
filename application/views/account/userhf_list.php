<script>
  $(".preloader").fadeIn();
  </script>
<nav aria-label="breadcrumb">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><i class="fas fa-home"></i><a href="<?php echo base_url()."home";?>">&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('user_hf');?></li>
  </ol>
</nav>



<div class="card">
<div class="card-header" id="headercard">&nbsp;
</div>
<div class="card-body">

<table class="table table-stripe">
<thead>
<tr>
    <th scope="col" colspan="2">
    <div class="input-group input-group-sm">
    <input type="text" id="search" name="search" class="form-control" placeholder="Pencarian">
    <div class="input-group-append"><span class="btn  btn-primary" id="src"><i class="fas fa-search"></i></span></div>
</div>
</th>
    <th scope="col" class="text-right">
    &nbsp;
    <a href="#cardform" class="btn btn-light btn-xs" id="add" data-toggle="modal" data-target="#modalForm"><i class="fas fa-plus-circle"></i>&nbsp;<?php echo $this->lang->line('add');?></a>
</th>

</tr>

<tr>
    <th scope="col"><?php echo $this->lang->line('number');?></th>
    <th scope="col"><?php echo $this->lang->line('username');?></th>
    <th scope="col"><?php echo $this->lang->line('user_fullname');?></th>
    <th scope="col"><?php echo $this->lang->line('healthfacility_name');?></th>
    <th scope="col" class="d-none d-sm-block"><?php echo $this->lang->line('action');?></th>
</tr>
</thead>
<tbody id="dataBody">
    <?php
    $i=1;
    foreach($response as $list){ ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->hf_username?></td>
    <td scope="col"><?php echo $list->hf_name;?></td>
    <td scope="col" class="text-center d-none d-sm-block">
    <a href="#" class="btn btn-danger btn-sm" onclick="conf('<?php echo $list->hf_id; ?>')"><i class="fas fa-trash-alt"></i>&nbsp;<?php echo $this->lang->line('delete');?></a>
    &nbsp;
    <a href="#" id="btnedit" class="btn btn-success btn-sm"   onclick="edit('<?php echo $list->hf_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>

    </td>
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>


</div>
<div class="card-footer text-center">
</div>
</div>
