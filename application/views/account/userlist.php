<script>
  $(".preloader").fadeIn();
  </script>
<nav aria-label="breadcrumb" style="margin-top: 50px;">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('user');?></li>
  </ol>
</nav>


<!---- Form Add New User -->
<div class="modal fade" id="modalForm">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-merah">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('data_form');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form method="POST" action="<?php echo base_url()."account/user/add";?>" id="shipper">
        <div class="form-row">
<div class="col-md-12">
                <div class="form-label-group">
              <input type="hidden" id="user_id" value="">
              <input type="hidden" id="user_adminlevel" value="<?php echo $admin;?>">
                <input type="text" id="user_name" name="user_name" class="form-control" placeholder="<?php echo $this->lang->line('username');?>" required="required" autofocus="autofocus">
                  <label for="user_name"><?php echo $this->lang->line('username');?></label>
            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <input type="password" id="user_passwd" name="user_passwd" class="form-control user_pass" placeholder="<?php echo $this->lang->line('user_password');?>" required="required" autofocus="autofocus">
                  <label for="user_passwd" class="user_pass"><?php echo $this->lang->line('password');?></label>
            
                </div>
              </div>
</div>

<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <input type="text" id="user_fullname" name="user_fullname" class="form-control" placeholder="<?php echo $this->lang->line('user_fullname');?>" required="required" autofocus="autofocus">
                  <label for="user_fullname"><?php echo $this->lang->line('user_fullname');?></label>

            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <input type="text" id="user_phnumber" name="user_phnumber" class="form-control" placeholder="<?php echo $this->lang->line('user_phnumber');?>" required="required" autofocus="autofocus">
                  <label for="user_phnumber"><?php echo $this->lang->line('phone_number');?></label>

            
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <select name="user_group" id="user_group" class="form-control custom-select"  required="required" placeholder="<?php echo $this->lang->line("username_group");?>">
                      <option value=""><?php echo $this->lang->line("username_group");?></option>
                  <?php

foreach($usergroup as $group){
?>
<option value="<?php echo $group->group_id; ?>"><?php echo $this->lang->line($group->group_name);?></option>
<?php
}
?>

</select>

            
                </div>
              </div>
</div>




<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <select name="user_province" id="user_province" class="form-control custom-select"  placeholder="<?php echo $this->lang->line("province");?>" disabled="disabled">
                      <option value=""><?php echo $this->lang->line("province");?></option>
                  <?php

foreach($province as $provlist){
?>
<option value="<?php echo $provlist->province_code; ?>"><?php echo $provlist->province_name;?></option>
<?php
}
?>

</select>        
                </div>
              </div>
</div>



<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <select name="user_district" id="user_district" class="form-control custom-select"   placeholder="<?php echo $this->lang->line("district");?>"  disabled="disabled">
                      <option value=""><?php echo $this->lang->line("district");?></option>
                  

</select>        
                </div>
              </div>
</div>



<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <select name="user_unit" id="user_unit" class="form-control custom-select"   placeholder="<?php echo $this->lang->line("unit");?>"  disabled="disabled">
                      <option value=""><?php echo $this->lang->line("user_unit");?></option>
                  

</select>        
                </div>
              </div>
</div>



<div class="form-row">
<div class="col-md-12" style="margin-top: 10px;">
                <div class="form-label-group">
              
                <select name="user_hf_group" id="user_hf_group" class="form-control custom-select"   placeholder="<?php echo $this->lang->line("unit");?>"  disabled="disabled">
                      <option value=""><?php echo $this->lang->line("user_hf_group");?></option>
                      <?php for($i=1;$i<=5;$i++){?>
                      <option value="<?php echo $i;?>"><?php echo $this->lang->line("user_hf_group_".$i);?></option>
                      <?php } ?>

</select>        
                </div>
              </div>
</div>


<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <a class="btn btn-success btn-block"   id="btnSubmit"><?php echo $this->lang->line('submit');?></a>
               
               
                </div>
              </div>
</div>

        </form>
        </div>
      </div>
    </div>
</div>


<div class="card">
<div class="card-header bg-merah" id="headercard"><?php
$max = ceil($total/50);
  if($this->uri->segment(4)>5 && ($this->uri->segment(4)+5)>6){

  
   if($this->uri->segment(4)==$max || ($this->uri->segment(4)+2)>$max){ 
     $maxnum = 1+$this->uri->segment(4);
  }else{ 
    $maxnum = 3+$this->uri->segment(4);
  }
  
  $start = $maxnum-5;
  $prev = $start-1; 

}else{
      $maxnum = 6;
      $start = 1;
      $prev = 1;

  }
  ?>
<ul class="pagination pagination-sm justify-content-center">
  <li class="page-item prev"><a class="page-link fas fa-arrow-circle-left" href="<?php echo $prev;?>">&nbsp;</a></li>
  

<?php
//echo $max;
  for($i=$start;$i<=($maxnum-1);$i++){

    if($i==$this->uri->segment(4)){
        ?>
        <li class="page-item active" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo $i;?>"><?php echo $i;?></a></li>
        <?php

    }else{

      ?>
<li class="page-item" id="page_<?php echo $i;?>"><a class="page-link" href="<?php echo $i;?>"><?php echo $i;?></a></li>

      <?php
    }

  }
  ?>
  <li class="page-item next"><a class="page-link fas fa-arrow-circle-right" href="<?php echo $maxnum; ?>">&nbsp;</a></li>
</ul>
</div>

<div class="card-body">
<div class="input-group col-lg-4">
    <input type="text" id="search" name="search" class="form-control" placeholder="<?php echo $this->lang->line('search');?>" aria-describedby="src">
    <div class="input-group-append"><span class="btn  btn-success " id="src"><i class="fas fa-search"></i></span></div>&nbsp
    <a href="#cardform" class="btn btn-success btn-xs" id="add" data-toggle="modal" data-target="#modalForm"><i class="fas fa-plus-circle"></i>&nbsp;<?php echo $this->lang->line('add');?></a>
</div>

</div>
<div class="card-body">
<div class="tableFixHead">
<table class="table table-stripe">
<thead>
<tr class="bg-merah">
    <th scope="col"><?php echo $this->lang->line('number');?></th>
    <th scope="col"><?php echo $this->lang->line('username');?></th>
    <th scope="col"><?php echo $this->lang->line('user_fullname');?></th>
    <th scope="col"><?php echo $this->lang->line('username_group');?></th>
    <th scope="col" class="text-center"><?php echo $this->lang->line('action');?></th>
</tr>
</thead>
<tbody id="dataBody">
    <?php
    $i=1;
    foreach($response as $list){ ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->user_name?></td>
    <td scope="col"><?php echo $list->user_fname?></td>
    <td scope="col"><?php echo $this->lang->line($list->group_name);?></td>
    <td scope="col" class="text-center">
    <a href="#" class="btn btn-danger btn-sm" onclick="conf('<?php echo $list->user_id; ?>')"><i class="fas fa-trash-alt"></i>&nbsp;<?php echo $this->lang->line('delete');?></a>
    &nbsp;
    <a href="#" id="btnedit" class="btn btn-success btn-sm"   onclick="edit('<?php echo $list->user_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>
    &nbsp;
    <a href="#" id="chpasswd" class="btn btn-success btn-sm"   onclick="chpass('<?php echo $list->user_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('change_password');?></a>
    </td>
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>

</div>


</div>
<div class="card-footer text-center bg-merah">
</div>
</div>



<div class="modal fade" id="modalPass">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-merah">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('change_password');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">


        <form method="POST" action="<?php echo base_url()."account/user/add";?>" id="shipper">
        <div class="form-row">
<div class="col-md-12">
                <div class="form-label-group">
              <input type="hidden" id="pass_user_id" value="">
                <input type="text" id="pass_user_name" name="pass_user_name" class="form-control" placeholder="<?php echo $this->lang->line('username');?>" required="required" autofocus="autofocus" readonly="readonly">
                  <label for="pass_user_name"><?php echo $this->lang->line('username');?></label>
            
                </div>
              </div>
</div>



<div class="form-row">
<div class="col-md-12"  style="padding-top:10px">
                <div class="form-label-group">
                <input type="password" id="pass_new_user_passwd" name="pass_new_user_passwd" class="form-control" placeholder="<?php echo $this->lang->line('password');?>" required="required" autofocus="autofocus">
                  <label for="pass_new_user_passwd"><?php echo $this->lang->line('new_password');?></label>
            
                </div>
              </div>
</div>



<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <a class="btn btn-success btn-block"   id="btnChpass"><?php echo $this->lang->line('submit');?></a>
               
               
                </div>
              </div>
</div>


        </form>




        </div>
        <div class="modal-footer bg-merah">&nbsp;</div>
      </div>
    </div>
</div>


<script>
    function conf(id){
        if(confirm("<?php echo $this->lang->line("delete_confirmation");?>")){
            document.location = "<?php echo base_url()."account/user/delete/";?>"+id;
        }
    }

    function chpass(id){
      $('#modalPass').modal('show');
      $.ajax({
        url:"<?php echo base_url()."account/user/detail/";?>"+id,
        type:"POST",
        dataType:"json",
        success:function(jdata){
          $('#pass_user_id').val(jdata.response.user_id);
          $('#pass_user_name').val(jdata.response.user_name);
        }
      })
    }
    function edit(id){
      $('.preloader').fadeIn();
      $.ajax({
        url:"<?php echo base_url()."account/user/detail/";?>"+id,
        type:"POST",
        dataType:"json",
        success:function(jdata){
          $('#user_passwd').attr('disabled','disabled');
          $('.user_pass').hide();
          $('#user_id').val(jdata.response.user_id);
          $('#user_name').val(jdata.response.user_name);
          $('#user_fullname').val(jdata.response.user_fname);
          $('#user_phnumber').val(jdata.response.user_phnumber);
          $('#user_group').val(jdata.response.user_group);
          if(jdata.response.user_province!=''){
          $('#user_province').removeAttr("disabled").val(jdata.response.user_province);
          }

          if(jdata.response.user_district!=''){
            $.ajax({
        url :'<?php echo base_url()."master/district/districtbyprovince";?>',
        type:'POST',
        dataType:'json',
        data:{
            'province_code':$('#user_province').val()
        },
        success:function(jsondata){

            var str ='<option value=""><?php echo $this->lang->line('district');?></option>';
            $.each(jsondata.response,function(i,item){
              if(jdata.response.user_district==item.district_code){
                str +='<option value="'+item.district_code+'" selected>'+item.district_name+'</option>';
              }else{
                str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
              }
            })
            $('#user_district').html(str).removeAttr('disabled').val(jdata.response.user_district);
            $(".preloader").fadeOut();
        }
    });
    //alert(jdata.response.user_district);
         // $('#user_district').removeAttr("disabled").val(jdata.response.user_district);
          }
          if(jdata.response.user_unit!=''){

            if(jdata.response.user_group=='6'){
    $(".preloader").fadeIn();
    $.ajax({
        url :'<?php echo base_url()."master/hf/hfbydistrict";?>',
        type:'POST',
        dataType:'json',
        data:{
            'hf_district':jdata.response.user_district
        },
        success:function(jxdata){

            var str ='<option value=""><?php echo $this->lang->line('health_facility');?></option>';
            $.each(jxdata.response,function(i,item){
                if(jdata.response.user_unit==item.hf_code){
                  str +='<option value="'+item.hf_code+'" selected>'+item.hf_name+'</option>';

                }else{
                str +='<option value="'+item.hf_code+'">'+item.hf_name+'</option>';
                }
            })
            $('#user_unit').html(str).removeAttr('disabled');
            $('#user_hf_group').removeAttr('disabled');
            $(".preloader").fadeOut();
        }
    })
    $('#user_hf_group').removeAttr('disabled').val(jdata.response.user_hf_group);
  }else if(jdata.response.user_group=='7'){
    $.ajax({
        url :'<?php echo base_url()."master/shipper/list";?>',
        type:'POST',
        dataType:'json',
        success:function(jxdata){

            var str ='<option value=""><?php echo $this->lang->line('shipper_name');?></option>';
            $.each(jxdata.response,function(i,item){
              if(jdata.response.user_unit==item.shipper_code){
                str +='<option value="'+item.shipper_code+'" selected>'+item.shipper_name+'</option>';

              }else{
                str +='<option value="'+item.shipper_code+'">'+item.shipper_name+'</option>';
              }
            
            
            })
            $('#user_unit').html(str).removeAttr('disabled');
            $(".preloader").fadeOut();
        }
    })
  }




          $('#user_unit').removeAttr("disabled").val(jdata.response.user_unit);
    
          }



          $('#modalForm').modal('show');
          $(".preloader").fadeOut();


        }
      })


    }
    $(document).ready(function(){

      var usergroup = "<?php echo $this->session->userdata('user_group');?>";
      var userprovince = "<?php echo $this->session->userdata('user_province');?>";
      var userdistrict ="<?php echo $this->session->userdata('user_district');?>";
      var userunit = "<?php echo $this->session->userdata('user_unit');?>";

$('#btnChpass').click(function(){
  $.ajax({
            url:"<?php echo base_url()."account/user/update/";?>"+$('#pass_user_id').val(),
            type:"POST",
            dataType:"json",
            data:{
               "user_passwd":$('#pass_new_user_passwd').val()
            },
            success:function(jdata){
                $(".preloader").fadeOut();
                if(jdata.status=="success"){
                    $('#modalPass').modal('hide');
                    alert(jdata.message);
                    document.location = "<?php echo base_url()."account/user/list/1";?>";
                }else{
                    alert(jdata.message);
                }
            }

        })

})

      
      if(usergroup=='3'){
        $('#user_province').removeAttr('disabled').val(userprovince);
        $.ajax({
        url :'<?php echo base_url()."master/district/districtbyprovince";?>',
        type:'POST',
        dataType:'json',
        data:{
            'province_code':userprovince
        },
        success:function(jdata){

            var str ='<option value=""><?php echo $this->lang->line('district');?></option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
            })
            $('#user_district').html(str).removeAttr('disabled');
          //  $(".preloader").fadeOut();
        }
    })

        
      }else if(usergroup=='4'){
        

        $('#user_province').removeAttr('disabled').val(userprovince);

        $.ajax({
        url :'<?php echo base_url()."master/district/detail";?>',
        type:'POST',
        dataType:'json',
        data:{
            'district_code':userdistrict
        },
        success:function(jsdata){

           
                str = '<option value="'+jsdata.district_code+'">'+jsdata.district_name+'</option>';

            $('#user_district').html(str).removeAttr('disabled').val(userdistrict)
          //  $(".preloader").fadeOut();
        }
    })

    $('#user_group').change(function(){
      if($('#user_group').val()=='6'){
   // $(".preloader").fadeIn();
    $.ajax({
        url :'<?php echo base_url()."master/hf/hfbydistrict";?>',
        type:'POST',
        dataType:'json',
        data:{
            'hf_district':$('#user_district').val()
        },
        success:function(jdata){

            var str ='<option value=""><?php echo $this->lang->line('health_facility');?></option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.hf_code+'">'+item.hf_name+'</option>';
            })
            $('#user_unit').html(str).removeAttr('disabled');
            $('#user_hf_group').removeAttr('disabled');
           // $(".preloader").fadeOut();
        }
    })
  }else if($('#user_group').val()=='7'){
    $.ajax({
        url :'<?php echo base_url()."master/shipper/list";?>',
        type:'POST',
        dataType:'json',
        success:function(jdata){

            var str ='<option value=""><?php echo $this->lang->line('shipper_name');?></option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.shipper_code+'">'+item.shipper_name+'</option>';
            })
            $('#user_unit').html(str).removeAttr('disabled');
            $(".preloader").fadeOut();
        }
    })
  }
    })

      }else if(usergroup=='6'){
        $('#user_province').removeAttr('disabled').val(userprovince);
        $.ajax({
        url :'<?php echo base_url()."master/district/detail";?>',
        type:'POST',
        dataType:'json',
        data:{
            'district_code':userdistrict
        },
        success:function(jsdata){

           
                str = '<option value="'+jsdata.district_code+'">'+jsdata.district_name+'</option>';

            $('#user_district').html(str).removeAttr('disabled').val(userdistrict)
          //  $(".preloader").fadeOut();
        }
    })


    $.ajax({
        url :'<?php echo base_url()."master/hf/detail";?>',
        type:'POST',
        dataType:'json',
        data:{
            'hf_code':userunit
        },
        success:function(jsdata){

           
                str = '<option value="'+jsdata.hf_code+'">'+jsdata.hf_name+'</option>';

            $('#user_unit').html(str).removeAttr('disabled').val(userunit)
          //  $(".preloader").fadeOut();
        }
    })
    $('#user_group').val(usergroup);


    $('#user_group').change(function(){
      if($('#user_group').val()=='6'){
        $('#user_hf_group').removeAttr('disabled').show();
   // $(".preloader").fadeIn();
    
  }else if($('#user_group').val()=='7'){
    $.ajax({
        url :'<?php echo base_url()."master/shipper/list";?>',
        type:'POST',
        dataType:'json',
        success:function(jdata){

            var str ='<option value=""><?php echo $this->lang->line('shipper_name');?></option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.shipper_code+'">'+item.shipper_name+'</option>';
            })
            $('#user_unit').html(str).removeAttr('disabled');
            $(".preloader").fadeOut();
            $('#user_hf_group').attr('disabled','disabled').hide();
        }
    })
  }
    })


   


      }










    
        $(".preloader").fadeOut();

        $('#user_group').change(function(){
          if($('#user_group').val()!='1' && $('#user_group').val()!='2'){
            $('#user_province').removeAttr('disabled');
          }else{
            $('#user_province').attr('disabled','disabled');
          }
        });

    $('#user_province').change(function(){
   if($('#user_group').val()!=3 && $('#user_group').val()!=9){

   // $(".preloader").fadeIn();
    $.ajax({
        url :'<?php echo base_url()."master/district/districtbyprovince";?>',
        type:'POST',
        dataType:'json',
        data:{
            'province_code':$('#user_province').val()
        },
        success:function(jdata){

            var str ='<option value=""><?php echo $this->lang->line('district');?></option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.district_code+'">'+item.district_name+'</option>';
            })
            $('#user_district').html(str).removeAttr('disabled');
          //  $(".preloader").fadeOut();
        }
    })
   
   }
});

$('#user_district').change(function(){
  if($('#user_group').val()=='6'){
   // $(".preloader").fadeIn();
    $.ajax({
        url :'<?php echo base_url()."master/hf/hfbydistrict";?>',
        type:'POST',
        dataType:'json',
        data:{
            'hf_district':$('#user_district').val()
        },
        success:function(jdata){

            var str ='<option value=""><?php echo $this->lang->line('health_facility');?></option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.hf_code+'">'+item.hf_name+'</option>';
            })
            $('#user_unit').html(str).removeAttr('disabled');
            $('#user_hf_group').removeAttr('disabled');
           // $(".preloader").fadeOut();
        }
    })
  }else if($('#user_group').val()=='7'){
    $.ajax({
        url :'<?php echo base_url()."master/shipper/list";?>',
        type:'POST',
        dataType:'json',
        success:function(jdata){

            var str ='<option value=""><?php echo $this->lang->line('shipper_name');?></option>';
            $.each(jdata.response,function(i,item){
                str +='<option value="'+item.shipper_code+'">'+item.shipper_name+'</option>';
            })
            $('#user_unit').html(str).removeAttr('disabled');
            $(".preloader").fadeOut();
        }
    })
  }
})

    $('#btnSubmit').click(function(){
      $('#modalForm').modal('hide');
      if($('#user_group').val()=="<?php echo $this->session->userdata('user_group');?>"){
        var adminLevel = "0";
      }else{
        var adminLevel = "1";
      }
        $(".preloader").fadeIn();

        if($('#user_id').val()!=''){


          $.ajax({
            url:"<?php echo base_url()."account/user/update/";?>"+$('#user_id').val(),
            type:"POST",
            dataType:"json",
            data:{
                "user_name":$('#user_name').val(),
                "user_fname":$('#user_fullname').val(),
                "user_phnumber":$('#user_phnumber').val(),
                "user_group":$('#user_group').val(),
                "user_province":$('#user_province').val(),
                "user_district":$('#user_district').val(),
                "user_unit":$('#user_unit').val(),
                "user_hf_group":$('#user_hf_group').val(),
                "user_adminlevel":adminLevel
            },
            success:function(jdata){
                $(".preloader").fadeOut();
                if(jdata.status=="success"){
                   
                    alert(jdata.message);
                    document.location = "<?php echo base_url()."account/user/list/1";?>";
                }else{
                    alert(jdata.message);
                }
            }

        })





        }else{

        $.ajax({
            url:"<?php echo base_url()."account/user/add";?>",
            type:"POST",
            dataType:"json",
            data:{
                "user_name":$('#user_name').val(),
                "user_passwd":$("#user_passwd").val(),
                "user_fname":$('#user_fullname').val(),
                "user_phnumber":$('#user_phnumber').val(),
                "user_group":$('#user_group').val(),
                "user_province":$('#user_province').val(),
                "user_district":$('#user_district').val(),
                "user_unit":$('#user_unit').val(),
                "user_hf_group":$('#user_hf_group').val(),
                "user_adminlevel":adminLevel
            },
            success:function(jdata){
                $(".preloader").fadeOut();
                if(jdata.status=="success"){
                    $('#modalForm').modal('hide');
                    alert(jdata.message);
                    document.location = "<?php echo base_url()."account/user/list/1";?>";
                }else{
                    alert(jdata.message);
                }
            }

        })

        }



    });



    $('#user_name').blur(function(){
      $.ajax({
        url:"<?php echo base_url()."account/user/validation";?>",
        type:"POST",
        dataType:"json",
        data:{
          "user_name":$('#user_name').val()
        },
        success:function(jdata){
          if(jdata.response.user_id!=$('#user_id').val()){
            
           alert('<?php echo $this->lang->line("username_exists");?>');
           $('#user_name').val().focus(); 
          }
        }

      })
    })

    $('#src').click(function(){
$('#dataBody').load('<?php echo base_url()."account/user/search";?>',{
  "search":$('#search').val()
})

    })

    });
    </script>