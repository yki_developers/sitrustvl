<script>
  $(".preloader").fadeIn();
  </script>
<nav aria-label="breadcrumb" style="margin-top:50px">
  <ol class="breadcrumb">
    <li class="breadcrumb-item"><a href="<?php echo base_url()."home";?>"><i class="fas fa-home"></i>&nbsp;<?php echo $this->lang->line('home');?></a></li>
    <li class="breadcrumb-item active" aria-current="page"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('user_group');?></li>
  </ol>
</nav>


<!---- Form Add New Fasyankes -->
<div class="modal fade" id="modalForm">
    <div class="modal-dialog modal-dialog-centered">
      <div class="modal-content">
      
        <!-- Modal Header -->
        <div class="modal-header bg-merah">
          <h4 class="modal-title"><i class="fas fa-database"></i>&nbsp;<?php echo $this->lang->line('data_form');?></h4>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        
        <!-- Modal body -->
        <div class="modal-body">
          
        <form method="POST" action="<?php echo base_url()."account/group/add";?>" id="shipper">
        <div class="form-row">
<div class="col-md-12">
                <div class="form-label-group">
              
                <input type="text" id="group_name" name="group_name" class="form-control" placeholder="<?php echo $this->lang->line('group_name');?>" required="required" autofocus="autofocus">
                  <label for="group_name"><?php echo $this->lang->line('group');?></label>
            
                </div>
              </div>
</div>






<div class="form-row">
<div class="col-md-12" style="padding-top:10px">
                <div class="form-label-group">
                
                <a class="btn btn-success btn-block"   id="btnSubmit"><?php echo $this->lang->line('submit');?></a>
               
               
                </div>
              </div>
</div>

        </form>
        </div>
      </div>
    </div>
</div>


<div class="card">
<div class="card-header bg-merah" id="headercard">&nbsp;
</div>
<div class="card-body">

<table class="table table-stripe">
<thead>
<tr>
    <th scope="col" colspan="3">
    <div class="input-group input-group-sm">
   
</div>
</th>
    <th scope="col" class="text-right">
    &nbsp;
    <a href="#cardform" class="btn btn-success btn-xs" id="add" data-toggle="modal" data-target="#modalForm"><i class="fas fa-plus-circle"></i>&nbsp;<?php echo $this->lang->line('add');?></a>
</th>

</tr>

<tr class="bg-merah">
    <th scope="col"><?php echo $this->lang->line('number');?></th>
    <th scope="col"><?php echo $this->lang->line('group_id');?></th>
    <th scope="col"><?php echo $this->lang->line('group');?></th>
    <th scope="col" class="d-none d-sm-block text-center"><?php echo $this->lang->line('action');?></th>
</tr>
</thead>
<tbody id="dataBody">
    <?php
    $i=1;
    foreach($response as $list){ ?>
    <tr>
    <th scope="row"><?php echo $i;?></th>
    <td scope="col"><?php echo $list->group_name?></td>
    <td scope="col"><?php echo $this->lang->line($list->group_name);?></td>
    <td scope="col" class="text-center d-none d-sm-block">
    <a href="#" class="btn btn-danger btn-sm" onclick="conf('<?php echo $list->group_id; ?>')"><i class="fas fa-trash-alt"></i>&nbsp;<?php echo $this->lang->line('delete');?></a>
    &nbsp;
    <a href="#" id="btnedit" class="btn btn-success btn-sm"   onclick="edit('<?php echo $list->group_id;?>')"><i class="fas fa-edit"></i>&nbsp;<?php echo $this->lang->line('edit');?></a>

    </td>
    
</tr>

    <?php $i++;
} ?>
</tbody>
</table>


</div>
<div class="card-footer text-center">
</div>
</div>

<script>
    function conf(id){
        if(confirm("<?php echo $this->lang->line("delete_confirmation");?>")){
            document.location = "<?php echo base_url()."account/group/delete/";?>"+id;
        }
    }
    $(document).ready(function(){
    
        $(".preloader").fadeOut();

        

    

    $('#btnSubmit').click(function(){
        $(".preloader").fadeIn();

        $.ajax({
            url:"<?php echo base_url()."account/group/add";?>",
            type:"POST",
            dataType:"json",
            data:{
               "group_name":$('#group_name').val()
            },
            success:function(jdata){
                $(".preloader").fadeOut();
                if(jdata.status=="success"){
                    $('#modalForm').modal();
                    alert(jdata.message);
                    document.location = "<?php echo base_url()."account/group/list";?>";
                }else{
                    alert(jdata.message);
                }
            }

        })



    });



    });
    </script>