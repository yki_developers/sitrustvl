<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
if ( ! function_exists('xpert_result'))
{
    function xpert_result($val){
        switch($val){
            case 1:$val= "negative";break;
            case 2:$val="rs";break;
            case 3:$val="rr";break;
            case 4:$val="ri";break;
            case 5:$val="invalid";break;
            case 6:$val="error";break;
            case 7:$val="nr";break;
        }
        return $val;
    }
}

if(!function_exists('age_range')){
    function age_range($val){
        switch($val){
            case $val<=75 :$val="<u><</u>2 bln";break;
            case $val>75 && $val<=195 :$val="2 - 6 bln";break;
            case $val>195 && $val<=375 :$val="6 - 12 bln";break;
            case $val>375 && $val<=555 :$val="12 - 18 bln";break;
            case $val>555:$val=">18 bln";break;
        }
        return $val;
    }
}


if(!function_exists('presumptive_type')){
    function presumptive_type($val){
        switch($val){
            case 1:$val="DS TB";break;
            case 2:$val="DR TB";break;
        }
        return $val;
    }
}

if(!function_exists('stool_app')){
    function stool_app($val){
        switch($val){
            case 1:$val="Formed";break;
            case 2:$val="Unformed";break;
            case 3:$val="liquid";break;
        }
        return $val;
    }
}


if(!function_exists('sputum_app')){
    function sputum_app($val){
        switch($val){
            case 1:$val="Mucoid purulent";break;
            case 2:$val="Bloodstreak";break;
            case 3:$val="Saliva";break;
        }
        return $val;
    }
}

if(!function_exists('cold_chain')){
    function cold_chain($val){
        switch($val){
            case 1:$val="cold_chain_yes";break;
            case 2:$val="cold_chain_partly";break;
            case 3:$val="cold_chain_no";break;
        }
        return $val;
    }
}

if(!function_exists('micro_result')){
    function micro_result($val){
        switch($val){
            case 1:$val="+3";break;
            case 2:$val="+2";break;
            case 3:$val="+1";break;
            case 4:$val="1-9";break;
            case 5:$val="Negative";
        }
        return $val;
    }
}

if(!function_exists('specimen_type')){
    function specimen_type($val){
        switch($val){
            case 1:$val="sputum";break;
            case 2:$val="stool";break;
        }
        return $val;
    }
}

if(!function_exists('specimen_test')){
    function specimen_test($val){
        switch($val){
            case 1:$val="Microscopic";break;
            case 2:$val="xpert";break;
        }
        return $val;
    }
}

if(!function_exists('anatomic_location')){
    function anatomic_location($val){
        switch($val){
            case 1:$val="Pulmonary";break;
            case 2:$val="Extrapulmonary";break;
        }
        return $val;
    }
}


if(!function_exists('exam_purpose')){
    function exam_purpose($val){
        switch($val){
            case 1:$val="Diagnoses - DS TB";break;
            case 2:$val="Diagnoses - DR TB";break;
            case 3:$val="Follow UP";break;
            case 4:$val="Follow Up After Completion";break;
        }
        return $val;
    }
}

if(!function_exists('specimen_sex')){
    function specimen_sex($val){
        switch($val){
            case 1:$val="L";break;
            case 2:$val="P";break;

        }
        return $val;
    }
}




if(!function_exists('order_status')){
    function order_status($val){
        switch($val){
            case 0:$val="status_draft";break;
            case 1:$val="status_waiting_confirmation";break;
            case 2:$val="status_waiting_pickup";break;
            case 3:$val="status_picked_up";break;
            case 4:$val="status_delivered";break;
            case 5:$val="status_received";break;
            case 6:$val="status_finished";break;

        }
        return $val;
    }
}



if(!function_exists('specimen_vl_status')){
    function specimen_vl_status($val){
        switch($val){
            case 0:$val="Draft";break;
            case 1:$val="Menunggu Konfirmasi Lab";break;
            case 2:$val="Menunggu diambil kurir";break;
            case 3:$val="diambil kurir";break;
            case 4:$val="Sampai tujuan";break;
            case 5:$val="Diterima";break;
            case 6:$val="Selesai";break;
        }
        return $val;
    }
}


if(!function_exists('specimen_vl_type')){
    function specimen_vl_type($val){
        switch($val){
            case 1:$val="Plasma";break;
            case 2:$val="Serum";break;
            case 3:$val="Whole blood";break;
            case 4:$val="DBS";break;
        }
        return $val;
    }
}


if(!function_exists('specimen_vl_test')){
    function specimen_vl_test($val){
        switch($val){
            case 1:$val="DNA HIV/EID";break;
            case 2:$val="RNA HIV/VL";break;
        }
        return $val;
    }
}


if(!function_exists('specimen_eid_number')){
    function specimen_eid_number($val){
        switch($val){
            case 1:$val="DNA HIV/EID 1";break;
            case 2:$val="DNA HIV/EID 2";break;
            case 3:$val="DNA HIV/EID 3";break;
        }
        return $val;
    }
}


if(!function_exists('specimen_vl_approval')){
    function specimen_vl_approval($val){
        switch($val){
            case 0:$val="Belum dikonfirmasi";break;
            case 1:$val="Menunggu diambil kurir";break;
            case 2:$val="Ditolak";break;
        }
        return $val;
    }
}



if(!function_exists('specimen_test_periode')){
    function specimen_test_periode($val){
       if($val>=3 && $val<=9){
           $val = "6 bln";
       }elseif($val>=10 && $val<=18){
           $val = "12 bln";
       }elseif($val>=19 && $val<=30){
        $val = "24 bln";
    }elseif($val>=31 && $val<=42){
        $val = "36 bln";
    }elseif($val>=43 && $val<=54){
        $val = "48 bln";
    }elseif($val>=55 && $val<=66){
        $val = "60 bln";
    }elseif($val>=67){
        $val = "72 bln";
    }
        return "0 bln";
    }
}



if(!function_exists('db_date')){
    function db_date($val){
        $dt = explode("-",$val);
        $dbformat = $dt[2]."-".$dt[1]."-".$dt[0];
        return $dbformat;
    }
}

if(!function_exists('local_date')){
    function local_date($val){
        $dt = explode($val,"-");
        $dbformat = $dt[2]."-".$dt[1]."-".$dt[0];
        return $dbformat;
    }
}

if(!function_exists('remove_data')){
    function remove_data($array,$keys){
        foreach($keys as $key){
            unset($array[$key]);
        }
        return $array;
    }
}


if(!function_exists('result_exam')){
    function result_exam($val){
        switch($val){
            case 1:$val=">10000000";break;
            case 2:$val="<40";break;
            case 4:$val="TIDAK TERDETEKSI";break;
            case 5:$val="INVALID";break;
            case 6:$val="ERROR";break;
            case 7:$val="TIDAK ADA HASIL";break;
            default : $val=$val;break;

        }
        return $val;
    }
}


if(!function_exists('result_eid')){
    function result_eid($val){
        switch($val){
            case 1:$val="TIDAK TERDETEKSI";break;
            case 2:$val="TERDETEKSI";break;
            case 3:$val="INVALID";break;
            case 4:$val="ERROR";break;
            case 5:$val="TIDAK ADA HASIL";break;

        }
        return $val;
    }
}

if(!function_exists('specimen_condition')){
    function specimen_condition($val){
        switch($val){
            case "1":$val="Kondisi Baik";break;
            case "0":$val="Kondisi Rusak";break;
            default:$val="";break;

        }
        return $val;
        }
    }

if(!function_exists('date_ark')){
    function date_ark($val){
        if($val=='0000-00-00' || $val=='00-00-0000'){
            return "00-00-0000";

        }else{
        $xp = explode("-",$val);
        /*
        $bln = array(
            "01"=>"Jan",
            "02"=>"Feb",
            "03"=>"Mar",
            "04"=>"Apr",
            "05"=>"Mei",
            "06"=>"Jun",
            "07"=>"Jul",
            "08"=>"Agt",
            "09"=>"Sep",
            "10"=>"Okt",
            "11"=>"Nop",
            "12"=>"Des"
        );
        if(strlen($xp[0])!=4){
        $tgl = $xp[0];
        $bl = $xp[1];
        $th = $xp[2];
        }else{
            $tgl = $xp[2];
        $bl = $xp[1];
        $th = $xp[0];
        }*/

       // return $tgl." ".$bln[$bl]." ".$th;
       return $xp[1]."/".$xp[0]."/".$xp[2];
    }
    }
}

if(!function_exists('localdate')){
    function localdate($val){
        
        $xp = explode("-",$val);
       
       return $xp[2]."-".$xp[1]."-".$xp[0];
    }
    }
