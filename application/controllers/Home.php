<?php
Class Home extends CI_Controller{
    
    public function __construct(){
        parent::__construct();
      if(!$this->session->userdata('user_name')){
            redirect(base_url()."login");
        }
        $this->load->model('sys/M_module');
        $this->load->model("transactional/M_order");
        $this->load->model("transactional/M_pickup");
    }

    public function index(){
        //echo "xhome";
        //print_r($this->session->userdata());

    if($this->session->userdata('user_group')=='7'){
        $id = array("order_shipper_id"=>$this->session->userdata('user_unit'),"hf_district"=>$this->session->userdata('user_district'),"order_approved"=>"1");
    }else{
        
        //$neworder = array("order_hf_recipient"=>$this->session->userdata('user_unit'),"order_approved"=>"0");
        
        $id =array("order_shipper_id"=>$this->session->userdata('user_unit'),"hf_district"=>$this->session->userdata('user_district'),"order_approved"=>"1");
    }

    //print_r($id);
    $data['parent'] = $this->M_module->getParentModule($this->session->userdata('user_group'));
    $data['userdata'] = $this->session->userdata();

    if($this->session->userdata('user_group')=='6'){
        if($this->session->userdata('user_hf_group')=='5'){
            $neworder = array("order_hf_recipient"=>$this->session->userdata('user_unit'),"order_approved"=>"0");
    
        }else{
            $neworder = array("order_hf_sender"=>$this->session->userdata('user_unit'),"order_approved"=>"0");
        }
        $data['neworder'] = $this->M_order->getOrderInfo($neworder,'1');
        $data['neworderEid'] = $this->M_order->getOrderInfo($neworder,'1','1');
    } 
   
    $check = $this->M_pickup->checkNetwork($this->session->userdata('user_name'));
    if($check>0){
        $data['pickup'] = $this->M_order->getOrderPickup($this->session->userdata('user_name'));
    }else{
    $data['pickup'] = $this->M_order->getOrderInfo($id,'2');
    }
   
//echo "xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx".$check;
//echo $data['pickup'] = $this->M_order->getOrderPickup($this->session->userdata('user_name'));
    //echo $this->M_order->getOrderInfo($id,'2');
    if($check>0){
        $data['ondelivery'] = $this->M_order->getOrderDelivered($this->session->userdata('user_name'));
    }else{
    $data['ondelivery'] = $this->M_order->getOrderInfo($id,'3');
    }

    $data['delivered'] = $this->M_order->getOrderInfo($id,'4');
    $data['deliveredEid'] = $this->M_order->getOrderInfo($id,'4','1');

    $data['result'] = $this->M_order->getOrderInfo($id,'5');
    $data['resultEid'] = $this->M_order->getOrderInfo($id,'5','1');

    $data['orderread'] = $this->M_order->orderRead($this->session->userdata('user_unit'));


   $this->template->renderpage('frontpage/home',$data);
    }

}