<?php
Class Login extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('auth/M_login');
    }

    public function index(){
    if($this->session->userdata('username')!=null){
        redirect('home');
    }
       $data['lang']  = $this->M_login->language();
      
       $this->template->loginpage('auth/login',$data);
    }

    public function auth(){
        $param = array(
            "user_name"=>$this->input->post('user_name'),
            "user_passwd"=>md5($this->input->post('user_password'))
        );

        $response = $this->M_login->getUser($param);
       // print_r($response);
        if($response){
            $json['status'] = '200';
            $this->session->set_userdata((array)$response);
            $this->M_login->update(array("user_lastlog"=>date("Y-m-d H:i:s"),"user_playerid_web"=>$this->input->post("playerid")),$param['user_name']);
           
        }else{
            $json['status'] = "error";
            $response = false;
        }
        $json['response'] =$response;
       echo json_encode($json);
    }

    public function logout(){
        $userid = $this->session->userdata('user_id');
        $this->M_login->update(array("user_playerid_web"=>null),$userid);
        $this->session->sess_destroy();
        
    redirect('login');
    }

    public function playerid(){
        $json['response'] = $this->M_login->playerid($this->input->post('playerId'));
        $data = array("playerid"=>$this->input->post('playerId'));
        $this->session->set_userdata($data);
        return json_encode($json);
    }
}