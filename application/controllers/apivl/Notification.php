<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;
Class Notification extends REST_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('notification/M_notification');
       // $this->load->model('jne/M_jne');
        
    }

    public function index_get($id){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
          
       
       if($result = $this->M_notification->getDataList($id)){
        //$this->M_order->updateRead($id);
       // $result["decode"] = $decodedToken;
           // $result->total_specimen = $this->M_order->countSpecimen($result->order_id);
     


        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;
    
    

    }


    public function unread_get($id){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
        
       
       if($result['unread_notif']= $this->M_notification->getUnread($id)){
        $this->M_notification->update($id);
      
           // $result->total_specimen = $this->M_order->countSpecimen($result->order_id);
     


        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;

    }


    public function detail_get($id){

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
          
       
       if($result = $this->M_notification->getDetail($id)){
        $this->M_notification->update($id);
      
           // $result->total_specimen = $this->M_order->countSpecimen($result->order_id);
     


        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;

    }







    public function readall_put($id){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
          
       
       if($result = $this->M_notification->readAll($id)){
        //$this->M_notification->update($id);
      
           // $result->total_specimen = $this->M_order->countSpecimen($result->order_id);
     


        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;

    }


}

