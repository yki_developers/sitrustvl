<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

Class Version extends REST_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('sys/M_version');
    }

    public function index_get(){
        $version = $this->M_version->getversion();
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$version);
        $this->set_response($response,REST_Controller::HTTP_OK); 
    }



}