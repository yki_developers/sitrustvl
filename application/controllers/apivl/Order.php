<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;
Class Order extends REST_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('transactional/M_order');
        $this->load->model("notification/M_notification");
        $this->load->model("auth/M_login");
       // $this->load->model('jne/M_jne');
        
    }

    public function index_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
        }else{
            $rdata = $row;
        }

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($result = $this->M_order->insert($rdata)){
        $ordernumber = $rdata['order_hf_sender']."/".date("Ymd")."/".$result;
        $this->M_order->update(array("order_number"=>$ordernumber),$result);

        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;
    
    }


    public function index_get($id){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
          
       
       if($result = $this->M_order->getDataList($id)){
        $this->M_order->updateRead($id);
      
           // $result->total_specimen = $this->M_order->countSpecimen($result->order_id);
     


        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;
    
    
    
    }


    public function listall_get($id){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
          
       
       if($result = $this->M_order->getDataListAll($id)){
        $this->M_order->updateRead($id);
      
           // $result->total_specimen = $this->M_order->countSpecimen($result->order_id);
     


        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;
    
    
    
    }


    public function eid_get($id){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
          
       
       if($result = $this->M_order->getDataEidList($id)){
        $this->M_order->updateRead($id);
      
           // $result->total_specimen = $this->M_order->countSpecimen($result->order_id);
     


        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;
    
    
    
    }




    public function confirmation_get($id){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
          
       
       if($result = $this->M_order->confirmation($id)){
        $this->M_order->updateRead($id);
      
           // $result->total_specimen = $this->M_order->countSpecimen($result->order_id);
     


        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;
    
    
    
    }



    public function received_get($id){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
          
       
       if($result = $this->M_order->received($id)){
        $this->M_order->updateRead($id);
      
           // $result->total_specimen = $this->M_order->countSpecimen($result->order_id);
     


        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;
    
    
    
    }



    public function index_put($id){
       
            $rdata = $this->put();
            
        

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       

       if($result = $this->M_order->update($rdata,$id)){

        if(array_key_exists("order_status",$rdata)){
            $user = explode("::",$xp[5]);
            $par = array("user_name"=>$user[0]);
            $userData = $this->M_login->getUser($par);
            

            $lab  = $this->M_order->detail(array("order_id"=>$id));
            $playerid = $this->M_reverse->getPlayerId($lab->order_hf_recipient);
            $hf_sender = $this->M_reverse->hfName($lab->order_hf_sender);
            $hf_recipient = $this->M_reverse->hfName($lab->order_hf_recipient);
            $shipper_name = $this->M_reverse->shipperName($lab->order_shipper_id);

            if($rdata['order_status']=='1'){
            $data = array("status_order_id"=>$id,"status_date_update"=>date("Y-m-d H:i:s"),"status_description"=>"Menunggu Konfirmasi Lab");
            $udata = array("order_date"=>date("Y-m-d"));
            $this->M_order->update($udata,$id);
            $notxt = $hf_sender->hf_name." akan mengirimkan specimen";
            $recipient = $lab->order_hf_recipient;
            $sender = $lab->order_hf_sender;
            }elseif($rdata['order_status']=='2'){
                if($rdata['order_approved']=='2'){
                    $data = array("status_order_id"=>$id,"status_date_update"=>date("Y-m-d H:i:s"),"status_description"=>"Terkonfirmasi - Ditolak (".$rdata['order_reason'].")");
                    $notxt = $hf_recipient->hf_name." Menolak permintaan";
                    $sender = $lab->order_hf_recipient;
            $recipient = $lab->order_hf_sender;


                }else{
                    $ndata = array("order_date_confirm"=>date("Y-m-d H:i:s"));
                    $this->M_order->update($ndata,$id);
                $data = array("status_order_id"=>$id,"status_date_update"=>date("Y-m-d H:i:s"),"status_description"=>"Terkonfirmasi - Menunggu diambil");
                $notxt = $hf_recipient->hf_name." Menerima permintaan pemeriksaan, silahkan menunggu kurir datang";
                $sender = $lab->order_hf_recipient;
                $recipient = $lab->order_hf_sender;
               // $courier = $lab->order_shipper_id;
               // $shipper = $this->M_reverse->getPlayerId($lab->order_shipper_id);
               // $txtnote = $hf_sender->hf_name." Mengajukan order untuk pengiriman";
                /*
                foreach($shipper as $listplay){
                   
                  // $this->notif($txtnote,$listplay->user_playerid);

                   $content = array("notif_order_id"=>$id,"notif_message"=>$txtnote,"notif_recipient"=>$courier,"notif_sender"=>$sender);
           
            $this->M_notification->insert($content);
                  }*/    
            
            }

            
            }elseif($rdata['order_status']=='3'){
                
                $data = array("status_order_id"=>$id,"status_date_update"=>date("Y-m-d H:i:s"),"status_description"=>"Diambil oleh kurir");
                $notxt = $shipper_name->shipper_name." Telah mengambil paket untuk dikirim";
                $sender = $lab->order_shipper_id;
                $recipient = $lab->order_hf_sender;
                $date = array("order_date_pickup"=>date("Y-m-d H:i:s"));
            $this->M_order->update($date,$id);
            }elseif($rdata['order_status']=='4'){
                
                $data = array("status_order_id"=>$id,"status_date_update"=>date("Y-m-d H:i:s"),"status_description"=>"Paket sampai tujuan");
                $notxt = " Paket telah sampai di ".$hf_recipient->hf_name;
                $recipient = $lab->order_hf_sender;
            $sender = $lab->order_shipper_id;
            $date = array("order_date_delivered"=>date("Y-m-d H:i:s"));
            $this->M_order->update($date,$id);
            }elseif($rdata['order_status']=='5'){
                $notxt = " Paket telah diterima di ".$hf_recipient->hf_name;
                $data = array("status_order_id"=>$id,"status_date_update"=>date("Y-m-d H:i:s"),"status_description"=>"Paket Sudah diterima");
                $recipient = $lab->order_hf_sender;
                $sender = $lab->order_hf_recipient;
                $date = array("order_date_received"=>date("Y-m-d H:i:s"));
                $this->M_order->update($date,$id);
            }elseif($rdata['order_status']=='6'){
                
                $data = array("status_order_id"=>$id,"status_date_update"=>date("Y-m-d H:i:s"),"status_description"=>"Selesai");
                $notxt = "Hasil pemeriksaan specimen telah keluar";
                $recipient = $lab->order_hf_sender;
            $sender = $lab->order_hf_recipient;
            }elseif($rdata['order_status']=="0"){
                $data = array("status_order_id"=>$id,"status_date_update"=>date("Y-m-d H:i:s"),"status_description"=>"Order Dibatalkan");
                $notxt = "Order Dibatalkan";
                $sender = $lab->order_hf_sender;
            $recipient = $lab->order_hf_recipient;
            }
          
            $this->M_order->updateStatus($data);
            $content = array("notif_order_id"=>$id,"notif_message"=>$notxt,"notif_recipient"=>$recipient,"notif_sender"=>$sender);
           
            $this->M_notification->insert($content);
           
            foreach($playerid as $listplay){
            $this->notif($notxt,$listplay->user_playerid);
            }
        }


        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;
    
    }

    private function notif($rdata,$playerid){
        $content = array("en"=>$rdata);
        $id = $this->input->post('idcontent');

        $data = array(
            //"app_id"=>"0d689450-d333-4f4e-be6b-ce63043cdb6e", //(production)
            "app_id"=>"96541c65-2e40-403d-bc77-99484284e515", //develop
            "include_player_ids"=>array($playerid),

            /* "included_segments" => array(
                'All'
            ),*/
            "data"=>array("idcontent"=>$id),
            "contents"=>$content
        );

        $jsondata = json_encode($data);
        //3print_r($jsondata);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Content-Type: application/json; charset=utf-8',
            //'Authorization: Basic MmRlZGFlNWQtNTE5MC00ZWZlLTgzMmEtZDg2MTYzMDE3MTEx' // Production
            'Authorization: Basic OWU0OTliYmQtOGQwYi00NTQzLWFmMTMtYTIyMzY5NDIyZDFk' // development
        ));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsondata);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    public function sendorder_put($id){
        $rdata = $this->put();
            
        

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($result = $this->M_order->update($rdata,$id)){
           if(array_key_exists("order_status",$rdata)){

        $data = array("status_order_id"=>$id,"status_date_update"=>date("Y-m-d H:i:s"),"status_description"=>"Menunggu Konfirmasi Lab");
        $this->M_order->updateStatus($data);
        $lab  = $this->M_order->detail(array("order_id"=>$id));
        $playerid = $this->M_reverse->getPlayerId($lab->order_hf_recipient);
        $hf_sender = $this->M_reverse->hfName($lab->order_hf_sender);
        foreach($playerid as $listplay){
          echo $this->notif($hf_sender->hf_name." Akan mengirimkan Specimen",$listplay->user_playerid);
        }
    }






        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;
    }


    public function index_delete($id){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($result = $this->M_order->delete($id)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;
    
    
    
    }



    public function detail_get($id){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($result = $this->M_order->detail(array("order_id"=>$id))){
           $result->total_specimen = $this->M_order->countSpecimen($id);
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;
    
    
    
    }


    


}
