<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;
Class Result extends REST_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('transactional/M_result');
        $this->load->model('transactional/M_order');
       // $this->load->model('jne/M_jne');
        
    }

    public function index_get($id){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
          
       
       if($result = $this->M_result->getDataList($id)){
        $this->M_order->updateRead($id);
      
           // $result->total_specimen = $this->M_order->countSpecimen($result->order_id);
     


        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;
    
    
    
    }




    public function vldetail_get($id){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
          
       
       if($result = $this->M_result->detail($id)){
        //$this->M_order->updateRead($id);
      
           // $result->total_specimen = $this->M_order->countSpecimen($result->order_id);
     


        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;
    
    
    
    }

    public function vlinsert_post(){

        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
        }else{
            $rdata = $row;
        }

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
          
       
       if($result = $this->M_result->vlinsert($rdata)){
           $this->M_result->resultFlagUpdate($rdata['vl_result_specimen_num_id']);
       // $this->M_order->updateRead($id);
      
           // $result->total_specimen = $this->M_order->countSpecimen($result->order_id);
     


        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;
    
    
    
    }




    public function vlupdate_put($id){

        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
    
            $rdata = $this->put();
        

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
          
       
       if($result = $this->M_result->update($rdata,$id)){
         //  $this->M_result->resultFlagUpdate($rdata['vl_result_specimen_num_id']);
       // $this->M_order->updateRead($id);
      
           // $result->total_specimen = $this->M_order->countSpecimen($result->order_id);
     


        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;
    
    
    
    }



    public function eidinsert_post(){

        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $rdata = $this->input->post();
        }else{
            $rdata = $row;
        }

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
          
       
       if($result = $this->M_result->eidinsert($rdata)){
       // $this->M_order->updateRead($id);
      
           // $result->total_specimen = $this->M_order->countSpecimen($result->order_id);
     

           $this->M_result->resultFlagUpdate($rdata['eid_result_specimen_num_id']);
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;
    
    
    
    }


    public function eidupdate_put($id){

        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
    
            $rdata = $this->put();
        

        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
          
       
       if($result = $this->M_result->eidupdate($rdata,$id)){
          // $this->M_result->resultFlagUpdate($rdata['eid_result_specimen_num_id']);
       // $this->M_order->updateRead($id);
      
           // $result->total_specimen = $this->M_order->countSpecimen($result->order_id);
     


        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;
    
    
    
    }



    public function eiddetail_get($id){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
          
       
       if($result = $this->M_result->eiddetail($id)){
        //$this->M_order->updateRead($id);
      
           // $result->total_specimen = $this->M_order->countSpecimen($result->order_id);
     


        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;
    
    
    
    }


}