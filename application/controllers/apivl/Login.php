<?php
defined('BASEPATH') OR exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

Class Login extends REST_controller{ 
    private $urlReset;
public function __construct(){
    parent::__construct();
    header('Access-Control-Allow-Origin: *');
    header("Access-Control-Allow-Headers: xkey, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Request-Method");
    header("Access-Control-Allow-Methods: GET, POST, OPTIONS, PUT, DELETE");
    $this->load->model('auth/M_login');

}

public function index_post(){
    $data = file_get_contents("php://input");
    $row = json_decode($data,true);

    if(!$this->input->post()){
$authUser = array("user_name"=>$row['user_name'],"user_passwd"=>md5($row['user_password']));
$playerid = $row['user_playerid'];
    }else{
$authUser =  array("user_name"=>$this->input->post('user_name'),"user_passwd"=>md5($this->input->post('user_password')));
$playerid = $this->input->post('user_playerid');
    }

   

    $headers=$this->input->request_headers();
    if($dataq = $this->M_login->tokenGet($headers['Xkey'])){
        $src_token = $dataq->token."_".$authUser['user_name']."::".$this->input->post('user_password');
        $token = AUTHORIZATION::generateToken($src_token);
       
        if($id=$this->M_login->getUser($authUser)){
            if($id->group_id=='6'){
                $unit = $this->M_login->getFaskes($id->user_unit);
                $id->hf_name = $unit->hf_name;
            }elseif($id->group_id=='7'){
                $unit = $this->M_login->getKurir($id->user_unit);
                $id->shipper_name = $unit->shipper_name;
            }


            

            $id->role = $this->M_login->getDataRoleList($id->group_id);
            $id->token = $token;
            $lastlog = array("user_playerid"=>$playerid,"user_lastlog"=>date("Y-m-d H:i:s"));
            $up= $this->M_login->update($lastlog,$authUser['user_name']);
            $ret = array(
                "status"=>REST_Controller::HTTP_OK,
                "error"=>null,
                "message"=>"Accepted",
                "response"=>$id
            );
            $this->set_response($ret, REST_Controller::HTTP_OK);
        }else{

            $ret = array(
                "status"=>REST_Controller::HTTP_OK,
                "error"=>null,
                "message"=>"Accepted",
                "response"=>false
            );
            
            $this->set_response($ret, REST_Controller::HTTP_OK);

        }   
    
    }
	return;	

}


public function logout_post(){
    $data = file_get_contents("php://input");
    $row = json_decode($data,true);
if($this->input->post()){
    $rdata= $this->input->post("user_name");
}else{
    $rdata = $row['user_name'];
}

$playerid= array("user_playerid"=>null);
if($this->M_login->update($playerid,$rdata)){
    $ret = array(
        "status"=>REST_Controller::HTTP_OK,
        "error"=>null,
        "message"=>"Acceptep",
        "response"=>true
    );
    $this->set_response($ret, REST_Controller::HTTP_OK);
}


}

}
