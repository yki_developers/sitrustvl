<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;
Class Master extends REST_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('geo/M_province');
        $this->load->model('geo/M_district');
        $this->load->model('geo/M_subdistrict');
        $this->load->model('unit/M_healthfacility');
        $this->load->model('unit/M_shipper');
        $this->load->model('network/M_laboratorium');
       // $this->load->model('jne/M_jne');
        
    }


public function province_get(){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($result = $this->M_province->getList()){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;
    
    
    
    }


    public function district_get($id){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($result = $this->M_district->getDistrictByProvince($id)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;
    
    
    
    }



    public function subdistrict_get($id){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($result = $this->M_subdistrict->getByDistrict($id)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;
    
    
    
    }


    public function hf_sender_get($id){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($result = $this->M_healthfacility->getListByDistrict($id)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;
    
    
    
    }




    public function hf_eid_get(){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($result = $this->M_healthfacility->getListEid()){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;
    
    
    
    }


    public function hf_destination_get($id){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($result = $this->M_laboratorium->getDataNetwork($id)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;
    
    
    
    }


    public function shipper_get(){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($result = $this->M_shipper->getDataList()){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;
    
    
    
    }



}