<?php
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;
Class Specimen extends REST_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('transactional/M_specimen');
       // $this->load->model('jne/M_jne');
        
    }

    public function index_post(){
        $data = file_get_contents("php://input");
        $row = json_decode($data,true);
        if($this->input->post()){
            $specimen = $this->input->post();
            $rdata = $this->input->post();
        }else{
            $rdata = $row;
            $specimen = $row;
        }

        

        $rdata = remove_data($rdata,["specimen_order_id","specimen_id","specimen_date_collected","specimen_type","specimen_exam_category","specimen_doctor_name","specimen_time_collected","specimen_eid_number"]);
        $specimen = remove_data($specimen,["patient_name","patient_nid","patient_regnas","patient_med_record","patient_bday","patient_art_date","patient_sex","patient_province","patient_district","patient_subdistrict","patient_address","patient_eid_mother","patient_eid_mother_nid","patient_eid_mother_regnas"]);
        if($this->input->post('specimen_exam_category')=='2'){
        $pid = array("patient_nid"=>$rdata['patient_nid'],"patient_regnas"=>$rdata['patient_regnas']);
        }else{
            $pid = array("patient_nid"=>null,"patient_regnas"=>null);
        }
        
        $sid = array("specimen_id"=>$specimen['specimen_id'],"specimen_order_id"=>$specimen['specimen_order_id']);
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
    
    
           if($xp[3]==$headers['Xkey']){
 $nm = $this->M_specimen->getSpecimen($sid);
 if($nm>0){
    $response = array("status"=>REST_Controller::HTTP_OK,"error"=>"duplicate","message"=>"duplicate specimen id","response"=>false);
    $this->set_response($response,REST_Controller::HTTP_OK);  

 }else{   

    if($this->input->post("specimen_exam_category")=='2'){   
    if(!$m=$this->M_specimen->getPatientId($pid)){

       
        if($result = $this->M_specimen->insert($rdata)){

        $specimen['specimen_patient_id'] = $result;
        $rp = $this->M_specimen->insertSpecimen($specimen);

        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$rp);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>false);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
    
    
    }else{
        $specimen['specimen_patient_id'] = $m->patient_id;
        if($rp = $this->M_specimen->insertSpecimen($specimen)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$rp);
        $this->set_response($response,REST_Controller::HTTP_OK);
        }else{
            $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>false);
            $this->set_response($response,REST_Controller::HTTP_OK);  
        }
    }
}else{

    if($result = $this->M_specimen->insert($rdata)){

        $specimen['specimen_patient_id'] = $result;
        $rp = $this->M_specimen->insertSpecimen($specimen);

        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$rp);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>false);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
}



}


          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;
    
    }






    public function index_put($id){
        $rdata =$this->put();
        //$specimen = $this->put();

        //$rdata = remove_data($rdata,["specimen_order_id","specimen_id","specimen_date_collected","specimen_type","specimen_exam_category","specimen_doctor_name"]);
        //$specimen = remove_data($specimen,["patient_name","patient_nid","patient_regnas","patient_med_record","patient_bday","patient_art_date","patient_sex"]);
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($result = $this->M_specimen->update($rdata,$id)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;
    }

    public function index_get($id){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($result = $this->M_specimen->getDataList($id)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;

    }



    public function index_delete($id){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($result = $this->M_specimen->delete($id)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;

    }

    public function detail_get($id){
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($result = $this->M_specimen->getDetailSpecimen($id)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;

    }


    public function feedback_put($id){

        $rdata =$this->put();
        $headers=$this->input->request_headers();
        if (array_key_exists('Authorization', $headers) && !empty($headers['Authorization'])) {
                     $author = explode(" ",$headers['Authorization']);
                   $decodedToken = AUTHORIZATION::validateToken(str_replace('"',"",$author[1]));
       
                   //return $decodedToken;
           if ($decodedToken != false) {
           $xp = explode("_",$decodedToken);
       if($xp[3]==$headers['Xkey']){
       
       if($result = $this->M_specimen->updateSpecimen($rdata,$id)){
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Success","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
    
       }else{
        $response = array("status"=>REST_Controller::HTTP_OK,"error"=>null,"message"=>"Failed","response"=>$result);
        $this->set_response($response,REST_Controller::HTTP_OK);  
       }
       
          
         
       }else{
        $response = array(
            "status"=>REST_Controller::HTTP_UNAUTHORIZED,
            "error"=>"Invalid Token Authorization",
        );
       $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
                   }
                   
               }else{
                $response = array(
                    "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                    "error"=>"Invalid Token Authorization",
                );
               $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
               }
       
           }else{
       
            $response = array(
                "status"=>REST_Controller::HTTP_UNAUTHORIZED,
                "error"=>"No Token Authorization"
            );
           $this->set_response($response, REST_Controller::HTTP_UNAUTHORIZED);
           }
           return;


    }

}