<?php
Class Order extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('transactional/M_order');
        //$this->load->model('unit/M_healthfacility');
    }

    public function index(){
        $id = $this->session->userdata('user_unit');
        $data['orderlist'] = $this->M_order->getDataList($id);
        $data['lab'] = $this->M_order->getDestination($id);
        $data['kurir'] = $this->M_order->getCourier();
        $this->M_order->updateRead($id);
        $this->template->renderpage('transactional/orderlist',$data);
    }


    public function eid(){
        $id = $this->session->userdata('user_unit');
        $data['orderlist'] = $this->M_order->getDataEidList($id);
        $data['lab'] = $this->M_order->getEidDestination();
        $data['kurir'] = $this->M_order->getCourier();
        $this->M_order->updateRead($id);
        $this->template->renderpage('transactional/eidlist',$data);
    }

    public function datalist(){
        $id = $this->session->userdata('user_unit');
        $data['orderlist'] = $this->M_order->getDataList($id);
        $data['lab'] = $this->M_order->getDestination($id);
        $data['kurir'] = $this->M_order->getCourier();
        $this->load->view('transactional/search_orderlist',$data);
    }

   public function add(){
       $rdata=$this->input->post();
       $response = $this->M_order->insert($rdata);
       if($response!=0){
           $ordernumber = $this->session->userdata('user_unit')."/".date("Ymd")."/".$response;
           if($this->M_order->update(array("order_number"=>$ordernumber),$response)){
               $json = array(
            "status"=>"success",
            "message"=>$this->lang->line("data_save"),
            "order_id"=>$response);
           }else{
            $json = array(
                "status"=>"error",
                "message"=>$this->lang->line("failed"),
                "response"=>false);
           }

           echo json_encode($json);
       }

   }

   private function notif($rdata,$playerid){
    $content = array("en"=>$rdata);
    $id = $this->input->post('idcontent');

    $data = array(
        "app_id"=>"96541c65-2e40-403d-bc77-99484284e515",
        "include_player_ids"=>array($playerid),
        "data"=>array("idcontent"=>$id),
        "contents"=>$content
    );

    $jsondata = json_encode($data);
    //3print_r($jsondata);
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications");
    curl_setopt($ch, CURLOPT_HTTPHEADER, array(
        'Content-Type: application/json; charset=utf-8',
        'Authorization: Basic OWU0OTliYmQtOGQwYi00NTQzLWFmMTMtYTIyMzY5NDIyZDFk'
    ));
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
    curl_setopt($ch, CURLOPT_HEADER, FALSE);
    curl_setopt($ch, CURLOPT_POST, TRUE);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $jsondata);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
    
    $response = curl_exec($ch);
    curl_close($ch);
    return $response;
}

   public function finish($id){

    $lab  = $this->M_order->detail(array("order_id"=>$id));
    $playerid = $this->M_reverse->getPlayerId($lab->order_hf_recipient);
    $hf_sender = $this->M_reverse->hfName($lab->order_hf_sender);
    //$hf_recipient = $this->M_reverse->hfName($lab->order_hf_recipient);
    //$shipper_name = $this->M_reverse->shipperName($lab->order_shipper_id);
    $data = array("status_order_id"=>$id,"status_date_update"=>date("Y-m-d H:i:s"),"status_description"=>"Menunggu Konfirmasi Lab");
    $notxt = $hf_sender->hf_name." akan mengirimkan specimen ";

       $rdata = array("order_status"=>"1","order_date"=>date("Y-m-d"));
       if($this->M_order->update($rdata,$id)){
        $this->M_order->updateStatus($data);  
        foreach($playerid as $listplay){
          $this->notif($notxt,$listplay->user_playerid);
          $this->notif($notxt,$listplay->user_playerid_web);
        }
           redirect(base_url()."transactional/order/list");
       }

   }


   public function eidfinish($id){

    $lab  = $this->M_order->detail(array("order_id"=>$id));
    $playerid = $this->M_reverse->getPlayerId($lab->order_hf_recipient);
    $hf_sender = $this->M_reverse->hfName($lab->order_hf_sender);
    //$hf_recipient = $this->M_reverse->hfName($lab->order_hf_recipient);
    //$shipper_name = $this->M_reverse->shipperName($lab->order_shipper_id);
    $data = array("status_order_id"=>$id,"status_date_update"=>date("Y-m-d H:i:s"),"status_description"=>"Menunggu Konfirmasi Lab");
    $notxt = $hf_sender->hf_name." akan mengirimkan specimen ";

       $rdata = array("order_status"=>"1","order_date"=>date("Y-m-d"));
       if($this->M_order->update($rdata,$id)){
        $this->M_order->updateStatus($data);  
        foreach($playerid as $listplay){
          $this->notif($notxt,$listplay->user_playerid);
          $this->notif($notxt,$listplay->user_playerid_web);
        }
           redirect(base_url()."transactional/order/eid");
       }

   }

   public function eidrec($id){
    $lab  = $this->M_order->detail(array("order_id"=>$id));
    $playerid = $this->M_reverse->getPlayerId($lab->order_hf_recipient);
    $hf_sender = $this->M_reverse->hfName($lab->order_hf_sender);
    $hf_recipient = $this->M_reverse->hfName($lab->order_hf_recipient);
    $shipper_name = $this->M_reverse->shipperName($lab->order_shipper_id);
    $notxt = " Paket telah diterima di ".$hf_recipient->hf_name;
    $data = array("status_order_id"=>$id,"status_date_update"=>date("Y-m-d H:i:s"),"status_description"=>"Paket Sudah diterima");

    $rdata = array("order_status"=>"5","order_date_received"=>date("Y-m-d H:i:s"));
    if($this->M_order->update($rdata,$id)){
        $this->M_order->updateStatus($data);  
        foreach($playerid as $listplay){
          $this->notif($notxt,$listplay->user_playerid);
          $this->notif($notxt,$listplay->user_playerid_web);
        }
        redirect(base_url()."transactional/order/eidreceived");
    }

}



   public function rec($id){
    $lab  = $this->M_order->detail(array("order_id"=>$id));
    $playerid = $this->M_reverse->getPlayerId($lab->order_hf_recipient);
    $hf_sender = $this->M_reverse->hfName($lab->order_hf_sender);
    $hf_recipient = $this->M_reverse->hfName($lab->order_hf_recipient);
    $shipper_name = $this->M_reverse->shipperName($lab->order_shipper_id);
    $notxt = " Paket telah diterima di ".$hf_recipient->hf_name;
    $data = array("status_order_id"=>$id,"status_date_update"=>date("Y-m-d H:i:s"),"status_description"=>"Paket Sudah diterima");

    $rdata = array("order_status"=>"5","order_date_received"=>date("Y-m-d H:i:s"));
    if($this->M_order->update($rdata,$id)){
        $this->M_order->updateStatus($data);  
        foreach($playerid as $listplay){
          $this->notif($notxt,$listplay->user_playerid);
          $this->notif($notxt,$listplay->user_playerid_web);
        }
        redirect(base_url()."transactional/order/received");
    }

}


   public function pickup($id){
    $lab  = $this->M_order->detail(array("order_id"=>$id));
    $playerid = $this->M_reverse->getPlayerId($lab->order_hf_recipient);
    $hf_sender = $this->M_reverse->hfName($lab->order_hf_sender);
    $hf_recipient = $this->M_reverse->hfName($lab->order_hf_recipient);
    $shipper_name = $this->M_reverse->shipperName($lab->order_shipper_id);
    $data = array("status_order_id"=>$id,"status_date_update"=>date("Y-m-d H:i:s"),"status_description"=>"Diambil oleh kurir");
    $notxt = $shipper_name->shipper_name." Telah mengambil paket untuk dikirim ";
    $rdata = array("order_status"=>"3","order_date_pickup"=>date("Y-m-d H:i:s"),"order_user_pickup"=>$this->session->userdata('user_id'));
    $rdata['order_pickup_info'] = $this->input->post('order_pickup_info');
    if($this->M_order->update($rdata,$id)){
        $this->M_order->updateStatus($data);  
        foreach($playerid as $listplay){
          $this->notif($notxt,$listplay->user_playerid);
          $this->notif($notxt,$listplay->user_playerid_web);
        }
        //redirect(base_url()."transactional/pickup/list");
    $json['status'] = "success";
    $json['message'] = $this->lang->line("data_save");
    }
    echo json_encode($json);

}


public function delivered($id){
    $lab  = $this->M_order->detail(array("order_id"=>$id));
    $playerid = $this->M_reverse->getPlayerId($lab->order_hf_recipient);
    $hf_sender = $this->M_reverse->hfName($lab->order_hf_sender);
    $hf_recipient = $this->M_reverse->hfName($lab->order_hf_recipient);
    $shipper_name = $this->M_reverse->shipperName($lab->order_shipper_id);
    $data = array("status_order_id"=>$id,"status_date_update"=>date("Y-m-d H:i:s"),"status_description"=>"Paket sampai tujuan");
    $notxt = " Paket telah sampai di ".$hf_recipient->hf_name;

    $rdata = array("order_status"=>"4","order_date_delivered"=>date("Y-m-d H:i:s"));
    $rdata['order_shipper_info'] = $this->input->post('order_shipper_info');
    if($this->M_order->update($rdata,$id)){
        $this->M_order->updateStatus($data);  
        foreach($playerid as $listplay){
          $this->notif($notxt,$listplay->user_playerid);
          $this->notif($notxt,$listplay->user_playerid_web);
        }
        $json['status'] = "success";
        $json['message'] = $this->lang->line("data_save");
    }

    echo json_encode($json);

}


   public function detail(){
       $rdata = $this->input->post();
       if($json['response'] = $this->M_order->detail($rdata)){
           $json['status'] ="success";
       };

       echo json_encode($json);
   }
   public function update($id){
       $rdata = $this->input->post();
       if($this->M_order->update($rdata,$id)){
        $json = array(
     "status"=>"success",
     "message"=>$this->lang->line("data_save"),
     "response"=>true);
    }else{
     $json = array(
         "status"=>"error",
         "message"=>$this->lang->line("failed"),
         "response"=>false);
    }

    echo json_encode($json);

   }

   public function confirmation(){
      
    $data['orderlist'] = $this->M_order->confirmation($this->session->userdata("user_unit")); 
    $this->template->renderpage('transactional/confirm_list',$data);
   }


   public function eidconfirmation(){
      
    $data['orderlist'] = $this->M_order->eidconfirmation($this->session->userdata("user_unit")); 
    $this->template->renderpage('transactional/eid_confirm_list',$data);
   }


   public function eidapproved($id){
    $lab  = $this->M_order->detail(array("order_id"=>$id));
    $playerid = $this->M_reverse->getPlayerId($lab->order_hf_recipient);
    $hf_sender = $this->M_reverse->hfName($lab->order_hf_sender);
    $hf_recipient = $this->M_reverse->hfName($lab->order_hf_recipient);
    $shipper_name = $this->M_reverse->shipperName($lab->order_shipper_id);

    $rdata = array("order_approved"=>"1","order_status"=>"2","order_date_confirm"=>date("Y-m-d H:i:s"));
    $data = array("status_order_id"=>$id,"status_date_update"=>date("Y-m-d H:i:s"),"status_description"=>"Terkonfirmasi - Menunggu diambil");
    if($this->M_order->update($rdata,$id)){
        $this->M_order->updateStatus($data);  
        redirect(base_url()."transactional/order/eidconfirmation");
    }
}


public function eidrejected($id){
    $lab  = $this->M_order->detail(array("order_id"=>$id));
    $playerid = $this->M_reverse->getPlayerId($lab->order_hf_recipient);
    $hf_sender = $this->M_reverse->hfName($lab->order_hf_sender);
    $hf_recipient = $this->M_reverse->hfName($lab->order_hf_recipient);
    $shipper_name = $this->M_reverse->shipperName($lab->order_shipper_id);
    
    $notxt = $hf_recipient->hf_name." Menolak permintaan ";
    $rdata = array("order_approved"=>"2","order_status"=>"2");
    $data = array("status_order_id"=>$id,"status_date_update"=>date("Y-m-d H:i:s"),"status_description"=>"Terkonfirmasi - Ditolak (".$rdata['order_reason'].")");
    if($this->M_order->update($rdata,$id)){
        $this->M_order->updateStatus($data);  
            foreach($playerid as $listplay){
              $this->notif($notxt,$listplay->user_playerid);
              $this->notif($notxt,$listplay->user_playerid_web);
            }
        redirect(base_url()."transactional/order/eidconfirmation");
    }
}


public function eidreceived(){
      
    $data['orderlist'] = $this->M_order->received($this->session->userdata("user_unit"),"1"); 
    $this->template->renderpage('transactional/eid_received_list',$data);
   }








   public function confirmationsearch(){
    $order_id = $this->input->post("search");
    if($this->input->post("order_kategori")=='1'){
        $data['orderlist'] = $this->M_order->eidconfirmation($this->session->userdata("user_unit"),$order_id);
    }else{
    $data['orderlist'] = $this->M_order->confirmation($this->session->userdata("user_unit"),$order_id);
    }
    $this->load->view("transactional/search_confirm",$data);

   }

   public function confirmationdata(){

    if($this->input->post("order_kategori")=='1'){
        $data['orderlist'] = $this->M_order->eidconfirmation($this->session->userdata("user_unit")); 
    }else{
    $data['orderlist'] = $this->M_order->confirmation($this->session->userdata("user_unit")); 
    }
    $this->load->view('transactional/confirm_data',$data);
   }
   

   public function rejected($id){
    $lab  = $this->M_order->detail(array("order_id"=>$id));
    $playerid = $this->M_reverse->getPlayerId($lab->order_hf_recipient);
    $hf_sender = $this->M_reverse->hfName($lab->order_hf_sender);
    $hf_recipient = $this->M_reverse->hfName($lab->order_hf_recipient);
    $shipper_name = $this->M_reverse->shipperName($lab->order_shipper_id);
    
    $notxt = $hf_recipient->hf_name." Menolak permintaan ";
    $rdata = array("order_approved"=>"2","order_status"=>"2");
    $data = array("status_order_id"=>$id,"status_date_update"=>date("Y-m-d H:i:s"),"status_description"=>"Terkonfirmasi - Ditolak (".$rdata['order_reason'].")");
    if($this->M_order->update($rdata,$id)){
        $this->M_order->updateStatus($data);  
            foreach($playerid as $listplay){
              $this->notif($notxt,$listplay->user_playerid);
              $this->notif($notxt,$listplay->user_playerid_web);
            }
        redirect(base_url()."transactional/order/confirmation");
    }
}


    public function approved($id){
        $lab  = $this->M_order->detail(array("order_id"=>$id));
        $playerid = $this->M_reverse->getPlayerId($lab->order_hf_recipient);
        $hf_sender = $this->M_reverse->hfName($lab->order_hf_sender);
        $hf_recipient = $this->M_reverse->hfName($lab->order_hf_recipient);
        $shipper_name = $this->M_reverse->shipperName($lab->order_shipper_id);

        $rdata = array("order_approved"=>"1","order_status"=>"2","order_date_confirm"=>date("Y-m-d H:i:s"));
        

     
        $data = array("status_order_id"=>$id,"status_date_update"=>date("Y-m-d H:i:s"),"status_description"=>"Terkonfirmasi - Menunggu diambil");
       // $notxt = $hf_recipient->hf_name." Menerima permintaan pemeriksaan, silahkan menunggu kurir datang ";
        

        if($this->M_order->update($rdata,$id)){
            $this->M_order->updateStatus($data);  
            /* foreach($playerid as $listplay){
              $this->notif($notxt,$listplay->user_playerid);
              $this->notif($notxt,$listplay->user_playerid_web);
            }*/
            redirect(base_url()."transactional/order/confirmation");
        
        }
    }


    public function received(){
      
        $data['orderlist'] = $this->M_order->received($this->session->userdata("user_unit")); 
        $this->template->renderpage('transactional/received_list',$data);
       }

public function search_received(){
$rdata = $this->input->post('search');

    $data['orderlist'] = $this->M_order->searchReceived($this->session->userdata("user_unit"),$rdata); 
$this->load->view('transactional/search_received_list',$data);
}


       public function search(){
        $id = $this->session->userdata('user_unit');
        $search = $this->input->post('search');
        $data['orderlist'] = $this->M_order->search($search,$id);
        $this->load->view('transactional/search_orderlist',$data);
    }

    

    public function delete($idorder){
        $del = $this->M_order->delete($idorder);
        if($del){
            redirect(base_url()."transactional/order/list");
        }
    }

}
   