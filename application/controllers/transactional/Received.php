<?php

Class Received extends CI_Controller{
public function __construct()
    {
        parent::__construct();
        $this->load->model('transactional/M_received');
        $this->load->model("geo/M_province");
    }

    public function index($id){
        $data['condition'] = $this->M_received->conditionchecked($id);
        $data['specimen'] = $this->M_received->getDataList($id);
        $this->template->renderpage("transactional/received_detail",$data);
    }

    public function eidlist($id){
        $data['condition'] = $this->M_received->conditionchecked($id);
        $data['specimen'] = $this->M_received->getDataList($id);
        $this->template->renderpage("transactional/eid_received_detail",$data);
    }


    public function search($id){
        $rdata = $this->input->post("search");
        if($this->input->post("order_kategori")){
            $data['specimen'] = $this->M_received->searchDataList($id,$rdata,$this->input->post("order_kategori")); 
            $this->load->view("transactional/search_eid_received_detail",$data);   
        }else{
            $data['specimen'] = $this->M_received->searchDataList($id,$rdata);
            $this->load->view("transactional/search_received_detail",$data);
        }
        //$data['specimen'] = $this->M_received->searchDataList($id,$rdata);
       
    }

    public function conditional(){
        $rdata = array("specimen_condition"=>$this->input->post('specimen_condition'),
        "specimen_condition_descript"=>$this->input->post('specimen_condition_descript'));
        $id=$this->input->post("specimen_num_id");
        //$rdata = array("specimen_condition"=>$val);
        if($this->M_received->update($rdata,$id)){
        //redirect(base_url()."transactional/received/list/".$oid);

        $json['status']="success";
        $json['message'] = $this->lang->line("data_save");
        
    }else{
        $json['status']="error";
        $json['message'] = $this->lang->line("data_failed");
    }
    echo json_encode($json);
    }

}