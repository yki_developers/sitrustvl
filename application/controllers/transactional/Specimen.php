<?php
Class Specimen extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('transactional/M_specimen');
        $this->load->model("geo/M_province");
    }

    public function index($id){
        $data['specimen'] = $this->M_specimen->getDataList($id);
        $data['province'] = $this->M_province->getList();
        $data['order'] = $this->M_specimen->getStatus($id);
        $this->template->renderpage("transactional/specimen_list",$data);
    }

    public function eid($id){
        $data['specimen'] = $this->M_specimen->getDataList($id);
        $data['province'] = $this->M_province->getList();
        $data['order'] = $this->M_specimen->getStatus($id);
        $this->template->renderpage("transactional/specimen_eidlist",$data);
    }

    public function add(){
        $rdata = $this->input->post();
        $response = $this->M_specimen->insert($rdata);
        if($response){
            $json = array("status"=>"success","message"=>$this->lang->line('data_save'),"response"=>$response);
        }else{
            $json =  array("status"=>"error","message"=>$this->lang->line('failed'),"response"=>false);

        }
       // $json['orderid'] = $this->input->post('specimen_order_id');
        echo json_encode($json);
    }


    public function input(){
        $rdata = $this->input->post();
        $response = $this->M_specimen->insertSpecimen($rdata);
        if($response){
            $json = array("status"=>"success","message"=>$this->lang->line('data_save'),"response"=>$response);
        }else{
            $json =  array("status"=>"error","message"=>$this->lang->line('failed'),"response"=>false);

        }
       $json['orderid'] = $this->input->post('specimen_order_id');
        echo json_encode($json);
    }


    public function delete($id,$sid){
        if($this->M_specimen->delete($id)){
            redirect(base_url()."transactional/specimen/list/".$sid);
        }
    }


    public function patientid(){
        $rdata = $this->input->post();
        $response = $this->M_specimen->getNid($rdata);
       if($response){
           $json['status']="success";
           $json["response"] = $response;
       }else{
           $json['status']="nodata";
           $json['response'] = false;
       }
       //$json['orderid'] = $this->input->post('specimen_order_id');
        echo json_encode($json);
    }

    public function checkspecimen(){
        
       
            $rdata = $this->input->post();
            $response = $this->M_specimen->getSpecimen($rdata);
            if($response>0){
                $json['status']=false;
            }else{
                $json['status']=true;
            }
        
      
        //$json['orderid'] = $this->input->post('specimen_order_id');
         echo json_encode($json['status']);
    }

    public function detail(){
        $rdata = $this->input->post("specimen_num_id");
        $response = $this->M_specimen->getDetailSpecimen($rdata);
        if($response){
            $json['status']="success";
            $json["response"] = $response;
        }else{
            $json['status']="error";
            $json["response"]=false;
        }
         echo json_encode($json);
    }

    public function update($pid){
        $rdata = $this->input->post();
        $response = $this->M_specimen->updatePatient($rdata,$pid);
        if($response){
            $json['status']="success";
            $json["response"] = $response;
            $json['message'] = $this->lang->line("data_save");
        }else{
            $json['status']="error";
            $json["response"]=false;
            $json['message'] = $this->lang->line("data_failed");
        }
         echo json_encode($json);

    }

    public function updatespecimen($sid){
        $rdata = $this->input->post();
        $response = $this->M_specimen->updateSpecimen($rdata,$sid);
        if($response){
            $json['status']="success";
            $json['message'] =$this->lang->line("data_save");
            $json["response"] = $response;
        }else{
            $json['status']="error";
            $json["response"]=false;
        }
         echo json_encode($json);

    }

    public function reorderSpecimen($oid){
        $rdata = $this->input->post();
        $response = $this->M_specimen->SpecimenReorder($rdata,$oid);
        if($response){
            $json['status']="success";
            $json['message'] =$this->lang->line("data_save");
            $json["response"] = $response;
        }else{
            $json['status']="error";
            $json["response"]=false;
        }
         echo json_encode($json);

    }

    public function search(){
        $search = $this->input->post('search');
        $order_id = $this->input->post("order_id");
        $data['specimen'] = $this->M_specimen->search($search,$order_id);
        //print_r($data);
        $this->load->view('transactional/specimen_search',$data);
    }


}