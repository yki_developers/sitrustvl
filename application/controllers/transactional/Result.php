<?php
Class Result extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('transactional/M_result');
        $this->load->model('transactional/M_order');
    }

    public function index(){
        $data['orderlist'] = $this->M_result->getDataList($this->session->userdata('user_unit'));
        $this->template->renderpage("transactional/result_list",$data);
    }

    public function eidlist(){
        $data['orderlist'] = $this->M_result->getDataList($this->session->userdata('user_unit'),"1");
        $this->template->renderpage("transactional/eid_result_list",$data);
    }



    public function search(){
        $rdata = $this->input->post("search");
        $data['orderlist'] = $this->M_result->searchDataList($this->session->userdata('user_unit'),$rdata);
        $this->load->view("transactional/search_result_list",$data);
    }

    public function finish($id){
        $rdata = array("order_status"=>"6");
        if($this->M_order->update($rdata,$id)){
            redirect(base_url()."transactional/result/list");
        };
    }

    public function eidfinish($id){
        $rdata = array("order_status"=>"6");
        if($this->M_order->update($rdata,$id)){
            redirect(base_url()."transactional/result/eidlist");
        };
    }


    public function searchbyspecimen(){
        $rdata = $this->input->post('search');

        $id=$this->session->userdata("user_unit");
        $data['specimen'] = $this->M_result->searchResultList($id,$rdata);
        //print_r($data);
        $this->template->renderpage("transactional/search_byspecimen_result",$data);
    }

    public function searchresultspecimen(){
        $rdata = $this->input->post('search');
        $id=null;
        $data['specimen'] = $this->M_result->searchResult($id,$rdata);
        //print_r($data);
        $this->load->view("transactional/search_specimen_result",$data);
    }


    public function specimen($id){
        $data['resultchecked'] = $this->M_result->resultChecked($id);
        $data['vlchecked'] = $this->M_result->vldocumenChecked($id);
        $data['eidchecked'] = $this->M_result->eiddocumenChecked($id);
        $data["orderinfo"] = $this->M_result->orderKategori($id);
        $data['specimen'] = $this->M_result->getList($id);
        //echo $data['specimen'];
        $this->template->renderpage("transactional/specimen_result",$data);
    }

    public function eiddetail($id){
        $data['resultchecked'] = $this->M_result->resultChecked($id);
        $data['vlchecked'] = $this->M_result->vldocumenChecked($id);
        $data['eidchecked'] = $this->M_result->eiddocumenChecked($id);
        $data["orderinfo"] = $this->M_result->orderKategori($id);
        $data['specimen'] = $this->M_result->getList($id);
        $this->template->renderpage("transactional/specimen_result",$data);
    }



    public function searchspecimen($id){
        $rdata = $this->input->post('search');
        $data['specimen'] = $this->M_result->searchResult($id,$rdata);
        //print_r($data);
        $this->load->view("transactional/search_specimen_result",$data);
    }


    public function vlresult($id){
        $data['specimen'] = $this->M_result->getSpecimen($id,'2');
        $this->template->renderpage('transactional/result_form_vl',$data);
    }
    public function vlresultedit($id){
        $data['specimen'] = $this->M_result->detail($id);
        $this->template->renderpage('transactional/result_form_vl_edit',$data);

    }

    public function vlupdate(){
       // print_r($_FILES['foto']);
       $date = date("YmdHis");
        $dpath = "./assets/resultdoc/";
        $fname = $_FILES['foto']['name']."_".$date;
        $tmpname = $_FILES['foto']['tmp_name'];

        $contents = file_get_contents($tmpname);
        $dataContent = base64_encode($contents);
        

        $split = explode("/",$_FILES['foto']['type']);
        $md5 = md5($fname).".".$split[1];
        $op = fopen($dpath.$md5,"w");
        $doupload=fwrite($op,$dataContent);
        fclose($op);
        //$doupload = move_uploaded_file($tmpname,$dpath.$md5);
        
        
        if($doupload){
            $rdata['vl_result_dokumen'] = $md5;
        if($json['response'] =$this->M_result->update($rdata,$this->input->post('specimen_num_id'))){
            $json['status']='200';
            $json['message']=$this->lang->line('data_save');
        }else{
            $json['status']='error';
            $json['message']=$this->lang->line('failed');
        }
        
    
    }
    echo json_encode($json);

    }



    public function eidupdate(){
        // print_r($_FILES['foto']);
        $date = date("YmdHis");
        $dpath = "./assets/resultdoc/";
        $fname = $_FILES['foto']['name']."_".$date;
        $tmpname = $_FILES['foto']['tmp_name'];

         $contents = file_get_contents($tmpname);
         $dataContent = base64_encode($contents);

         $split = explode("/",$_FILES['foto']['type']);
         $md5 = "eid_".$this->input->post('specimen_num_id')."_".md5($fname).".".$split[1];
         //$doupload = move_uploaded_file($tmpname,$dpath.$md5);
         $op = fopen($dpath.$md5,"w");
         $doupload=fwrite($op,$dataContent);
         fclose($op);
         
         if($doupload){
             $rdata['eid_result_dokumen'] = $md5;
         if($json['response'] =$this->M_result->eidupdate($rdata,$this->input->post('specimen_num_id'))){
             $json['status']='200';
             $json['message']=$this->lang->line('data_save');
         }else{
             $json['status']='error';
             $json['message']=$this->lang->line('failed');
         }
         
     
     }
     echo json_encode($json);
 
     }



    public function updateresult($idnum){
        // print_r($_FILES['foto']);
        $rdata = $this->input->post();
         if($json['response'] =$this->M_result->update($rdata,$idnum)){
             $json['status']='success';
             $json['message']=$this->lang->line('data_save');
         }else{
             $json['status']='error';
             $json['message']=$this->lang->line('failed');
         }

echo json_encode($json);

    }


    public function updateresulteid($idnum){
        // print_r($_FILES['foto']);
        $rdata = $this->input->post();
         if($json['response'] =$this->M_result->eidupdate($rdata,$idnum)){
             $json['status']='success';
             $json['message']=$this->lang->line('data_save');
         }else{
             $json['status']='error';
             $json['message']=$this->lang->line('failed');
         }

echo json_encode($json);

    }

    public function vlinsert(){
        $rdata = $this->input->post();
        if($json['response'] = $this->M_result->vlinsert($rdata)){
            $this->M_result->resultFlagUpdate($rdata['vl_result_specimen_num_id']);
            $json['status'] = "success";
            $json['message'] = $this->lang->line("data_save");
        }else{
            $json['status']="error";
            $json['message'] = $this->lang->line('failed');
            $json['response'] = false;
        }

        echo json_encode($json);
    }



    public function eidresult($id){
        $data['specimen'] = $this->M_result->getSpecimen($id,'1');
        $this->template->renderpage('transactional/result_form_eid',$data);
    }


    public function eidresultedit($id){
        $data['specimen'] = $this->M_result->eiddetail($id);
        $this->template->renderpage('transactional/result_form_eid_edit',$data);

    }


    public function eidinsert(){
        $rdata = $this->input->post();
        if($json['response'] = $this->M_result->eidinsert($rdata)){
            $this->M_result->resultFlagUpdate($rdata['eid_result_specimen_num_id']);
            $json['status'] = "success";
            $json['message'] = $this->lang->line("data_save");
        }else{
            $json['status']="error";
            $json['message'] = $this->lang->line('failed');
            $json['response'] = false;
        }

        echo json_encode($json);
    }

    public function eidviewresult($numid){
        $json['response'] = $this->M_result->eiddetail($numid);
        echo json_encode($json);
    }


    public function viewresult($numid){
        $json['response'] = $this->M_result->detail($numid);
        echo json_encode($json);
    }

    public function files($id){
        //$files = $this->M_result->getDocument($id);
        $this->template->renderpage('transactional/result_document');
    }


    public function eidfiles($id){
        //$files = $this->M_result->getDocument($id);
        $this->template->renderpage('transactional/result_eid_document');
    }

    public function validation(){
        $this->load->model("account/M_user");
        $rdata = array(
            "user_name"=>$this->input->post("user_name"),
            "user_passwd"=>md5($this->input->post("user_passwd"))
        );
        if($json['response'] = $this->M_user->validate($rdata)){
            $json['status'] = "success";
            $json['result_id'] = $this->uri->segment('4');
        }else{
            $json['status'] = "error";
           
        }
        
        echo json_encode($json);
    }

    public function docview($id){
        $ndata = $this->M_result->getVLresult($id);
        $ext = explode(".",$ndata->vl_result_dokumen);
        $tmpfname = tempnam("./tmpfiles","DOC-");
        $op = fopen("./assets/resultdoc/".$ndata->vl_result_dokumen,"r");
        $b64string=fread($op,filesize("./assets/resultdoc/".$ndata->vl_result_dokumen));
        $data['string'] = $b64string;
        $decode64 = base64_decode($b64string);
        fclose($op);
        $data['ext'] = $ext[1];
        $this->load->view('transactional/result_view_document',$data);
       // unlink("./tmpfiles/".$ndata->vl_result_dokumen);
    }


    public function eiddocview($id){
        $ndata = $this->M_result->getEIDresult($id);
        $ext = explode(".",$ndata->eid_result_dokumen);
        $tmpfname = tempnam("./tmpfiles","DOC-");
        $op = fopen("./assets/resultdoc/".$ndata->eid_result_dokumen,"r");
        $b64string=fread($op,filesize("./assets/resultdoc/".$ndata->eid_result_dokumen));
        $data['string'] = $b64string;
        $decode64 = base64_decode($b64string);
        fclose($op);
        $data['ext'] = $ext[1];
        $this->load->view('transactional/result_view_document',$data);
       // unlink("./tmpfiles/".$ndata->vl_result_dokumen);
    }
}