<?php
Class Pickup extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("transactional/M_pickup");
    }

    public function index(){
        $check = $this->M_pickup->checkNetwork($this->session->userdata('user_name'));
        if($check>0){
            $data['orderlist'] = $this->M_pickup->getDataListInternal($this->session->userdata('user_name'));

        }else{
        $data['orderlist'] = $this->M_pickup->getDataListRev($this->session->userdata('user_name'));
        }
       
        $this->template->renderpage('transactional/pickup_list',$data);
        //echo "<p><p><p>".$data['orderlist'];
    }


    public function delivered(){
        $check = $this->M_pickup->checkNetwork($this->session->userdata('user_name'));

       // $data['orderlist'] = $this->M_pickup->getDeliveredList($this->session->userdata('user_unit'),$this->session->userdata('user_district'));
        if($check>0){
            $data['orderlist'] = $this->M_pickup->getDeliveredListInternal($this->session->userdata('user_name'));

        }else{
        $data['orderlist'] = $this->M_pickup->getDeliveredListRev($this->session->userdata('user_name'));
        }
       
        $this->template->renderpage('transactional/delivered_list',$data);
    }

    public function search(){
        $search = $this->input->post('search');
        $data['orderlist'] = $this->M_pickup->search($search,$this->input->post('order_status'),$this->session->userdata("user_district"));
        if($this->input->post("order_status")=='2'){
        $this->load->view("transactional/search_pickup",$data);
        }else{
            $this->load->view("transactional/search_delivered",$data);
        }
    }
}