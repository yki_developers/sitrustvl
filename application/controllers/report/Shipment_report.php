<?php
Class Shipment_report extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("report/M_shipment");
        $this->load->model("report/M_report");
    }

    public function index($page){

        if($this->session->userdata('user_group')=='8'){
            $tofa = $this->M_report->getTofaArea($this->session->userdata("user_name"));
           $tfs = array();
           foreach($tofa as $tf){
               $tfs[] = $tf->wa_province;
           }
            $level = $tfs;
        }elseif($this->session->userdata('user_group')=='2'){
            $level=null;
        }

        $data['response'] = $this->M_shipment->getShipmentReport($level,$page);
        $data['total'] = $this->M_shipment->getTotalReport($level);
        $this->template->renderpage('report/shipment_report',$data);
    }

    public function shipmentxls(){

        if($this->session->userdata('user_group')=='8'){
            $tofa = $this->M_report->getTofaArea($this->session->userdata("user_name"));
           $tfs = array();
           foreach($tofa as $tf){
               $tfs[] = $tf->wa_province;
           }
            $level = $tfs;
        }elseif($this->session->userdata('user_group')=='2'){
            $level=null;
        }

        $data['response'] = $this->M_shipment->getShipmentReportXLS($level);
        $this->load->view('report/shipment_excel_report',$data);


    }
}