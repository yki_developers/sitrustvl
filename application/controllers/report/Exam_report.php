<?php
Class Exam_report extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("report/M_report");
        
    }

    public function vlreport($page){
        if($this->session->userdata('user_group')=='6'){
           
           if($this->session->userdata('user_hf_group')=='5'){
            $level = array("order_hf_recipient"=>$this->session->userdata('user_unit'));

           }else{ 
        $level = array("order_hf_sender"=>$this->session->userdata('user_unit'));
           }
           
          //$level = array("order_hf_sender"=>$this->session->userdata('user_unit'));
    
    }elseif($this->session->userdata('user_group')=='4'){
            $level = array("hf_district"=>$this->session->userdata('user_district'));
        }elseif($this->session->userdata('user_group')=='3'){
            $level = array("hf_province"=>$this->session->userdata('user_province'));
        }elseif($this->session->userdata('user_group')=='8'){
            $tofa = $this->M_report->getTofaArea($this->session->userdata("user_name"));
           $tfs = array();
           foreach($tofa as $tf){
               $tfs[] = $tf->wa_province;
           }
            $level = $tfs;
        }else{
            $level=null;
        }
        if($this->uri->segment("4")!=''){
            $page=$this->uri->segment("4");
        }else{
            $page="1";
        }
        $group = $this->session->userdata("user_group");
        $data['report'] = $this->M_report->getVLReport($level,$page,$group);
        $data['total'] = $this->M_report->getTotalReport($level,$group);
        //echo $level;
        $this->template->renderpage('report/report_vl',$data);
    }


    public function searchreport(){

        if($this->session->userdata('user_group')=='6'){
           
            if($this->session->userdata('user_hf_group')=='5'){
             $level = array("order_hf_recipient"=>$this->session->userdata('user_unit'));
 
            }else{ 
         $level = array("order_hf_sender"=>$this->session->userdata('user_unit'));
            }
            
           //$level = array("order_hf_sender"=>$this->session->userdata('user_unit'));
     
     }elseif($this->session->userdata('user_group')=='4'){
             $level = array("hf_district"=>$this->session->userdata('user_district'));
         }elseif($this->session->userdata('user_group')=='3'){
             $level = array("hf_province"=>$this->session->userdata('user_province'));
         }elseif($this->session->userdata('user_group')=='8'){
             $tofa = $this->M_report->getTofaArea($this->session->userdata("user_name"));
            $tfs = array();
            foreach($tofa as $tf){
                $tfs[] = $tf->wa_province;
            }
             $level = $tfs;
         }else{
             $level=null;
         }

         $group = $this->session->userdata("user_group");
         $search = $this->input->post('search');
         $data['report'] = $this->M_report->searchVLreport($level,$group,$search);
         $this->load->view('report/search_report_vl',$data);

    }


    public function internalvlreport(){
        if($this->session->userdata('user_group')=='6'){
          
        $level = array("hf_code"=>$this->session->userdata('user_unit'));
           
    
    }elseif($this->session->userdata('user_group')=='4'){
            $level = array("hf_district"=>$this->session->userdata('user_district'));
        }elseif($this->session->userdata('user_group')=='3'){
            $level = array("hf_province"=>$this->session->userdata('user_province'));
        }elseif($this->session->userdata('user_group')=='8'){
            $tofa = $this->M_report->getTofaArea($this->session->userdata("user_name"));
           $tfs = array();
           foreach($tofa as $tf){
               $tfs[] = $tf->wa_province;
           }
            $level = $tfs;
        }else{
            $level=null;
        }
        if($this->session->userdata("user_group")=='8'){
            $data['report'] = $this->M_report->getInternalReportTOFA($level);

         }else{
        $data['report'] = $this->M_report->getInternalReport($level);
         }
        //echo $level;
        $this->template->renderpage('report/report_internal_vl',$data);
    }



   



    public function internalvlmonitoring($page){
        if($this->session->userdata('user_group')=='6'){
          
        $level = array("hf_code"=>$this->session->userdata('user_unit'));
           
    
    }elseif($this->session->userdata('user_group')=='4'){
            $level = array("hf_district"=>$this->session->userdata('user_district'));
        }elseif($this->session->userdata('user_group')=='3'){
            $level = array("hf_province"=>$this->session->userdata('user_province'));
        }elseif($this->session->userdata('user_group')=='8'){
            $tofa = $this->M_report->getTofaArea($this->session->userdata("user_name"));
           $tfs = array();
           foreach($tofa as $tf){
               $tfs[] = $tf->wa_province;
           }
            $level = $tfs;

        }else{
            $level=null;
        }
        if($this->session->userdata("user_group")=='8'){
            $data['report'] = $this->M_report->getInternalMonitoringTOFA($level,$page,50);

         }else{
        $data['report'] = $this->M_report->getInternalMonitoring($level,$page,50);
         }
        //echo $level;
        $data['total'] = $this->M_report->countMonInternal($level);
        $this->template->renderpage('report/monitoring_internal_vl',$data);
    }


    public function internalvlmonitoringxls(){
        if($this->session->userdata('user_group')=='6'){
          
        $level = array("hf_code"=>$this->session->userdata('user_unit'));
           
    
    }elseif($this->session->userdata('user_group')=='4'){
            $level = array("hf_district"=>$this->session->userdata('user_district'));
        }elseif($this->session->userdata('user_group')=='3'){
            $level = array("hf_province"=>$this->session->userdata('user_province'));
        }elseif($this->session->userdata('user_group')=='8'){
            $tofa = $this->M_report->getTofaArea($this->session->userdata("user_name"));
           $tfs = array();
           foreach($tofa as $tf){
               $tfs[] = $tf->wa_province;
           }
            $level = $tfs;

        }else{
            $level=null;
        }

        if($this->session->userdata("user_group")=='8'){
            $data['report'] = $this->M_report->getInternalMonitoringTOFAXLS($level);

         }else{
        $data['report'] = $this->M_report->getInternalMonitoringXLS($level);
         }
        //echo $level;
        //$data['total'] = $this->M_report->countMonInternal($level);
        $this->load->view('report/monitoring_internal_vl_excel',$data);
    }





    public function vlmonitoring($page){
        if($this->session->userdata('user_group')=='6'){
            if($this->session->userdata('user_hf_group')=='5'){
                $level = array("order_hf_recipient"=>$this->session->userdata('user_unit'));

            }else{

        $level = array("order_hf_sender"=>$this->session->userdata('user_unit'));
            }
    
    }elseif($this->session->userdata('user_group')=='4'){
            $level = array("hf_district"=>$this->session->userdata('user_district'));
        }elseif($this->session->userdata('user_group')=='3'){
            $level = array("hf_province"=>$this->session->userdata('user_province'));
        }elseif($this->session->userdata('user_group')=='8'){
            $tofa = $this->M_report->getTofaArea($this->session->userdata("user_name"));
           $tfs = array();
           foreach($tofa as $tf){
               $tfs[] = $tf->wa_province;
           }
            $level = $tfs;


        }else{
            $level=null;
        }
        /*
        if($this->session->userdata("user_group")=='8'){
            $data['orderlist'] = $this->M_report->getMonitoringOrderTOFA($level,$page,100);

         }else{
        //$data['orderlist'] = $this->M_report->getMonitoringOrder($level,$page,50);
        $data['orderlist'] = $this->M_report->getMonitoringMerge($level,$page,100);
        }*/
        $group = $this->session->userdata("user_group");
        $data['orderlist'] = $this->M_report->getMonitoringMerge($level,$page,$group,"2");

        $data['total'] = $this->M_report->countAll($level,$group,"2");


        //print_r($data);
        
        $this->template->renderpage('report/monitoring_report_vl',$data);
    }



    public function eidmonitoring($page){
        if($this->session->userdata('user_group')=='6'){
            if($this->session->userdata('user_hf_group')=='5'){
                $level = array("order_hf_recipient"=>$this->session->userdata('user_unit'));

            }else{

        $level = array("order_hf_sender"=>$this->session->userdata('user_unit'));
            }
    
    }elseif($this->session->userdata('user_group')=='4'){
            $level = array("hf_district"=>$this->session->userdata('user_district'));
        }elseif($this->session->userdata('user_group')=='3'){
            $level = array("hf_province"=>$this->session->userdata('user_province'));
        }elseif($this->session->userdata('user_group')=='8'){
            $tofa = $this->M_report->getTofaArea($this->session->userdata("user_name"));
           $tfs = array();
           foreach($tofa as $tf){
               $tfs[] = $tf->wa_province;
           }
            $level = $tfs;


        }else{
            $level=null;
        }
        /*
        if($this->session->userdata("user_group")=='8'){
            $data['orderlist'] = $this->M_report->getMonitoringOrderTOFA($level,$page,100);

         }else{
        //$data['orderlist'] = $this->M_report->getMonitoringOrder($level,$page,50);
        $data['orderlist'] = $this->M_report->getMonitoringMerge($level,$page,100);
        }*/
        $group = $this->session->userdata("user_group");
        $data['orderlist'] = $this->M_report->getMonitoringMerge($level,$page,$group,"1");

        $data['total'] = $this->M_report->countAll($level,$group,"1");
        

        //print_r($data);
        
        $this->template->renderpage('report/monitoring_report_eid',$data);
    }




    public function vlmonitoringxls(){
        if($this->session->userdata('user_group')=='6'){
            /*  
           if($this->session->userdata('user_hf_group')=='5'){
            $level = array("order_hf_recipient"=>$this->session->userdata('user_unit'));

           }else{ 
        $level = array("order_hf_sender"=>$this->session->userdata('user_unit'));
           }
           */
          $level = array("order_hf_sender"=>$this->session->userdata('user_unit'));
    }elseif($this->session->userdata('user_group')=='4'){
            $level = array("hf_district"=>$this->session->userdata('user_district'));
        }elseif($this->session->userdata('user_group')=='3'){
            $level = array("hf_province"=>$this->session->userdata('user_province'));
        }elseif($this->session->userdata('user_group')=='8'){
                $tofa = $this->M_report->getTofaArea($this->session->userdata("user_name"));
               $tfs = array();
               foreach($tofa as $tf){
                   $tfs[] = $tf->wa_province;
               }
                $level = $tfs;
        }else{
            $level=null;
        }
        $group = $this->session->userdata("user_group");
        $data['orderlist'] = $this->M_report->getMonitoringMergeXLS($level,$group,"2"); 
        $this->load->view('report/monitoring_report_vl_xls',$data);
    }


    public function eidmonitoringxls(){
        if($this->session->userdata('user_group')=='6'){
             /*  
           if($this->session->userdata('user_hf_group')=='5'){
            $level = array("order_hf_recipient"=>$this->session->userdata('user_unit'));

           }else{ 
        $level = array("order_hf_sender"=>$this->session->userdata('user_unit'));
           }
           */
          $level = array("order_hf_sender"=>$this->session->userdata('user_unit'));
    
    }elseif($this->session->userdata('user_group')=='4'){
            $level = array("hf_district"=>$this->session->userdata('user_district'));
        }elseif($this->session->userdata('user_group')=='3'){
            $level = array("hf_province"=>$this->session->userdata('user_province'));
        }elseif($this->session->userdata('user_group')=='8'){
                $tofa = $this->M_report->getTofaArea($this->session->userdata("user_name"));
               $tfs = array();
               foreach($tofa as $tf){
                   $tfs[] = $tf->wa_province;
               }
                $level = $tfs;
        }else{
            $level=null;
        }
        $group = $this->session->userdata("user_group");
        $data['orderlist'] = $this->M_report->getMonitoringMergeXLS($level,$group,"1"); 
        $this->load->view('report/monitoring_report_vl_xls',$data);
    }

    public function search(){

        if($this->session->userdata('user_group')=='6'){
            /*  
           if($this->session->userdata('user_hf_group')=='5'){
            $level = array("order_hf_recipient"=>$this->session->userdata('user_unit'));

           }else{ 
        $level = array("order_hf_sender"=>$this->session->userdata('user_unit'));
           }
           */
          $level = array("order_hf_sender"=>$this->session->userdata('user_unit'));
    
    }elseif($this->session->userdata('user_group')=='4'){
            $level = array("e.hf_district"=>$this->session->userdata('user_district'));
        }elseif($this->session->userdata('user_group')=='3'){
            $level = array("e.hf_province"=>$this->session->userdata('user_province'));
        }elseif($this->session->userdata('user_group')=='8'){
            $tofa = $this->M_report->getTofaArea($this->session->userdata("user_name"));
           $tfs = array();
           foreach($tofa as $tf){
               $tfs[] = $tf->wa_province;
           }
            $level = $tfs;
    }else{
            $level=null;
        }
        $group = $this->session->userdata("user_group");
        $search = $this->input->post('search');
        $data['orderlist'] = $this->M_report->search($search,$level,$group);
//print_r($data);
        $this->load->view('report/searchlist_monitoring',$data);
    }
    public function vlexcel(){
        if($this->session->userdata('user_group')=='6'){
          /*  if($this->session->userdata('user_hf_group')=='5'){
                $level = array("order_hf_recipient"=>$this->session->userdata('user_unit'));
    
               }else{ 
            $level = array("order_hf_sender"=>$this->session->userdata('user_unit'));
               }
               */
              $level = array("order_hf_sender"=>$this->session->userdata('user_unit'));
        }elseif($this->session->userdata('user_group')=='4'){
            $level = array("hf_district"=>$this->session->userdata('user_district'));
        }elseif($this->session->userdata('user_group')=='3'){
            $level = array("hf_province"=>$this->session->userdata('user_province'));
        }elseif($this->session->userdata('user_group')=='8'){
            $tofa = $this->M_report->getTofaArea($this->session->userdata("user_name"));
           $tfs = array();
           foreach($tofa as $tf){
               $tfs[] = $tf->wa_province;
           }
            $level = $tfs;
    }else{
            $level=null;
        }
        $group = $this->session->userdata("user_group");
        $data['report'] = $this->M_report->getVLReportXLS($level,$group);
       //print_r($data);
        //echo $level." ".$group;
       $this->load->view('report/report_excel_vl',$data);
    }


    public function labvlexcel(){
        
        $level = array("order_hf_recipient"=>$this->session->userdata('user_unit'));
        $group = $this->session->userdata("user_group");
        $data['report'] = $this->M_report->getVLReportXLS($level,$group);
       //print_r($data);
        //echo $level." ".$group;
       $this->load->view('report/report_excel_vl',$data);
    }





    public function excelark(){
        if($this->session->userdata('user_group')=='6'){
            //if($this->session->userdata('user_hf_group')=='5'){
               // $level = array("order_hf_recipient"=>$this->session->userdata('user_unit'));
    
              // }else{ 
            $level = array("order_hf_sender"=>$this->session->userdata('user_unit'));
             //  }
        }elseif($this->session->userdata('user_group')=='4'){
            $level = array("hf_district"=>$this->session->userdata('user_district'));
        }elseif($this->session->userdata('user_group')=='3'){
            $level = array("hf_province"=>$this->session->userdata('user_province'));
        }elseif($this->session->userdata('user_group')=='8'){
            $tofa = $this->M_report->getTofaArea($this->session->userdata("user_name"));
           $tfs = array();
           foreach($tofa as $tf){
               $tfs[] = $tf->wa_province;
           }
            $level = $tfs;
    }else{
            $level=null;
        }
        $group = $this->session->userdata("user_group");
        $data['report'] = $this->M_report->getVLReportARK($level,$group);
        //echo $level;
        $this->load->view('report/report_excel_ark',$data);
    }



    public function internalvlexcel(){
        if($this->session->userdata('user_group')=='6'){
            $level = array("hf_code"=>$this->session->userdata('user_unit'));
           
        }elseif($this->session->userdata('user_group')=='4'){
            $level = array("hf_district"=>$this->session->userdata('user_district'));
        }elseif($this->session->userdata('user_group')=='3'){
            $level = array("hf_province"=>$this->session->userdata('user_province'));
        }else{
            $level=null;
        }
        $data['report'] = $this->M_report->getInternalReport($level);
        //echo $level;
        $this->load->view('report/report_excel_internal_vl',$data);
    }


    public function eidreport(){
        if($this->session->userdata('user_group')=='6'){
            if($this->session->userdata('user_hf_group')=='1' || $this->session->userdata('user_hf_group')=='2' || $this->session->userdata('user_hf_group')=='3' || $this->session->userdata('user_hf_group')=='4'){
        $level = array("order_hf_sender"=>$this->session->userdata('user_unit'));

            }elseif($this->session->userdata('user_hf_group')=='5' || $this->session->userdata('user_hf_group')=='6'){
                $level = array("order_hf_recipient"=>$this->session->userdata('user_unit'));
            }

            
        }elseif($this->session->userdata('user_group')=='4'){
            $level = array("hf_district"=>$this->session->userdata('user_district'));
        }elseif($this->session->userdata('user_group')=='3'){
            $level = array("hf_province"=>$this->session->userdata('user_province'));
        }elseif($this->session->userdata('user_group')=='8'){
            $tofa = $this->M_report->getTofaArea($this->session->userdata("user_name"));
           $tfs = array();
           foreach($tofa as $tf){
               $tfs[] = $tf->wa_province;
           }
            $level = $tfs;
        }else{
            $level=null;
        }


        if($this->uri->segment("4")!=''){
            $page=$this->uri->segment("4");
        }else{
            $page="1";
        }

        $group = $this->session->userdata("user_group");

        $data['report'] = $this->M_report->getEIDReport($level,$page,$group);
       $data['total'] = $this->M_report->getTotalEidReport($level,$group);
        $this->template->renderpage('report/report_eid',$data);
    }



    public function eidexcel(){
        if($this->session->userdata('user_group')=='6'){
            /*  
           if($this->session->userdata('user_hf_group')=='5'){
            $level = array("order_hf_recipient"=>$this->session->userdata('user_unit'));

           }else{ 
        $level = array("order_hf_sender"=>$this->session->userdata('user_unit'));
           }
           */
          $level = array("order_hf_sender"=>$this->session->userdata('user_unit'));
        }elseif($this->session->userdata('user_group')=='4'){
            $level = array("hf_district"=>$this->session->userdata('user_district'));
        }elseif($this->session->userdata('user_group')=='3'){
            $level = array("hf_province"=>$this->session->userdata('user_province'));
        }elseif($this->session->userdata('user_group')=='8'){
            $tofa = $this->M_report->getTofaArea($this->session->userdata("user_name"));
           $tfs = array();
           foreach($tofa as $tf){
               $tfs[] = $tf->wa_province;
           }
            $level = $tfs;
    }else{
            $level=null;
        }
        $group = $this->session->userdata("user_group");
        $data['report'] = $this->M_report->getEIDReportXLS($level,$group);
        //echo $level;
        $this->load->view('report/report_excel_eid',$data);
    }



    public function specimen($orderid){
        $data['specimen'] = $this->M_report->getMonitoringSpecimen($orderid);
       // print_r($data);
        //$data['result'] = $this->M_report->getResultVl($data->specimen_num_id);
        $this->template->renderpage('report/monitoring_specimen',$data);
    }

    public function statusresultxls(){


        if($this->session->userdata('user_group')=='6'){
            /*  
           if($this->session->userdata('user_hf_group')=='5'){
            $level = array("order_hf_recipient"=>$this->session->userdata('user_unit'));

           }else{ 
        $level = array("order_hf_sender"=>$this->session->userdata('user_unit'));
           }
           */
          $level = array("order_hf_sender"=>$this->session->userdata('user_unit'));
        }elseif($this->session->userdata('user_group')=='4'){
            $level = array("hf_district"=>$this->session->userdata('user_district'));
        }elseif($this->session->userdata('user_group')=='3'){
            $level = array("hf_province"=>$this->session->userdata('user_province'));
        }elseif($this->session->userdata('user_group')=='8'){
            $tofa = $this->M_report->getTofaArea($this->session->userdata("user_name"));
           $tfs = array();
           foreach($tofa as $tf){
               $tfs[] = $tf->wa_province;
           }
            $level = $tfs;
    }else{
            $level=null;
        }
        $group = $this->session->userdata("user_group");
        $data['report'] = $this->M_report->getMonitoringHasil($level,$group,"2");
        //echo $level;
        $this->load->view('report/report_monitoring_hasil_xls',$data);

    }




    public function eidstatusresultxls(){


        if($this->session->userdata('user_group')=='6'){
            /*  
           if($this->session->userdata('user_hf_group')=='5'){
            $level = array("order_hf_recipient"=>$this->session->userdata('user_unit'));

           }else{ 
        $level = array("order_hf_sender"=>$this->session->userdata('user_unit'));
           }
           */
          $level = array("order_hf_sender"=>$this->session->userdata('user_unit'));
        }elseif($this->session->userdata('user_group')=='4'){
            $level = array("hf_district"=>$this->session->userdata('user_district'));
        }elseif($this->session->userdata('user_group')=='3'){
            $level = array("hf_province"=>$this->session->userdata('user_province'));
        }elseif($this->session->userdata('user_group')=='8'){
            $tofa = $this->M_report->getTofaArea($this->session->userdata("user_name"));
           $tfs = array();
           foreach($tofa as $tf){
               $tfs[] = $tf->wa_province;
           }
            $level = $tfs;
    }else{
            $level=null;
        }
        $group = $this->session->userdata("user_group");
        $data['report'] = $this->M_report->getMonitoringHasil($level,$group,"1");
        //echo $level;
        $this->load->view('report/report_monitoring_hasil_eid_xls',$data);

    }

}