<?php
Class Laboratorium extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("network/M_laboratorium");
        $this->load->model("geo/M_province");
    }

    public function index($page){
        if($this->session->userdata('user_unit')==''){
            $data['total'] = $this->M_laboratorium->countAll();
        $data['response'] = $this->M_laboratorium->getDataList($page);
        }else{
            $data['total'] = $this->M_laboratorium->countUnit($this->session->userdata('user_unit'));
            $data['response'] = $this->M_laboratorium->getNetwork($this->session->userdata('user_unit'),$page);
            $data['origin'] = $this->M_laboratorium->oriname($this->session->userdata('user_unit'));
        }
        //$data['hflist'] = $this->M_healthfacility->getDataList();

        $data['province'] = $this->M_province->getList();
        if($this->session->userdata("user_group")!='6'){
        $this->template->renderpage("network/laboratorium_list",$data);
        }else{
            $this->template->renderpage("network/laboratorium_ref",$data);
        }
    }

    public function new($id){
        $data['origin'] = $this->M_laboratorium->oriname($id);
        $data['province'] = $this->M_province->getList();
        $data['response'] = $this->M_laboratorium->getDataNetwork($id);
        $this->template->renderpage("network/laboratorium_form",$data);
    }

    public function add(){
        $rdata = $this->input->post();
        $response = $this->M_laboratorium->insert($rdata);
        if($response){
            $json = array("status"=>"success","message"=>$this->lang->line('data_save'),"response"=>$response);
        }else{
            $json = array("status"=>"error","message"=>$this->lang->line("failed"),"response"=>false);
        }
        echo json_encode($json);
    }

    public function delete($id){
        if($json['response'] = $this->M_laboratorium->delete($id)){
           redirect(base_url()."network/laboratorium/list/1");
        }else{
            $json['status']="error";
        };
        echo json_encode($json);
    }

    public function update($id){
        $response = $this->db->M_laboratorium->update($rdata,$id);
        if($response){
            $json = array("status"=>"success","message"=>$this->lang->line('data_save'),"response"=>$response);
        }else{
            $json = array("status"=>"error","message"=>$this->lang->line("failed"),"response"=>false);
        }
        echo json_encode($json);
    }

    public function search(){
        $search = $this->input->post('search');
        $data['response'] =$this->M_laboratorium->search($search);
        $this->load->view("network/search_laboratorium",$data);
    }
}