<?php
Class Kurirnetwork extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model("network/M_kurir");
        $this->load->model("unit/M_shipper");
        $this->load->model("account/M_user");
        $this->load->model("unit/M_healthfacility");
    }

    public function index($hfcode){
        $data['faskes'] = $this->M_healthfacility->detail($hfcode);
        $data['shipperlist'] = $this->M_shipper->getDataActiveList();
        $data['list'] = $this->M_kurir->getListKurir($hfcode);
        $this->template->renderpage('network/kurir',$data);
    }

    public function userkurir(){
        //$json = array();
        $json['response'] = $this->M_user->getListUserKurir($this->input->post('shipper_code'),$this->input->post('district_code'));
        echo json_encode($json);
    }

    public function insertkurir(){
        $rdata = $this->input->post();
        if($this->M_kurir->insert($rdata)){
            $json['status']='success';
        }else{
            $json['status']="error";
        }

        echo json_encode($json);
    }

    public function datalist($hfcode){
        $data['list'] = $this->M_kurir->getListKurir($hfcode);
        $this->load->view('network/kurirlist',$data);
    }

    public function delete($hfcode){
        $rdata = $this->input->post();
        if($this->M_kurir->delete($rdata)){
            $json['status']='success';
        }else{
            $json['status']="error";
        }

        echo json_encode($json);

    }
}