<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
Class Language extends CI_Controller{
    public function __construct(){
        parent::__construct();
        $this->load->model('M_language');
    }

    public function switchlang($lang=""){
        $dblang = $this->M_language->deflang();
        $language = ($lang!= "") ? $lang : $dblang->language;
        $this->session->set_userdata('site_lang', $language);
        redirect($_SERVER['HTTP_REFERER']);

    }

    public function index(){
        echo "language";
    }
}