<?php
Class Shipper extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('user_name')){
            redirect(base_url()."login");
        }else{
            $this->load->model('unit/M_shipper');
        }

    }

    public function index(){
        $data['shipper']= $this->M_shipper->getDataList();
        $this->template->renderpage("unit/shipper",$data);
    }

    public function add(){
        $rdata = $this->input->post();
        if($add=$this->M_shipper->insert($rdata)){
            $jcode = array('status'=>'success',"message"=>$this->lang->line("data_save"),"respone"=>$add);
        }else{
            $jcode = array('status'=>'error',"message"=>$this->lang->line("failed"),"response"=>false);
        }
        echo json_encode($jcode);
    }

    public function update($id){
        $rdata = $this->input->post();
        if($this->M_shipper->update($rdata,$id)){
            $jcode = array('status'=>'success',"message"=>$this->lang->line("data_save"),"response"=>true);
        }else{
            $jcode = array('status'=>'error',"message"=>$this->lang->line("failed"),"response"=>false);
        }
        echo json_encode($jcode);
    }

    public function delete($id){
        if($this->M_shipper->delete($id)){
            redirect(base_url()."master/shipper");
        }
    }

    public function detail($id){
        $jdata['response'] = $this->M_shipper->detail($id);
        echo json_encode($jdata);
    }

    public function getlist(){
        $json['response'] = $this->M_shipper->getDataActiveList();
        echo json_encode($json);
    }



}