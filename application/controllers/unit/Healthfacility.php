<?php
Class Healthfacility extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('user_name')){
            redirect(base_url()."login");
        }
        $this->load->model("unit/M_healthfacility");
        $this->load->model("geo/M_province");
        $this->load->model("geo/M_district");
        $this->load->model("geo/M_subdistrict");
    }

    public function index(){
      
            $page = $this->uri->segment('4');
       
        $data['province'] = $this->M_province->getList();
        $data['response'] = $this->M_healthfacility->getDataList($page,'50');
       $data['total'] = $this->M_healthfacility->countAll();
       $data['hftype'] = $this->M_healthfacility->typeofhf();
        $this->template->renderpage('unit/healthfacility',$data);

    }

    public function add(){
        $rdata = $this->input->post();
        if($n = $this->M_healthfacility->insert($rdata)){
            $jcode = array('status'=>'success',"message"=>$this->lang->line("data_save"),"respone"=>$n);
        }else{
            $jcode = array('status'=>'error',"message"=>$this->lang->line("failed"),"response"=>false);
        }
        echo json_encode($jcode);

    }

    public function update($id){
        $rdata = $this->input->post();
        $n = $this->M_healthfacility->update($rdata,$id);
        if($n){
            $jcode = array('status'=>'success',"message"=>$this->lang->line("data_save"),"respone"=>$n);
        }else{
            $jcode = array('status'=>'error',"message"=>$this->lang->line("failed"),"response"=>false);
        }
        echo json_encode($jcode);

    }

    public function delete($id){
        if($this->M_healthfacility->delete($id)){
            redirect(base_url()."master/hf/list/1");
        }

    }

    public function search(){
        $search = $this->input->post('search');
        $data['hfsearch'] = $this->M_healthfacility->search($search);
        $this->load->view('unit/searchlist_hf',$data); 
    }

    public function detail($id){
        $response['response']=$this->M_healthfacility->detail($id);

        echo json_encode($response);
    }

    public function hfbydistrict(){
        $id = $this->input->post('hf_district');
        $jdata['response'] = $this->M_healthfacility->getListByDistrict($id);
        echo json_encode($jdata);
    }

    public function hfreflist(){
        $id = $this->input->post('hf_district');
        $sr = $this->input->post("hf_code");
        $jdata['response'] = $this->M_healthfacility->getListRefByDistrict($id,$sr);
        echo json_encode($jdata);
    }

    public function updatelist(){

    }
}


