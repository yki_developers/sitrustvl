<?php
Class Subdistrict extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('user_name')){
            redirect(base_url()."login");
        }

        $this->load->model('geo/M_subdistrict');
        $this->load->model('geo/M_province');
        $this->load->model('geo/M_district');
    }

    public function index(){
        if(!$this->input->post()){
            $page="1";
        }else{
            $page = $this->input->post('page');
        }
        $data['province'] = $this->M_province->getList();
        $data['response'] = $this->M_subdistrict->getDataList($page,'50');
        $data['total'] = $this->M_subdistrict->countAll();
        $this->template->renderpage('geo/list_subdistrict',$data);

    }

    public function bydistrict(){
        $district = $this->input->post("district_code");
        if($json['response'] = $this->M_subdistrict->getByDistrict($district)){
            $json['status'] = "success";
        }else{
            $json['status'] = "error";
        }

        echo json_encode($json);

    }

    public function add(){
        $rdata = $this->input->post();
        if($n = $this->M_subdistrict->insert($rdata)){
            $jcode = array('status'=>'success',"message"=>$this->lang->line("data_save"),"respone"=>$n);
        }else{
            $jcode = array('status'=>'error',"message"=>$this->lang->line("failed"),"response"=>false);
        }
        echo json_encode($jcode);

    }

    public function update($id){
        $rdata = $this->input->post();
        $n = $this->M_subdistrict->update($rdata,$id);
        if($n){
            $jcode = array('status'=>'success',"message"=>$this->lang->line("data_save"),"respone"=>$n);
        }else{
            $jcode = array('status'=>'error',"message"=>$this->lang->line("failed"),"response"=>false);
        }
        echo json_encode($jcode);

    }

    public function delete($id){
        if($this->M_subdistrict->delete($id)){
            redirect(base_url()."master/subdistrict/list/1");
        }

    }

    public function search(){
        $search = $this->input->post('search');
        $data['response'] = $this->M_subdistrict->search($search);
        $this->load->view('geo/searchlist_subdistrict',$data); 
    }

    public function detail(){
        $response=$this->M_subdistrict->getDetail($this->input->post('subdistrict_code'));
        echo json_encode($response);
    }

    public function updatelist(){

    }
}