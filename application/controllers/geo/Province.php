<?php
Class Province extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('user_name')){
            redirect(base_url()."login");
        }
        $this->load->model('geo/M_province');
    }

    public function index(){
       $data['prov'] = $this->M_province->getList();
        $this->template->renderpage('geo/list_province',$data); 

    }

    public function datalist(){
        $data['prov'] = $this->M_province->getList();
        $this->load->view('geo/searchlist_province',$data);
    }


    public function search(){
        $search = $this->input->post('search');
        $data['prov'] = $this->M_province->search($search);
        $this->load->view('geo/searchlist_province',$data); 
    }

    public function add(){
        $rdata = $this->input->post();
        if($n = $this->M_province->insert($rdata)){
            $jcode = array('status'=>'success',"message"=>$this->lang->line("data_save"),"respone"=>$n);
        }else{
            $jcode = array('status'=>'error',"message"=>$this->lang->line("failed"),"response"=>false);
        }
        echo json_encode($jcode);

    }

    public function update($id){
        $rdata = $this->input->post();
        $n = $this->M_province->update($rdata,$id);
        if($n){
            $jcode = array('status'=>'success',"message"=>$this->lang->line("data_save"),"respone"=>$n);
        }else{
            $jcode = array('status'=>'error',"message"=>$this->lang->line("failed"),"response"=>false);
        }
        echo json_encode($jcode);

    }

    public function delete($id){
        if($this->M_province->delete($id)){
            redirect(base_url()."master/province/list");
        }

    }

}