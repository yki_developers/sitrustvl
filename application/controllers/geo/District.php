<?php

Class District extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('user_name')){
            redirect(base_url()."login");
        }
        $this->load->model('geo/M_district');
        $this->load->model("geo/M_province");
    }

    public function index($page){
      
        $data['province'] = $this->M_province->getList();
        $data['response'] = $this->M_district->getDataList($page,'50');
        $data['total'] = $this->M_district->countAll();
        $this->template->renderpage('geo/list_district',$data);
    }



    public function add(){
        $rdata = $this->input->post();
        if($n = $this->M_district->insert($rdata)){
            $jcode = array('status'=>'success',"message"=>$this->lang->line("data_save"),"respone"=>$n);
        }else{
            $jcode = array('status'=>'error',"message"=>$this->lang->line("failed"),"response"=>false);
        }
        echo json_encode($jcode);
    }



 public function delete($id){
        if($this->M_district->delete($id)){
            redirect(base_url()."master/district/list");
        }

    }

    public function search(){
        $search = $this->input->post('search');
        $data['response'] = $this->M_district->search($search);
        $this->load->view('geo/searchlist_district',$data); 
    }

    public function update($id){
        $rdata = $this->input->post();
        $n = $this->M_district->update($rdata,$id);
        if($n){
            $jcode = array('status'=>'success',"message"=>$this->lang->line("data_save"),"respone"=>$n);
        }else{
            $jcode = array('status'=>'error',"message"=>$this->lang->line("failed"),"response"=>false);
        }
        echo json_encode($jcode);
    }


    public function detail(){
       $response=$this->M_district->getDetail($this->input->post('district_code'));
       echo json_encode($response);
    }
    public function districtbyprovince(){
        $response['response'] = $this->M_district->getDistrictByProvince($this->input->post('province_code'));
        echo json_encode($response);

    }


}