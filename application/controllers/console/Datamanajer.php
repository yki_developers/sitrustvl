<?php
Class Datamanajer extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("console/M_datamanajer");
        $this->load->model("transactional/M_order");
    }

    public function index(){
        $this->template->renderpage("console/datamanajer");
    }

    public function mergersubmit(){
        $origin = explode(",",$this->input->post("order_origin"));
        $id=array();
        for($i=0;$i<sizeof($origin);$i++){
            $id[$i] = $origin[$i];
        }

        $data = $id;
        $dest = $this->input->post("order_dest");

        if($this->M_datamanajer->mergeOrder($data,$dest)){
            $json['status'] ="success";
            $json['message'] = "Data Berhasil diupdate";
        }else{
            $json['status'] ="error";
            $json['message'] = "Data Gagal diupdate";
        }

        echo json_encode($json);
    }

    public function getOrder(){
        $id = array("a.order_id"=>$this->input->post("order_id"));

        if($json['response']=$this->M_order->detail($id)){
            $json['status'] ="success";

        }else{
            $json['status']="error";
        }
        echo json_encode($json);
    }

    
}