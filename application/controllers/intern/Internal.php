<?php
Class Internal extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('intern/M_internal');
        $this->load->model("geo/M_province");
    }

    public function index(){
        $data['datalist'] = $this->M_internal->getDataList($this->session->userdata("user_unit"));
        $data['province'] = $this->M_province->getList();
        $this->template->renderpage('intern/internal_list',$data);
    }

    public function search(){
        $search = $this->input->post('search');
        $data['datalist'] = $this->M_internal->search($search);
        $this->load->view("intern/search_internal_list",$data);
    }

    public function searchresult(){
        $search = $this->input->post('search');
        $data['datalist'] = $this->M_internal->searchresult($search);
        $this->load->view("intern/search_internal_result_list",$data);
    }

    public function forminternal(){
        $this->template->renderpage('intern/internal_form');
    }

public function detail($id){
    if($json['response'] = $this->M_internal->getSpecimen($id)){
        $json['status']='200';
    }else{
        $json['status']='Error';
        $json['response'] = false;
    };

    echo json_encode($json);
    
}
public function delete($id){
    $del = $this->M_internal->delete($id);
    if($del){
        redirect(base_url()."intern/internal");
    }
}

public function received($id){
    $date = date("Y-m-d H:i:s");
$rdata = array("specimen_internal_received"=>"5","specimen_internal_received_date"=>$date);
    $upd = $this->M_internal->update($rdata,$id);
    if($upd){
        redirect(base_url()."intern/internal/result");
    }
}

public function received_eid($id){
    $date = date("Y-m-d H:i:s");
$rdata = array("specimen_internal_received"=>"5","specimen_internal_received_date"=>$date);
    $upd = $this->M_internal->update($rdata,$id);
    if($upd){
        redirect(base_url()."intern/internal/result_eid");
    }
}


    public function insert(){
        $rdata = $this->input->post();
        $response = $this->M_internal->insert($rdata);
        if($response){
            $json['status'] = "success";
            $json['response'] = $response;
            $json['message'] = $this->lang->line('data_save');
        }else{
            $json['status'] = "error";
            $json["message"] = "Gagal Simpan";
        }

        echo json_encode($json);
    }


    public function result(){
        $data['datalist'] = $this->M_internal->getDataSpecimenList($this->session->userdata("user_unit"));
        $this->template->renderpage('intern/internal_result_list',$data);
    }

    public function result_eid(){
        $data['datalist'] = $this->M_internal->getDataSpecimenList($this->session->userdata("user_unit"),"1");
        $this->template->renderpage('intern/internal_result_list',$data);

    }


    public function vlresult($id){
        $data['specimen'] = $this->M_internal->getSpecimen($id);
        $this->template->renderpage('intern/internal_result_form_vl',$data);
    }

    public function editvlresult($id){
        $data['specimen'] = $this->M_internal->getSpecimen($id);
        $this->template->renderpage('intern/edit_internal_result_form_vl',$data);
    }

    public function insertresult($id){
        $rdata = $this->input->post();
        $response = $this->M_internal->insertVlResult($rdata);
        if($response){
            $this->M_internal->update(array('specimen_result_flag'=>"1","specimen_internal_received"=>"6"),$id);
            $json['status'] = "success";
            $json['response'] = $response;
            $json['message'] = $this->lang->line('data_save');
        }else{
            $json['status'] = "error";
            $json["message"] = "Gagal Simpan";
        }

        echo json_encode($json);

    }



    public function insertresult_eid($id){
        $rdata = $this->input->post();
        $response = $this->M_internal->insertEidResult($rdata);
        if($response){
            $this->M_internal->update(array('specimen_result_flag'=>"1","specimen_internal_received"=>"6"),$id);
            $json['status'] = "success";
            $json['response'] = $response;
            $json['message'] = $this->lang->line('data_save');
        }else{
            $json['status'] = "error";
            $json["message"] = "Gagal Simpan";
        }

        echo json_encode($json);

    }


    public function eidresult($id){
        $data['specimen'] = $this->M_internal->getSpecimen($id);
        $this->template->renderpage('intern/internal_result_form_eid',$data);
    }

    public function update($id){
        $rdata = $this->input->post();
        $response = $this->M_internal->update($rdata,$id);
        if($response){
            $json['status'] = "success";
            $json['response'] = $response;
            $json['message'] = $this->lang->line('data_save');
        }else{
            $json['status'] = "error";
            $json["message"] = "Gagal Simpan";
        }

        echo json_encode($json);
    }


    public function updateresultvl($id){
        $rdata = $this->input->post();
        $response = $this->M_internal->updateVLresult($rdata,$id);
        if($response){
            $json['status'] = "success";
            $json['response'] = $response;
            $json['message'] = $this->lang->line('data_save');
        }else{
            $json['status'] = "error";
            $json["message"] = "Gagal Simpan";
        }

        echo json_encode($json);
    }


}