<?php
Class Eidinternal extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("intern/M_internal");
        $this->load->model("geo/M_province");
    }

    public function index(){
        $data['province'] = $this->M_province->getList();
        $data['datalist'] = $this->M_internal->getEidDataList($this->session->userdata("user_unit"));
        $this->template->renderpage('intern/eid_internal_list',$data);
    }



    public function insert(){
        $rdata = $this->input->post();
        $response = $this->M_internal->insert($rdata);
        if($response){
            $json['status'] = "success";
            $json['response'] = $response;
            $json['message'] = $this->lang->line('data_save');
        }else{
            $json['status'] = "error";
            $json["message"] = "Gagal Simpan";
        }

        echo json_encode($json);
    }


    public function delete($id){
        $del = $this->M_internal->delete($id);
        if($del){
            redirect(base_url()."intern/eidinternal");
        }
    }
    
}