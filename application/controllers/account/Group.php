<?php
Class Group extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('account/M_group');
    }

    public function index(){
        $data['response'] = $this->M_group->getDataList();
        $this->template->renderpage("account/grouplist",$data);
    }

    public function add(){
        $rdata = $this->input->post();
        $response = $this->M_group->insert($rdata);
        if($response){
            $json = array("status"=>"success","message"=>$this->lang->line("data_save"),"response"=>$response);
        }else{
            $json = array("status"=>"error","message"=>$this->lang->line("failed"),"response"=>false);
        }

        echo json_encode($json);
    }

    public function delete($id){
        if($this->M_group->delete($id)){
            redirect(base_url()."account/group/list");
        }
    }

    public function update($id){
        $rdata = $this->input->post();
        $response = $this->M_group->update($rdata,$id);
        if($response){
            $json = array("status"=>"success","message"=>$this->lang->line("data_save"),"response"=>$response);
        }else{
            $json = array("status"=>"error","message"=>$this->lang->line("failed"),"response"=>false);
        }

        echo json_encode($json);

    }

    public function detail($id){
        $json['response'] = $this->M_group->detail($id);
        echo json_encode($json);
    }
}