<?php 
Class User extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        if(!$this->session->userdata('user_name')){
            redirect(base_url()."login");
        }else{
            if($this->session->userdata("user_adminlevel")!='1'){
                redirect(base_url()."home");
            }
            $this->load->model('account/M_user');
            $this->load->model('account/M_group');
            $this->load->model('geo/M_province');
        }
    }

    public function index($page=1){
        
        if($this->session->userdata('user_group')=='1'){
            $par = array("user_group>="=>"1");
            $gid = array("group_id>="=>"1");
            $data['admin'] = '1';
            $data['province'] = $this->M_province->getList();
        }elseif($this->session->userdata('user_group')=='2'){
            $par = array("user_group>="=>"2","group_id<"=>"7");
            $gid = array("group_id>="=>"2","group_id<"=>"7");
            $data['admin'] = '0';
            $data['province'] = $this->M_province->getList();
        }elseif($this->session->userdata('user_group')=='3'){
            $par = array("user_group>="=>"3","group_id<"=>"7","user_province"=>$this->session->userdata("user_province"));
            $gid = array("group_id>="=>"3","group_id<"=>"7");
            $data['admin'] = '0';
            $data['province'] = $this->M_province->getList($this->session->userdata("user_province"));
        }elseif($this->session->userdata('user_group')=='4'){
            $par = array("user_group>="=>"4","group_id<"=>"7","user_district"=>$this->session->userdata("user_district"));
            $gid = array("group_id>="=>"4","group_id<"=>"7");
            $data['admin'] = '0';
            $data['province'] = $this->M_province->getList($this->session->userdata("user_province"));
        }elseif($this->session->userdata('user_group')=='6'){
            $par = array("user_group"=>"6","group_id<"=>"7","user_unit"=>$this->session->userdata("user_unit"));
            $gid = array("group_id"=>"6","group_id<"=>"7");
            $data['admin'] = '0';
            $data['province'] = $this->M_province->getList($this->session->userdata("user_province"));
        }elseif($this->session->userdata('user_group')=='7'){
            $par = array("user_group"=>"7","user_unit"=>$this->session->userdata("user_unit"));
            $gid = array("group_id>="=>"7");
            $data['admin'] = '0';
            $data['province'] = $this->M_province->getList($this->session->userdata("user_province"));
        }elseif($this->session->userdata('user_group')=='8'){
            $par = array("user_group>="=>"3");
            $gid = array("group_id>="=>"3","group_id<"=>"8");
            $data['admin'] = '0';
            $data['province'] = $this->M_province->getList();
        }else{
            $par=null;
            $gid=null;
            $data['admin'] = '1';
        }
        $data['usergroup'] = $this->M_group->getDataList($gid);
        $data['response'] = $this->M_user->getDataList($page,'50',$par);
        $data['total'] = $this->M_user->countAll();

        
        
        $this->template->renderpage('account/userlist',$data);
    }
    
    public function add(){
        $rdata = $this->input->post();
        $mdata = md5($this->input->post('user_passwd'));
        unset($rdata['user_passwd']);
        $rdata['user_passwd'] = $mdata;
        if($user=$this->M_user->insert($rdata)){
            
            $jcode = array('status'=>'success',"message"=>$this->lang->line("data_save"),"respone"=>$user);
        }else{
            $jcode = array('status'=>'error',"message"=>$this->lang->line("failed"),"response"=>false);
        }
        echo json_encode($jcode);
    }

    public function delete($id){
        if($this->M_user->delete($id)){
            redirect(base_url()."account/user/list/1");
        }
    }
    


    public function usergroup(){
        $data['usergroup'] = $this->M_user->getDataGroupList();
        $this->template->renderpage('account/grouplist',$data);
    }

    public function delgroup($id){
        if($this->M_user->deleteGroup($id)){
            redirect(base_url()."user/group");
        }
    }

    public function userrole(){
        $data['userrole'] = $this->M_user->getDataRoleList();
        $this->template->renderpage('account/rolelist',$data);
    }

    public function delrole($id){
        if($this->M_user->deleteRole($id)){
            redirect(base_url()."user/role");
        }
    }

    public function chpassword($passwd,$id){

        $rdata = array("user_passwd"=>md5($passwd));
        $this->db->where("user_name",$id);
        return $this->db->update(DB_USER,$rdata);
    }

    public function detail($id){
        if($json['response']=$this->M_user->detail($id)){
            $json['status']='200';
        }else{
            $json['status']='error';
        }

        echo json_encode($json);
    }

    public function update($id){
        $rdata = $this->input->post();
        if(array_key_exists("user_passwd",$rdata)){
            unset($rdata['user_passwd']);
            $rdata["user_passwd"] = md5($this->input->post("user_passwd"));
        }
        if($json['response']=$this->M_user->update($rdata,$id)){
            $json['status'] = "success";
            $json['message'] = $this->lang->line("data_save");
        }else{
            $json['status'] ='error';
            $json['message'] = $this->lang->line("failed");
        }
        echo json_encode($json);
    }


    public function validation(){
        $rdata = $this->input->post();
        $json['response'] = $this->M_user->validate($rdata);
        echo json_encode($json);
    }

    public function search(){
        $rdata = $this->input->post('search');
        $json['response'] = $this->M_user->search($rdata);
        $this->load->view('account/user_search',$json);
    }






}