<?php
Class Newdashboard extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("dashboard/M_dashboard");
        $this->load->model("geo/M_province");
    }

    public function index($level){
        $data['prop'] = $this->M_province->getList();
        if($level=='nasional'){
            $this->template->renderpage("dashboard/dashboard_nasional",$data);

        }elseif($level=='propinsi'){
            //$data['nama_propinsi'] = $this->M_reverse->provName($this->session->userdata('user_province'));
            $data['kode_propinsi'] = $this->session->userdata("user_province");

            $this->template->renderpage("dashboard/dashboard_propinsi",$data);
            

        }elseif($level=='kabupaten'){
            $data['kode_propinsi'] = $this->session->userdata("user_province");
            $data['kode_kabupaten'] = $this->session->userdata("user_district");
            $this->template->renderpage("dashboard/dashboard_kabupaten",$data);

        }elseif($level=='fasyankes'){
            $data['kode_propinsi'] = $this->session->userdata("user_province");
            $data['kode_kabupaten'] = $this->session->userdata("user_district");
            $data['kode_fasyankes'] = $this->session->userdata("user_unit");
            $this->template->renderpage("dashboard/dashboard_fasyankes",$data);
        }
    }

    public function datanasional(){
        if($this->input->post('propinsi')=='999'){
            $lev = "nasional";
            $rdata=null; 
        }else{
            if($this->input->post('kabupaten')=='99999'){
            $lev = "propinsi";
            $rdata = array("dash_province"=>$this->input->post('propinsi'));
            $data['province'] = $this->input->post("propinsi");
            }else{
                if($this->input->post('fasyankes')=='P9'){
                    $lev = "kabupaten";
                    $rdata = array("dash_district"=>$this->input->post('kabupaten'));
                    $data['district'] = $this->input->post("kabupaten");
                }else{
                    $lev = "fasyankes";
                    $rdata = array("dash_hf_sender"=>$this->input->post('fasyankes'));
                    $data['fasyankes'] = $this->input->post("fasyankes");
                }
            }
            
        }

        $p_start = $this->input->post("periode_start");
        $p_end = $this->input->post("periode_end");

        $data['specimen_status'] = $this->M_dashboard->getSpecimenDikirimNasional($rdata,$p_start,$p_end);
        $data['specimen_hasil'] = $this->M_dashboard->getSpecimenHasilNasional($rdata,$p_start,$p_end);
        $data['specimen_tat'] = $this->M_dashboard->getTATNasional($rdata,$p_start,$p_end);


        $data['eid_status'] = $this->M_dashboard->getSpecimenDikirimNasionalEID($rdata,$p_start,$p_end);
        $data['eid_hasil'] = $this->M_dashboard->getSpecimenHasilNasionalEID($rdata,$p_start,$p_end);
        $data['eid_tat'] = $this->M_dashboard->getTATNasionalEID($rdata,$p_start,$p_end);



        $data['periode'] = $this->M_dashboard->getChartPeriode($p_start,$p_end);
        $data['level'] = $lev;

        $this->load->view("dashboard/newnasional",$data);

    }




    public function dataregional($level,$kode){


   if($level=='propinsi'){

            if($this->input->post('kabupaten')=='99999'){
            $lev = "propinsi";
            $rdata = array("dash_province"=>$this->input->post('propinsi'));
            $data['province'] = $this->input->post("propinsi");
            }else{
                if($this->input->post('fasyankes')=='P9'){
                    $lev = "kabupaten";
                    $rdata = array("dash_district"=>$this->input->post('kabupaten'));
                    $data['district'] = $this->input->post("kabupaten");
                }else{
                    $lev = "fasyankes";
                    $rdata = array("dash_hf_sender"=>$this->input->post('fasyankes'));
                    $data['fasyankes'] = $this->input->post("fasyankes");
                }
            }
        }elseif($level=='kabupaten'){
            if($this->input->post('fasyankes')=='P9'){
                $lev = "kabupaten";
                $rdata = array("dash_district"=>$this->input->post('kabupaten'));
                $data['district'] = $this->input->post("kabupaten");
            }else{
                $lev = "fasyankes";
                $rdata = array("dash_hf_sender"=>$this->input->post('fasyankes'));
                $data['fasyankes'] = $this->input->post("fasyankes");
            }
        }elseif($level=='fasyankes'){
            $lev = "fasyankes";
                $rdata = array("dash_hf_sender"=>$this->input->post('fasyankes'));
                $data['fasyankes'] = $this->input->post("fasyankes");

        }     
        

        $p_start = $this->input->post("periode_start");
        $p_end = $this->input->post("periode_end");

        $data['specimen_status'] = $this->M_dashboard->getSpecimenDikirimNasional($rdata,$p_start,$p_end);
        $data['specimen_hasil'] = $this->M_dashboard->getSpecimenHasilNasional($rdata,$p_start,$p_end);
        $data['specimen_tat'] = $this->M_dashboard->getTATNasional($rdata,$p_start,$p_end);


        $data['eid_status'] = $this->M_dashboard->getSpecimenDikirimNasionalEID($rdata,$p_start,$p_end);
        $data['eid_hasil'] = $this->M_dashboard->getSpecimenHasilNasionalEID($rdata,$p_start,$p_end);
        $data['eid_tat'] = $this->M_dashboard->getTATNasionalEID($rdata,$p_start,$p_end);



        $data['periode'] = $this->M_dashboard->getChartPeriode($p_start,$p_end);
        $data['level'] = $lev;

        $this->load->view("dashboard/newnasional",$data);

    }



}