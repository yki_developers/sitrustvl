<?php
Class Chart extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("dashboard/M_chart");
        
    }



    public function chart_pengiriman_nasional($p_start,$p_end){
        $json['data'] = $this->M_chart->getListPengirimanNasional($p_start,$p_end);

       foreach($json['data'] as $dt){
           $jd[] = $dt->value;
       }
       $total = array_sum($jd);
        $json['chart'] = array("palette"=>"2",
        "caption"=>"Jumlah Spesimen VL Dikirim ",
        "subCaption"=>"Total Spesimen : ".$total,
     

      "bgcolor"=>"406181, 6DA5DB",
      "bgalpha"=>"100",
      "basefontcolor"=>"000000",
      "canvasbgalpha"=>"0",
      "canvasbordercolor"=>"FFFFFF",
      "divlinecolor"=>"FFFFFF",
      "divlinealpha"=>"100",
      "numvdivlines"=>"0",
      "vdivlineisdashed"=>"0",
      "showalternatevgridcolor"=>"1",
      "linecolor"=>"ff0000",
      "anchorradius"=>"4",
      "anchorbgcolor"=>"BBDA00",
      "anchorbordercolor"=>"FFFFFF",
      "anchorborderthickness"=>"2",
     "plotbordercolor"=> "83B242",
     "plotGradientColor"=>"",
     "canvasbordercolor"=> "000000",
     "canvasborderthickness"=> "1",
     "showvalues"=>"1",
     "showsum"=>"1",
     "showlabels"=>"1",
    "exportenabled"=>"1",
    "toolbarHalign"=>'left',
    "chartBottomMargin"=>"30",
    "labeldisplay"=>"rotate"
     );
     
     
     echo json_encode($json);
    }



    public function chart_pengiriman_regional($level,$kode,$p_start,$p_end){

        if($level=='propinsi'){
            $json['data'] = $this->M_chart->getListPengirimanPropinsi($kode,$p_start,$p_end);
         }elseif($level=='kabupaten'){
            $json['data'] = $this->M_chart->getListPengirimanKabupaten($kode,$p_start,$p_end);
    
         }elseif($level=='fasyankes'){
             
         }

         foreach($json['data'] as $dt){
            $jd[] = $dt->value;
        }
        $total = array_sum($jd);


        $json['chart'] = array("palette"=>"2",
        "caption"=>"Jumlah Spesimen VL Dikirim ",
        "subCaption"=>"Total Spesimen : ".$total,

      "bgcolor"=>"406181, 6DA5DB",
      "bgalpha"=>"100",
      "basefontcolor"=>"000000",
      "canvasbgalpha"=>"0",
      "canvasbordercolor"=>"FFFFFF",
      "divlinecolor"=>"FFFFFF",
      "divlinealpha"=>"100",
      "numvdivlines"=>"0",
      "vdivlineisdashed"=>"0",
      "showalternatevgridcolor"=>"1",
      "linecolor"=>"ff0000",
      "anchorradius"=>"4",
      "anchorbgcolor"=>"BBDA00",
      "anchorbordercolor"=>"FFFFFF",
      "anchorborderthickness"=>"2",
     "plotbordercolor"=> "83B242",
     "plotGradientColor"=>"",
     "canvasbordercolor"=> "000000",
     "canvasborderthickness"=> "1",
     "showvalues"=>"1",
     "showsum"=>"1",
     "showlabels"=>"1",
    "exportenabled"=>"1",
    "toolbarHalign"=>'left',
    "chartBottomMargin"=>"30",
    "labeldisplay"=>"rotate"
     );
    

  
     
     
     echo json_encode($json);
    }






    public function chart_trend_nasional($per,$p_start,$p_end){
        $json['data']= $this->M_chart->getTrendNasional($per,$p_start,$p_end);
        foreach($json['data'] as $ldata){
            $dm[] = $ldata->value;
        }
        $total= array_sum($dm);
        $json['chart'] = array("palette"=>"2",
        "caption"=>"Tren Pengiriman Spesimen VL",
        "subCaption"=>"Total Spesimen :".$total,
     

      "bgcolor"=>"406181, 6DA5DB",
      "bgalpha"=>"100",
      "basefontcolor"=>"000000",
      "canvasbgalpha"=>"0",
      "canvasbordercolor"=>"FFFFFF",
      "divlinecolor"=>"FFFFFF",
      "divlinealpha"=>"100",
      "numvdivlines"=>"0",
      "vdivlineisdashed"=>"0",
      "showalternatevgridcolor"=>"1",
      "linecolor"=>"ff0000",
      "anchorradius"=>"4",
      "anchorbgcolor"=>"BBDA00",
      "anchorbordercolor"=>"FFFFFF",
      "anchorborderthickness"=>"2",
     "plotbordercolor"=> "83B242",
     "canvasbordercolor"=> "83B242",
     "canvasborderthickness"=> "1",
     "showvalues"=>"0",
     "showsum"=>"1",
     "showlabels"=>"1",
    "exportenabled"=>"1",
    "toolbarHalign"=>'left',
    "labeldisplay"=>"rotate"
     );
   
     echo json_encode($json);
    }



    public function chart_trend_propinsi($kode,$per,$p_start,$p_end){
        $json['data']= $this->M_chart->getTrendPropinsi($kode,$per,$p_start,$p_end);
        foreach($json['data'] as $ldata){
            $dm[] = $ldata->value;
        }
        $total= array_sum($dm);
        $json['chart'] = array("palette"=>"2",
        "caption"=>"Tren Pengiriman Spesimen VL",
        "subCaption"=>"Total Spesimen :".$total,
     

      "bgcolor"=>"406181, 6DA5DB",
      "bgalpha"=>"100",
      "basefontcolor"=>"000000",
      "canvasbgalpha"=>"0",
      "canvasbordercolor"=>"FFFFFF",
      "divlinecolor"=>"FFFFFF",
      "divlinealpha"=>"100",
      "numvdivlines"=>"0",
      "vdivlineisdashed"=>"0",
      "showalternatevgridcolor"=>"1",
      "linecolor"=>"ff0000",
      "anchorradius"=>"4",
      "anchorbgcolor"=>"BBDA00",
      "anchorbordercolor"=>"FFFFFF",
      "anchorborderthickness"=>"2",
     "plotbordercolor"=> "83B242",
     "canvasbordercolor"=> "83B242",
     "canvasborderthickness"=> "1",
     "showvalues"=>"0",
     "showsum"=>"1",
     "showlabels"=>"1",
    "exportenabled"=>"1",
    "toolbarHalign"=>'left',
    "labeldisplay"=>"rotate"
     );
     

   
     /*
     foreach($jk as $list){
         $json['data']['label'] = $list->label;
         $json['data']['value'] = $list->value;
     }*/
     echo json_encode($json);



    }




    public function chart_trend_kabupaten($kode,$per,$p_start,$p_end){
        $json['data']= $this->M_chart->getTrendKabupaten($kode,$per,$p_start,$p_end);
        foreach($json['data'] as $ldata){
            $dm[] = $ldata->value;
        }
        $total= array_sum($dm);
        $json['chart'] = array("palette"=>"2",
        "caption"=>"Tren Pengiriman Spesimen VL",
        "subCaption"=>"Total Spesimen :".$total,

      "bgcolor"=>"406181, 6DA5DB",
      "bgalpha"=>"100",
      "basefontcolor"=>"000000",
      "canvasbgalpha"=>"0",
      "canvasbordercolor"=>"FFFFFF",
      "divlinecolor"=>"FFFFFF",
      "divlinealpha"=>"100",
      "numvdivlines"=>"0",
      "vdivlineisdashed"=>"0",
      "showalternatevgridcolor"=>"1",
      "linecolor"=>"ff0000",
      "anchorradius"=>"4",
      "anchorbgcolor"=>"BBDA00",
      "anchorbordercolor"=>"FFFFFF",
      "anchorborderthickness"=>"2",
     "plotbordercolor"=> "83B242",
     "canvasbordercolor"=> "83B242",
     "canvasborderthickness"=> "1",
     "showvalues"=>"0",
     "showsum"=>"1",
     "showlabels"=>"1",
    "exportenabled"=>"1",
    "toolbarHalign"=>'left',
    "labeldisplay"=>"rotate"
     );
     

 
     /*
     foreach($jk as $list){
         $json['data']['label'] = $list->label;
         $json['data']['value'] = $list->value;
     }*/
     echo json_encode($json);



    }



    public function chart_trend_fasyankes($kode,$per,$p_start,$p_end){
        $json['data']= $this->M_chart->getTrendFasyankes($kode,$per,$p_start,$p_end);
        foreach($json['data'] as $ldata){
            $dm[] = $ldata->value;
        }
        $total= array_sum($dm);
        $json['chart'] = array("palette"=>"2",
        "caption"=>"Tren Pengiriman Spesimen VL",
        "subCaption"=>"Total Spesimen :".$total,
     

      "bgcolor"=>"406181, 6DA5DB",
      "bgalpha"=>"100",
      "basefontcolor"=>"000000",
      "canvasbgalpha"=>"0",
      "canvasbordercolor"=>"FFFFFF",
      "divlinecolor"=>"FFFFFF",
      "divlinealpha"=>"100",
      "numvdivlines"=>"0",
      "vdivlineisdashed"=>"0",
      "showalternatevgridcolor"=>"1",
      "linecolor"=>"ff0000",
      "anchorradius"=>"4",
      "anchorbgcolor"=>"BBDA00",
      "anchorbordercolor"=>"FFFFFF",
      "anchorborderthickness"=>"2",
     "plotbordercolor"=> "83B242",
     "canvasbordercolor"=> "83B242",
     "canvasborderthickness"=> "1",
     "showvalues"=>"0",
     "showsum"=>"1",
     "showlabels"=>"1",
    "exportenabled"=>"1",
    "toolbarHalign"=>'left',
    "labeldisplay"=>"rotate"
     );
     

    

     echo json_encode($json);



    }




    

    public function chart_specimen_condition($p_start,$p_end){
        $json = array();


       
            $mdata = $this->M_chart->getKondisiSpesimenNasional($p_start,$p_end);
     


        $json['chart'] = array("palette"=>"1",
        "bgcolor"=>"406181, 6DA5DB",
        "bgalpha"=>"100",
        "basefontcolor"=>"000000",
        "canvasbgalpha"=>"0",
         "caption"=>"Kondisi Spesimen",
          "subCaption"=>"(N = $mdata->total_specimen) ",
           "showpercentageinlabel"=>"1",
           "showpercentvalues"=> "1",
           "showvalues"=>"1",
           "showlabels"=>"0",
           "showlegend"=>"1",
           "enableSmartLabels"=> "0",
           "pieRadius"=>"150",
           "exportenabled"=>"1",
           "toolbarHalign"=>'left'
        );

        

        $json['data']=array();
        $json['data']=array(
            
            array("label"=>"Rusak : $mdata->db_spc_broken ","color"=>"ff0000","value"=>$mdata->db_spc_broken),
            array("label"=>"Baik : $mdata->db_spc_good","color"=>"00ff00","value"=>$mdata->db_spc_good)
        );

        echo json_encode($json);
    }


    public function chart_specimen_condition_regional($lev,$kode,$p_start,$p_end){
        $json = array();


      if($lev=='propinsi'){
            $mdata = $this->M_chart->getKondisiSpesimenPropinsi($kode,$p_start,$p_end);
        }elseif($lev=='kabupaten'){
            $mdata = $this->M_chart->getKondisiSpesimenKabupaten($kode,$p_start,$p_end);
        }elseif($lev=='fasyankes'){
            $mdata = $this->M_chart->getKondisiSpesimenFaskes($kode,$p_start,$p_end);
        }

        
        $json['chart'] = array("palette"=>"1",
        "bgcolor"=>"406181, 6DA5DB",
        "bgalpha"=>"100",
        "basefontcolor"=>"000000",
        "canvasbgalpha"=>"0",
         "caption"=>"Kondisi Spesimen",
          "subCaption"=>"(N = $mdata->total_specimen)",
           "showpercentageinlabel"=>"1",
           "showpercentvalues"=> "1",
           "showvalues"=>"1",
           "showlabels"=>"0",
           "showlegend"=>"1",
           "enableSmartLabels"=> "0",
           "pieRadius"=>"150",
           "exportenabled"=>"1",
           "toolbarHalign"=>'left'
        );

        

        $json['data']=array();
        $json['data']=array(
            
            array("label"=>"Rusak : $mdata->db_spc_broken ","color"=>"ff0000","value"=>$mdata->db_spc_broken),
            array("label"=>"Baik : $mdata->db_spc_good","color"=>"00ff00","value"=>$mdata->db_spc_good)
        );

        echo json_encode($json);
    }



   
}