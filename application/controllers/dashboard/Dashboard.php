<?php
Class Dashboard extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model("dashboard/M_dashboard");
    }

    public function faskes(){
        $data['list'] = $this->M_dashboard->getDataSpecimen($this->session->userdata('user_unit'));
        $this->template->renderpage('dashboard/faskes',$data);

    }

    public function nasional(){
        $this->template->renderpage('dashboard/nasional');
    }


    public function propinsi(){
        $this->template->renderpage('dashboard/propinsi');
    }

    public function kabupaten(){
        $this->template->renderpage('dashboard/kabupaten');
    }
}