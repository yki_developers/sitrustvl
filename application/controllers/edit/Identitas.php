<?php
Class Identitas extends CI_Controller{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('edit/M_identitas');
        $this->load->model('geo/M_province');
    }

    public function index($id){
        //$id= $this->uri->segment('3');
        $data['province'] = $this->M_province->getList();
        $data['identitas'] = $this->M_identitas->getIdentitas($id);
        $this->template->renderpage('edit/identitas',$data);
    }

    public function update($idpatient){
        $rdata = $this->input->post();
        $update = $this->M_identitas->update($rdata,$idpatient);
        if($update){
            $json = array(
                "status"=>"success",
                "message"=>"Update data berhasil",
                "response"=>true
            );
        }else{
            $json = array(
                "status"=>"error",
                "message"=>"Update Gagal",
                "response"=>false
            );
        }

        echo json_encode($json);
    }

    public function checknik(){
        $rdata = $this->input->post();

        $fcheck = $this->M_identitas->checknik($rdata);
        if($fcheck>0){
            echo "true";
        }else{

            $mdata['patient_nid'] = $rdata['patient_nid'];

            $scheck = $this->M_identitas->checknik($mdata);
            if($scheck>0){
                echo "false";
            }else{
                echo "true";
            }



        }

    }



    public function checkregnas(){
        $rdata = $this->input->post();

        $fcheck = $this->M_identitas->checknik($rdata);
        if($fcheck>0){
            echo "true";
        }else{

            $mdata['patient_regnas'] = $rdata['patient_regnas'];

            $scheck = $this->M_identitas->checknik($mdata);
            if($scheck>0){
                echo "false";
            }else{
                echo "true";
            }



        }

    }

        
}