<?php
Class M_tracking extends CI_Model{
    public function __construct(){
        parent::__construct();
    }
    public function getDataList($id){
        $this->db->where("status_order_id",$id);
        $this->db->order_by("status_id","DESC");
        return $this->db->get(DB_DATA_ORDER_STATUS)->result();
    }
}