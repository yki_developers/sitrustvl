<?php
Class M_version extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function getversion(){
        return $this->db->get("hp_sys_version")->row();
    }
    
}