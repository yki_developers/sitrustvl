<?php
Class M_module extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function getParentModule($rdata){
        $this->db->where('a.role_group',$rdata);
        $this->db->where('b.module_parent','0');
        $this->db->from(DB_USER_ROLE." a");
        $this->db->join(DB_SYS_MODULE." b","a.role_module=b.module_id");
        $this->db->order_by("b.module_number","ASC");
        return $this->db->get()->result();
    }

    public function getChildModule($param){
        $this->db->where($param);
        $this->db->from(DB_USER_ROLE." a");
        $this->db->join(DB_SYS_MODULE." b","a.role_module=b.module_id");
        $this->db->order_by("b.module_number","ASC");
        return $this->db->get()->result();
    }

    public function subModule($param){
        $this->db->where($param);
        $this->db->from("hp_sys_submodule ");
        $this->db->join(DB_SYS_MODULE,"submodule_module_id=module_id");
        $this->db->order_by("module_number","ASC");
        return $this->db->get()->result();

    }
}