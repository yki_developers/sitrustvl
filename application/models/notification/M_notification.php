<?php
Class M_notification extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function insert($rdata){
        return $this->db->insert(DB_DATA_NOTIFIKASI,$rdata);
    }

    public function update($id){
        $this->db->where("notif_id",$id);
        return $this->db->update(DB_DATA_NOTIFIKASI,array("notif_read"=>"2"));
    }

    public function checkKurirId($id){
        $this->db->where("shipper_code",$id);
        $q = $this->db->get(DB_MASTER_SHIPPER);
        return $q->num_rows();
    }

    

    public function getDataList($id){
        $this->db->where("notif_recipient",$id);
        $this->db->order_by("notif_id","DESC");
        return $this->db->get(DB_DATA_NOTIFIKASI)->result();
    }

    public function getDetail($id){
        $this->db->where("notif_id",$id);
        return $this->db->get(DB_DATA_NOTIFIKASI)->row();
    }

    public function getUnread($id){
        $this->db->where("notif_recipient",$id);
        $this->db->where("notif_read","1");
        return $this->db->get(DB_DATA_NOTIFIKASI)->num_rows();
    }


    public function readAll($id){
        $this->db->where("notif_recipient",$id);
        return $this->db->update(DB_DATA_NOTIFIKASI,array("notif_read"=>"2"));
    }
    



}