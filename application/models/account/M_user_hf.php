<?php
Class M_user_hf extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function getDataList($par){
        return $this->db->get(DB_USER)->result();
    }

    public function insert($rdata){
        return $this->db->insert(DB_USER,$rdata);
    }

    public function delete($id){
        return $this->db->delete(DB_USER,array("hf_id"=>$id));
    }
}