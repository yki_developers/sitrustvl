<?php
Class M_user extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function getDataList($page,$size,$par=null){
        $start = ($page-1)*$size;
        $this->db->select("a.*");
        $this->db->select("b.group_name");
        if($par!=null){
            $this->db->where($par);
        }
        $this->db->limit($size,$start);
        $this->db->from(DB_USER." a");
        $this->db->join(DB_USER_GROUP." b","a.user_group=b.group_id");
        return $this->db->get()->result();
    }

    



    public function countAll(){
        return $this->db->count_all_results(DB_USER);
    }

    public function insert($rdata){
        return $this->db->insert(DB_USER,$rdata);
    }

    public function delete($id){
        return $this->db->delete(DB_USER,array("user_id"=>$id));
    }


    public function getDataGroupList(){
        return $this->db->get(DB_USER_GROUP)->result();
    }

    public function insertGroup($rdata){
        return $this->db->insert(DB_USER_GROUP,$rdata);
    }

    public function deleteGroup($id){
        return $this->db->delete(DB_USER_GROUP,array("group_id"=>$id));
    }

    
    public function getDataRoleList(){
        $this->db->select("a.*,b.module_name,c.group_name");
        $this->db->from(DB_USER_ROLE." a");
        $this->db->join(DB_SYS_MODULE." b");
        $this->db->join(DB_USER_GROUP." c");
        return $this->db->get()->result();
    }

    public function deleteRole($id){
        return $this->db->delete(DB_USER_ROLE,array("role_id"=>$id));
    }

    public function chPassword($rdata,$id){
        $newpasswd = md5($rdata);
        $this->db->where("user_name",$id);
        return $this->db->update(DB_USER,array("user_passwd"=>$newpasswd));
    }
    public function detail($id){
        $this->db->where("user_id",$id);
        return $this->db->get(DB_USER)->row();
    }

    public function update($rdata,$id){
        $this->db->where("user_id",$id);
        return $this->db->update(DB_USER,$rdata);
    }

    public function validate($rdata){
        $this->db->where($rdata);
        return $this->db->get(DB_USER)->row();
        
    }
    
    public function search($search){
        $this->db->like("user_name",$search);
        $this->db->or_like("user_fname",$search);
        $this->db->select("a.*");
        $this->db->select("b.group_name");
        $this->db->from(DB_USER." a");
        $this->db->join(DB_USER_GROUP." b","a.user_group=b.group_id");

        return $this->db->get()->result();

        
    }

    public function getListUserKurir($hfkode,$dcode){
        $this->db->where("user_unit",$hfkode);
        $this->db->where("user_district",$dcode);
        return $this->db->get(DB_USER)->result();
    //return $this->db->get_compiled_select(DB_USER);
    }

    

}