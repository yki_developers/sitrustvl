<?php
Class M_group extends CI_Model{

    public function __construct()
    {
        parent::__construct();
    }

    public function getDataList($par=null){
        if($par!=null){
            $this->db->where($par);
        }
        return $this->db->get(DB_USER_GROUP)->result();
    }

    public function insert($rdata){
        return $this->db->insert(DB_USER_GROUP,$rdata);
    }

    public function update($rdata,$id){
        return $this->db->update(DB_USER_GROUP,$rdata,array("group_id"=>$id));
    }

    public function delete($id){
        return $this->db->delete(DB_USER_GROUP,array("group_id"=>$id));
    }

    public function detail($id){
        $this->db->where("group_id",$id);
        return $this->db->get(DB_USER_GROUP)->row();
    }






}