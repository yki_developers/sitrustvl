<?php
Class M_shipper extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function getDataList(){
        return $this->db->get(DB_MASTER_SHIPPER)->result();
    }

    public function getDataActiveList(){
        $this->db->where("shipper_status","1");
        return $this->db->get(DB_MASTER_SHIPPER)->result();
    }

    public function insert($rdata){
        return $this->db->insert(DB_MASTER_SHIPPER,$rdata);
    }

    public function delete($id){
        return $this->db->delete(DB_MASTER_SHIPPER,array("shipper_id"=>$id));
    }

    public function update($rdata,$id){
        return $this->db->update(DB_MASTER_SHIPPER,$rdata,array("shipper_id"=>$id));
    }

    public function detail($id){
        $this->db->where("shipper_id",$id);
        return $this->db->get(DB_MASTER_SHIPPER)->row();
    }
    
}