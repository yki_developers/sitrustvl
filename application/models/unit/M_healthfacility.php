<?php
Class M_healthfacility extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function getDataList($page,$size){
        $start = ($page-1)*$size;
        $this->db->select("a.*,b.province_name,c.district_name");
        $this->db->limit($size,$start);
        $this->db->from(DB_MASTER_HEALTFACILITY." a");
        $this->db->join(DB_MASTER_PROVINCE." b","a.hf_province=b.province_code");
        $this->db->join(DB_MASTER_DISTRICT." c","a.hf_district=c.district_code");
        return $this->db->get()->result();
    }

    public function countAll(){
        return $this->db->count_all_results(DB_MASTER_HEALTFACILITY);
    }

    public function insert($rdata){
        return $this->db->insert(DB_MASTER_HEALTFACILITY,$rdata);
    }

    public function update($rdata,$id){
        $this->db->where("hf_code",$id);
        $this->db->set($rdata);
       //return $this->db->get_compiled_update(DB_MASTER_HEALTFACILITY);
        return $this->db->update(DB_MASTER_HEALTFACILITY);
    }

    public function delete($id){
        return $this->db->delete(DB_MASTER_HEALTFACILITY,array("hf_code"=>$id));
    }
    
    public function detail($id){
        $this->db->where('hf_code',$id);
        return $this->db->get(DB_MASTER_HEALTFACILITY)->row();
    }

    public function search($search){
        $this->db->like("hf_name",$search);
        $this->db->or_like("hf_code",$search);
        

        $this->db->from(DB_MASTER_HEALTFACILITY." a");
        $this->db->join(DB_MASTER_PROVINCE." b","a.hf_province=b.province_code");
        $this->db->join(DB_MASTER_DISTRICT." c","a.hf_district=c.district_code");
        return $this->db->get()->result();
    }

    public function typeofhf(){
        return $this->db->get(DB_MASTER_TYPE_HEALTFACILITY)->result();
    }

    public function getListByDistrict($id){
        $this->db->where('hf_district',$id);
        return $this->db->get(DB_MASTER_HEALTFACILITY)->result();
    }

    public function getListRefByDistrict($id,$sr){
        $this->db->where('hf_district',$id);
       $this->db->where("hf_referral","1");
        $this->db->where("hf_code!=",$sr);
        return $this->db->get(DB_MASTER_HEALTFACILITY)->result();
    }


    public function getListEid(){
    $this->db->select("hf_code as labnetwork_destination");
    $this->db->select("hf_name");
    $this->db->where("hf_eid","1");
    return $this->db->get(DB_MASTER_HEALTFACILITY)->result();
    }


}