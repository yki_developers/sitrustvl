<?php
Class M_report extends CI_Model{
    public function __construct()
    {
        parent::__construct();
        define("VIEW_VL_ARK","hp_view_result_vl");
        define("VIEW_VL_REPORT","hp_view_rekap_report_vl");
        define("VIEW_EID_REPORT","hp_view_rekap_report_eid");
        define("VIEW_VL_INT_REPORT","hp_view_internal_result_vl");
        define("VIEW_INTERNAL_MONITORING","hp_view_internal_monitoring_vl");
        define("DB_DATA_MONITORING","hp_data_rekap_monitoring");
    }

    public function getVLReport($par=null,$page=1,$group){
        
        if($par!=null){
        if($group=='8'){
            $this->db->where_in("hf_province",$par);
        }else{
            $this->db->where($par);
        }
        }
        $start = ($page-1)*100;
        $this->db->limit(100,$start);
      //  return $this->db->get_compiled_select();
        return $this->db->get(VIEW_VL_REPORT)->result();
    }


    public function searchVLreport($par=null,$group,$search){
        if($par!=null){
        if($group=='8'){
            $this->db->where_in("hf_province",$par);
        }else{
            $this->db->where($par);
        }
    }
        
        $this->db->group_start();
        $this->db->like("SUBSTRING_INDEX(order_number,'/',-1)",$search,"none");
        $this->db->or_like("patient_name",$search);
        $this->db->or_like("patient_regnas",$search,"none");
        $this->db->or_like("specimen_id",$search,"none");
        $this->db->group_end();

        return $this->db->get(VIEW_VL_REPORT)->result();
    }


    public function getVLReportXLS($par=null,$group){
        
        if($par!=null){
        if($group=='8'){
            $this->db->where_in("hf_province",$par);
        }else{
            $this->db->where($par);
        }
        }
      //  return $par." ".$group;
      //return $this->db->get_compiled_select(VIEW_VL_REPORT);
       return $this->db->get(VIEW_VL_REPORT)->result();
    }


    public function getTotalReport($par=null,$group){
        if($par!=null){
            if($group=='8'){
                $this->db->where_in("hf_province",$par);
            }else{
                $this->db->where($par);
            }
            }
            return $this->db->count_all_results(VIEW_VL_REPORT);


    }

    public function getVLReportARK($par=null,$group){
        if($par!=null){
            if($group=='8'){
                $this->db->where_in("hf_province",$par);
            }else{
                $this->db->where($par);
            }
        }
        $this->db->where("vl_result_vlresult_optional<","5");
      //  return $this->db->get_compiled_select();
        return $this->db->get(VIEW_VL_REPORT)->result();
    }

    public function getInternalReport($par=null){
        if($par!=null){
            $this->db->where($par);
        }
      //  return $this->db->get_compiled_select();
        return $this->db->get(VIEW_VL_INT_REPORT)->result();
    }

    public function getInternalReportTOFA($par=null){
        if($par!=null){
            $this->db->where_in("hf_province",$par);
        }
      //  return $this->db->get_compiled_select();
        return $this->db->get(VIEW_VL_INT_REPORT)->result();
    }


    public function getInternalMonitoring($par=null,$page,$size){
        $start = ($page-1)*$size;
        if($par!=null){
            $this->db->where($par);
        }
        $this->db->limit($size,$start);
        //  return $this->db->get_compiled_select();
        return $this->db->get(VIEW_INTERNAL_MONITORING)->result();
    }

    public function getInternalMonitoringTOFA($par=null,$page,$size){
        $start = ($page-1)*$size;
        if($par!=null){
            $this->db->where_in("hf_province",$par);
        }
        $this->db->limit($size,$start);
        //  return $this->db->get_compiled_select();
        return $this->db->get(VIEW_INTERNAL_MONITORING)->result();
    }

    public function getInternalMonitoringTOFAXLS($par=null){
       
        if($par!=null){
            $this->db->where_in("hf_province",$par);
        }
        //  return $this->db->get_compiled_select();
        return $this->db->get(VIEW_INTERNAL_MONITORING)->result();
    }


    public function getInternalMonitoringXLS($par=null){
       
        if($par!=null){
            $this->db->where($par);
        }
        //  return $this->db->get_compiled_select();
        return $this->db->get(VIEW_INTERNAL_MONITORING)->result();
    }


    public function countMonInternal($par){
        if($par!=null){
            $this->db->where($par);
        }
      
        //  return $this->db->get_compiled_select();
        //return $this->db->get(VIEW_INTERNAL_MONITORING)->result();

        
        return $this->db->count_all_results(VIEW_INTERNAL_MONITORING);
    }


    public function getEIDReport($par=null,$page,$group){
        if($par!=null){
            if($group=='8'){
                $this->db->where_in("hf_province",$par);
            }else{
                $this->db->where($par);
            }
            }
            $start = ($page-1)*100;
            $this->db->limit(100,$start);
        return $this->db->get(VIEW_EID_REPORT)->result();
    }


    public function getEIDReportXLS($par=null,$group){
        
        if($par!=null){
        if($group=='8'){
            $this->db->where_in("hf_province",$par);
        }else{
            $this->db->where($par);
        }
        }
      //  return $this->db->get_compiled_select();
      return $this->db->get(VIEW_EID_REPORT)->result();
    }



    public function getTotalEidReport($par=null,$group){
        if($par!=null){
            if($group=='8'){
                $this->db->where_in("hf_province",$par);
            }else{
                $this->db->where($par);
            }
            }
            return $this->db->count_all_results(VIEW_EID_REPORT);


    }


    

    public function getMonitoringOrder($id,$page=1,$size){
        $start = ($page-1)*$size;

        $this->db->select("a.*,b.*");
        $this->db->select("'External' as order_type");
        $this->db->select("c.hf_name as faskes_penerima");
        $this->db->select("e.hf_name as faskes_pengirim");
        $this->db->select("c.hf_province as hf_province");
        $this->db->select("c.hf_district as hf_district");
        $this->db->select("d.shipper_name");
        if($id!=null){
        $this->db->where($id);
        }
        //$this->db->where("order_approved!=",'2');
        $this->db->order_by("a.order_id","DESC");
        $this->db->limit($size,$start);
        $this->db->from(DB_DATA_ORDER." a");
        $this->db->join("hp_view_total_specimen b","a.order_id=b.specimen_order_id","left");
        $this->db->join(DB_MASTER_HEALTFACILITY." c","a.order_hf_recipient=c.hf_code");
        $this->db->join(DB_MASTER_SHIPPER." d","a.order_shipper_id=d.shipper_code");
        $this->db->join(DB_MASTER_HEALTFACILITY." e","a.order_hf_sender=e.hf_code");
       return $this->db->get()->result();
       //return $this->db->get_compiled_select();
    }


    


    private function InsertMonitoringOrderInternal($id,$group){
       

        if($id!=null){
            if($group!="8"){
            $this->db->where($id);
            }else{
                $this->db->where_in("hf_province",$id);
            }

            }
            $q = $this->db->get_compiled_select("hp_view_rekap_monitoring_internal");  
        return $this->db->query("REPLACE into ".DB_DATA_MONITORING." (".$q.")");
            
    }

    private function InsertMonitoringOrderEksternal($id,$group){
       

        if($id!=null){
            if($group!="8"){
                $this->db->where($id);
                }else{
                    $this->db->where_in("hf_province",$id);
                }
            }
            $q = $this->db->get_compiled_select("hp_view_rekap_monitoring_eksternal");  
        return $this->db->query("REPLACE into ".DB_DATA_MONITORING." (".$q.")");
            
    }

    
    private function deleteMonitoring($id,$group){
        if($id!=null){
            if($group!="8")
            {
                if($group!='2'){
            $this->db->where($id);
                }
            }else{
                $this->db->where_in("hf_province",$id);
            }

            }
            return $this->db->delete(DB_DATA_MONITORING);

    }

    public function monitoringOrderForTOFA($id){
        $this->db->where_in("hf_province",$id);
        $int=$this->db->get_compiled_select("hp_view_rekap_monitoring_internal"); 


        $this->db->where_in("hf_province",$id);
        $ext=$this->db->get_compiled_select("hp_view_rekap_monitoring_eksternal"); 
        return $this->db->query("REPLACE into ".DB_DATA_MONITORING." ".$int." union ".$ext);
    }


    public function getMonitoringMerge($id,$page=1,$group,$kategori){
        $start = ($page-1)*100;
    
//if($this->deleteMonitoring($id,$group)){


        if($this->InsertMonitoringOrderInternal($id,$group)){
            if($this->InsertMonitoringOrderEksternal($id,$group)){
       
        if($id!=null){
        if($group!="8"){
            $this->db->where($id);
        }else{
            $this->db->where_in("hf_province",$id);
        }
            
        }
           $this->db->order_by("order_id","DESC");
            $this->db->limit(100,$start);
            $this->db->where("order_kategori",$kategori);
            return $this->db->get(DB_DATA_MONITORING)->result();
        }
    }
//}


    }

    public function getMonitoringMergeXLS($id,$group,$kategori){
      //  $start = ($page-1)*100;
    
     // if($this->deleteMonitoring($id,$group)){
        if($this->InsertMonitoringOrderInternal($id,$group)){
            if($this->InsertMonitoringOrderEksternal($id,$group)){
       
        if($id!=null){
        if($group!="8"){
            $this->db->where($id);
        }else{
            $this->db->where_in("hf_province",$id);
        }
            
        }
           // $this->db->order_by("order_id","DESC");
           // $this->db->limit(100,$start);
           $this->db->where("order_kategori",$kategori);
            return $this->db->get(DB_DATA_MONITORING)->result();
        }
    }
//}
    }


    public function getMonitoringOrderTOFA($id,$page=1,$size){
        $start = ($page-1)*$size;
        
            if($this->monitoringOrderForTOFA($id)){
        $this->db->where_in("hf_province",$id);
        $this->db->limit($size,$start);
        return $this->db->get(DB_DATA_MONITORING)->result();
            }
        
       //return $this->db->get_compiled_select();
    }


    public function getMonitoringOrderTOFAXLS($id,$page=1){
        $start = ($page-1)*100;

      
        $this->db->where_in("hf_province",$id);
    
       // $this->db->limit(100,$start);
        return $this->db->get(DB_DATA_MONITORING)->result();
       //return $this->db->get_compiled_select();
    }


    public function getMonitoringOrderXLS($id=null,$page=1){
        $start = ($page-1)*100;
            if($id!=null){
                $this->db->where($id);
                }
              //  $this->db->limit(100,$start);
            return $this->db->get(DB_DATA_MONITORING)->result();
        
    }


    public function countAll($id,$group,$kategori){
        
        if($id!=null){
            if($group!="8"){
                $this->db->where($id);
            }else{
                $this->db->where_in("hf_province",$id);
            }
                
            }
            $this->db->where("order_kategori",$kategori);
        return $this->db->count_all_results(DB_DATA_MONITORING);
    }

    public function search($search,$lev=null,$group){
        //$start = ($page-1)*$size;

        $this->db->select("a.*");
        $this->db->select("c.hf_name as faskes_penerima");
        $this->db->select("e.hf_name as faskes_pengirim");
        $this->db->group_start();
        $this->db->like("a.order_number",$search,'before');
        $this->db->or_like("c.hf_name",$search);
        $this->db->or_like("e.hf_name",$search);
        $this->db->or_like("a.order_type",$search,'none');
        //$this->db->where("a.order_approved!='2'");
     $this->db->group_end();
     if($group!='8'){
     if($lev!=null){
         $this->db->where($lev);
     }
     }else{
        $this->db->where_in("e.hf_province",$lev);

    }
    
       
        $this->db->from(DB_DATA_MONITORING." a");
        //$this->db->join("hp_view_total_specimen b","a.order_id=b.specimen_order_id","left");
        $this->db->join(DB_MASTER_HEALTFACILITY." c","a.order_hf_recipient=c.hf_code");
        $this->db->join(DB_MASTER_HEALTFACILITY." e","a.order_hf_sender=e.hf_code");
       //return $this->db->get_compiled_select();
         return $this->db->get()->result();
    }


    public function getMonitoringSpecimen($id){
        $this->db->select("*");
        $this->db->where("specimen_order_id",$id);
        $this->db->from(DB_DATA_PATIENT);
        $this->db->join(DB_DATA_SPECIMEN,"specimen_patient_id=patient_id");
        $this->db->join(DB_DATA_ORDER,"specimen_order_id=order_id");
        return $this->db->get()->result();
    }

    public function getResultVl($numid){
        $this->db->select("*");
        $this->db->where("vl_result_specimen_num_id",$numid);
        return $this->db->get(DB_DATA_VLRESULT)->row();
    }


    

    public function getTofaArea($user){
        //$this->db->select(" CONCAT('\'',GROUP_CONCAT(wa_province SEPARATOR '\',\''),'\'') AS province");
        $this->db->where("wa_user_name",$user);
        return $this->db->get("hp_user_wa_tofa")->result();
    }

    public function getMonitoringHasilInternal($par=null,$group,$category){
         
        if($par!=null){
            if($group=='8'){
                $this->db->where_in("hf_province",$par);
            }else{
                $this->db->where($par);
            }
            }

        if($category=='1'){
            $this->db->select("*");
            return $this->db->get_compiled_select("hp_view_monitoring_status_hasil_internal_eid");
        }else{
            $this->db->select("*");
            return $this->db->get_compiled_select("hp_view_monitoring_status_hasil_internal_vl");
        }
    }

    public function getMonitoringHasilEksternal($par=null,$group,$category){
         
        if($par!=null){
            if($group=='8'){
                $this->db->where_in("hf_province",$par);
            }else{
                $this->db->where($par);
            }
            }

        if($category=='1'){
            $this->db->select("*");
            return $this->db->get_compiled_select("hp_view_monitoring_status_hasil_eksternal_eid");
        }else{
            $this->db->select("*");
            return $this->db->get_compiled_select("hp_view_monitoring_status_hasil_eksternal_vl");
        }
    }


    public function getMonitoringHasil($par=null,$group,$category){
         
        if($par!=null){
            if($group=='8'){
                $this->db->where_in("hf_province",$par);
            }else{
                $this->db->where($par);
            }
            }
        $int = $this->getMonitoringHasilInternal($par,$group,$category);
        $eks = $this->getMonitoringHasilEksternal($par,$group,$category);
        return $this->db->query($int." UNION ALL ".$eks)->result();
    }


}
