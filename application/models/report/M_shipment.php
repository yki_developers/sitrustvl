<?php
Class M_shipment extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function getShipmentReport($par=null,$page){
        if($par!=null){
            $this->db->where_in("hf_province",$par);
        }

        $start = ($page-1)*100;
        $this->db->limit(100,$start);
      //  return $this->db->get_compiled_select();
        return $this->db->get(DB_VIEW_SHIPMENT_MONITORING)->result();
    }


    public function getTotalReport($par=null){
        if($par!=null){
            $this->db->where_in("hf_province",$par);
        }
        return $this->db->count_all_results(DB_VIEW_SHIPMENT_MONITORING);
    }



    public function getShipmentReportXLS($par=null){
        if($par!=null){
            $this->db->where_in("hf_province",$par);
        }
      //  return $this->db->get_compiled_select();
        return $this->db->get(DB_VIEW_SHIPMENT_MONITORING)->result();
    }


}