<?php
Class M_kurir extends CI_Model{
public function __construct(){
    parent::__construct();
}
public function getListKurir($hfcode){
   $this->db->where("hf_code",$hfcode);
   return $this->db->get(DB_VIEW_NETWORK_KURIR)->result();
}

public function getNamaKurir($kabKota){
    $this->db->where("user_district",$kabKota);
    $this->db->where("user_group","7");
    return $this->db->get(DB_USER)->result();
}

public function insert($rdata){
    return $this->db->insert(DB_KURIR_NETWORK,$rdata);
}

public function delete($rdata){
    $this->db->where($rdata);
    return $this->db->delete(DB_KURIR_NETWORK);
}


}