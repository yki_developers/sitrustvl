<?php
Class M_laboratorium extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function getDataList($page){
     $start = ($page-1)*50;


     if($this->session->userdata("user_group")=='3'){

        $this->db->where("b.hf_province",$this->session->userdata("user_province"));
    }elseif($this->session->userdata("user_group")=='4'){
        $this->db->where("b.hf_district",$this->session->userdata("user_district"));
        
    }
        $this->db->select("b.hf_code");
        $this->db->select("b.hf_name as origin_name");
        $this->db->select("c.hf_name  as destination_name");
        $this->db->select("c.hf_code as destination_code");
        $this->db->from(DB_DATA_LABNETWORK." a");
        $this->db->join(DB_MASTER_HEALTFACILITY." b","a.labnetwork_origin=b.hf_code");
        $this->db->join(DB_MASTER_HEALTFACILITY." c","a.labnetwork_destination=c.hf_code");
        $this->db->limit(50,$start);
       // $this->db->group_by("b.hf_code,b.hf_name");
        return $this->db->get()->result();
    }


    public function search($search){
        $this->db->select("b.hf_code");
        $this->db->select("b.hf_name as origin_name");
        $this->db->select("GROUP_CONCAT(c.hf_name SEPARATOR '<br>') as destination_name");
        $this->db->like("b.hf_code",$search,'none');
        $this->db->or_like("b.hf_name",$search);
        $this->db->or_like("c.hf_name",$search);
        $this->db->from(DB_DATA_LABNETWORK." a");
        $this->db->join(DB_MASTER_HEALTFACILITY." b","a.labnetwork_origin=b.hf_code");
        $this->db->join(DB_MASTER_HEALTFACILITY." c","a.labnetwork_destination=c.hf_code");
        $this->db->group_by("b.hf_code,b.hf_name");
        return $this->db->get()->result();
    }
    public function getNetwork($id,$page){
        $start = ($page-1)*50;
        $this->db->select("b.hf_code");
        $this->db->select("b.hf_name as origin_name");
        $this->db->select("GROUP_CONCAT(c.hf_name SEPARATOR '<br>') as destination_name");
        $this->db->from(DB_DATA_LABNETWORK." a");
        $this->db->where('a.labnetwork_origin',$id);
        $this->db->join(DB_MASTER_HEALTFACILITY." b","a.labnetwork_origin=b.hf_code");
        $this->db->join(DB_MASTER_HEALTFACILITY." c","a.labnetwork_destination=c.hf_code");
        $this->db->limit(50,$start);
        $this->db->group_by("b.hf_code,b.hf_name");
        return $this->db->get()->result();
    }


    public function countUnit($id){
        $this->db->select("b.hf_code");
        $this->db->select("b.hf_name as origin_name");
        $this->db->select("GROUP_CONCAT(c.hf_name SEPARATOR '<br>') as destination_name");
        $this->db->from(DB_DATA_LABNETWORK." a");
        $this->db->where('a.labnetwork_origin',$id);
        $this->db->join(DB_MASTER_HEALTFACILITY." b","a.labnetwork_origin=b.hf_code");
        $this->db->join(DB_MASTER_HEALTFACILITY." c","a.labnetwork_destination=c.hf_code");
        $this->db->group_by("b.hf_code,b.hf_name");
        return $this->db->count_all_results();

    }
    public function countAll(){
        return $this->db->count_all_results(DB_DATA_LABNETWORK);
    }

    public function insert($rdata){
        return $this->db->insert(DB_DATA_LABNETWORK,$rdata);
    }

    public function delete($id){
        $exp = explode("_",$id);
        $this->db->where("labnetwork_origin",$exp[0]);
        $this->db->where("labnetwork_destination",$exp[1]);
        return $this->db->delete(DB_DATA_LABNETWORK);
    }


    public function oriname($id){
        $this->db->where("hf_code",$id);
        return $this->db->get(DB_MASTER_HEALTFACILITY)->row();
    }

    public function hfList($district){
        $this->db->where("hf_district",$district);
        //$this->db->where("hf_referral","1");
        return $this->db->get(DB_MASTER_HEALTFACILITY)->result();
    }

    public function getDataNetwork($id){
        $this->db->select("*");
        $this->db->select("hf_name");
        $this->db->select("province_name");
        $this->db->select("district_name");
        $this->db->where("labnetwork_origin",$id);
        $this->db->from(DB_DATA_LABNETWORK);
        $this->db->join(DB_MASTER_HEALTFACILITY,"labnetwork_destination=hf_code");
        $this->db->join(DB_MASTER_PROVINCE,"hf_province=province_code");
        $this->db->join(DB_MASTER_DISTRICT,"hf_district=district_code");
        return $this->db->get()->result();
    }


}