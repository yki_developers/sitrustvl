<?php
Class M_chart_eid extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

   




    public function getTrendNasional($per,$p_start,$p_end){
        if($per<=31){
            $this->db->select("date_format(chart_order_date,'%d %b %Y') as label");
            $this->db->select_sum("chart_terkirim","value");
            $this->db->group_by("chart_order_date");
        }else{
            $this->db->select("concat('Minggu-',chart_order_week,' ',chart_order_year) as label");
            $this->db->select_sum("chart_terkirim","value");
            $this->db->group_by("chart_order_week");
        }
        $this->db->order_by("chart_thbl","ASC");
        $this->db->where("chart_order_date between '$p_start' and '$p_end'");
        return $this->db->get(DBVIEW_CHART_TRENDLINE_EID)->result();

    }

    public function getTrendPropinsi($propinsi,$per,$p_start,$p_end){
        if($per<=31){
            $this->db->select("date_format(chart_order_date,'%d %b %Y') as label");
            $this->db->select_sum("chart_terkirim","value");
            $this->db->group_by("chart_order_date");
        }else{
            $this->db->select("concat('Minggu-',chart_order_week,' ',chart_order_year) as label");
            $this->db->select_sum("chart_terkirim","value");
            $this->db->group_by("chart_order_week");
        }
        $this->db->where("chart_order_date between '$p_start' and '$p_end'");
        $this->db->where("chart_province",$propinsi);
        $this->db->order_by("chart_thbl","ASC");
        return $this->db->get(DBVIEW_CHART_TRENDLINE_EID)->result();


    }



    public function getTrendKabupaten($kabupaten,$per,$p_start,$p_end){
        if($per<=31){
            $this->db->select("date_format(chart_order_date,'%d %b %Y') as label");
            $this->db->select_sum("chart_terkirim","value");
            $this->db->group_by("chart_order_date");
        }else{
            $this->db->select("concat('Minggu-',chart_order_week,' ',chart_order_year) as label");
            $this->db->select_sum("chart_terkirim","value");
            $this->db->group_by("chart_order_week");
        }
        $this->db->where("chart_order_date between '$p_start' and '$p_end'");
        $this->db->where("chart_district",$kabupaten);
        $this->db->order_by("chart_thbl","ASC");
        return $this->db->get(DBVIEW_CHART_TRENDLINE_EID)->result();


    }



    
    public function getTrendFasyankes($fasyankes,$per,$p_start,$p_end){
        if($per<=31){
            $this->db->select("date_format(chart_order_date,'%d %b %Y') as label");
            $this->db->select_sum("chart_terkirim","value");
            $this->db->group_by("chart_order_date");
        }else{
            $this->db->select("concat('Minggu-',chart_order_week,' ',chart_order_year) as label");
            $this->db->select_sum("chart_terkirim","value");
            $this->db->group_by("chart_order_week");
        }
        $this->db->where("chart_order_date between '$p_start' and '$p_end'");
        $this->db->where("chart_hf_sender",$fasyankes);
        $this->db->order_by("chart_thbl","ASC");
        return $this->db->get(DBVIEW_CHART_TRENDLINE_EID)->result();


    }

    


    public function getKondisiSpesimenNasional($p_start,$p_end,$kategori='1'){
        $this->db->select_sum("dash_goodcondition","db_spc_good");
        $this->db->select_sum("dash_notgood","db_spc_broken");
        $this->db->select_sum("dash_total_specimen","total_specimen");
        $this->db->where("dash_specimen_exam_category",$kategori);
        $this->db->where("dash_order_date between '$p_start' and '$p_end'");
        $this->db->where("dash_order_status>='5'");
        return $this->db->get(DBVIEW_CHART_SPECIMEN_TERKIRIM)->row();
    }

    public function getKondisiSpesimenPropinsi($prop,$p_start,$p_end,$kategori='1'){
        $this->db->select_sum("dash_goodcondition","db_spc_good");
        $this->db->select_sum("dash_notgood","db_spc_broken");
        $this->db->select_sum("dash_total_specimen","total_specimen");
        $this->db->where("dash_specimen_exam_category",$kategori);
        $this->db->where("dash_order_date between '$p_start' and '$p_end'");
        $this->db->where("dash_order_status>='5'");
        $this->db->where("dash_province",$prop);
        $this->db->group_by("dash_province");
        return $this->db->get(DBVIEW_CHART_SPECIMEN_TERKIRIM)->row();
    }

    public function getKondisiSpesimenKabupaten($kab,$p_start,$p_end,$kategori='1'){
        $this->db->select_sum("dash_goodcondition","db_spc_good");
        $this->db->select_sum("dash_notgood","db_spc_broken");
        $this->db->select_sum("dash_total_specimen","total_specimen");
        $this->db->where("dash_specimen_exam_category",$kategori);
        $this->db->where("dash_order_date between '$p_start' and '$p_end'");
        $this->db->where("dash_order_status>='5'");
        $this->db->where("dash_district",$kab);
        $this->db->group_by("dash_district");
        return $this->db->get(DBVIEW_CHART_SPECIMEN_TERKIRIM)->row();
    }


    public function getKondisiSpesimenFaskes($faskes,$p_start,$p_end,$kategori='1'){
        $this->db->select_sum("dash_goodcondition","db_spc_good");
        $this->db->select_sum("dash_notgood","db_spc_broken");
        $this->db->select_sum("dash_total_specimen","total_specimen");
        $this->db->where("dash_specimen_exam_category",$kategori);
        $this->db->where("dash_order_date between '$p_start' and '$p_end'");
        $this->db->where("dash_order_status>='5'");
        $this->db->where("dash_hf_sender",$faskes);
        $this->db->group_by("dash_hf_sender");
        return $this->db->get(DBVIEW_CHART_SPECIMEN_TERKIRIM)->row();
    }





    public function getListPengirimanNasional($p_start,$p_end,$kategori='1'){
        $this->db->select("dash_province_name as label");
        $this->db->select_sum("dash_total_specimen","value");
        $this->db->select("CONCAT('newchart-jsonurl-','".base_url()."dashboard/chart_eid/chart_pengiriman_regional/propinsi/',dash_province,'/','$p_start','/','$p_end') as link");

        $this->db->where("dash_order_date between '$p_start' AND '$p_end'");
        $this->db->where("dash_specimen_exam_category",$kategori);
        $this->db->where("dash_order_status>='3'");
        $this->db->order_by("sum(dash_total_specimen)","DESC");
        $this->db->group_by("dash_province");
        return $this->db->get(DBVIEW_CHART_SPECIMEN_TERKIRIM)->result();
    }

    public function getListPengirimanPropinsi($kode,$p_start,$p_end,$kategori='1'){
        $this->db->select("dash_district_name as label");
        $this->db->select_sum("dash_total_specimen","value");
        $this->db->select("CONCAT('newchart-jsonurl-','".base_url()."dashboard/chart_eid/chart_pengiriman_regional/kabupaten/',dash_district,'/','$p_start','/','$p_end') as link");

        $this->db->where("dash_order_date between '$p_start' AND '$p_end'");
        $this->db->where("dash_specimen_exam_category",$kategori);
        $this->db->where("dash_order_status>='3'");
        $this->db->where("dash_province",$kode);
        $this->db->order_by("sum(dash_total_specimen)","DESC");
        $this->db->group_by("dash_district");
        return $this->db->get(DBVIEW_CHART_SPECIMEN_TERKIRIM)->result();
    }



    public function getListPengirimanKabupaten($kode,$p_start,$p_end,$kategori='1'){
        $this->db->select("dash_hf_name as label");
        $this->db->select_sum("dash_total_specimen","value");
        $this->db->where("dash_order_date between '$p_start' AND '$p_end'");
        $this->db->where("dash_specimen_exam_category",$kategori);
        $this->db->where("dash_order_status>='3'");
        $this->db->where("dash_district",$kode);
        $this->db->order_by("sum(dash_total_specimen)","DESC");
        $this->db->group_by("dash_hf_sender");
        return $this->db->get(DBVIEW_CHART_SPECIMEN_TERKIRIM)->result();
    }




}