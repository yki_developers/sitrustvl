<?php
Class M_dashboard extends CI_Model{
    public function __construct()
    {
        parent::__construct();
        define("DB_DASHBOARD_SPECIMEN","hp_view_dashboard_specimen");
    }

    public function getDataSpecimen($id){
        $this->db->where("order_hf_sender",$id);
    return $this->db->get(DB_DASHBOARD_SPECIMEN)->row();
    }

  public function getSpecimenDikirimNasional($rdata=null,$p_start,$p_end){
      
   $this->db->select_sum("dash_specimen_dikirim","specimen_dikirim");
   $this->db->select_sum("dash_specimen_diterima","specimen_diterima");
   if($rdata!=null){
       $this->db->where($rdata);
   } 
   
   $this->db->where("dash_order_date between '$p_start' and '$p_end'");
      return $this->db->get(DBVIEW_DASHBOARD_STATUS_DETAIL,$rdata)->row();
  }

  public function getSpecimenHasilNasional($rdata=null,$p_start,$p_end){
      
    $this->db->select_sum("dash_total","specimen_adahasil");
    $this->db->select_sum("dash_tersupresi","specimen_tersupresi");
    if($rdata!=null){
        $this->db->where($rdata);
    } 
    
    $this->db->where("dash_order_date between '$p_start' and '$p_end'");
       return $this->db->get(DBVIEW_DASHBOARD_HASIL_DETAIL,$rdata)->row();
   }

   public function getTATNasional($rdata=null,$p_start,$p_end){
      
    $this->db->select("avg(dash_perspecimen) as dash_total,count(dash_specimen_num_id) as specimen");
    if($rdata!=null){
        $this->db->where($rdata);
    } 
    
    $this->db->where("dash_order_date between '$p_start' and '$p_end'");
       return $this->db->get(DBVIEW_DASHBOARD_TAT_VL,$rdata)->row();
   }














   public function getSpecimenDikirimNasionalEID($rdata=null,$p_start,$p_end){
      
    $this->db->select_sum("dash_specimen_dikirim","eid_dikirim");
    $this->db->select_sum("dash_specimen_diterima","eid_diterima");
    if($rdata!=null){
        $this->db->where($rdata);
    } 
    
    $this->db->where("dash_order_date between '$p_start' and '$p_end'");
       return $this->db->get(DBVIEW_DASHBOARD_EID_STATUS_DETAIL,$rdata)->row();
   }
 
   public function getSpecimenHasilNasionalEID($rdata=null,$p_start,$p_end){
       
     $this->db->select_sum("dash_total","eid_adahasil");
     $this->db->select_sum("dash_detected","eid_detected");
     if($rdata!=null){
         $this->db->where($rdata);
     } 
     
     $this->db->where("dash_order_date between '$p_start' and '$p_end'");
        return $this->db->get(DBVIEW_DASHBOARD_EID_HASIL_DETAIL,$rdata)->row();
    }
 
    public function getTATNasionalEID($rdata=null,$p_start,$p_end){
       
     $this->db->select("avg(dash_perspecimen) as dash_total,count(dash_specimen_num_id) as specimen");
     if($rdata!=null){
         $this->db->where($rdata);
     } 
     
     $this->db->where("dash_order_date between '$p_start' and '$p_end'");
        return $this->db->get(DBVIEW_DASHBOARD_EID_TAT,$rdata)->row();
    }









   public function getChartPeriode($p_start,$p_end){
       $this->db->select("DATEDIFF('$p_end','$p_start') as hari");
       $this->db->select("'$p_start' as start_date");
       $this->db->select("'$p_end' as end_date");
       return $this->db->get()->row();
   }


}