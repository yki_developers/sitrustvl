<?php
Class M_reverse extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function getBrokenCondition($order_id){
        $this->db->where("specimen_order_id",$order_id);
        return $this->db->get(DB_VIEW_BROKEN_CONDITION)->result();
    }
    public function hfName($id){
        $this->db->where("hf_code",$id);
        return $this->db->get(DB_MASTER_HEALTFACILITY)->row();
    }

    public function provName($id){
        $this->db->where("province_code",$id);
        return $this->db->get(DB_MASTER_PROVINCE)->row();
    }

    public function distName($id){
        $this->db->where("district_code",$id);
        return $this->db->get(DB_MASTER_DISTRICT)->row();
    }

    public function shipperName($id){
        $this->db->where("shipper_code",$id);
        return $this->db->get(DB_MASTER_SHIPPER)->row();
    }

    public function getPlayerId($hfcode){
        $this->db->where("user_unit",$hfcode);
        return $this->db->get(DB_USER)->result();
    }

    public function getWebPlayerId($code){
        $this->db->where($code);
        return $this->db->get(DB_USER)->result();

    }

    public function getAge($date1,$date2){
        $this->db->select("TIMESTAMPDIFF(YEAR,'".$date1."','".$date2."') AS thn, TIMESTAMPDIFF(MONTH,'".$date1."','".$date2."')%12 AS bln");
        return $this->db->get()->row();
    }
}