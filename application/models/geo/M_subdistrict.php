<?php
Class M_subdistrict extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function getDataList($page,$size){
        $this->db->select("a.*,b.province_name,c.district_name");
        $start = ($page-1)*$size;
        $this->db->limit($size,$start);
        $this->db->from(DB_MASTER_SUBDISTRICT." a");
        $this->db->join(DB_MASTER_PROVINCE." b","a.subdistrict_province=b.province_code");
        $this->db->join(DB_MASTER_DISTRICT." c","a.subdistrict_district=c.district_code");
        return $this->db->get()->result();
    }

    public function countAll(){
        return $this->db->count_all_results(DB_MASTER_SUBDISTRICT);
    }

    public function insert($rdata){
        return $this->db->insert(DB_MASTER_SUBDISTRICT,$rdata);
    }

    public function update($rdata,$id){
        return $this->db->update(DB_MASTER_SUBDISTRICT,$rdata,array("subdistrict_code"=>$id));
    }

    public function delete($id){
        return $this->db->delete(DB_MASTER_SUBDISTRICT,array("subdistrict_code"=>$id));
    }

    public function search($search){
        $this->db->select("a.*,b.province_name,c.district_name");
        $this->db->limit('50','0');
        $this->db->like("a.subdistrict_name",$search);
        $this->db->from(DB_MASTER_SUBDISTRICT." a");
        $this->db->join(DB_MASTER_PROVINCE." b","a.subdistrict_province=b.province_code");
        $this->db->join(DB_MASTER_DISTRICT." c","a.subdistrict_district=c.district_code");
        return $this->db->get()->result();
    }
    

    public function getDetail($id){
        $this->db->select("*");
        $this->db->where("subdistrict_code",$id);
        return $this->db->get(DB_MASTER_SUBDISTRICT)->row();
    }

    public function getByDistrict($id){
        $this->db->where("subdistrict_district",$id);
        return $this->db->get(DB_MASTER_SUBDISTRICT)->result();
    }
}