<?php
Class M_district extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function getDataList($page,$size){
        $this->db->select("a.*,b.province_name");
        $start = ($page-1)*$size;
        $this->db->limit($size,$start);
        $this->db->from(DB_MASTER_DISTRICT." a");
        $this->db->join(DB_MASTER_PROVINCE." b","a.district_province=b.province_code");
        return $this->db->get()->result();
    }

    public function countAll(){
        return $this->db->count_all_results(DB_MASTER_DISTRICT);
    }

    public function insert($rdata){
        return $this->db->insert(DB_MASTER_DISTRICT,$rdata);
    }

    public function update($rdata,$id){
        return $this->db->update(DB_MASTER_DISTRICT,$rdata,array("district_code"=>$id));
    }

    public function delete($id){
        return $this->db->delete(DB_MASTER_DISTRICT,array("district_code"=>$id));
    }

    public function search($search){
        $this->db->select("a.*,b.province_name");
        $this->db->limit('50','0');
        $this->db->like("a.district_name",$search);
        $this->db->from(DB_MASTER_DISTRICT." a");
        $this->db->join(DB_MASTER_PROVINCE." b","a.district_province=b.province_code");
        return $this->db->get()->result();
    }

    public function getDetail($id){
        $this->db->select("*");
        $this->db->where("district_code",$id);
        return $this->db->get(DB_MASTER_DISTRICT)->row();
    }

    public function getDistrictByProvince($id){
        $this->db->where("district_province",$id);
        return $this->db->get(DB_MASTER_DISTRICT)->result();
    }
}