<?php
Class M_province extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function getList($prov=null){
        if($prov!=null){
            $this->db->where("province_code",$prov);
        }
        return $this->db->get(DB_MASTER_PROVINCE)->result();
    }

    public function insert($rdata){
        return $this->db->insert(DB_MASTER_PROVINCE,$rdata);
    }

    public function delete($rdata){
        return $this->db->delete(DB_MASTER_PROVINCE,array("province_code"=>$rdata));
    }

    public function update($rdata,$id){
        //return $this->db->get_compiled_update(DB_MASTER_PROVINCE,$rdata,array('province_code'=>$id));
        return $this->db->update(DB_MASTER_PROVINCE,$rdata,array('province_code'=>$id));
    }

    public function search($search){
        $this->db->like('province_name',$search);
        $this->db->or_like('province_name',$search);
        return $this->db->get(DB_MASTER_PROVINCE)->result();
    }
}