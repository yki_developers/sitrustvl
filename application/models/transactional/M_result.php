<?php
Class M_result extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function getDataList($id,$kat='2'){
        $this->db->select("a.*,b.*");
        $this->db->select("d.hf_name as faskes_pengirim");
        $this->db->select("e.hf_name as faskes_penerima");
        $this->db->select("f.shipper_name");
        $this->db->where("a.order_hf_recipient",$id);
        $this->db->where("a.order_approved","1");
        $this->db->where("a.order_status","5");
        $this->db->where("a.order_kategori",$kat);
        $this->db->order_by("a.order_id","DESC");
        $this->db->from(DB_DATA_ORDER." a");

        $this->db->join(DB_MASTER_HEALTFACILITY." d","a.order_hf_sender=d.hf_code");
        $this->db->join(DB_MASTER_HEALTFACILITY." e","a.order_hf_recipient=e.hf_code");
        $this->db->join(DB_MASTER_SHIPPER." f","a.order_shipper_id=f.shipper_code");
        $this->db->join("hp_view_total_specimen b","a.order_id=b.specimen_order_id","left");
        return $this->db->get()->result();
    }

    public function searchDataList($id,$rdata){
        $this->db->select("a.*,b.*");
        $this->db->select("d.hf_name as faskes_pengirim");
        $this->db->select("e.hf_name as faskes_penerima");
        $this->db->select("f.shipper_name");
        $this->db->group_start();
        $this->db->where("a.order_hf_recipient",$id);
        $this->db->where("a.order_approved","1");
        $this->db->where("a.order_status","5");
        $this->db->group_end();
        $this->db->like("a.order_number",$rdata,'left');
        $this->db->or_like("d.hf_name",$rdata);

        $this->db->order_by("a.order_id","DESC");
        $this->db->from(DB_DATA_ORDER." a");

        $this->db->join(DB_MASTER_HEALTFACILITY." d","a.order_hf_sender=d.hf_code");
        $this->db->join(DB_MASTER_HEALTFACILITY." e","a.order_hf_recipient=e.hf_code");
        $this->db->join(DB_MASTER_SHIPPER." f","a.order_shipper_id=f.shipper_code");
        $this->db->join("hp_view_total_specimen b","a.order_id=b.specimen_order_id","left");
        return $this->db->get()->result();
    }


    public function getList($id){
        $this->db->select("a.*,b.*,c.*,g.*,h.*");
        $this->db->select("d.hf_name as faskes_pengirim");
        $this->db->select("e.hf_name as faskes_penerima");
        $this->db->select("f.shipper_name");

        $this->db->where("specimen_order_id",$id);
        $this->db->where("specimen_condition","1");
        $this->db->from(DB_DATA_PATIENT." a");
        $this->db->join(DB_DATA_SPECIMEN." b","specimen_patient_id=patient_id");
        $this->db->join(DB_DATA_ORDER." c","specimen_order_id=order_id");
        $this->db->join(DB_MASTER_HEALTFACILITY." d","c.order_hf_sender=d.hf_code");
        $this->db->join(DB_MASTER_HEALTFACILITY." e","c.order_hf_recipient=e.hf_code");
        $this->db->join(DB_MASTER_SHIPPER." f","c.order_shipper_id=f.shipper_code");
        $this->db->join(DB_DATA_VLRESULT." g","b.specimen_num_id=g.vl_result_specimen_num_id","left");
        $this->db->join(DB_DATA_EIDRESULT." h","b.specimen_num_id=h.eid_result_specimen_num_id","left");
        //return $this->db->get_compiled_select();
        return $this->db->get()->result();
    }

    
    public function searchResult($id=null,$search){
        $this->db->select("a.*,b.*,c.*,g.*,h.*");
        $this->db->select("d.hf_name as faskes_pengirim");
        $this->db->select("e.hf_name as faskes_penerima");
        $this->db->select("f.shipper_name");
        $this->db->group_start();
        if($id!=null){
        $this->db->where("specimen_order_id",$id);
        }
        $this->db->where("specimen_condition","1");
        $this->db->group_end();
        $this->db->group_start();
        $this->db->like("a.patient_name",$search,'both');
        $this->db->or_like("b.specimen_id",$search,'left');
        $this->db->group_end();
        $this->db->from(DB_DATA_PATIENT." a");
        $this->db->join(DB_DATA_SPECIMEN." b","specimen_patient_id=patient_id");
        $this->db->join(DB_DATA_ORDER." c","specimen_order_id=order_id");
        $this->db->join(DB_MASTER_HEALTFACILITY." d","c.order_hf_sender=d.hf_code");
        $this->db->join(DB_MASTER_HEALTFACILITY." e","c.order_hf_recipient=e.hf_code");
        $this->db->join(DB_MASTER_SHIPPER." f","c.order_shipper_id=f.shipper_code");
        $this->db->join(DB_DATA_VLRESULT." g","b.specimen_num_id=g.vl_result_specimen_num_id","left");
        $this->db->join(DB_DATA_EIDRESULT." h","b.specimen_num_id=h.eid_result_specimen_num_id","left");
        //return $this->db->get_compiled_select();
        return $this->db->get()->result();

    }

    public function searchResultList($id=null,$search){
        $this->db->select("a.*,b.*,c.*,g.*,h.*");
        $this->db->select("d.hf_name as faskes_pengirim");
        $this->db->select("e.hf_name as faskes_penerima");
        $this->db->select("f.shipper_name");
        $this->db->group_start();
        if($id!=null){
        $this->db->where("c.order_hf_recipient",$id);
        }
        $this->db->where("specimen_condition","1");
        $this->db->group_end();
        $this->db->group_start();
        $this->db->like("a.patient_name",$search,'both');
        $this->db->or_like("b.specimen_id",$search,'left');
        $this->db->group_end();
        $this->db->from(DB_DATA_PATIENT." a");
        $this->db->join(DB_DATA_SPECIMEN." b","specimen_patient_id=patient_id");
        $this->db->join(DB_DATA_ORDER." c","specimen_order_id=order_id");
        $this->db->join(DB_MASTER_HEALTFACILITY." d","c.order_hf_sender=d.hf_code");
        $this->db->join(DB_MASTER_HEALTFACILITY." e","c.order_hf_recipient=e.hf_code");
        $this->db->join(DB_MASTER_SHIPPER." f","c.order_shipper_id=f.shipper_code");
        $this->db->join(DB_DATA_VLRESULT." g","b.specimen_num_id=g.vl_result_specimen_num_id","left");
        $this->db->join(DB_DATA_EIDRESULT." h","b.specimen_num_id=h.eid_result_specimen_num_id","left");
        $this->db->limit(100,0);
        //return $this->db->get_compiled_select();
        return $this->db->get()->result();

    }


    public function getSpecimen($id,$i){
        $this->db->select("*");
        $this->db->where("specimen_num_id",$id);
        $this->db->where("specimen_exam_category",$i);
        $this->db->from(DB_DATA_PATIENT);
        $this->db->join(DB_DATA_SPECIMEN,"specimen_patient_id=patient_id");
        $this->db->join(DB_DATA_ORDER,"specimen_order_id=order_id");
        
        return $this->db->get()->row();
    }

    public function detail($num_id){
        $this->db->select("*");
        $this->db->where("specimen_num_id",$num_id);
        $this->db->from(DB_DATA_VLRESULT);
        $this->db->join(DB_DATA_SPECIMEN,"specimen_num_id=vl_result_specimen_num_id");
        $this->db->join(DB_DATA_PATIENT,"specimen_patient_id=patient_id");
        return $this->db->get()->row();
    }

    public function orderKategori($id){
        $this->db->select("order_kategori");
        $this->db->where("order_id",$id);
        return $this->db->get(DB_DATA_ORDER)->row();
    }

    public function resultChecked($id){
        $this->db->select("count(specimen_num_id) as total,SUM(IF(specimen_result_flag!='1',1,0)) as noresult");
        $this->db->where("specimen_order_id",$id);
        $this->db->where("specimen_condition","1");
        $this->db->group_by("specimen_order_id");
        return $this->db->get(DB_DATA_SPECIMEN)->row();
    }

    public function vldocumenChecked($order_id){
        $this->db->select("SUM(IF(ISNULL(vl_result_dokumen),1,0)) as dokumen");
        $this->db->where("specimen_order_id",$order_id);
        $this->db->group_by('specimen_order_id');
        $this->db->where("specimen_condition","1");
        $this->db->from(DB_DATA_VLRESULT);
        $this->db->join(DB_DATA_SPECIMEN,'specimen_num_id=vl_result_specimen_num_id');
       return $this->db->get()->row();
    }


    public function eiddocumenChecked($order_id){
        $this->db->select("SUM(IF(ISNULL(eid_result_dokumen),1,0)) as dokumen");
        $this->db->where("specimen_order_id",$order_id);
        $this->db->group_by('specimen_order_id');
        $this->db->where("specimen_condition","1");
        $this->db->from(DB_DATA_EIDRESULT);
        $this->db->join(DB_DATA_SPECIMEN,'specimen_num_id=eid_result_specimen_num_id');
       return $this->db->get()->row();
    }


    public function resultFlagUpdate($id){
        $this->db->where("specimen_num_id",$id);
        return $this->db->update(DB_DATA_SPECIMEN,array("specimen_result_flag"=>"1"));
    }
    

    public function insert($rdata){
        return $this->db->insert(DB_DATA_PATIENT,$rdata);
    }

    public function vlinsert($rdata){
        return $this->db->insert(DB_DATA_VLRESULT,$rdata);
    }

    public function eidinsert($rdata){
        return $this->db->insert(DB_DATA_EIDRESULT,$rdata);
    }

    public function update($rdata,$id){
        $this->db->where("vl_result_specimen_num_id",$id);
        return $this->db->update(DB_DATA_VLRESULT,$rdata);
    }


    public function eiddetail($num_id){
    
        $this->db->select("*");
        $this->db->where("specimen_num_id",$num_id);
        $this->db->from(DB_DATA_EIDRESULT);
        $this->db->join(DB_DATA_SPECIMEN,"specimen_num_id=eid_result_specimen_num_id","right");
        $this->db->join(DB_DATA_PATIENT,"specimen_patient_id=patient_id");
        //return $this->db->get_compiled_select();
        return $this->db->get()->row();
    }


    public function eidupdate($rdata,$id){
        $this->db->where("eid_result_specimen_num_id",$id);
        return $this->db->update(DB_DATA_EIDRESULT,$rdata);
    }


    public function getVLresult($id){
        $this->db->where("vl_result_id",$id);
        return $this->db->get(DB_DATA_VLRESULT)->row();
    }


    public function getEIDresult($id){
        $this->db->where("eid_result_id",$id);
        return $this->db->get(DB_DATA_EIDRESULT)->row();
    }



    
}