<?php
Class M_order extends CI_Model{
    public function __construct()
    {
        parent::__construct();
        
        define("DB_REF_LABNETWORK","hp_ref_labnetwork");
    }


public function getDistrictSender($id){
    $this->db->select("hf_district");
    $this->db->where("order_id",$id);
    $this->db->from(DB_DATA_ORDER);
    $this->db->join(DB_MASTER_HEALTFACILITY,"hf_code=order_hf_sender");
    return $this->db->get()->row();
}

    public function getDataList($id){
        $this->db->select("a.*,b.*");
        $this->db->select("c.hf_name as faskes_penerima");
        $this->db->select("e.hf_name as faskes_pengirim");
        $this->db->select("d.shipper_name");
        $this->db->where("order_hf_sender",$id);
        $this->db->where("order_kategori","2");
        $this->db->order_by("a.order_id","DESC");
        $this->db->from(DB_DATA_ORDER." a");
        
        $this->db->join("hp_view_total_specimen b","a.order_id=b.specimen_order_id","left");
        $this->db->join(DB_MASTER_HEALTFACILITY." c","a.order_hf_recipient=c.hf_code");
        $this->db->join(DB_MASTER_SHIPPER." d","a.order_shipper_id=d.shipper_code");
        $this->db->join(DB_MASTER_HEALTFACILITY." e","a.order_hf_sender=e.hf_code");
        return $this->db->get()->result();
    }


    public function getDataListAll($id){
        $this->db->select("a.*,b.*");
        $this->db->select("c.hf_name as faskes_penerima");
        $this->db->select("e.hf_name as faskes_pengirim");
        $this->db->select("d.shipper_name");
        $this->db->where("order_hf_sender",$id);
        //$this->db->where("order_kategori","2");
        $this->db->order_by("a.order_id","DESC");
        $this->db->from(DB_DATA_ORDER." a");
        
        $this->db->join("hp_view_total_specimen b","a.order_id=b.specimen_order_id","left");
        $this->db->join(DB_MASTER_HEALTFACILITY." c","a.order_hf_recipient=c.hf_code");
        $this->db->join(DB_MASTER_SHIPPER." d","a.order_shipper_id=d.shipper_code");
        $this->db->join(DB_MASTER_HEALTFACILITY." e","a.order_hf_sender=e.hf_code");
        return $this->db->get()->result();
    }



    public function getDataEidList($id){
        $this->db->select("a.*,b.*");
        $this->db->select("c.hf_name as faskes_penerima");
        $this->db->select("e.hf_name as faskes_pengirim");
        $this->db->select("d.shipper_name");
        $this->db->where("order_hf_sender",$id);
        $this->db->where("order_kategori","1");
        $this->db->order_by("a.order_id","DESC");
        $this->db->from(DB_DATA_ORDER." a");
        $this->db->join("hp_view_total_specimen b","a.order_id=b.specimen_order_id","left");
        $this->db->join(DB_MASTER_HEALTFACILITY." c","a.order_hf_recipient=c.hf_code");
        $this->db->join(DB_MASTER_SHIPPER." d","a.order_shipper_id=d.shipper_code");
        $this->db->join(DB_MASTER_HEALTFACILITY." e","a.order_hf_sender=e.hf_code");
        return $this->db->get()->result();
    }




    public function search($search,$id){
        $this->db->select("a.*,b.*");
        $this->db->select("c.hf_name as faskes_penerima");
        $this->db->select("e.hf_name as faskes_pengirim");
        $this->db->select("d.shipper_name");
        $this->db->where("order_hf_sender",$id);
        $this->db->where("a.order_id",$search);
        $this->db->order_by("a.order_id","DESC");
        $this->db->from(DB_DATA_ORDER." a");
        $this->db->join("hp_view_total_specimen b","a.order_id=b.specimen_order_id","left");
        $this->db->join(DB_MASTER_HEALTFACILITY." c","a.order_hf_recipient=c.hf_code");
        $this->db->join(DB_MASTER_SHIPPER." d","a.order_shipper_id=d.shipper_code");
        $this->db->join(DB_MASTER_HEALTFACILITY." e","a.order_hf_sender=e.hf_code");
        return $this->db->get()->result();
    }


    public function getMonitoringOrder($id){
        $this->db->select("a.*,b.*");
        $this->db->select("c.hf_name as faskes_penerima");
        $this->db->select("e.hf_name as faskes_pengirim");
        $this->db->select("d.shipper_name");
        $this->db->where($id);
        $this->db->order_by("a.order_id","DESC");
        $this->db->from(DB_DATA_ORDER." a");
        $this->db->join("hp_view_total_specimen b","a.order_id=b.specimen_order_id","left");
        $this->db->join(DB_MASTER_HEALTFACILITY." c","a.order_hf_recipient=c.hf_code");
        $this->db->join(DB_MASTER_SHIPPER." d","a.order_shipper_id=d.shipper_code");
        $this->db->join(DB_MASTER_HEALTFACILITY." e","a.order_hf_sender=e.hf_code");
        return $this->db->get()->result();
    }


    public function getDestination($id){
        $this->db->where("labnetwork_origin",$id);
        return $this->db->get(DB_REF_LABNETWORK)->result();
    }

    public function getEidDestination(){
        $unit = $this->session->userdata("user_unit");

        $this->db->where("hf_eid","1");
        $this->db->where("hf_code<>'".$unit."'");
        return $this->db->get(DB_MASTER_HEALTFACILITY)->result();
    }

    public function getCourier(){
        $this->db->where("shipper_status","1");
        return $this->db->get(DB_MASTER_SHIPPER)->result();
    }

    public function insert($rdata){
        $this->db->insert(DB_DATA_ORDER,$rdata);
        return $this->db->insert_id();
    }

    public function update($rdata,$id){
        
        return $this->db->update(DB_DATA_ORDER,$rdata,array("order_id"=>$id));
    }

    public function countSpecimen($id){
        $this->db->where("specimen_order_id",$id);
        $q = $this->db->get(DB_DATA_SPECIMEN);
        return $q->num_rows(); 
    }

    public function totalCategory($id){
        $this->db->select("sum(if(specimen_exam_category='2','1','0')) as specimen_vl_total");
        $this->db->select("sum(if(specimen_exam_category='1','1','0')) as specimen_eid_total");
        $this->db->group_by('specimen_order_id');
        $this->db->where("specimen_order_id",$id);
        return $this->db->get(DB_DATA_SPECIMEN)->row();
    }

    public function countallSpecimen(){
        $this->db->select("specimen_order_id,count(specimen_num_id) as total_specimen");
        return $this->db->get(DB_DATA_SPECIMEN)->result();
        
    }

    public function confirmation($id,$order_id=null){

        $this->db->select("a.*,b.*");
        $this->db->select("c.hf_name as faskes_penerima");
        $this->db->select("e.hf_name as faskes_pengirim");
        $this->db->select("d.shipper_name");
        
        $this->db->group_start();
        $this->db->where("a.order_hf_recipient",$id);
        $this->db->where("a.order_approved","0");
        $this->db->where("a.order_status","1");
        $this->db->where("a.order_kategori","2");
        $this->db->group_end();
        if($order_id!=null){
            $this->db->like("a.order_id",$order_id,"after");
        }

        $this->db->order_by("a.order_id","DESC");
        $this->db->from(DB_DATA_ORDER." a");

        $this->db->join("hp_view_total_specimen b","a.order_id=b.specimen_order_id","left");
        $this->db->join(DB_MASTER_HEALTFACILITY." c","a.order_hf_recipient=c.hf_code");
        $this->db->join(DB_MASTER_SHIPPER." d","a.order_shipper_id=d.shipper_code");
        $this->db->join(DB_MASTER_HEALTFACILITY." e","a.order_hf_sender=e.hf_code");
        return $this->db->get()->result();
    }


    public function eidconfirmation($id,$order_id=null){

        $this->db->select("a.*,b.*");
        $this->db->select("c.hf_name as faskes_penerima");
        $this->db->select("e.hf_name as faskes_pengirim");
        $this->db->select("d.shipper_name");
        
        $this->db->group_start();
        $this->db->where("a.order_hf_recipient",$id);
        $this->db->where("a.order_approved","0");
        $this->db->where("a.order_status","1");
        $this->db->where("a.order_kategori","1");
        $this->db->group_end();
        if($order_id!=null){
            $this->db->like("a.order_id",$order_id,"after");
        }

        $this->db->order_by("a.order_id","DESC");
        $this->db->from(DB_DATA_ORDER." a");

        $this->db->join("hp_view_total_specimen b","a.order_id=b.specimen_order_id","left");
        $this->db->join(DB_MASTER_HEALTFACILITY." c","a.order_hf_recipient=c.hf_code");
        $this->db->join(DB_MASTER_SHIPPER." d","a.order_shipper_id=d.shipper_code");
        $this->db->join(DB_MASTER_HEALTFACILITY." e","a.order_hf_sender=e.hf_code");
        return $this->db->get()->result();
    }


    public function received($id,$kat='2'){

        $this->db->select("a.*,b.*");
        $this->db->select("c.hf_name as faskes_penerima");
        $this->db->select("e.hf_name as faskes_pengirim");
        $this->db->select("d.shipper_name");
        $this->db->where("a.order_hf_recipient",$id);
        $this->db->where("order_approved","1");
        $this->db->where("order_status","4");
        $this->db->where("order_kategori",$kat);
        $this->db->order_by("a.order_id","DESC");
        $this->db->from(DB_DATA_ORDER." a");

        $this->db->join("hp_view_total_specimen b","a.order_id=b.specimen_order_id","left");
        $this->db->join(DB_MASTER_HEALTFACILITY." c","a.order_hf_recipient=c.hf_code");
        $this->db->join(DB_MASTER_SHIPPER." d","a.order_shipper_id=d.shipper_code");
        $this->db->join(DB_MASTER_HEALTFACILITY." e","a.order_hf_sender=e.hf_code");
        return $this->db->get()->result();
    }

    public function searchReceived($id,$data,$kat='2'){
        $this->db->select("a.*,b.*");
        $this->db->select("c.hf_name as faskes_penerima");
        $this->db->select("e.hf_name as faskes_pengirim");
        $this->db->select("d.shipper_name");
        $this->db->group_start();
        $this->db->where("a.order_hf_recipient",$id);
        $this->db->where("order_approved","1");
        $this->db->where("order_status","4");
        $this->db->where("order_kategori",$kat);
        $this->db->group_end();
        $this->db->like("order_id",$data,'left');
        $this->db->order_by("a.order_id","DESC");
        $this->db->from(DB_DATA_ORDER." a");

        $this->db->join("hp_view_total_specimen b","a.order_id=b.specimen_order_id","left");
        $this->db->join(DB_MASTER_HEALTFACILITY." c","a.order_hf_recipient=c.hf_code");
        $this->db->join(DB_MASTER_SHIPPER." d","a.order_shipper_id=d.shipper_code");
        $this->db->join(DB_MASTER_HEALTFACILITY." e","a.order_hf_sender=e.hf_code");
        return $this->db->get()->result();
    }

    public function detail($id){
        $this->db->select("a.*");
        $this->db->select("b.hf_name as faskes_penerima");
        $this->db->select("c.shipper_name");
        $this->db->where($id);
        
        $this->db->from(DB_DATA_ORDER." a");
        $this->db->join(DB_MASTER_HEALTFACILITY." b",'a.order_hf_recipient=b.hf_code');
        $this->db->join(DB_MASTER_SHIPPER." c","a.order_shipper_id=c.shipper_code");
        return $this->db->get()->row();
    }

    public function getOrderPickup($userid){
        $this->db->select("count(order_id) as neworder");
        $this->db->where("user_name",$userid);
        $this->db->where("order_status","2");
        $this->db->where("order_approved","1");
        //$this->db->where("order_kategori",$kategori);
        $this->db->from(DB_DATA_ORDER);
        $this->db->join(DB_VIEW_NETWORK_KURIR,"(user_unit=order_shipper_id && ref_int_hf_code=order_hf_sender && user_name='$userid')");
        return $this->db->get()->row();
        //return $this->db->get_compiled_select();
    }

    public function getOrderDelivered($userid){
        $this->db->select("count(order_id) as neworder");
        $this->db->where("user_name",$userid);
        $this->db->where("order_status","3");
        $this->db->where("order_approved","1");
        //$this->db->where("order_kategori",$kategori);
        $this->db->from(DB_DATA_ORDER);
        $this->db->join(DB_VIEW_NETWORK_KURIR,"(user_unit=order_shipper_id && ref_int_hf_code=order_hf_sender && user_name='$userid')");
        return $this->db->get()->row();
        //return $this->db->get_compiled_select();
    }


    public function getOrderInfo($id,$status,$kategori='2'){
        $this->db->select("count(order_id) as neworder");
    
        $this->db->where($id);
        $this->db->where("order_status",$status);
        if($status!='2' && $status!='3'){
        $this->db->where("order_kategori",$kategori);
        }
        $this->db->from(DB_DATA_ORDER);
        $this->db->join(DB_MASTER_HEALTFACILITY,"order_hf_sender=hf_code");
        //$this->db->group_by("order_status");
       //return $this->db->get_compiled_select();
        return $this->db->get()->row();
    }

   


    public function orderRead($id){
        $this->db->select("count(order_id) as neworder");
        $this->db->where("order_hf_sender",$id);
        $this->db->where("order_read","0");
        return $this->db->get(DB_DATA_ORDER)->row();
    }

    public function updateRead($id){
        $this->db->where("order_hf_sender",$id);
        $this->db->where("order_read","0");
        $this->db->SET("order_read","1");
        return $this->db->update(DB_DATA_ORDER);
    }

    public function delete($id){
        return $this->db->delete(DB_DATA_ORDER,array("order_id"=>$id));
    }

    public function updateStatus($rdata){
        return $this->db->insert(DB_DATA_ORDER_STATUS,$rdata);
    }
}