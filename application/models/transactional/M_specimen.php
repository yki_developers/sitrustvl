<?php
Class M_specimen extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function getDataList($id){
        $this->db->select("*");
        $this->db->where("specimen_order_id",$id);
        $this->db->from(DB_DATA_PATIENT);
        $this->db->join(DB_DATA_SPECIMEN,"specimen_patient_id=patient_id");
        $this->db->join(DB_DATA_ORDER,"specimen_order_id=order_id");
        return $this->db->get()->result();
    }

    public function getDetailSpecimen($id){
        $this->db->select("*");
        $this->db->select("date(specimen_date_collected) as date_collected");
        $this->db->select("time(specimen_date_collected) as time_collected");
        $this->db->where("specimen_num_id",$id);
        $this->db->from(DB_DATA_PATIENT);
        $this->db->join(DB_DATA_SPECIMEN,"specimen_patient_id=patient_id");
       // $this->db->join(DB_DATA_ORDER,"specimen_order_id=order_id");
       //return $this->db->get_compiled_select(); 
       return $this->db->get()->row();
    }


    public function getStatus($id){
        $this->db->select("*");
        $this->db->where("order_id",$id);
        return $this->db->get(DB_DATA_ORDER)->row();
    }


    public function getPatientId($id){
        $this->db->select("*");
        $this->db->where("patient_nid",$id['patient_nid']);
        $this->db->or_where("patient_regnas",$id['patient_regnas']);
        return $this->db->get(DB_DATA_PATIENT)->row();
    }

    public function getNid($id){
        $this->db->where($id);
        return $this->db->get(DB_DATA_PATIENT)->row();
    }

    public function getSpecimen($id){
        $this->db->where($id);
        $q = $this->db->get(DB_DATA_SPECIMEN);
        return $q->num_rows();
    }

    public function getSpecimenExist($id){
        unset($id['specimen_num_id']);
        $this->db->select("specimen_num_id");
        $this->db->where($id);
        $q = $this->db->get(DB_DATA_SPECIMEN);
        return $q->row();
        //return $q->num_rows();
    }

    public function insert($rdata){
        $this->db->insert(DB_DATA_PATIENT,$rdata);
        return $this->db->insert_id();
    }
    public function insertSpecimen($rdata){
        return $this->db->insert(DB_DATA_SPECIMEN,$rdata);
        
    }

    public function delete($id){
        $this->db->where("specimen_num_id",$id);
        return $this->db->delete(DB_DATA_SPECIMEN);
    }

    public function updatePatient($rdata,$id){
        $this->db->where("patient_id",$id);
        return $this->db->update(DB_DATA_PATIENT,$rdata);
    }

    public function updateSpecimen($rdata,$id){
        $this->db->where("specimen_num_id",$id);
        return $this->db->update(DB_DATA_SPECIMEN,$rdata);
    }

    public function SpecimenReorder($rdata,$id){
        $this->db->where("specimen_order_id",$id);
        return $this->db->update(DB_DATA_SPECIMEN,$rdata);
    }

    public function update($rdata,$id){
       //$patient = remove_data($rdata,["specimen_order_id","specimen_id","specimen_date_collected","specimen_type","specimen_exam_category","specimen_doctor_name","patient_id"]);
        //$specimen = remove_data($rdata,["patient_name","patient_nid","patient_regnas","patient_med_record","patient_bday","patient_art_date","patient_sex","patient_id"]);
        //$this->db->where("specimen_num_id",$id);
        
        foreach($rdata as $key=>$value){
            if(preg_match("/^patient_/",$key)){
                if($key!="patient_id"){
              $patient[$key] = $value;
                }
            }else{
                $specimen[$key] = $value;
            }
        }
        if($this->db->update(DB_DATA_SPECIMEN,$specimen,array("specimen_num_id"=>$id))){
            return $this->db->update(DB_DATA_PATIENT,$patient,array("patient_id"=>$rdata['patient_id']));
        }

    }



    public function search($search,$order_id){
        $this->db->group_start();
        $this->db->like('patient_name',$search,'both');
        $this->db->or_like("patient_nid",$search,'none');
        $this->db->or_like("patient_regnas",$search,'none');
        $this->db->or_like("specimen_id",$search,'none');
        $this->db->group_end();
        $this->db->like("order_id",$order_id,'none');

        $this->db->from(DB_DATA_PATIENT);
        $this->db->join(DB_DATA_SPECIMEN,"specimen_patient_id=patient_id");
        $this->db->join(DB_DATA_ORDER,"specimen_order_id=order_id");
        return $this->db->get()->result();
       // return $this->db->get_compiled_select();

    }



}