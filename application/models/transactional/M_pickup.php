<?php
Class M_pickup extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function checkNetwork($userid){
        $this->db->where("ref_int_username",$userid);
        $this->db->from(DB_DATA_ORDER);
        $this->db->join(DB_VIEW_NETWORK_KURIR,"(ref_int_hf_code=order_hf_sender) && (shipper_code=order_shipper_id)");
        $q = $this->db->get();
        return $q->num_rows();
    }

    public function getDataList($id,$kabkota){

        $this->db->select("a.*,b.*");
        $this->db->select("c.hf_name as faskes_penerima");
        $this->db->select("e.hf_name as faskes_pengirim");
        $this->db->select("d.shipper_name");
        $this->db->where("a.order_shipper_id",$id);
        $this->db->where("order_approved","1");
        $this->db->where("order_status","2");
        $this->db->where("e.hf_district",$kabkota);
        $this->db->order_by("a.order_id","DESC");
        $this->db->from(DB_DATA_ORDER." a");
        $this->db->join("hp_view_total_specimen b","a.order_id=b.specimen_order_id","left");
        $this->db->join(DB_MASTER_HEALTFACILITY." c","a.order_hf_recipient=c.hf_code");
        $this->db->join(DB_MASTER_SHIPPER." d","a.order_shipper_id=d.shipper_code");
        $this->db->join(DB_MASTER_HEALTFACILITY." e","a.order_hf_sender=e.hf_code");
        return $this->db->get()->result();
    }

    public function getDataListRev($uid){

        $this->db->select("a.*,b.*");
        $this->db->select("c.hf_name as faskes_penerima");
        $this->db->select("e.hf_name as faskes_pengirim");
        $this->db->select("d.shipper_name");
        $this->db->where("f.user_name",$uid);
        $this->db->where("order_approved","1");
        $this->db->where("order_status","2");
        //$this->db->where("e.hf_district",$kabkota);
        $this->db->order_by("a.order_id","DESC");
        $this->db->from(DB_DATA_ORDER." a");
        $this->db->join("hp_view_total_specimen b","a.order_id=b.specimen_order_id","left");
        $this->db->join(DB_MASTER_HEALTFACILITY." c","a.order_hf_recipient=c.hf_code");
        $this->db->join(DB_MASTER_SHIPPER." d","a.order_shipper_id=d.shipper_code");
        $this->db->join(DB_MASTER_HEALTFACILITY." e","a.order_hf_sender=e.hf_code");
        $this->db->join(DB_USER." f","(f.user_unit=a.order_shipper_id && f.user_district=e.hf_district && f.user_name='$uid')");
        return $this->db->get()->result();
       //return $this->db->get_compiled_select();
    }


    public function getDataListInternal($id){

        $this->db->select("a.*,b.*");
        $this->db->select("c.hf_name as faskes_penerima");
        $this->db->select("e.hf_name as faskes_pengirim");
        $this->db->select("d.shipper_name");
        $this->db->where("f.ref_int_username",$id);
        $this->db->where("order_approved","1");
        $this->db->where("order_status","2");
        $this->db->order_by("a.order_id","DESC");
        $this->db->from(DB_DATA_ORDER." a");
        $this->db->join("hp_view_total_specimen b","a.order_id=b.specimen_order_id","left");
        $this->db->join(DB_MASTER_HEALTFACILITY." c","a.order_hf_recipient=c.hf_code");
        $this->db->join(DB_MASTER_SHIPPER." d","a.order_shipper_id=d.shipper_code");
        $this->db->join(DB_MASTER_HEALTFACILITY." e","a.order_hf_sender=e.hf_code");
        $this->db->join(DB_VIEW_NETWORK_KURIR." f","(f.ref_int_hf_code=a.order_hf_sender) && (f.shipper_code=a.order_shipper_id)");
        return $this->db->get()->result();
    //return $this->db->get_compiled_select();
    }

    public function search($search,$status,$kabkota){
        $this->db->select("a.*,b.*");
        $this->db->select("c.hf_name as faskes_penerima");
        $this->db->select("e.hf_name as faskes_pengirim");
        $this->db->select("d.shipper_name");
        $this->db->group_start();
        $this->db->like('order_id',$search,'none');
        $this->db->or_like("order_number",$search,'none');
        $this->db->group_end();
        $this->db->like("order_approved","1",'none');
        $this->db->like("order_status",$status,'none');
        $this->db->like("e.hf_district",$kabkota,'none');
       
        $this->db->order_by("a.order_id","DESC");
        $this->db->from(DB_DATA_ORDER." a");
        $this->db->join("hp_view_total_specimen b","a.order_id=b.specimen_order_id","left");
        $this->db->join(DB_MASTER_HEALTFACILITY." c","a.order_hf_recipient=c.hf_code");
        
        $this->db->join(DB_MASTER_SHIPPER." d","a.order_shipper_id=d.shipper_code");
        $this->db->join(DB_MASTER_HEALTFACILITY." e","a.order_hf_sender=e.hf_code");
        return $this->db->get()->result();
    }

    public function getDeliveredList($id,$kabkota){

        $this->db->select("a.*,b.*");
        $this->db->select("c.hf_name as faskes_penerima");
        $this->db->select("e.hf_name as faskes_pengirim");
        $this->db->select("d.shipper_name");
        $this->db->where("a.order_shipper_id",$id);
        $this->db->where("order_approved","1");
        $this->db->where("order_status","3");
        $this->db->where("e.hf_district",$kabkota);
        $this->db->order_by("a.order_id","DESC");
        $this->db->from(DB_DATA_ORDER." a");

        $this->db->join("hp_view_total_specimen b","a.order_id=b.specimen_order_id","left");
        $this->db->join(DB_MASTER_HEALTFACILITY." c","a.order_hf_recipient=c.hf_code");
        $this->db->join(DB_MASTER_SHIPPER." d","a.order_shipper_id=d.shipper_code");
        $this->db->join(DB_MASTER_HEALTFACILITY." e","a.order_hf_sender=e.hf_code");
        return $this->db->get()->result();
    }


    public function getDeliveredListRev($uid){

        $this->db->select("a.*,b.*");
        $this->db->select("c.hf_name as faskes_penerima");
        $this->db->select("e.hf_name as faskes_pengirim");
        $this->db->select("d.shipper_name");
        
        $this->db->where("order_approved","1");
        $this->db->where("order_status","3");
        $this->db->where("f.user_name",$uid);
        $this->db->order_by("a.order_id","DESC");
        $this->db->from(DB_DATA_ORDER." a");
        $this->db->join("hp_view_total_specimen b","a.order_id=b.specimen_order_id","left");
        $this->db->join(DB_MASTER_HEALTFACILITY." c","a.order_hf_recipient=c.hf_code");
        $this->db->join(DB_MASTER_SHIPPER." d","a.order_shipper_id=d.shipper_code");
        $this->db->join(DB_MASTER_HEALTFACILITY." e","a.order_hf_sender=e.hf_code");
        $this->db->join(DB_USER." f","((f.user_unit=a.order_shipper_id) && (f.user_district=e.hf_district) && (f.user_name='$uid'))");
        return $this->db->get()->result();
    }

    public function getDeliveredListInternal($uid){

        $this->db->select("a.*,b.*");
        $this->db->select("c.hf_name as faskes_penerima");
        $this->db->select("e.hf_name as faskes_pengirim");
        $this->db->select("d.shipper_name");
        $this->db->where("f.ref_int_username",$uid);
        $this->db->where("order_approved","1");
        $this->db->where("order_status","3");
        $this->db->order_by("a.order_id","DESC");
        $this->db->from(DB_DATA_ORDER." a");
        $this->db->join("hp_view_total_specimen b","a.order_id=b.specimen_order_id","left");
        $this->db->join(DB_MASTER_HEALTFACILITY." c","a.order_hf_recipient=c.hf_code");
        $this->db->join(DB_MASTER_SHIPPER." d","a.order_shipper_id=d.shipper_code");
        $this->db->join(DB_MASTER_HEALTFACILITY." e","a.order_hf_sender=e.hf_code");
        $this->db->join(DB_VIEW_NETWORK_KURIR." f","(f.ref_int_hf_code=a.order_hf_sender) && (f.shipper_code=a.order_shipper_id)");
        return $this->db->get()->result();
    //return $this->db->get_compiled_select();
    }
}