<?php
Class M_received extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function getDataList($id){
        $this->db->select("*");
        $this->db->where("specimen_order_id",$id);
        $this->db->from(DB_DATA_PATIENT);
        $this->db->join(DB_DATA_SPECIMEN,"patient_id=specimen_patient_id");
        $this->db->join(DB_DATA_ORDER,"specimen_order_id=order_id");
        return $this->db->get()->result();
    }

    public function searchDataList($id,$data,$kat='2'){
        $this->db->select("*");
        
        $this->db->where("specimen_order_id",$id);
        $this->db->where("order_kategori",$kat);
        $this->db->group_start();
        $this->db->like('patient_name',$data);
        $this->db->or_like('specimen_id',$data,'left');
        $this->db->group_end();

        $this->db->from(DB_DATA_PATIENT);
        $this->db->join(DB_DATA_SPECIMEN,"patient_id=specimen_patient_id");
        $this->db->join(DB_DATA_ORDER,"specimen_order_id=order_id");
        return $this->db->get()->result();
    }

    public function conditionchecked($id){
        $this->db->select("count(specimen_num_id) as total,SUM(IF(specimen_condition is null,1,0)) as noconfirm,SUM(IF(specimen_condition='1',1,0)) as good,SUM(IF(specimen_condition='0',1,0)) as broke");
        $this->db->where("specimen_order_id",$id);
        $this->db->group_by("specimen_order_id");
        return $this->db->get(DB_DATA_SPECIMEN)->row();
    }


    public function insert($rdata){
        return $this->db->insert(DB_DATA_PATIENT,$rdata);
    }

    public function update($rdata,$id){
        $this->db->where("specimen_num_id",$id);
        return $this->db->update(DB_DATA_SPECIMEN,$rdata);
    }

    public function delete($id){
        $this->db->where("patient_id",$id);
        return $this->db->delete(DB_DATA_PATIENT,array("patient_id"=>$id));
    }

}