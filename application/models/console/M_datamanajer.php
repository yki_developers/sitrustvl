<?php
Class M_datamanajer extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    
    private function convertSpecimen($data,$destination){
        $this->db->where_in("specimen_order_id",$data);
        return $this->db->update(DB_DATA_SPECIMEN,array("specimen_order_id"=>$destination));
    }
    private function deleteOrder($data){
        $this->db->where_in("order_id",$data);
        return $this->db->delete(DB_DATA_ORDER);
    }
    private function deleteMonitoring($data){
        $this->db->where_in("order_id",$data);
return $this->db->delete("hp_data_rekap_monitoring");
    }
    public function MergeOrder($rdata,$dest){
        if($this->convertSpecimen($rdata,$dest)){
            if($this->deleteOrder($rdata)){
                return $this->deleteMonitoring($rdata);
            }
        }

    }


}