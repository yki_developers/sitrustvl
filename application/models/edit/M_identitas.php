<?php
Class M_identitas extends CI_Model{
    public function __construct()
    {
        parent::__construct();
    }

    public function getIdentitas($id){
        $this->db->where("patient_id",$id);
        return $this->db->get(DB_DATA_PATIENT)->row();
    }

    public function update($rdata,$idpatient){
        return $this->db->update(DB_DATA_PATIENT,$rdata,array("patient_id"=>$idpatient));
    }

    public function checkNIK($rdata){
        $this->db->where($rdata);
        $q = $this->db->get(DB_DATA_PATIENT);
        return $q->num_rows();
    }

    
}