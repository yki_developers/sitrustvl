<?php
Class M_internal extends CI_Model{

    public function __construct()
    {
        parent::__construct();
    }

    public function getDataList($id){
        $this->db->select("*");
        $this->db->where("specimen_internal_hf_code",$id);
        $this->db->where("specimen_internal_exam_category","2");
        $this->db->order_by("specimen_internal_num_id","desc");
        $this->db->from(DB_DATA_INTERNAL_SPECIMEN);
        $this->db->join(DB_DATA_PATIENT,"patient_id=specimen_internal_patient_id");
        $this->db->join(DB_DATA_INTERNAL_VLRESULT,"specimen_internal_num_id=vl_int_specimen_num_id","left");
        return $this->db->get()->result();
    }


    public function getEidDataList($id){
        $this->db->select("*");
        $this->db->where("specimen_internal_hf_code",$id);
        $this->db->where("specimen_internal_exam_category","1");
        $this->db->order_by("specimen_internal_num_id","desc");
        $this->db->from(DB_DATA_INTERNAL_SPECIMEN);
        $this->db->join(DB_DATA_PATIENT,"patient_id=specimen_internal_patient_id");
        $this->db->join(DB_DATA_INTERNAL_EIDRESULT,"specimen_internal_num_id=eid_int_specimen_num_id","left");
        return $this->db->get()->result();
    }


    public function getDataSpecimenList($id,$kat='2'){
        $this->db->select("*");
        $this->db->where("specimen_internal_hf_code",$id);
        $this->db->where("specimen_internal_exam_category",$kat);
        
        $this->db->order_by("specimen_internal_num_id","desc");
        $this->db->from(DB_DATA_INTERNAL_SPECIMEN);
        $this->db->join(DB_DATA_PATIENT,"patient_id=specimen_internal_patient_id");
        $this->db->join(DB_DATA_INTERNAL_VLRESULT,"specimen_internal_num_id=vl_int_specimen_num_id","left");
        return $this->db->get()->result();
    }

    public function search($search){
        $this->db->select("*");
        $this->db->group_start();
        $this->db->like("patient_name",$search);
        $this->db->or_like("patient_nid",$search,'before');
        $this->db->or_like("patient_regnas",$search,'before');
        $this->db->group_end();

        $this->db->where("specimen_internal_hf_code",$this->session->userdata("user_unit"));
        $this->db->order_by("specimen_internal_num_id","desc");
        $this->db->from(DB_DATA_INTERNAL_SPECIMEN);
        $this->db->join(DB_DATA_PATIENT,"patient_id=specimen_internal_patient_id");
        $this->db->join(DB_DATA_INTERNAL_VLRESULT,"specimen_internal_num_id=vl_int_specimen_num_id","left");
        return $this->db->get()->result();
    }

    public function getDataExamList($id){
        $this->db->select("*");
        $this->db->where("specimen_internal_hf_code",$id);
       // $this->db->where("specimen_result_flag","0");
       $this->db->order_by("specimen_internal_num_id","desc");
        $this->db->from(DB_DATA_INTERNAL_SPECIMEN);
        $this->db->join(DB_DATA_PATIENT,"patient_id=specimen_internal_patient_id");
        return $this->db->get()->result();
    }


    public function searchresult($search){
        $this->db->select("*");
        $this->db->group_start();
        $this->db->like("patient_name",$search);
        $this->db->or_like("patient_nid",$search,'before');
        $this->db->or_like("patient_regnas",$search,'before');
        $this->db->group_end();

        $this->db->where("specimen_internal_hf_code",$this->session->userdata("user_unit"));
       // $this->db->where("specimen_result_flag","0");
        $this->db->from(DB_DATA_INTERNAL_SPECIMEN);
        $this->db->join(DB_DATA_PATIENT,"patient_id=specimen_internal_patient_id");
        $this->db->join(DB_DATA_INTERNAL_VLRESULT,"specimen_internal_num_id=vl_int_specimen_num_id","left");
        return $this->db->get()->result();
    }

    public function getSpecimen($id){
        $this->db->select("*");
        $this->db->where("specimen_internal_num_id",$id);
      
        $this->db->from(DB_DATA_INTERNAL_SPECIMEN);
        $this->db->join(DB_DATA_PATIENT,"patient_id=specimen_internal_patient_id");
        $this->db->join(DB_DATA_INTERNAL_VLRESULT,"specimen_internal_num_id=vl_int_specimen_num_id","left");
        return $this->db->get()->row();
    }

    public function insert($rdata){
        return $this->db->insert(DB_DATA_INTERNAL_SPECIMEN,$rdata);
    }

    public function insertVlResult($rdata){
        return $this->db->insert(DB_DATA_INTERNAL_VLRESULT,$rdata);

    }

    public function insertEidResult($rdata){
        return $this->db->insert(DB_DATA_INTERNAL_EIDRESULT,$rdata);

    }

    public function update($rdata,$id){
        $this->db->where("specimen_internal_num_id",$id);
        return $this->db->update(DB_DATA_INTERNAL_SPECIMEN,$rdata);
    }

    public function updateVLresult($rdata,$id){
        $this->db->where("vl_int_specimen_num_id",$id);
        return $this->db->update(DB_DATA_INTERNAL_VLRESULT,$rdata);
    }

    public function delete($id){
        return $this->db->delete(DB_DATA_INTERNAL_SPECIMEN,array("specimen_internal_num_id"=>$id));
    }

}