<?php
Class M_login extends CI_Model{
    public function __construct(){
        parent::__construct();
    }
    private function tblUser($id=null){
        return "t_admm ".$id;
    }
    public function language(){
        return $this->db->get(DB_SYS_LANGUAGE)->result();
    }

    public function tokenGet($xkey){
        $this->db->select("CONCAT_WS('_',id,user_id,UUID(),'".$xkey."',level) as token");
        $this->db->where("key",$xkey);
       $q = $this->db->get(DB_REST_API_KEY);
     return $q->row();
       //return $this->db->get_compiled_select();
    }

    public function getUser($param){
        $this->db->where($param);
        $this->db->from(DB_USER." a");
        $this->db->join(DB_USER_GROUP." b","a.user_group=b.group_id");
       // return $this->db->get_compiled_select();
        return $this->db->get()->row();
    }

    public function getFaskes($id){
        $this->db->where("hf_code",$id);
        return $this->db->get(DB_MASTER_HEALTFACILITY)->row();

    }


    public function getKurir($id){
        $this->db->where("shipper_code",$id);
        return $this->db->get(DB_MASTER_SHIPPER)->row();
    }



    public function update($rdata,$id){
        $this->db->where('user_name',$id);
       $this->db->set($rdata);
        return $this->db->update(DB_USER);
    }


public function playerid($pid){
    return $this->db->insert("tmp_network",array("playerid"=>$pid));
}

    public function getDataRoleList($idgroup){
        $this->db->where("a.role_group",$idgroup);
        $this->db->select("a.*,b.module_name,c.group_name");
        $this->db->from(DB_USER_ROLE." a");
        $this->db->join(DB_SYS_MODULE." b","a.role_module=b.module_id");
        $this->db->join(DB_USER_GROUP." c","a.role_group=c.group_id");
        return $this->db->get()->result();
    }

}