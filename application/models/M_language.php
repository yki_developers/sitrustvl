<?php
Class M_language extends CI_Model {
    public function __construct()
    {
        parent::__construct();
    }

    public function defLang(){
        $this->db->where('default','1');
        return $this->db->get(DB_SYS_LANGUAGE)->row();
    }
}