<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

/*
| -------------------------------------------------------------------------
| Sample REST API Routes
| -------------------------------------------------------------------------
*/
//$route['api/example/users/(:num)'] = 'api/example/users/id/$1'; // Example 4
//$route['api/example/users/(:num)(\.)([a-zA-Z0-9_-]+)(.*)'] = 'api/example/users/id/$1/format/$3$4'; // Example 8
/*$route['api/pasien'] = 'getdatapasien/pasien';
$route['api/fase'] = 'getdatapasien/fasepengobatan';
$route['api/auth'] = 'auth/token';
$route['api/pasienAll'] = 'getdatapasien/pasienAll';
$route['api/absensi/(:any)'] = 'getdatapasien/absensi/$1';
$route['api/listpasien']['POST'] = 'getpasien/listpasien';
$route['api/labid'] = 'unittb/laboratory';
$route['api/xpert']['POST'] = 'unittb/exam';
$route['api/xpert']['PUT'] = 'unittb/exam';
$route['api/faskes'] = 'faskes/datafaskes';
$route['api/lab/(:any)'] = 'unittb/labid/$1';
*/
$route['lang/(:any)']['GET'] = "language/switchlang/$1";
$route['login']='auth/login/index';
$route['login/auth']['POST'] = 'auth/login/auth';
$route['login/logout'] = 'auth/login/logout';

$route['master/province/list'] = 'geo/province/index';
$route['master/province/add'] = 'geo/province/add';
$route['master/province/delete/(:any)'] = 'geo/province/delete/$1';
$route['master/province/update/(:any)'] = 'geo/province/update/$1';
$route['master/province/search'] = 'geo/province/search';
$route['master/province/updatelist'] = 'geo/province/datalist';


$route['master/district/list/(:num)'] = 'geo/district/index/$1';
$route['master/district/add'] = 'geo/district/add';
$route['master/district/delete/(:any)'] = 'geo/district/delete/$1';
$route['master/district/update/(:any)'] = 'geo/district/update/$1';
$route['master/district/search'] = 'geo/district/search';
$route['master/district/updatelist'] = 'geo/district/datalist';
$route['master/district/detail'] = 'geo/district/detail';
$route['master/district/districtbyprovince'] = 'geo/district/districtbyprovince';


$route['master/subdistrict/list/(:num)'] = 'geo/subdistrict/index/$1';
$route['master/subdistrict/add'] = 'geo/subdistrict/add';
$route['master/subdistrict/delete/(:any)'] = 'geo/subdistrict/delete/$1';
$route['master/subsubdistrict/update/(:any)'] = 'geo/subdistrict/update/$1';
$route['master/subdistrict/search'] = 'geo/subdistrict/search';
$route['master/subdistrict/updatelist'] = 'geo/subdistrict/datalist';
$route['master/subdistrict/detail/(:any)'] = 'geo/subdistrict/detail/$1';
$route['master/subdistrict/bydistrict'] = 'geo/subdistrict/bydistrict';

$route['master/hf/list/(:num)'] = 'unit/healthfacility/index/$1';
$route['master/hf/add'] = 'unit/healthfacility/add';
$route['master/hf/delete/(:any)'] = 'unit/healthfacility/delete/$1';
$route['master/hf/update/(:any)'] = 'unit/healthfacility/update/$1';
$route['master/hf/search'] = 'unit/healthfacility/search';
$route['master/hf/updatelist'] = 'unit/healthfacility/datalist';
$route['master/hf/detail/(:any)'] = 'unit/healthfacility/detail/$1';
$route['master/hf/hfbydistrict'] = 'unit/healthfacility/hfbydistrict';
$route['master/hf/reflist'] = 'unit/healthfacility/hfreflist';
$route['master/shipper'] = 'unit/shipper/index';
$route['master/shipper/add'] = 'unit/shipper/add';
$route['master/shipper/delete/(:any)'] = 'unit/shipper/delete/$1';
$route['master/shipper/update/(:any)'] = 'unit/shipper/update/$1';
$route['master/shipper/detail/(:any)'] = 'unit/shipper/detail/$1';
$route['master/shipper/list']= 'unit/shipper/getlist';


$route['account/user/list/(:any)'] = 'account/user/index/$1';
$route['account/user/add'] = 'account/user/add';
$route['account/user/delete/(:any)'] = 'account/user/delete/$1';

$route['account/group/list/(:any)'] = 'account/group/index/$1';
$route['account/group/add'] = 'account/group/add';
$route['account/group/update/(:any)'] = 'account/group/update/$1';
$route['account/group/detail/(:any)'] = 'account/group/detail/$1';
$route['account/group/delete/(:num)'] = 'account/group/delete/$1';


$route['transactional/order/list'] = 'transactional/order/index';
$route['transactional/order/add'] = 'transactional/order/add';
$route['transactional/specimen/list/(:any)'] = 'transactional/specimen/index/$1';
$route['transactional/specimen/add'] = 'transactional/specimen/add';
$route['transactional/specimen/delete/(:any)/(:any)'] = 'transactional/specimen/delete/$1/$2';
$route['transactional/pickup/list'] = 'transactional/pickup/index';
$route['transactional/pickup/add'] = 'transactional/pickup/add';
$route['transactional/order/received'] = 'transactional/order/received';
$route['transactional/received/list/(:any)'] = 'transactional/received/index/$1';
$route['transactional/result/list'] = 'transactional/result/index';
$route['transactional/result/detail/(:any)'] = 'transactional/result/specimen/$1';

$route['network/laboratorium/list/(:any)'] = 'network/laboratorium/index/$1';
$route['network/laboratorium/detail/(:any)'] = 'network/laboratorium/detail/$1';
$route['network/laboratorium/add'] = 'network/laboratorium/add';
$route['network/laboratorium/delete/(:any)/(:any)'] = 'network/laboratorium/delete/$1/$2';
$route['network/laboratorium/new/(:any)'] = 'network/laboratorium/new/$1';

$route['network/kurir/list/(:any)'] = 'network/kurirnetwork/index/$1';
$route['network/kurir/add'] = 'network/kurirnetwork/insertkurir';
$route['network/kurir/datalist/(:any)'] = 'network/kurirnetwork/datalist/$1';


$route['dashboard/faskes'] = 'dashboard/dashboard/fasyankes';

$route['apivl/login']['POST'] = "apivl/login/index";
$route['apivl/login/logout']['POST'] = "apivl/login/logout";
$route['apivl/order/list/(:any)']['GET'] = "apivl/order/index/$1";
$route['apivl/order/eid/(:any)']['GET'] = "apivl/order/eid/$1";
$route['apivl/order/confirmation/(:any)']['GET'] = "apivl/order/confirmation/$1";
$route['apivl/order/received/(:any)']['GET'] = "apivl/order/received/$1";
$route['apivl/pickup/list/(:any)']['GET'] = 'apivl/pickup/index/$1';
$route['apivl/pickup/listall/(:any)']['GET'] = 'apivl/pickup/listall/$1';
$route['apivl/pickup/delivered/(:any)']['GET'] = 'apivl/pickup/delivered/$1';
$route['apivl/result/vlresult']['POST'] = 'apivl/result/vlinsert';
$route['apivl/result/list/(:any)']['GET'] = 'apivl/result/index/$1';
$route['apivl/result/eidresult']['POST'] = 'apivl/result/eidinsert';
$route['apivl/result/eiddetail/(:any)']['GET'] = 'apivl/result/eiddetail/$1';


$route['apivl/master/province']['GET'] = 'apivl/master/province';
$route['apivl/master/district/(:any)']['GET'] = 'apivl/master/district/$1';
$route['apivl/master/subdistrict/(:any)']['GET'] = 'apivl/master/subdistrict/$1';
$route['apivl/master/hf_sender/(:any)']['GET'] = 'apivl/master/hf_sender/$1';
$route['apivl/master/hf_eid']['GET'] = 'apivl/master/hf_eid';
$route['apivl/master/hf_destination/(:any)']['GET'] = 'apivl/master/hf_destination/$1';
$route['apivl/master/shipper/(:any)']['GET'] = 'apivl/master/shipper/$1';

$route['apivl/order/add']['POST'] = "apivl/order/index";
$route['apivl/order/update/(:any)']['PUT'] = "apivl/order/index/$1";
$route['apivl/order/delete/(:any)']['DELETE'] = "apivl/order/index/$1";
$route['apivl/order/detail/(:any)']['GET'] = "apivl/order/detail/$1";
$route['apivl/specimen/list/(:any)']['GET'] = 'apivl/specimen/index/$1';
$route['apivl/specimen/add']['POST'] = 'apivl/specimen/index';
$route['apivl/specimen/delete/(:any)']['DELETE'] = 'apivl/specimen/index/$1';
$route['apivl/specimen/update/(:any)']['PUT'] = 'apivl/specimen/index/$1';
$route['apivl/specimen/detail/(:any)']['GET'] = 'apivl/specimen/detail/$1';
$route['apivl/specimen/feedback/(:any)']['GET'] = 'apivl/specimen/feedback/$1';

$route['apivl/notification/list/(:any)']['GET'] = 'apivl/notification/index/$1';
$route['apivl/notification/detail/(:any)']['GET'] = 'apivl/notification/detail/$1';
$route['apivl/notification/readall/(:any)']['PUT'] = 'apivl/notification/readall/$1';

$route['apivl/user/chpassword/(:any)']['PUT'] = 'apivl/user/chpassword/$1';
$route['apivl/version']['GET'] = 'apivl/version/index';

$route['edit/identitas/id/(:any)'] = 'edit/identitas/index/$1';

$route['dashboard/(:any)'] = 'dashboard/newdashboard/index/$1';
/*
$route['dashboard/propinsi'] = 'dashboard/dashboard/propinsi';
$route['dashboard/kabupaten'] = 'dashboard/dashboard/kabupaten';
$route['dashboard/faskes'] = 'dashboard/dashboard/faskes';
*/

