<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

define('SITE_NAME','SITRUST (SISTEM INFORMASI UNTUK TRANSPORTASI SPESIMEN)');

define('DB_SYS_LANGUAGE','hp_sys_language');
define('DB_USER','hp_user_data');
define('DB_USER_GROUP','hp_user_group');
define('DB_USER_ROLE','hp_user_role');
define('DB_SYS_MODULE','hp_sys_module');
define('DB_MASTER_PROVINCE','hp_master_province');
define('DB_MASTER_DISTRICT','hp_master_district');
define('DB_MASTER_SUBDISTRICT','hp_master_subdistrict');
define('DB_MASTER_HEALTFACILITY','hp_master_healthfacilities');
define('DB_MASTER_TYPE_HEALTFACILITY','hp_ref_hf_type');
define('DB_MASTER_SHIPPER','hp_master_shipper');
define('DB_MASTER_SHIPPER_DETAIL','hp_master_shipper_detail');
define('DB_USER_HF','hp_user_hf');
define('DB_USER_KURIR','hp_user_kurir');
define('DB_DATA_ORDER','hp_data_order');
define('DB_DATA_PATIENT','hp_data_patient_vl');
define('DB_DATA_SPECIMEN','hp_data_specimen_vl');
define('DB_DATA_LABNETWORK','hp_ref_labnetwork');
define('DB_DATA_VLRESULT',"hp_data_vl_result");
define('DB_DATA_EIDRESULT',"hp_data_eid_result");
define('DB_REST_API_KEY','rest_keys');
define("DB_DATA_INTERNAL_SPECIMEN","hp_data_specimen_internal_vl");
define("DB_DATA_INTERNAL_VLRESULT","hp_data_vl_result_internal");
define("DB_DATA_INTERNAL_EIDRESULT","hp_data_eid_result_internal");
define("DB_DATA_ORDER_STATUS","hp_data_order_status");
define("DB_DATA_NOTIFIKASI","hp_data_notifikasi");

define("DB_VIEW_SHIPMENT_MONITORING","hp_view_shipment_monitoring");
define("DB_VIEW_BROKEN_CONDITION","hp_view_broken_condition");
define("DB_VIEW_REKAP_MONITORING_EKS","hp_view_rekap_monitoring_eksternal");
define("DB_VIEW_REKAP_MONITORING_INT","hp_view_rekap_monitoring_internal");


define("DB_VIEW_DATA_BASIC","db_view_data_basic_merge");
define("DB_VIEW_BASIC_RESULT","db_view_basic_result");
define("DB_VIEW_REKAP_MINGGUAN_NASIONAL","db_view_rekap_mingguan_nasional");
define("DB_VIEW_REKAP_MINGGUAN_PROPINSI","db_view_rekap_mingguan_perpropinsi");
define("DB_VIEW_REKAP_MINGGUAN_KABUPATEN","db_view_rekap_mingguan_perkabupaten");
define("DB_VIEW_REKAP_MINGGUAN_FASKES","db_view_rekap_mingguan_perfaskes");

define("DB_VIEW_REKAP_BULANAN_NASIONAL","db_view_rekap_bulanan_nasional");
define("DB_VIEW_REKAP_BULANAN_PROPINSI","db_view_rekap_bulanan_propinsi");
define("DB_VIEW_REKAP_BULANAN_KABUPATEN","db_view_rekap_bulanan_kabupaten");
define("DB_VIEW_REKAP_BULANAN_FASKES","db_view_rekap_bulanan_faskes");


define("DB_VIEW_TREND_NASIONAL","db_view_trend_nasional");
define("DB_VIEW_TREND_PROPINSI","db_view_trend_propinsi");
define("DB_VIEW_TREND_KABUPATEN","db_view_trend_kabupaten");
define("DB_VIEW_TREND_FASKES","db_view_trend_faskes");

define("DB_KURIR_NETWORK","hp_ref_kurir_internal");
define("DB_VIEW_NETWORK_KURIR","hp_view_ref_kurir_network");
define("DBVIEW_DASHBOARD_STATUS_DETAIL","dashboard_status_detail");
define("DBVIEW_DASHBOARD_HASIL_DETAIL","dashboard_result_vl");
define("DBVIEW_DASHBOARD_TAT_VL","dashboard_ref_tatdasar_vl");
define("DBVIEW_CHART_TRENDLINE","dashboard_chart_trendline");
define("DBVIEW_CHART_SPECIMEN_TERKIRIM","dashboard_chart_pengiriman");


define("DBVIEW_DASHBOARD_EID_STATUS_DETAIL","dashboard_eid_status_detail");
define("DBVIEW_DASHBOARD_EID_HASIL_DETAIL","dashboard_eid_result");
define("DBVIEW_DASHBOARD_EID_TAT","dashboard_ref_tatdasar_eid");

define("DBVIEW_CHART_TRENDLINE_EID","dashboard_eid_chart_trendline");
