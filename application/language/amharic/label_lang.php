<?php
/* Language : EN */
$lang['SwitchLanguage'] = "Select Language";
$lang['username'] = "Username";
$lang['password'] = "Password";
$lang['rememberpassword'] = "Remember Password";
$lang['province'] = "Province";
$lang['district'] = "District";
$lang["receiver_facilties"] = "Receiver Facilities";
$lang['sender_facilities'] = "Sender Facilities";
$lang['health_facilities'] = "Health Facilities";
$lang['periode'] = "Periode";
$lang['dateformat'] ="int";
$lang['examinationType'] = "Examination Type";
$lang['dashboard'] = "Dashboard";
$lang['NumberOfOrder'] = "Total Orders";
$lang["NumberOfSpeciment"] = "Total Speciments";
$lang["NumOfSenderFacilities"] = "Number Of Facilities";
$lang["NumberOfDistrict"] = "Number of Districts";