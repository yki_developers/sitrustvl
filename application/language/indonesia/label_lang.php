<?php
/* Language : ID */
//A
$lang['action'] = "Tindakan";
$lang['add_network'] = "Jejaring Lab";
$lang['add_specimen'] = "Menambah Specimen";
$lang['address'] = "Alamat";



//B

//C
$lang['condition_bad'] = "Rusak";
$lang['condition_good'] = "baik";
//D
$lang['dashboard'] = "Dashboard";
$lang['data_form'] = "Formulir Data";
$lang['dateformat'] ="int";
$lang['district'] = "Kabupaten";
$lang['district_code'] = "Kode Kabupaten";
$lang['district_name'] = "Nama Kabupaten";
$lang['detination'] = "Tujuan";
$lang['detail'] = "Detail";
$lang['doctor_name'] = "Nama Dokter";
$lang['delivered'] = "Sampai Tujuan";

//E
$lang['examinationType'] = "Jenis Pemeriksaan";


//F
$lang['forgetpassword'] = "Lupa Password";
$lang['finish'] = "Selesai";
$lang['feedback'] = "Feedback";

//G
$lang['geo_structure'] = "Pengaturan Wilayah";
$lang['group'] = "Group";
$lang['group_id'] = "ID Group";

//H
$lang['health_facility'] = "Fasilitas Kesehatan";
$lang['home'] = "Beranda";
$lang['healthfacility_name'] = "Nama Faskes";
$lang['healthfacility_code'] = "Kode Faskes";
$lang['healthfacility_referral'] = "Faskes Rujukan";
$lang['healthfacility_type'] = "Jenis Fasyankes";
$lang['healthfacility_type_hospital'] = "Rumah Sakit";
$lang["healthfacility_type_phc"] = "Puskesmas";
$lang['healthfacility_type_clinic'] = "Klinik";

//I

$lang['internal'] = "Peremintaan Pemeriksaan Lab Internal";
$lang['internal_pdp'] ="Permintaan Pemeriksaan";
$lang["internal_result"] = "Hasil Pemeriksaan";
//J

//K

//L
$lang['logout'] = "Keluar";

//M
$lang['monitoring']="Pemantauan";
$lang['microscopic'] = "Mikroskopis";


//N
$lang['number'] = "No";
$lang['network'] = "Jejaring Layanan";
$lang["network_lab"] = "Jejaring Lab";
$lang["network_treatment"] = "Jejaring Pengobatan";
$lang["network_origin"] = "Fasyankes Asal";

//O
$lang['order'] = "Order";
$lang['order_number'] = "No. Order";
$lang['order_date'] = "Tanggal Order";
$lang['order_destination'] = "Tujuan";
$lang['order_courier'] = "Kurir";
$lang['order_status'] = "status";
$lang['origin'] = "Pengirim";
$lang['order_origin'] = "Faskes Pengirim";
$lang['order_rejected'] = "Permintaan Ditolak";
$lang["order_approved"] = "Permintaan diterima";
$lang["order_num_specimen"] = "Jumlah Spesimen";
$lang['order_reason'] = "Alasan Penolakan";
$lang['order_confirmation'] = "Konfirmasi Order";
//P
$lang['password'] = "Kata Sandi";
$lang['patient_reg_nation'] = "No. Register Nasional";
$lang['periode'] = "Periode";
$lang['profile'] = "Profil";
$lang['province'] = "Propinsi";
$lang['province_code'] = "Kode Propinsi";
$lang['province_name'] = "Nama Propinsi";
$lang['pickup'] = "Pengambilan";
$lang['pickup_confirmation'] = "Konfirmasi";
//new
$lang['phone_number'] = "Nomor Telp";

//Q

//R
$lang["recipient_facility"] = "Fasyankes Penerima";
$lang['ReferenceData'] = "Data Referensi";
$lang['rememberpassword'] = "Ingat Password";
$lang['report'] = "Laporan";
$lang['receive'] = "Penerimaan";
$lang['confirmation'] = "Konfirmasi";
$lang['result'] ="Hasil";
$lang['result_absolut'] = "Hasil Absolut";
$lang['rejected'] = "Ditolak";
$lang['approved'] = "diterima";

$lang['report_sender_facility'] = "Fasyankes Pengirim";
$lang['report_exam_facility'] = "Fasyankes Pemeriksa";
$lang['report_month_to'] = "Tes Bulan Ke";
$lang['report_periode'] = "Periode Tes";
$lang["report_year"] = "Tahun";
$lang["report_month"] = "Bulan";
$lang["report_date"] = "Tanggal Tes Terakhir";
$lang["result_view"] = "Lihat Hasil";
$lang["result_conclusion"] = "Kesimpulan Tes VL";
$lang["result_vl_test"] = "Hasil tes VL";
$lang["result_edit"] = "Edit Hasil";
//S
$lang['specimen_id'] = "ID Spesimen";
$lang['specimen_art_date'] = "Tanggal Mulai ART";
$lang['specimen_date_collected'] = "Tanggal Pengambilan Sampel";
$lang['specimen_test_type'] = "Kategori Pemeriksaan";
$lang['specimen_exam_date'] = "Tanggal tes VL";
$lang['specimen_date_release'] = "Tanggal Hasil Keluar";
$lang['specimen_exam_date_eid'] = "Tanggal tes EID";
$lang['sender_facilities'] = "Fasyankes Pengirim";
$lang['shipper_company'] = "Perusahaan Pengantaran";
$lang['shipper_code'] = "Kode Kurir";
$lang['shipper_name'] = "Nama Kurir";
$lang['specimen']="Spesimen";
$lang['SwitchLanguage'] = "Pilih Bahasa";
$lang['subdistrict'] = "Kecamatan";
$lang['subdistrict_code'] = "Kode Kecamatan";
$lang['subdistrict_name'] = "Nama Kecamatan";

//T

//U
$lang['username'] = "User ID";
$lang['UserData'] = "Data Pengguna";
$lang['unit'] = "Unit";
$lang['user_management'] = "Pengaturan Pengguna";
$lang['user'] = "Pengguna";
$lang['user_group'] = "Pengaturan Group";
$lang['user_role'] = "Pengaturan Akses";
$lang['user_fname'] = "Nama Depan";
$lang['user_lname'] = "Nama Belakang";
$lang['user_unit'] = "Unit Kerja";

$lang['username_group'] = "Group User";
$lang['user_hf'] = "Petugas Faskes";
$lang['user_kurir'] = "Petugas Kurir";
$lang['user_fullname'] = "Nama Lengkap";



$lang['user_group_superuser'] = "Superuser";
$lang['user_group_moh'] = "Nasional/Pusat";
$lang['user_group_dho'] = "Dinas Kesehatan KAB/Kota";
$lang['user_group_pho'] = "Dinas Kesehatan Propinsi";
$lang['user_group_facility'] = "Fasyankes";
$lang['user_group_courier'] = "Kurir";
$lang['user_group_tofa'] = "TOFA";
$lang['user_group_fac_receipt'] = "Fasyankes Penerima";
$lang['user_group_fac_sender'] = "Fasyankes Pengirim"; 
$lang['user_group_fac_both'] = "Pengirim dan Penerima";
$lang['user_hf_group'] = "Tugas Pokok dan Fungsi";
$lang["user_hf_group_1"] = "LAB Faskes Pengampu";
$lang['user_hf_group_2'] = "PDP Faskes Pengampu";
$lang['user_hf_group_3'] = "Lab Faskes Satelit";
$lang['user_hf_group_4'] = "PDP Faskes Satelit";
$lang['user_hf_group_5'] = "Lab Faskes Rujukan";

//V
$lang['vl_report'] = "Laporan Pemeriksaan VL";
$lang['vl_monitoring'] = "Monitoring Pengiriman VL";
$lang['vl_int_monitoring'] = "Monitoring Pemeriksaan Internal";
$lang['eid_report'] = "Laporan Pemeriksaan EID";
$lang['eid_monitoring'] = "Monitoring Pengiriman EID";

//W
$lang['welcome'] = "Selamat Datang";

//X
$lang['xpert'] = "TCM";

//Y

//Z



//add new 12/07/2020
$lang['username_exists'] = "User Name Sudah Digunakan";
$lang['vl_internal_result'] = "Hasil Pemeriksaan Internal (VL)";
$lang['total_specimen'] = "Total Specimen";
$lang['specimen_condition'] = "Kondisi Specimen";
$lang['specimen_condition_good'] = "Baik";
$lang['specimen_condition_broken'] = "Rusak";

$lang['total_specimen_condition_good'] = "Specimen Baik";
$lang['total_specimen_condition_broken'] = "Specimen Rusak";

$lang["order_pickup_date"] = "Dijemput";
$lang["order_arrival_date"] = "Sampai Tujuan";
$lang["order_received_date"] = "Tanggal Diterima";


$lang['mother_name'] = "Nama Ibu";
$lang['mother_nid'] = "NIK Ibu";
$lang['mother_regnas'] = "Regnas Ibu";
$lang["child_nid"] = "NIK Bayi";
$lang["child_form"] = "ISIAN DATA BAYI";

$lang['create_order'] = "Buat Order";
$lang['name'] = "Nama";
$lang['last_test'] = "Last test";
$lang["vl_test_month"] = "Bulan Tes VL";
$lang["vl_test_year"] = "Tahun Tes VL";

$lang["order_type"] = "Jenis Order";
$lang["order_cancel"]="Batalkan Order";
$lang['view_result_document'] = "Buka dokumen";
$lang['fu_last_status'] = "Status Akhir Follow Up";
$lang['fu_last_date'] = "Tanggal akhir Follow Up";


$lang['network_destination'] = "Lab Rujukan";

$lang['range_age'] = "Rentang Usia";
$lang['order_eid'] = "Order EID";
$lang["edit_patient"] = "Edit Identitas Pasien";
$lang["cancel"] = "Batal";
$lang["change_result_document"] = "Ubah dokumen";
$lang["edit_view_result"] ="Lihat/Edit Hasil";
$lang["document"] = "Dokumen Hasil";

$lang['lab_ref'] = "Lab Rujukan";
$lang["order_time"] = "Jam Order";
$lang["pickup_time"] = "Jam Penjemputan";
$lang["pickup_date"] = "Tanggal Penjemputan";
$lang["pickup_duration"] ="Durasi Pejemputan";
$lang["pickup_info"] = "Keterangan Penjemputan";
$lang["delivered_date"] ="Tanggal Spesimen Sampai Tujuan";
$lang["delivered_time"] = "Jam Spesimen Sampai Tujuan";
$lang["delivered_info"] = "Keterangan Sampai Tujuan";
$lang["shipment_monitoring"] = "Monitoring Kurir";
$lang["datacontrol"] = "Manajemen Data";
$lang["confirmation_received"] = "Konfirmasi Penerimaan";
$lang["condition_broken"] = "Keterangan Rusak";




















