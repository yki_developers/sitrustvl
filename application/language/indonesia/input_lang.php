<?php
/* Language : ID */

//A
$lang['add'] = "Tambah Baru";
$lang['allprovinces'] = "-- Semua Propinsi --";
$lang['alldistricts'] = "-- Semua Kabupaten --";
$lang['allfacilities'] = "-- Semua Fasilitas --";
$lang['allExamination'] = "-- Semua Pemeriksaan --";

///B

//C

//D
$lang['delete'] = "Hapus";
//E
$lang['edit'] = "Edit";
$lang['error'] = "ERROR";
//F
//G
//H
//I
$lang['invalid'] = "Invalid";
//J
//K
//L
$lang['login']     = "Masuk";
//M
$lang['month'] = "Bulan Ke";
//N
$lang['Negative'] = "Negatif";
$lang['nr'] = "Tidak ada hasil";
//O
//P
$lang['patient'] = "Pasien";
$lang['patient_address'] = "Alamat Pasien";
$lang['patient_name'] = "Nama Pasien";
$lang['child_name'] = "Nama Bayi";
$lang['patient_id'] = "ID Pasien";
$lang['patient_nid'] = "NIK";
$lang['patient_sex'] = "Jenis kelamin";
$lang['patient_bday'] = "Tanggal Lahir";
$lang['patient_regnas'] = "No. Register Nasional";
$lang['patient_med_record'] = "No. Rekam Medis";
$lang['patient_date_confirmed'] = "Tanggal Terkonfirmasi HIV+";

$lang['pickup_confirmation_date'] ="Tanggal Konfirmasi Penjemputan";
$lang['pickup_delivered'] ="Sampai Tujuan";
//Q
//R
$lang['rs'] = "Rif Sensitif";
$lang['rr'] = "Rif Resisten";
$lang['ri'] = "Rif Indeterminate";

$lang['result_lab_register'] = "Nomor Register Lab";
$lang['result_date'] = "Tanggal Hasil";
$lang["result_micro"] = "Hasil Mikroskopis";
$lang["result_sputum_xpert"] = "Hasil TCM Sputum";
$lang["result_stool_xpert"] = "Hasil TCM Stool";
//S
$lang['submit']   = "Simpan";
$lang["search"] = "Pencarian";
$lang['shipper_company'] = "Perusahaan Pengiriman";

$lang['specimen_cold_chain'] ="Pendingin";
$lang["cold_chain_yes"] = "Ya";
$lang["cold_chain_partly"] = "Ya, Sebagian";
$lang["cold_chain_no"] = "Tidak";
$lang['specimen_number'] = "Nomor/ID Spesimen";
$lang['specimen_test_type'] = "Jenis Pemeriksaan";
$lang['specimen_type'] = "Jenis Spesimen";
$lang['specimen_presumptive_type'] = "Kategori Terduga/Kasus";
$lang['specimen_type_child'] = "TB Anak";
$lang['specimen_type_hiv'] = "HIV";
$lang['specimen_type_dm'] = "DM";
$lang['specimen_type_malnutrition'] = "Malnutrisi";
$lang['specimen_type_recurrent_pneumonia'] = "Reccurrent Pneumonia";
$lang['specimen_type_persistent_pneumonia'] = "Persistent Pneumonia";
$lang['specimen_anatomic_loc'] = "Lokasi Anatomi";
$lang['specimen_anatomic_loc_extra'] = "Lokasi anatomi";
$lang['specimen_exam_purpose'] = "Tujuan Pemeriksaan";
$lang['specimen_fup_test'] = "Follow Up bulan  ke ";
$lang['specimen_fup_aftertreatment'] ="Follow Up Setelah Pengobatan";
$lang['specimen_national_regnum'] = "No. Register TB Nasional";
$lang['specimen_district_regnum'] = "No. Register Kab/Kota";

$lang['specimen_type_stool_app'] = "Kondisi Tinja";
$lang['specimen_type_sputum_app'] = "Kondisi Sputum";

$lang['sex_m'] = "Laki-laki";
$lang['sex_f'] = "Perempuan";

$lang['status_draft'] = "DRAFT";
$lang['status_waiting_confirmation'] = "Sedang Menunggu Konfirmasi Pengambilan";
$lang['status_waiting_pickup'] = "Menunggu Dijemput";
$lang['status_picked_up'] = "Paket Sudah Diambil Kurir";
$lang['status_delivered'] = "Paket Sudah sampai tujuan";
$lang['status_received'] = "Paket Telah diterima";
$lang['status_finished'] = "Selesai";
//T
//U
$lang['update'] = "Update";
//V
//W
//X
//Y
//Z



//added 09/07/2020
$lang['type_hospital'] = "Rumah Sakit";
$lang['type_phc']  = "Puskesmas";
$lang['type_clinic'] = "Klinik";

$lang['change_password'] = "Ganti Password";
$lang["old_password"] = "Password Lama";
$lang["new_password"] = "Password Baru";

$lang['main_task'] = "Tygas/Fungsi";
$lang['result_document'] = "Kirim Dokumen";
$lang['specimen_time_collected'] = "Jam Pengambilan spesimen";
$lang["specimen_eid_test_date"] = "Tanggal tes EID"; 