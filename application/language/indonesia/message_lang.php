<?php
/* Language : ID */
/* text message block */

//A
//B
//C
$lang['delete_confirmation'] = "apakah data ini akan dihapus?";
//D
$lang['data_save'] = "Data Berhasil disimpan";
$lang['data_failed'] = "Data Gagal Disimpan/Update";
//E
$lang['error'] = "Error";
//F
$lang['failed'] = "Gagal";
//G
//H
//I
//J
//K
//L
$lang['loginsuccess'] = "Login Berhasil";
$lang['loginfailed'] = "Gagal Login";
//M
//N
$lang['not_found'] = "Data Tidak ditemukan";
//O
//P
//Q
//R
//S
$lang['success'] = "Sukses";
//T
//U
//V
//W
//X
//Y
//Z

$lang['required'] = "Field ini Wajib Diisi";


